<?php

namespace NsUtil\Financial\Rounders;

use NsUtil\Financial\Contracts\Rounders;

class ABNT_NBR_5891 implements Rounders
{
    private $value, $precision, $config;

    public function __construct(float $value, int $precision = 2, array $config = [])
    {
        $this->value = $value;
        $this->precision = $precision;
        $this->config = [];
    }
    /**
     * Arredonda um valor monetário de acordo com as regras de arredondamento especificadas.
     *
     * @param float $value O valor a ser arredondado.
     * @param int $precision O número de casas decimais desejado (padrão é 2).
     * @return float O valor arredondado.
     */
    public function round(): float
    {
        return round($this->value, $this->precision, self::_getRoundByRules());
    }

    /**
     * Obtém a regra de arredondamento com base na ABNT/NBR 5891/1977
     *
     * @param float $num
     * @param integer $precision
     * @return int
     */
    public function _getRoundByRules(): int
    {
        $num = $this->value;
        $precision = $this->precision;

        $num_parts = explode('.', (string)$num);
        if (!isset($num_parts[1])) {
            return PHP_ROUND_HALF_UP;
        }

        $decimal_parts = str_split($num_parts[1]);
        if (!isset($decimal_parts[$precision - 1])) {
            // Menos decimais do que especificado, retorne qualquer modo
            return PHP_ROUND_HALF_UP;
        }

        $target_decimal = (int) $decimal_parts[$precision - 1];

        // Se não houver mais partes decimais além do dígito alvo, considere o próximo dígito como 0
        $next_decimal = (int) ($decimal_parts[$precision] ?? 0);
        $nextDecimalAfterLastDecimal = (int) ($decimal_parts[$precision + 1] ?? 0);

        switch (true) {
            case $next_decimal > 5:
            case $next_decimal === 5 && $nextDecimalAfterLastDecimal > 0:
            case $next_decimal === 5 && $nextDecimalAfterLastDecimal === 0 && $target_decimal % 2 !== 0: //(impar)
                $ret = PHP_ROUND_HALF_UP;
                break;
            default:
                $ret = PHP_ROUND_HALF_DOWN;
        }

        return $ret;
    }
}
