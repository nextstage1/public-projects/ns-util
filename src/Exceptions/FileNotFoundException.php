<?php

namespace NsUtil\Exceptions;

use Exception;
use NsUtil\Helper;
use NsUtil\Log;

class FileNotFoundException extends Exception
{
    /**
     * @var string
     */
    protected $path;

    /**
     * Constructor.
     *
     * @param string     $path
     * @param int        $code
     * @param \Exception $previous
     */
    public function __construct($path, $code = 0, $previous = null)
    {
        $this->path = $path;

        // Logar arquivos inexistentes, exceto pois faz parte do projeto
        if (stripos($this->path, '.env') === false) {
            Log::debug('file_not_found', 'Path: ' . $this->getPath());
        }

        parent::__construct('File not found at path: ' . $this->getPath(), $code, $previous);
    }

    /**
     * Get the path which was not found.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}
