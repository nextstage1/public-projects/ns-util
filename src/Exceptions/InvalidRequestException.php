<?php

namespace NsUtil\Exceptions;

use Exception;

class InvalidRequestException extends Exception
{
    public function __construct($message = null, $code = 400, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
