<?php

namespace NsUtil\Exceptions;

use Exception;

class FileAlreadyExistsException extends Exception
{
    public function __construct($message = null, $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
