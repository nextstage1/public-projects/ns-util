<?php

namespace NsUtil\Exceptions;

use Exception;
use Throwable;

class ModuleNotEnabledByFeatureFlags extends Exception
{

    public function __construct(string $moduleName, int $code = 0, ?Throwable $previous = null)
    {
        $message = "Module is disabled (FF: $moduleName)";
        parent::__construct($message, $code);
    }
}
