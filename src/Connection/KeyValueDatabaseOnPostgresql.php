<?php

namespace NsUtil\Connection;

use NsUtil\ConnectionPostgreSQL;
use NsUtil\Databases\Migrations;
use NsUtil\Helper;
use NsUtil\Helpers\DirectoryManipulation;

use function NsUtil\now;

class KeyValueDatabaseOnPostgresql
{
    private static ?ConnectionPostgreSQL $con = null;

    private static function getConnectionByEnv(): void
    {
        self::$con ??= new ConnectionPostgreSQL(
            getenv('DBHOST'),
            getenv('DBUSER'),
            getenv('DBPASS'),
            getenv('DBPORT'),
            getenv('DBNAME')
        );

        // migrations
        Migrations::migrateAndSetFlagOnFile(__DIR__ . '/migrations', 'keyvalue', self::$con);
    }

    private static function executeQuery(string $query, ?array $params = null): array
    {
        self::getConnectionByEnv();
        return self::$con->execQueryAndReturnPrepared($query, $params);
    }

    public static function set(string $tablename, string $key, ?string $value = null): void
    {
        $time = now()->format('Y-m-d H:i:s');
        self::executeQuery(
            "
                INSERT INTO _nsutil.keyvalue (created_at, tablename, key, value) 
                VALUES (?,?,?,?) 
                ON CONFLICT (tablename, key) 
                DO UPDATE SET value = ?, updated_at = ?",
            [$time, $tablename, $key, $value, $value, $time]
        );
    }

    public static function delete(string $tablename, string $key): void
    {
        self::executeQuery('DELETE FROM _nsutil.keyvalue WHERE tablename = ? AND key = ?', [$tablename, $key]);
    }

    public static function get(string $tablename, string $key): ?string
    {
        return self::executeQuery('SELECT value FROM "_nsutil".keyvalue WHERE tablename = ? AND key = ?', [$tablename, $key])[0]['value'] ?? null;
    }

    public static function list(string $tablename): array
    {
        return self::executeQuery('SELECT key, value FROM "_nsutil".keyvalue WHERE tablename = ?', [$tablename]);
    }

    public static function drop(string $tablename): void
    {
        self::executeQuery("DELETE FROM _nsutil.keyvalue WHERE tablename = ?", [$tablename]);
    }

    public static function setConnection(ConnectionPostgreSQL $con): void
    {
        self::$con = $con;
    }
}
