CREATE SCHEMA IF NOT EXISTS "_nsutil";

DROP TABLE IF EXISTS "_nsutil".sqlite;

CREATE TABLE
    IF NOT EXISTS "_nsutil".sqlite (
        id bigserial NOT NULL,
        created_at timestamp DEFAULT NULL,
        updated_at timestamp DEFAULT NULL,
        tablename varchar(250) NOT NULL,
        "key" varchar(250) NOT NULL,
        value text NOT NULL,
        CONSTRAINT sqlite_unique UNIQUE (key, tablename)
    );