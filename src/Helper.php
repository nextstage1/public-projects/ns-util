<?php

namespace NsUtil;

use NsUtil\Helpers\Utils as HelperNSHelper;

/**
 * Class Helper
 *
 * This class extends the HelperNSHelper class from the NsUtil\Helpers namespace.
 * It serves as a utility class that inherits all the static methods and properties
 * from the HelperNSHelper class, allowing for easier access to helper functions
 * throughout the application.
 */
class Helper extends HelperNSHelper {}


// use DateTime;
// use Exception;
// use NsUtil\Exceptions\FileNotFoundException;
// use stdClass;

// // Helper funcionrs
// class Helper
// {
//     public static $prefixo = ['mem_', 'sis_', 'anz_', 'aux_', 'app_', 'nsl_'];

//     /**
//      * Retorna um caminho absoluto de um arquivo
//      *
//      * @param string $dirArquivoSample
//      * @return void
//      */
//     public static function nsIncludeConfigFile(string $dirArquivoSample)
//     {
//         $dirArquivoSample = realpath($dirArquivoSample);
//         $temp = explode(DIRECTORY_SEPARATOR, $dirArquivoSample);
//         $configName = array_pop($temp);

//         // importar arquivo de configuração desta aplicação.
//         $t = explode(DIRECTORY_SEPARATOR, __DIR__);
//         $file = 'nao-deve-achar.json';
//         array_pop($t);
//         array_pop($t);
//         while (!file_exists($file)) {
//             array_pop($t);
//             $dir = implode(DIRECTORY_SEPARATOR, $t) . DIRECTORY_SEPARATOR;
//             $file = $dir . 'composer.json';
//         }
//         self::mkdir($dir . DIRECTORY_SEPARATOR . 'nsConfig', 0600);
//         $config = $dir . DIRECTORY_SEPARATOR . 'nsConfig' . DIRECTORY_SEPARATOR . $configName;
//         if (!file_exists($config)) {
//             copy($dirArquivoSample, $config);
//             echo "<h1>nsConfig: É necessário criar o arquivo de configuração '[DIR_COMPOSER]/nsConfig/$configName'. <br/>Tentei gravar um modelo. Caso não esteja, existe um padrão na raiz da aplicação.</h1>";
//             die();
//         }
//         include_once $config;
//         $var = str_replace('.php', '', $configName);
//         return $$var;
//     }

//     /**
//      * Sanitize function
//      *
//      * @param string $str
//      * @return string
//      */
//     public static function sanitize(string $str): string
//     {
//         $from = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
//         $to = "aaaaeeiooouucAAAAEEIOOOUUC";
//         $keys = array();
//         $values = array();
//         preg_match_all('/./u', $from, $keys);
//         preg_match_all('/./u', $to, $values);
//         $mapping = array_combine($keys[0], $values[0]);
//         $str = strtr($str, $mapping);
//         $str = preg_replace("/[^A-Za-z0-9]/", "_", (string) $str);
//         return $str;
//         //return str_replace(" ", "_", preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(trim((string)$str))));
//         //return str_replace(" ", "_", preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(trim((string)$str))));
//     }

//     public static function mkdir($path, $perm = 0777): void
//     {
//         if (strlen($path) > 0 && !is_dir($path) && !is_file($path)) {
//             @mkdir($path, $perm, true);
//         }
//     }




//     /**
//      * Convert special ASCII characters to standard characters
//      *
//      * @param string $string
//      * @return string
//      */
//     public static function convertAscii(string $string): string
//     {
//         return iconv('UTF-8', 'ASCII//TRANSLIT', $string);
//     }

//     public static function formatTextAllLowerFirstUpper($str)
//     {
//         $sempreMinusculas = ['a', 'ao', 'da', 'de', 'do', 'dos', 'das', 'ante', 'até', 'após', 'desde', 'em', 'entre', 'com', 'contra', 'para', 'por', 'perante', 'sem', 'sobe', 'sob'];
//         $string = ucwords(mb_strtolower($str));
//         foreach ($sempreMinusculas as $value) {
//             $string = str_ireplace(" $value ", " " . mb_strtolower($value) . " ", $string);
//         }
//         return $string;
//     }

//     public static function arrayOrderBy(&$array, $element, $sort = 'ASC')
//     {
//         usort($array, function ($a, $b) use ($element, $sort) {
//             if ($sort === 'ASC') {
//                 return $a[$element] > $b[$element] ? 1 : -1;
//             } else {
//                 return $a[$element] < $b[$element] ? 1 : -1;
//             }
//         });
//     }

//     /**
//      * Ira procurar num array multidimensional a chave e retornara um array correspondente aquela chave
//      *
//      * @param array $array
//      * @param string $chave
//      * @param string $valor
//      * @return array
//      */
//     public static function arraySearchByKey(array &$array, string $chave, string $valor): array
//     {
//         if (!is_array($array)) {
//             throw new Exception('NSUtil (NSH120): Variavel não é um array');
//         }
//         $key = array_search($valor, array_column($array, $chave));
//         if (false !== $key) {
//             return $array[$key];
//         } else {
//             return [];
//         }
//     }

//     /**
//      * Retorna a string no formato camelCase
//      * @param string|array $string
//      * @param array $prefixo
//      * @return string|array
//      */
//     public static function name2CamelCase($string, ?array $prefixo = null)
//     {
//         $prefixo ??= self::$prefixo;

//         if (is_array($string)) {
//             foreach ($string as $key => $value) {
//                 $out[self::name2CamelCase($key)] = $value;
//             }
//             return $out;
//         }

//         if (is_array($prefixo)) {
//             foreach ($prefixo as $val) {
//                 $string = str_replace($val, "", $string);
//             }
//         }

//         $string = str_replace('_', ' ', $string);
//         $string = str_replace('-', ' ', $string);
//         $out = lcfirst(str_replace(' ', '', ucwords($string)));
//         return $out;
//     }

//     /**
//      * Revert a string camelCase para camel_case
//      * @param string $string
//      * @return string
//      */
//     public static function reverteName2CamelCase($string)
//     {
//         $out = '';
//         $length = strlen((string) $string);

//         for ($i = 0; $i < $length; $i++) {
//             $currentChar = $string[$i];

//             if (ctype_upper($currentChar) && $string[$i] !== '.' && $string[$i] !== '|') {
//                 $out .= $i > 0 ? '_' : '';
//                 $currentChar = mb_strtolower($currentChar);
//             }
//             $out .= $currentChar;
//         }
//         return $out;
//     }

//     /**
//      * Cria a arvore de diretorios
//      * @param string $filename
//      * @return object
//      */
//     public static function createTreeDir($filename)
//     {
//         $path = str_replace('/', DIRECTORY_SEPARATOR, (string) $filename);
//         $parts = explode(DIRECTORY_SEPARATOR, $path);
//         $file = array_pop($parts);
//         $dir = implode(DIRECTORY_SEPARATOR, $parts);
//         self::mkdir($dir, 0777);
//         // @mkdir($dir, 0777, true);
//         return (object) ['path' => $dir, 'name' => $file];
//     }

//     public static function saveFile($filename, $name = false, $template = '<?php Header("Location:/");', $mode = "w+")
//     {
//         $filename = $filename . (($name) ? '/' . $name : '');
//         $file = self::createTreeDir($filename);
//         if (file_exists($filename) && $mode !== 'SOBREPOR') {
//             $file->name = '__NEW__' . $file->name;
//         }
//         $save = str_replace('/', DIRECTORY_SEPARATOR, $file->path . DIRECTORY_SEPARATOR . $file->name);
//         unset($filename);
//         file_put_contents($save, $template);
//         return file_exists($save);
//     }

//     public static function eficiencia_init()
//     {
//         return "Method is disabled. Use class Eficiencia()";
//     }

//     public static function endEficiencia()
//     {
//         return "Method is disabled. Use class Eficiencia()";
//     }

//     public static function directorySeparator(&$var): void
//     {
//         $var = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $var);
//     }

//     public static function deleteDir($pasta)
//     {
//         // Apenas para organizacao e compatibilidade de versoes
//         return DirectoryManipulation::deleteDirectory($pasta);
//     }


//     /**
//      * Remvoe um arquivop
//      *
//      * @param string $filepath
//      * @param boolean $apagarDiretorio
//      * @param boolean $trash
//      * @return bool
//      */
//     public static function deleteFile(string $filepath, bool $apagarDiretorio = false, bool $trash = false)
//     {
//         ///echo $filename;
//         $filename = (string) str_replace('/', DIRECTORY_SEPARATOR, $filepath);

//         if (is_dir($filename)) {
//             $dir = dir($filename);
//             while ($arquivo = $dir->read()) {
//                 if ($arquivo != '.' && $arquivo != '..') {
//                     self::deleteFile($filename . DIRECTORY_SEPARATOR . $arquivo, false, $trash);
//                 }
//             }
//             $dir->close();
//             if ($apagarDiretorio) {
//                 rmdir($filename);
//             }
//         } else {
//             if (file_exists($filename)) {
//                 unlink($filename);
//             }
//         }

//         return !file_exists($filename);
//     }

//     /**
//      * Permite o uso em ambientes com SSL
//      * 
//      * @param string $url
//      * @param bool $ssl Tru or false para validação de SSL. Default false
//      * @param int $timeout Timeout da chamada. Default 30 segundos
//      * @return string
//      */
//     public static function myFileGetContents(string $url, bool $ssl = false, int $timeout = 30): string
//     {
//         $config = [
//             'http' => [
//                 'timeout' => $timeout
//             ],
//             'ssl' => [
//                 'verify_peer' => $ssl,
//                 'verify_peer_name' => $ssl
//             ]
//         ];
//         $context = stream_context_create($config);
//         return file_get_contents($url, false, $context);
//     }

//     /**
//      * Método que encapsula uma chamada a uma URL
//      *
//      * @param string $url
//      * @param array $params
//      * @param string $method
//      * @param array $header
//      * @param boolean $ssl
//      * @param integer $timeout
//      * @return object
//      */
//     public static function curlCall(string $url, array $params = [], string $method = 'GET', array $header = ['Content-Type: application/json'], bool $ssl = true, int $timeout = 30): object
//     {
//         // Remover cookie em excesso
//         $cookiefile = Helper::getTmpDir() . DIRECTORY_SEPARATOR . 'NsUtilCurlCookie_' . md5((string) date('Ymd')) . '.txt';

//         $options = [
//             CURLOPT_URL => trim($url),
//             CURLOPT_CUSTOMREQUEST => strtoupper($method), // Definir o método HTTP (GET, POST, PUT, DELETE, etc.)
//             CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0',
//             CURLOPT_COOKIEFILE => $cookiefile,
//             CURLOPT_COOKIEJAR => $cookiefile,
//             CURLOPT_RETURNTRANSFER => true,
//             CURLOPT_FOLLOWLOCATION => true,
//             CURLOPT_ENCODING => "",
//             CURLOPT_AUTOREFERER => true,
//             CURLOPT_CONNECTTIMEOUT => $timeout,
//             CURLOPT_TIMEOUT => $timeout,
//             CURLOPT_MAXREDIRS => 10,
//             CURLOPT_SSL_VERIFYPEER => $ssl,
//             CURLOPT_SSL_VERIFYSTATUS => $ssl,
//             CURLOPT_HEADER => true,
//             CURLOPT_VERBOSE => false,
//             CURLOPT_HTTPHEADER => $header,
//         ];

//         if ($ssl) {
//             $options[CURLOPT_CAINFO] = __DIR__ . '/lib/cacert.pem';
//         }

//         // Configurar dados da requisição conforme o método HTTP
//         switch (strtoupper($method)) {
//             case 'POST':
//                 $options[CURLOPT_POST] = true;
//                 $options[CURLOPT_POSTFIELDS] = self::buildRequestBody($params, $header);
//                 break;
//             case 'PUT':
//             case 'DELETE':
//                 $options[CURLOPT_CUSTOMREQUEST] = strtoupper($method);
//                 $options[CURLOPT_POSTFIELDS] = self::buildRequestBody($params, $header);
//                 break;
//             case 'GET':
//                 $options[CURLOPT_HTTPGET] = true;
//                 if (count($params) > 0) {
//                     $options[CURLOPT_URL] .= '?' . http_build_query($params, '', '&', PHP_QUERY_RFC3986);
//                 }
//                 break;
//             default:
//                 // Caso outros métodos sejam necessários, podem ser adicionados aqui
//                 break;
//         }

//         // Inicializar cURL
//         $ch = curl_init();
//         curl_setopt_array($ch, $options);
//         $output = (string) curl_exec($ch);

//         // Capturar informações da requisição
//         $urlInfo = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
//         $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
//         $body = substr($output, $header_size);

//         // Obter cabeçalhos da resposta
//         $headers = [];
//         $responseHeaders = substr($output, 0, $header_size);
//         foreach (explode("\n", trim($responseHeaders)) as $line) {
//             $parts = explode(':', $line, 2);
//             if (count($parts) === 2) {
//                 $headers[trim($parts[0])] = trim($parts[1]);
//             }
//         }

//         // Montar objeto de retorno
//         $ret = (object) [
//             'content' => $body,
//             'errorCode' => curl_errno($ch),
//             'error' => curl_error($ch) ?: curl_errno($ch),
//             'status' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
//             'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
//             'headers' => $headers,
//             'url' => $urlInfo,
//         ];

//         // Fechar a sessão cURL
//         curl_close($ch);

//         return $ret;
//     }

//     /**
//      * Constrói o corpo da requisição com base nos parâmetros e no cabeçalho
//      *
//      * @param array $params
//      * @param array $header
//      * @return string|array
//      */
//     private static function buildRequestBody(array $params, array $header)
//     {
//         // Função para verificar se o cabeçalho indica JSON
//         $isJsonRequest = array_filter(
//             $header,
//             function ($headerLine) {
//                 return preg_match('/\bcontent-type\s*:\s*application\/json\b/i', $headerLine);
//             }
//         );

//         // Verifica se a função encontrou algum cabeçalho que corresponda ao padrão
//         if (!empty($isJsonRequest)) {
//             return json_encode($params);
//         } else {
//             return http_build_query($params, '', '&', PHP_QUERY_RFC3986);
//         }
//     }

//     /**
//      * Retorna um array associativo de um handle aberto via fopen
//      *
//      * @param mixed $handle
//      * @param string $explode
//      * @return mixed
//      */
//     public static function myFGetsCsv($handle, $explode = ';', $blockSize = 0, $enclosure = '"')
//     {
//         $data = fgetcsv($handle, $blockSize, $explode, $enclosure);
//         if ($data === false || $data === null) {
//             return false;
//         }

//         $substituions = [
//             '|EN|' => $explode,
//             chr(10) => "___NS_EOL___",
//             "\\n" => "___NS_EOL___",
//             "\\t" => ' ',
//             "\\r" => '',
//             "0x0d" => '',
//             ';' => ' ',
//             "'" => '',
//         ];

//         $line = (string) trim(
//             (string)
//             implode('[_|M|_]', $data)
//         );

//         $line = str_replace(
//             array_keys($substituions),
//             array_values($substituions),
//             $line
//         );

//         // devolver as novas linhas
//         $line = str_replace('\\', '', $line);
//         $line = str_replace(['___NS_EOL___'], ["\\n"], $line);

//         $line = mb_convert_encoding($line, "UTF-8");
//         $data = explode('[_|M|_]', $line);

//         return $data;
//     }

//     /**
//      * Conta a quantidade linhas em um arquivo CSV ou TXT
//      * @param string $file
//      * @return int
//      */
//     public static function linhasEmArquivo(string $file)
//     {
//         $l = 0;
//         if ($f = fopen($file, "r")) {
//             while ($d = fgets($f, 1000)) {
//                 $l++;
//             }
//         }
//         unset($d);
//         fclose($f);
//         return $l;
//     }

//     /**
//      * Trata de dados de entrada via APIs, removendo caracteres não desejados e nulls
//      * @param array $dados
//      * @return void
//      */
//     public static function recebeDadosFromView(&$dados)
//     {
//         if (is_array($dados)) {
//             foreach ($dados as $key => $value) {
//                 if (is_array($value)) {
//                     self::recebeDadosFromView($dados[$key]);
//                 } else {
//                     // tirar "undefined" do javascript
//                     if ($value === 'undefined' || $value === 'null' || $value === null) {
//                         unset($dados[$key]);
//                         continue;
//                     } else {
//                         $dados[$key] = Filter::string($value);
//                         $dados[$key] = str_replace(['NS21', '&#34;'], ['&', '"'], $dados[$key]);
//                     }
//                     if (substr((string) $key, 0, 2) === 'id') {
//                         $dados[$key] = (int) filter_var($value, FILTER_VALIDATE_INT);
//                     }
//                 }
//             }
//         }
//     }

//     public static function depara(array $depara, array $dados, $retornaSomenteDepara = true)
//     {
//         if ($retornaSomenteDepara) {
//             $out = [];
//         } else {
//             $out = $dados;
//         }
//         foreach ($depara as $key => $val) {
//             $out[$key] = $dados[$val];
//         }
//         return $out;
//     }

//     public static function compareString($str1, $str2, $case = false)
//     {
//         if (!$case) {
//             return (mb_strtoupper((string) $str1) === mb_strtoupper((string) $str2));
//         } else {
//             return ($str1 === $str2);
//         }
//     }

//     public static function fileGetEncoding($filename)
//     {
//         $so = php_uname();
//         $cod = '';
//         if (stripos((string) $so, 'linux') > -1) {
//             $cmd = 'file -bi ' . $filename . ' | sed -e "s/.*[ ]charset=//"';
//             $cod = shell_exec($cmd);
//         } else {
//             throw new Exception('getFileEncoding somente funciona em sistemas Linux. O seu é ' . $so);
//         }
//         return trim((string) $cod);
//     }

//     public static function fileConvertToUtf8($filepath, $output = false)
//     {
//         if (file_exists($filepath)) {
//             $enc = self::fileGetEncoding($filepath);
//             if (strlen($enc) > 0 && $enc !== 'utf-8' && stripos((string) $enc, 'ascii') === false) {
//                 $output = $output ? $output : $filepath;
//                 $cmd = "iconv -f $enc -t utf-8 -o $output $filepath ";
//                 $ret = shell_exec($cmd);
//                 if (strlen((string) $ret) > 0) {
//                     throw new Exception("Erro ao converter arquivo $filepath pata UTF-8: " . $ret);
//                 }
//             }
//         } else {
//             return 'File not exists';
//         }
//     }

//     public static function fileSearchRecursive($file_name, $dir_init, $deep = 10)
//     {
//         $dirarray = explode(DIRECTORY_SEPARATOR, rtrim($dir_init, DIRECTORY_SEPARATOR));
//         $count = 0;

//         while ($count < $deep) {
//             $filename = implode(DIRECTORY_SEPARATOR, $dirarray) . DIRECTORY_SEPARATOR . $file_name;

//             if (file_exists($filename)) {
//                 return realpath($filename);
//             }

//             if (empty($dirarray)) {
//                 break;
//             }

//             array_pop($dirarray);
//             $count++;
//         }

//         return false;
//     }

//     public static function packerAndPrintJS($js)
//     {
//         $packer = new Packer($js, 'Normal', true, false, true);
//         $packed_js = $packer->pack();
//         echo "<script>$packed_js</script>";
//     }

//     /**
//      * Verifica se o dados existe, se o conteudo é diferente de '' ou null ou false
//      * @param mixed $value
//      */
//     public static function hasContent($value, $type = 'string')
//     {
//         if (is_array($value)) {
//             foreach ($value as $item) {
//                 if (!self::hasContent($item, $type)) {
//                     return false;
//                 }
//             }
//             return true;
//         }
//         $out = false;
//         switch ($type) {
//             case 'int':
//                 $t = (int) $value;
//                 $out = $t > 0;
//                 break;
//             case 'array':
//                 $out = is_array($value);
//                 break;
//             case 'email':
//                 $out = is_string($value) && strlen((string) $value) > 0 && self::validaEmail((string) $value);
//                 break;
//             case 'cpf':
//             case 'cnpj':
//                 $out = is_string($value) && strlen((string) $value) > 0 && Validate::validaCpfCnpj((string) $value) === true;
//                 break;
//                 // default mode
//             default:
//                 $out = strlen((string) $value) > 0;
//                 break;
//         }
//         return $out;
//     }

//     /**
//      * Retorna um array com errors
//      * @param array $dadosObrigatorios Array contendo array: ['value' => $dados['idCurso'], 'msg' => 'Informe a data inicial', 'type' => 'int'],
//      */
//     public static function validarCamposObrigatorios($dadosObrigatorios)
//     {
//         $error = [];
//         foreach ($dadosObrigatorios as $item) {
//             $has = self::hasContent($item['value'], $item['type'] ?? 'string');
//             if ($has === false) {
//                 if ($item['key']) {
//                     //$item['msg'] = '{' . $item['key'] . '}: ' . $item['msg'];
//                     $error[$item['key']] = $item['msg'];
//                 } else {
//                     $error[] = $item['msg'];
//                 }
//             }
//         }
//         return $error;
//     }

//     public static function base64_to_jpeg($base64_string, $output_file)
//     {
//         // open the output file for writing
//         $ifp = fopen($output_file, 'wb');

//         // split the string on commas
//         // $data[ 0 ] == "data:image/png;base64"
//         // $data[ 1 ] == <actual base64 string>
//         $data = explode(',', $base64_string);

//         // we could add validation here with ensuring count( $data ) > 1
//         fwrite($ifp, base64_decode($data[1]));

//         // clean up the file resource
//         fclose($ifp);

//         return $output_file;
//     }

//     public static function gzReader($filename)
//     {
//         $out = '';
//         // Raising this value may increase performance
//         $buffer_size = 4096; // read 4kb at a time
//         // Open our files (in binary mode)
//         $file = gzopen($filename, 'rb');

//         // Keep repeating until the end of the input file
//         while (!gzeof($file)) {
//             // Read buffer-size bytes
//             // Both fwrite and gzread and binary-safe
//             $out .= gzread($file, $buffer_size);
//         }

//         return $out;
//     }

//     public static function extractDataAtributesFromHtml($html)
//     {
//         $list = explode("data-", $html);
//         unset($list[0]);
//         $out = [];
//         foreach ($list as $item) {
//             $atribute = explode('"', $item);
//             $out[str_replace('=', '', $atribute[0])] = $atribute[1];
//         }
//         return $out;
//     }

//     /**
//      * Detecta se a chamada foi feita de um dispositivo mobile
//      * @return boolean
//      */
//     public static function isMobile(): bool
//     {
//         return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", (string) ($_SERVER["HTTP_USER_AGENT"] ?? '-')) === 1;
//     }

//     public static function isJson($jsonString): bool
//     {
//         $isJson = false;
//         $jsonString = Filter::string($jsonString);

//         // Verifica se o conteúdo começa e termina com colchetes ou chaves
//         if (
//             (strpos($jsonString, '[') === 0 && strrpos($jsonString, ']') === strlen($jsonString) - 1) ||
//             (strpos($jsonString, '{') === 0 && strrpos($jsonString, '}') === strlen($jsonString) - 1)
//         ) {
//             // Verifica se o conteúdo é um JSON válido usando json_decode()
//             json_decode($jsonString);
//             $isJson = json_last_error() === JSON_ERROR_NONE;
//         }

//         return $isJson;
//     }

//     /**
//      * Retorna o IP em uso pelo cliente
//      *
//      * @return string
//      */
//     public static function getIP(): string
//     {
//         if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && strlen($_SERVER['HTTP_X_FORWARDED_FOR']) > 0) {
//             $parts = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
//             $ip = trim(end($parts));
//         } else {
//             $ip = $_SERVER['HTTP_CLIENT_IP'] ?? $_SERVER['REMOTE_ADDR'] ?? '-';
//         }
//         return Filter::string($ip);
//     }

//     /**
//      * Ira filtrar um array de entrada de dados conforme os tipos identificados
//      * @param mixed $var
//      * @return string
//      */
//     public static function filterSanitize($var)
//     {
//         if (is_array($var)) {
//             foreach ($var as $key => $value) {
//                 if (is_array($value)) {
//                     $var[$key] = self::filterSanitize($value);
//                 } else {
//                     if (substr((string) $key, 0, 2) === 'id') {
//                         $var[$key] = filter_var($value, FILTER_VALIDATE_INT);
//                     }
//                     if (stripos((string) $key, 'email') > -1) {
//                         $var[$key] = filter_var($value, FILTER_VALIDATE_EMAIL);
//                     } else {
//                         $var[$key] = Filter::string($value);
//                     }
//                 }
//             }
//             return $var;
//         } else {
//             return Filter::string($var);
//         }
//     }

//     public static function myFPutsCsv($file_path, $fields, $delimiter = ',', $enclosure = '"', $escape_char = "\\")
//     {
//         if (!file_exists($file_path)) {
//             Helper::saveFile($file_path, '', '');
//         }

//         $escaped_fields = [];

//         // Trata cada campo
//         foreach ($fields as $field) {
//             // Escapa aspas dentro do campo
//             $field = str_replace($enclosure, $escape_char . $enclosure, $field);

//             // Substitui quebras de linha reais por literais \n e \r
//             $field = str_replace(["\n", "\r"], ["\\n", "\\r"], $field);

//             // Envolve o campo com delimitadores se contiver o delimitador ou a quebra de linha escapada
//             if (strpos($field, $delimiter) !== false || strpos($field, "\n") !== false || strpos($field, $enclosure) !== false) {
//                 $field = "{$enclosure}{$field}{$enclosure}";
//             }

//             $escaped_fields[] = $field;
//         }

//         // Cria a linha CSV
//         $csv_line = implode($delimiter, $escaped_fields) . "\n";

//         // Escreve no arquivo
//         file_put_contents($file_path, $csv_line, FILE_APPEND);
//     }


//     /**
//      * 
//      * @param array $array 
//      * @param string $filepath if false, retorna em text
//      * @param bool $withBom
//      * @return mixed
//      */

//     public static function array2csv(array $array, ?string $filepath = null, bool $withBom = true, string $delimiter = ',')
//     {
//         // Manter o padrão entre as chaves
//         $trataed = [];
//         $keys = array_keys($array[0]);
//         foreach ($array as $val) {
//             $ni = [];
//             foreach ($keys as $k) {
//                 $ni[$k] = $val[$k] ?? '';
//             }
//             $trataed[] = $ni;
//         }
//         $array = $trataed;

//         if (null !== $filepath) {

//             // garantir que o arquivo exista
//             if (!file_exists($filepath)) {
//                 Helper::saveFile($filepath, '', '');
//             }

//             // BOM
//             if ($withBom) {
//                 file_put_contents($filepath, chr(0xEF) . chr(0xBB) . chr(0xBF), FILE_APPEND);
//             }

//             // Gravar o cabeçalho
//             self::myFPutsCsv($filepath, array_keys($array[0]));

//             // Gravar dados
//             foreach ($array as $linha) {
//                 foreach ($linha as $key => $val) {
//                     if (is_array($val) || is_object($val)) {
//                         $linha[$key] = json_encode($val, JSON_HEX_QUOT | JSON_HEX_APOS | JSON_UNESCAPED_UNICODE);
//                     }
//                 }
//                 self::myFPutsCsv($filepath, $linha, $delimiter);
//             }
//             // fclose($fp);
//             return file_exists($filepath);
//         } else {
//             $handle = fopen('php://temp', 'r+');
//             foreach ($array as $line) {
//                 foreach ($line as $key => $val) {
//                     if (is_array($val) || is_object($val)) {
//                         $line[$key] = json_encode($val, JSON_HEX_QUOT | JSON_HEX_APOS | JSON_UNESCAPED_UNICODE);
//                     }
//                 }
//                 fputcsv($handle, $line, $delimiter, '"');
//             }
//             rewind($handle);
//             $contents = '';
//             while (!feof($handle)) {
//                 $contents .= fread($handle, 8192);
//             }
//             fclose($handle);
//             return $contents;
//         }
//     }

//     public static function getSO()
//     {
//         return mb_strtolower(explode(' ', php_uname())[0]);
//     }

//     /**
//      * Remove all characters is not a number
//      *
//      * @param mixed $var
//      * @return string
//      */
//     public static function parseInt($var, $isNumber = false)
//     {
//         $str = (string) $var;

//         // mantém e considera o traço um valor negativo
//         if ($isNumber) {
//             // remover virgulas
//             $str = str_replace([',', '.'], '', $str);

//             if (preg_match('/-?\d+/', $str, $matches)) {
//                 return (int) $matches[0];
//             }

//             return null;
//         } else {
//             return preg_replace("/[^0-9]/", "", (string) $var);
//         }
//     }

//     /**
//      * json_dceo com tratamento de alguns carateres que causam sujeira
//      * @param string $json
//      * @return array
//      */
//     public static function jsonToArrayFromView($json)
//     {
//         return json_decode(str_replace('&#34;', '"', (string) $json), true);
//     }

//     public static function objectPHP2Array(stdClass $object)
//     {
//         return json_decode(json_encode($object), true);
//     }

//     /**
//      * Com base na data informada, retorna um array verificando se é feriado, prox dia util....
//      * 
//      * @param string $date formato yyyy-mm-dd
//      * @return array {"isDiaUtil":true,"proxDiaUtil":"2021-03-19","ultDiaUtil":"2021-03-17"}
//      */
//     public static function feriado($date)
//     {
//         $url = 'https://syncpay.usenextstep.com.br/api/util/feriado/' . self::parseInt($date);
//         $ret = self::curlCall($url, [], 'GET', ['Content-Type: application/json'], false, 10)->content ?? '{}';
//         $ret1 = json_decode($ret, true);
//         return $ret1['content'] ?? null;
//     }

//     /**
//      * Com base na data informaada, calcula a proxima data útil no calendario, com prazo N estabelecido
//      * @param string $vencimento Data inicial, no formato yyyy-mm-dd
//      * @param int $prazo Prazo em 
//      * @return string String, no formato yyyy-mm-dd
//      */
//     public static function calculaVencimentoUtil($vencimento, $prazo = 0)
//     {
//         if ($prazo > 0) {
//             // rotina para varrer somente dias úteis
//             $count = 0;
//             while ($count < $prazo) {
//                 $ret = self::feriado($vencimento);
//                 $vencimento = $ret['proxDiaUtil'];
//                 $count++;
//             }
//         }
//         return $vencimento;
//     }

//     /**
//      * Ira retornar um array contendo apenas os valores das chaves selecionadas
//      * @param array $origem
//      * @param array $chaves
//      */
//     public static function arrayReduceKeys(array $origem, array $chaves)
//     {
//         $out = [];
//         foreach ($chaves as $val) {
//             $out[$val] = $origem[$val];
//         }
//         return $out;
//     }

//     /**
//      * Ira buscar o path da aplicação, antes da pasta /vendor
//      */
//     public static function getPathApp()
//     {
//         return str_replace(DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'nextstage-brasil' . DIRECTORY_SEPARATOR . 'ns-util' . DIRECTORY_SEPARATOR . 'src', '', __DIR__);
//     }

//     /**
//      * Retorna um valor baseado em um tipo
//      * @param mixed $string
//      * @param string $type
//      */
//     public static function getValByType($string, string $type)
//     {
//         switch ($type) {
//             case 'int':
//             case 'serial':
//             case 'serial4':
//             case 'serial8':
//                 $out = filter_var($string, FILTER_VALIDATE_INT);
//                 if ($out === false) {
//                     $out = null;
//                 }
//                 break;
//             case 'double':
//                 $out = filter_var($string, FILTER_VALIDATE_FLOAT);
//                 if ($out === false) {
//                     $out = null;
//                 }
//                 break;
//             case 'html':
//                 $out = $string;
//                 break;
//             case 'bool':
//             case 'boolean':
//                 $out =
//                     self::compareString($string, 'true')
//                     || self::compareString($string, true)
//                     || self::compareString($string, '(true)')
//                     || self::compareString($string, '1')
//                     || self::compareString($string, 1);
//                 break;
//             default:
//                 $out = null === $string ? null : Filter::string($string);
//                 break;
//         }
//         return $out;
//     }

//     public static function formatDate($date, $escolha = 'arrumar', $datahora = false, $alterarTimeZone = false, $timezone = 'America/Sao_Paulo')
//     {
//         return (new Format($date, $timezone))->date($escolha, $datahora, $alterarTimeZone);
//     }

//     public static function formatCep($cep)
//     {
//         return (new Format($cep))->cep();
//     }

//     public static function formatCpfCnpj($var)
//     {
//         $var = self::parseInt($var);
//         if (strlen((string) $var) === 11) { // cpf
//             $out = substr((string) $var, 0, 3) . '.' . substr((string) $var, 3, 3) . '.' . substr((string) $var, 6, 3) . '-' . substr((string) $var, 9, 2);
//         } else if (strlen((string) $var) === 14) { // cnpj
//             $out = substr((string) $var, 0, 2) . '.' . substr((string) $var, 2, 3) . '.' . substr((string) $var, 5, 3) . '/' . substr((string) $var, 8, 4) . '-' . substr((string) $var, 12, 2);
//         } else {
//             $out = $var;
//         }
//         return $out;
//     }

//     /**
//      * Destaca um texto em string
//      *
//      * @param string $texto
//      * @param string $search
//      * @return void
//      */
//     public static function highlightText(string &$texto, string $search)
//     {
//         $searchsan = self::sanitize($search);
//         $textosan = self::sanitize($texto);
//         $inicio = stripos((string) $textosan, $searchsan);
//         if ($inicio >= 0) {
//             $trecho = \mb_substr((string) $texto, $inicio, strlen((string) $search));
//             $texto = \str_replace($trecho, '<span class="ns-highlight-text">' . $trecho . '</span>', $texto);
//         }
//     }

//     public static function getPsr4Name(?string $dir = null)
//     {
//         $dir ??= self::getPathApp();

//         $composer = file_get_contents(Helper::fileSearchRecursive('composer.json', $dir));
//         $composer = json_decode($composer, true);

//         return substr(
//             str_replace('\\\\', '\\', key($composer['autoload']['psr-4'])),
//             0,
//             -1
//         );
//     }

//     public static function getPhpUser()
//     {
//         if (function_exists('posix_getuid')) {
//             $uid = posix_getuid();
//             $userinfo = posix_getpwuid($uid);
//             return $userinfo;
//         } else {
//             return -1;
//         }
//     }

//     // Define uma função que poderá ser usada para validar e-mails usando regexp
//     public static function validaEmail($email)
//     {
//         //return
//         $er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
//         if (preg_match($er, (string) $email)) {
//             return true;
//         } else {
//             return false;
//         }
//     }

//     /**
//      * Verifica se a string é uma base64_encoded valida
//      * @param string $data
//      * @return boolean
//      */
//     public static function isBase64Encoded($string)
//     {
//         if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', (string) $string)) {
//             return true;
//         } else {
//             return false;
//         }
//     }

//     /**
//      * Prepara um valor para o formato americano decimal
//      *
//      * @param mixed $var
//      * @return float|mixed
//      */
//     public static function decimalFormat($var)
//     {
//         if (stripos((string) $var, ',') > -1) { // se achar virgula, veio da view, com formato. da base, nao vem virgula
//             $var = self::parseInt($var, true);
//             $var = substr((string) $var, 0, strlen((string) $var) - 2) . "." . substr((string) $var, strlen((string) $var) - 2, 2);
//         }
//         return $var;
//     }

//     /**
//      * Retorna um array com as variaveis para paginação de resultados
//      * @param int $page Pagina atual
//      * @param int $limitPerPage limite por página
//      * @param int $totalRegs total de registros do request
//      * @return array
//      */
//     public static function pagination(int $atualPage, int $limitPerPage, int $totalRegs): array
//     {
//         $ret = [];
//         $ret['atualPage'] = $atualPage;
//         $ret['nextPage'] = ((($limitPerPage * ($atualPage + 1)) < $totalRegs) ? ($atualPage + 1) : false);
//         $ret['prevPage'] = (($atualPage > 0) ? $atualPage - 1 : false);
//         $ret['totalPages'] = ceil(($totalRegs / $limitPerPage));
//         return $ret;
//     }

//     /**
//      * Método que converte um array php para o formato ".env" files
//      *
//      * @param array $config
//      * @return array
//      */
//     public static function array2env(array $config)
//     {
//         $out = [];
//         foreach ($config as $key => $value) {
//             if (is_object($value)) {
//                 $value = \json_decode(json_encode($value), true);
//             }
//             if (\is_array($value)) {
//                 $out[] = "\n[$key]";
//                 $temp = self::array2env($value);
//                 $out[] = implode("\t\n", $temp);
//                 $out[] = "\n";
//             }
//             $out[] = "$key=\"$value\"";
//         }
//         return $out;
//     }

//     /**
//      * Return de icon name
//      *
//      * @param string $filename
//      * @return string
//      */
//     public static function getThumbsByFilename(string $filename): string
//     {
//         $t = explode('.', $filename);
//         $extensao = array_pop($t);
//         $out = '';
//         switch (mb_strtoupper((string) $extensao)) {
//             case 'XLSX':
//             case 'XLS':
//                 $out = 'file-excel-o';
//                 break;
//             case 'PDF':
//                 $out = 'file-pdf-o';
//                 break;
//             case 'PNG':
//             case 'JPG':
//             case 'GIF':
//             case 'JPEG':
//                 $out = 'file-image-o';
//                 break;
//             case 'ZIP':
//                 $out = 'file-archive-o';
//                 break;
//             case 'MP3':
//             case 'AAC':
//                 $out = 'file-audio-o';
//                 break;
//             case 'AVI':
//             case 'MP4':
//                 $out = 'file-video-o';
//                 break;
//             default:
//                 $out = 'file';
//         }
//         return $out;
//     }

//     /**
//      * Executa uma busca no viacep e retorna
//      * @param string $cep
//      * @return stdClass
//      */
//     public static function buscacep(string $cep): stdClass
//     {
//         $cepSearch = self::parseInt($cep);
//         if (strlen($cepSearch) < 8) {
//             return (object) ['error' => "Quantidade de caracteres inválido: '$cep'"];
//         }
//         if ($cepSearch < 1) {
//             return (object) ['error' => "CEP inválido para pesquisa: '$cep'"];
//         }

//         $url = "https://viacep.com.br/ws/$cepSearch/json/";
//         $ret = file_get_contents($url);
//         return (object) json_decode($ret);
//     }
//     public static function setPaginacao(int $registros, int $limit, array &$out, array &$dados): void
//     {
//         $paginas = (int) ($registros / $limit);
//         $out['pagination'] = [];
//         $out['pagination']['page'] ??= 0;
//         $out['pagination']['atualPage'] = (int) $dados['pagina'];
//         $out['pagination']['totalPages'] = $paginas * $limit < $registros ? $paginas + 1 : $paginas;
//         $out['pagination']['totalItens'] = $registros;
//         $out['pagination']['nextPage'] = $out['pagination']['page'] + 1 < $out['pagination']['totalPages'] ? $out['pagination']['page'] + 1 : null;
//         $out['pagination']['previusPage'] = $out['pagination']['page'] - 1 >= 0 ? $out['pagination']['page'] - 1 : null;
//         $out['pagination']['initalPage'] = 0;
//         $out['pagination']['lastPage'] = $out['pagination']['totalPages'];
//     }

//     /**
//      * @return string
//      */
//     public static function getTmpDir(): string
//     {
//         // @codeCoverageIgnoreStart
//         if (function_exists('sys_get_temp_dir')) {
//             $tmp = sys_get_temp_dir();
//         } elseif (!empty($_SERVER['TMP'])) {
//             $tmp = $_SERVER['TMP'];
//         } elseif (!empty($_SERVER['TEMP'])) {
//             $tmp = $_SERVER['TEMP'];
//         } elseif (!empty($_SERVER['TMPDIR'])) {
//             $tmp = $_SERVER['TMPDIR'];
//         } else {
//             $tmp = getcwd();
//         }

//         return $tmp;
//     }

//     /**
//      * @return string
//      */
//     public static function getHost(): string
//     {
//         return php_uname('n');
//     }

//     public static function addConditionFromAPI(array &$condition, array $dados): void
//     {
//         if (isset($dados['conditions']) && is_array($dados['conditions'])) {
//             $newConditions = [];
//             foreach ($dados['conditions'] as $key => $val) {
//                 $isJson = json_decode($val, true);
//                 if ($isJson) {
//                     $val = $isJson;
//                 }
//                 $newConditions[$key . '_addedConditionFromAPI'] = $val;
//             }
//             $condition = array_merge($condition, $newConditions);
//         }
//     }


//     /**
//      * retorna o texto entre a tag selecionada
//      *
//      * @param string $msg
//      * @param string $tag
//      * @return array
//      */
//     public static function getTextByTag(string $msg, string $tag, string $tagOpen = '<', string $tagClose = '>'): array
//     {
//         $out = [];
//         $termOpen = $tagOpen . $tag . $tagClose;
//         $termClose = $tagOpen . '\/' . $tag . $tagClose;

//         if (stripos((string) $msg, $termOpen) !== false) {
//             // preg_match('/' . $termOpen . '(.*?)' . $termClose . '/s', $msg, $match);
//             preg_match('/' . $termOpen . '(.*?)' . $termClose . '/s', $msg, $match);
//             unset($match[0]);
//             $out = array_map(function ($item) {
//                 return trim(str_replace('\n', '<br/>', strip_tags((string) $item)));
//             }, $match);
//         }
//         return is_array($out) ? $out : [];
//     }

//     /**
//      * Método que retorna um array com as diferenças entre dois arrays
//      *
//      * @param array $arrayNew
//      * @param array $arrayOld
//      * @param array $keysToIgnore
//      * @return array
//      */
//     public static function arrayDiff(array $arrayNew, array $arrayOld, array $keysToIgnore = [])
//     {
//         $out = [];

//         $alteradosNovo = array_diff_assoc($arrayNew, $arrayOld);
//         $alteradosAntigo = array_diff_assoc($arrayOld, $arrayNew);
//         unset($alteradosNovo['error']);

//         if (count($alteradosNovo) > 0) {
//             foreach ($alteradosNovo as $key => $value) {

//                 if (array_search($key, $keysToIgnore) !== false) {
//                     continue;
//                 }

//                 $json = is_string($value) ? json_decode((string) $value, true) : null;

//                 if (is_array($json) && is_string($alteradosAntigo[$key])) {
//                     if (array_search($key, $keysToIgnore) !== false) {
//                         continue;
//                     }
//                     $jsonOLD = json_decode((string) $alteradosAntigo[$key], true);
//                     $out = array_merge($out, self::arrayDiff($json, $jsonOLD));
//                 } else {
//                     $out[] = [
//                         'field' => $key,
//                         'old' => isset($alteradosAntigo[$key]) ? $alteradosAntigo[$key] : null,
//                         'new' => $value
//                     ];
//                 }
//             }
//         }

//         return $out;
//     }

//     public static function httpsForce()
//     {
//         if ($_SERVER["HTTPS"] != "on") {
//             header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
//             die();
//         }
//     }

//     public static function generateCode($string)
//     {
//         return strtoupper(substr($string, 0, 1) . substr($string, -1) . substr(md5($string), 0, 2));
//     }

//     public static function shellExec($command)
//     {

//         $descriptorspec = [
//             0 => ['pipe', 'r'],
//             // stdin is a pipe that the child will read from
//             1 => ['pipe', 'w'],
//             // stdout is a pipe that the child will write to
//             2 => ['pipe', 'w'],
//             // stderr is a pipe that the child will write to
//         ];

//         $process = proc_open($command, $descriptorspec, $pipes);

//         if (is_resource($process)) {
//             fclose($pipes[0]); // Close the stdin pipe, since we won't be using it

//             // Read the output from stdout and stderr pipes
//             $output = '';
//             while (($buffer = fgets($pipes[1])) !== false || ($buffer = fgets($pipes[2])) !== false) {
//                 $output .= $buffer;
//                 echo $buffer; // Display the output in real-time
//                 flush(); // Force the output to be sent to the browser

//                 // If PHP output buffering is enabled, force it to be sent to the browser
//                 if (ob_get_length() > 0) {
//                     ob_flush();
//                 }
//             }

//             fclose($pipes[1]);
//             fclose($pipes[2]);

//             $return_value = proc_close($process);

//             // echo "Command returned: $return_value\n";
//         }
//     }

//     public static function commandPrintHeader($text, $size = 40)
//     {
//         $cmd = "header=\"$text\" && width=$size && padding=\$(((\$width - \${#header}) / 2)) && printf '%*s\n' \"\${COLUMNS:-40}\" \"\" | tr ' ' '-' | cut -c 1-\"\${width}\" && printf \"|%*s%s%*s|\n\" \$padding \"\" \"\$header\" \$padding \"\" && printf '%*s\n' \"\${COLUMNS:-80}\" \"\" | tr ' ' '-' | cut -c 1-\"\${width}\"";
//         self::shellExec($cmd);
//     }

//     public static function uniqueHash($prefix = '')
//     {
//         return md5($prefix . uniqid(rand(), true));
//     }

//     /**
//      * Obtém a data de vecimento do certificado anexado a URL ou null caso não exista
//      *
//      * @param string $url
//      * @return ?int
//      */
//     public static function sslGetEndDate(string $url)
//     {
//         if (Helper::getSO() !== 'linux') {
//             throw new Exception('Only Linux System');
//         }
//         // sanitize
//         $url = str_replace(['https://', 'http://', ':443', '/'], '', $url);
//         // sh
//         // $sh = "openssl s_client -connect $url:443 2>/dev/null | openssl x509 -noout -enddate | awk -F= '{print $2}'";
//         $sh = "timeout 5 openssl s_client -connect $url:443 2>/dev/null | openssl x509 -noout -enddate | awk -F= '{print $2}'";
//         $return = null;

//         $date = shell_exec($sh);
//         if (strlen($date) > 0) {
//             $date = new DateTime($date);
//             $return = $date->getTimestamp() ?? null;
//         }

//         return $return;
//     }

//     /**
//      * Read a CSV file and return a array
//      *
//      * @param string $csvFile
//      * @param string $explode
//      * @return array
//      */
//     public static function fileCSVToArray(string $csvFile, ?string $explode = null, int $blockSize = 0, string $enclosure = '"'): array
//     {
//         return (new LoadCSVToArray($csvFile))->run();
//     }

//     public static function name2KebabCase($string)
//     {
//         if (stripos((string) $string, '_') === false) {
//             $string = self::reverteName2CamelCase($string);
//         }

//         return str_replace('_', '-', $string);
//     }


//     public static function sizeToHumanReader(int $size)
//     {
//         $unidades = array('B', 'KB', 'MB', 'GB', 'TB');
//         $i = 0;

//         while ($size >= 1024 && $i < count($unidades) - 1) {
//             $size /= 1024;
//             $i++;
//         }

//         return round($size, 2) . ' ' . $unidades[$i];
//     }


//     /**
//      * Aplica um namespace específico ao conteúdo de um arquivo.
//      *
//      * Esta função substitui o namespace original no conteúdo do arquivo pelo namespace fornecido.
//      *
//      * @param string $file O caminho do arquivo ao qual o namespace será aplicado.
//      * @param string $originalNamespace O namespace original a ser substituído no conteúdo do arquivo.
//      *                                  O valor padrão é 'NsUtil'.
//      * @return void
//      */
//     public static function applyNamespace($file, string $originalNamespace = 'NsUtil'): void
//     {
//         // Obtém o namespace atual
//         $namespace = Helper::getPsr4Name();

//         // Lê o conteúdo do arquivo
//         $fileContent = file_get_contents($file);

//         // Substitui o namespace original pelo novo namespace no conteúdo do arquivo
//         $content = str_replace(
//             ["namespace $originalNamespace"],
//             ["namespace $namespace"],
//             $fileContent
//         );

//         // Salva o arquivo com o novo conteúdo
//         // O parâmetro 'SOBREPOR' indica que o arquivo será sobrescrito com o novo conteúdo
//         Helper::saveFile($file, false, $content, 'SOBREPOR');
//     }

//     public static  function singularize($word)
//     {
//         $pluralEndings = [
//             '/(alias|address)es$/i' => '\1',
//             '/([^aeiouy])ies$/i' => '\1y',
//             '/(ss)$/i' => 'ss',
//             '/(n)ews$/i' => '\1ews',
//             '/(r)ice$/i' => '\1ice',
//             '/(children)$/i' => 'child',
//             '/(m)en$/i' => '\1an',
//             '/(t)eeth$/i' => '\1ooth',
//             '/(f)eet$/i' => '\1oot',
//             '/(g)eese$/i' => '\1oose',
//             '/(m)ice$/i' => '\1ouse',
//             '/(x|ch|ss|sh)es$/i' => '\1',
//             '/(m)ovies$/i' => '\1ovie',
//             '/(s)eries$/i' => '\1eries',
//             '/([^aeiouy]|qu)ies$/i' => '\1y',
//             '/([lr])ves$/i' => '\1f',
//             '/(tive)s$/i' => '\1',
//             '/(hive)s$/i' => '\1',
//             '/(pri)ces$/i' => '\1ce',
//             '/(b)uses$/i' => '\1us',
//             '/(shoe)s$/i' => '\1',
//             '/(o)es$/i' => '\1',
//             '/(ax|test)es$/i' => '\1is',
//             '/(octop|vir)i$/i' => '\1us',
//             '/(status)$/i' => '\1',
//             '/(alias)es$/i' => '\1',
//             '/s$/i' => '',
//         ];

//         foreach ($pluralEndings as $pattern => $replacement) {
//             if (preg_match($pattern, $word)) {
//                 return preg_replace($pattern, $replacement, $word);
//             }
//         }

//         return $word;
//     }

//     public static function pluralize($word)
//     {
//         $singularEndings = [
//             '/(quiz)$/i' => '\1zes',
//             '/(matr|vert|ind)ix|ex$/i' => '\1ices',
//             '/(x|ch|ss|sh)$/i' => '\1es',
//             '/(r|t|h|s|z)$/i' => '\1es',
//             '/([^aeiouy]|qu)ies$/i' => '\1y',
//             '/(m)ovies$/i' => '\1ovie',
//             '/(s)eries$/i' => '\1eries',
//             '/(n)ews$/i' => '\1ews',
//             '/(child)$/i' => '\1ren',
//             '/(bus)$/i' => '\1es',
//             '/(woman)$/i' => '\1women',
//             '/(man)$/i' => '\1men',
//             '/(tooth)$/i' => '\1teeth',
//             '/(foot)$/i' => '\1feet',
//             '/(person)$/i' => '\1people',
//             '/(goose)$/i' => '\1geese',
//             '/(mouse)$/i' => '\1mice',
//             '/(cactus)$/i' => '\1cacti',
//             '/(knife)$/i' => '\1knives',
//             '/(leaf)$/i' => '\1leaves',
//             '/(life)$/i' => '\1lives',
//             '/(wife)$/i' => '\1wives',
//             '/(hero)$/i' => '\1heroes',
//             '/(potato)$/i' => '\1potatoes',
//             '/(tomato)$/i' => '\1tomatoes',
//             '/(buffalo)$/i' => '\1buffaloes',
//             '/(index)$/i' => '\1indices',
//             '/(alias)$/i' => '\1aliases',
//             '/(status)$/i' => '\1status',
//             '/(radius)$/i' => '\1radii',
//             '/(syllabus)$/i' => '\1syllabi',
//             '/(focus)$/i' => '\1foci',
//             '/(fungus)$/i' => '\1fungi',
//             '/(datum)$/i' => '\1data',
//             '/(appendix)$/i' => '\1appendices',
//             '/(bacterium)$/i' => '\1bacteria',
//             '/(curriculum)$/i' => '\1curricula',
//             '/^(compan)y$/i' => '\1ies', // adicionado padrão para "company" -> "companies"

//         ];

//         foreach ($singularEndings as $pattern => $replacement) {
//             if (preg_match($pattern, $word)) {
//                 return preg_replace($pattern, $replacement, $word);
//             }
//         }

//         return $word . 's';
//     }

//     public static function generateUuidV4(): string
//     {
//         $data = random_bytes(16);

//         // Define os bytes conforme a especificação do UUID v4
//         $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // Versão 4
//         $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // Variante RFC 4122

//         return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
//     }
// }
