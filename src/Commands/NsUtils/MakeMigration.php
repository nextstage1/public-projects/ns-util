<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\ConsoleTable;
use NsUtil\Helper;
use NsUtil\Template;
use NsUtil\Commands\Abstracts\Command;
use NsUtil\Commands\Utils\Util;
use NsUtil\DirectoryManipulation;
use function NsUtil\env;
use function NsUtil\now;

class MakeMigration extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'make:migration';

    /**
     * Handles the execution of the command.
     *
     * @param mixed $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        if (null === $args[0]) {
            throw new \Exception('Name of new command was not informed. Use: php nsutil make:migration MigrationName');
        }

        $className = Helper::sanitize($args[0]);

        $path = env('COMMANDS_MIGRATIONS_PATH', Helper::getPathApp() . '/_build/migrations');

        Helper::createTreeDir($path);

        $lastfiletime = DirectoryManipulation::getLastFileCreated($path);
        $lastfiletime = $lastfiletime > 0 ? $lastfiletime : time();
        $time = now('UTC');
        while ($time->timestamp() < $lastfiletime) {
            $time->add('10 second');
        }

        $filename = $path
            . DIRECTORY_SEPARATOR
            . $time->format('Ymd_His')
            . '_'
            . mb_strtolower($className) . '.sql';

        if (!Helper::saveFile($filename, false, '')) {
            throw new \Exception("It is not possible to create the file $filename");
        }

        chmod($filename, 0777);

        $this->success("File $filename was created!");
    }
}
