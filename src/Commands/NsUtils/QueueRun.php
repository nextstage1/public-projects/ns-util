<?php

namespace NsUtil\Commands\NsUtils;

use Closure;
use Exception;
use NsUtil\Helpers\Cpu;
use NsUtil\Helper;
use NsUtil\EnvFile;
use function NsUtil\env;
use function NsUtil\now;
use NsUtil\ConsoleTable;
use NsUtil\UniqueExecution;
use NsUtil\Connection\SQLite;
use function NsUtil\nsCommand;
use NsUtil\Queue\QueueHandler;
use NsUtil\ConnectionPostgreSQL;
use NsUtil\Commands\Abstracts\Command;
use NsUtil\Connection\KeyValueDatabaseOnPostgresql;
use NsUtil\Date;

/**
 * Queue Runner Class
 * 
 * Responsible for managing and executing queued tasks. Handles both synchronous and asynchronous queue processing.
 * 
 * Features:
 * - Supports multiple runners with configurable limits
 * - Handles sync and async queue types
 * - Automatic retry of failed/locked queues
 * - Graceful shutdown capability
 * - Runner monitoring and control
 * 
 * Environment variables used:
 * - QUEUE_LIMIT_PER_WORKER: Maximum number of concurrent tasks per worker (defaults to CPU count)
 * - QUEUE_MAX_SECONDS_PER_QUEUE: Maximum execution time per queue in seconds (default 60)
 * - QUEUE_MAX_RETRIES: Maximum number of retry attempts for failed queues (default 5)
 * - DBTYPE: Database type to use (sqlite/postgresql)
 * - DBPATH: Database path (for sqlite)
 * - DBNAME: Database name
 * 
 * @package NsUtil\Commands\NsUtils
 */
class QueueRun extends Command
{
    private $con = null;
    private int $runnerLimit;
    private int $runnerMaxSeconds;
    private int $runnerMaxRetries;
    private string $runnerName;
    private string $runnerNameBase;
    private string $queueNameToStop;
    private ?UniqueExecution $ue = null;
    private bool $reloadRuntimeLimit = true;
    private int $nextReloadEnv = 0;
    private string $ueName = '-not-defined-';

    private ?int $queueStartProcessing = null;

    private int $queueInitProcessing = 0;

    private static ?Closure $runnerQueueFunction = null;

    private static array $staticConfig = [];
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "queue:run";

    public function __construct()
    {
        // releitura da env no arquivo para garantir qualquer alteracao.
        EnvFile::applyEnvVariables();
        $cpusDisponiveis = max(1, Cpu::count() - 1);

        $this->runnerLimit = (int) env('QUEUE_LIMIT_PER_WORKER', $cpusDisponiveis);
        $this->runnerMaxSeconds = (int) env('QUEUE_MAX_SECONDS_PER_QUEUE', 60);
        $this->runnerMaxRetries = (int) env('QUEUE_MAX_RETRIES', 5);
        $this->runnerName = self::getRunnerName();
        $this->runnerNameBase = self::getRunnerName();
        $this->queueNameToStop = "nsutil-queue-stop-{$this->runnerName}";

        // Set default timezone to UTC
        date_default_timezone_set('UTC');
        $this->queueStartProcessing = now()->timestamp();

        if (null === self::$runnerQueueFunction) {
            self::$runnerQueueFunction = function (int $id, bool $assync) {
                return nsCommand("queue:run {$id}", null, $assync);
            };
        }
    }

    // Unique executiion unique to grant only one access to database
    private function setUniqueExecutionInstance(): void
    {
        $this->ueName = "running-at-{$this->runnerName}";
        $this->ue = new UniqueExecution($this->ueName);
        KeyValueDatabaseOnPostgresql::set('runner-on-execution', $this->ue->getRef(), 'Started at ' . now()->format('american'));

        // Verificar se ja existe as duas filas. Se houver, validar se ja existe a full para evitar erro de sync
        $runners = KeyValueDatabaseOnPostgresql::list('runner-on-execution');
        if (count($runners) >= 2) {
            $parts = explode(':', $this->ueName);
            $parts[1] = 'full';
            $ueName = implode(':', $parts);
            UniqueExecution::create($ueName, 3600);
        }
    }

    private function connect()
    {
        if (null === $this->con) {
            switch (env('DBTYPE')) {
                case 'sqlite':
                    $dbPath = env('DBPATH', Helper::getPathApp() . '/storage/database');
                    if (!is_dir($dbPath)) {
                        Helper::mkdir($dbPath);
                    }
                    $dbPath .= '/' . env('DBNAME', 'database') . '.sqlite';
                    $this->con = new SQLite($dbPath);
                    break;
                default:
                    $this->con = ConnectionPostgreSQL::getConnectionByEnv();
                    break;
            }
        }
    }

    public function __destruct()
    {
        if (null !== $this->con) {
            $this->con->close();
        }
    }


    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));

        $args = [
            (int) ($args[0] ?? -1), // idQueue
            (string) ($args[1] ?? null), // queueType
            (bool) ($args[2] ?? false), // continueAfterEmptyQueues
        ];

        [$id, $queueType, $continueAfterEmptyQueues] = $args;

        if ($id > 0) {
            QueueHandler::resolve($id);
            $this->success("$me: queue $id resolved");
        } else {

            $this->connect();

            KeyValueDatabaseOnPostgresql::setConnection($this->con);

            switch ($queueType) {
                case 'onlyAssync':
                    $queueAssyncType = ['true'];
                    $this->runnerName .= ":{$queueType}";
                    break;
                case 'onlySync':
                    QueueRun::checkOnlyOneRunnerSync();
                    $this->reloadRuntimeLimit = false;
                    $this->runnerLimit = 1;
                    $queueAssyncType = ['false'];
                    $this->runnerName .= ":{$queueType}";
                    break;
                default:
                    $queueAssyncType = [];
                    $runners = self::getRunners();
                    $onlyAssync = array_filter($runners, fn($runner) => stripos($runner['key'], 'onlyAssync') !== false);
                    $onlySync = array_filter($runners, fn($runner) => stripos($runner['key'], 'onlySync') !== false);
                    $full = array_filter($runners, fn($runner) => stripos($runner['key'], 'full') !== false);

                    if (count($onlyAssync) === 0) {
                        $queueAssyncType[] = 'true';
                    }
                    if (count($onlySync) === 0) {
                        $queueAssyncType[] = 'false';
                    }
                    if (count($full) === 0 && count($onlyAssync) === 0 && count($onlySync) === 0) {
                        $queueAssyncType = ['true', 'false'];
                    }

                    if (empty($queueAssyncType)) {
                        throw new Exception("Runners in execution: " . ConsoleTable::arrayToTableConsole($runners));
                    }

                    $this->runnerName .= ":full";

                    // Se nao for definido o tipo de fila, basta avaliar os pendentes e desligar
                    $continueAfterEmptyQueues = false;

                    break;
            }

            $running = true;
            $this->setUniqueExecutionInstance();

            $queueType = Helper::compareString((string) $queueAssyncType[0], 'true')
                ? 'assync'
                : 'sync';

            do {

                $limit = $this->getLimit();

                // nome para permitir varios worker atuando nas filas assync. A fila sync deve ser unica.
                $this->uniqueExecutionCreate($limit);

                // processo de monitracao para stop programado sem quebrar a fila ja em andamento
                if (Helper::compareString(KeyValueDatabaseOnPostgresql::get($this->queueNameToStop, $this->runnerNameBase), 'true')) {
                    $this->success("$me - Stop requested. Queue stopped.");
                    // $this->uniqueExecutionEnd();
                    $running = false;
                    exit;
                }


                // manter uma referencia local para realizar as estatisticas de forma mais controlada
                try {
                    UniqueExecution::create('queue-update-statistics', 15);
                    $this->retryDeadQueues();
                    $this->retryLockedQueues();
                    $this->updateStatistics();
                } catch (Exception $ex) {
                    //ignore
                }

                // limit of running queues on this runner
                if ($limit <= 0) {
                    usleep(500000);
                    $this->uniqueExecutionEnd();
                    continue;
                }

                $this->con->begin_transaction();

                $queryObterIDS = "
                            SELECT id_queue, is_assync_queue
                            FROM _nsutil.queue 
                            WHERE 
                                started_at_queue IS NULL 
                                AND init_after_queue <= '" . now()->sub('2 seconds')->format('american') . "'
                                AND NOT (data_queue ~@ '__locked_at__')
                                AND created_at_queue <= '" . now()->sub('2 seconds')->format('american') . "'
                                AND is_assync_queue in (" . implode(',', $queueAssyncType) . ")
                            ORDER BY priority_queue DESC, id_queue ASC 
                            FOR UPDATE SKIP LOCKED 
                            OFFSET 0 LIMIT $limit";

                $list = $this->con->execQueryAndReturn($queryObterIDS);

                if (is_array($list) && count($list) > 0) {
                    $idsList = implode(',', array_column($list, 'idQueue'));

                    $this->con->execQueryAndReturn("
                        UPDATE 
                            _nsutil.queue 
                        SET 
                            running_on_queue= '" . $this->runnerName . "',  
                            data_queue = data_queue || '" . json_encode(['__locked_at__' => now()->format('american')]) . "'::jsonb
                        WHERE 
                            id_queue IN ($idsList)
                    ");
                    $this->con->commit();

                    // assync queues
                    $sleeping = 0;
                    foreach (array_filter($list, fn($toFilter) => Helper::getValByType($toFilter['isAssyncQueue'], 'boolean') === true) as $item) {
                        // controle para somente usar assync quando nao for chamado de outro nohup
                        // nsCommand('queue:run ' . $item['idQueue'], null, true);
                        // $this->setRunnerQueueFunction(function (int $id, bool $assync) {
                        call_user_func(self::$runnerQueueFunction, $item['idQueue'], true);
                        $sleeping = 200000;
                    }

                    // sync queues
                    foreach (array_filter($list, fn($toFilter) => Helper::getValByType($toFilter['isAssyncQueue'], 'boolean') === false) as $item) {
                        // nsCommand('queue:run ' . $item['idQueue'], null, false);
                        call_user_func(self::$runnerQueueFunction, $item['idQueue'], false);
                    }
                    if ($sleeping > 0) {
                        usleep($sleeping);
                    }
                } else {
                    $this->con->rollback();
                    $running = $continueAfterEmptyQueues;
                    if ($running && !Helper::compareString(KeyValueDatabaseOnPostgresql::get($this->queueNameToStop, $this->runnerNameBase), 'true')) {
                        // Esperar 30 segundos antes de ir consultar novamente as filas existentes.
                        // Caso alguma entre, o farquivo setado ira informar. 
                        for ($i = 0; $i < 6; $i++) {
                            sleep(5);
                            $queueType = Helper::compareString((string) $queueAssyncType[0], 'true')
                                ? 'assync'
                                : 'sync';
                            if (QueueHandler::hasNewItens($queueType) === true) {
                                sleep(2);
                                $i = 1000;
                                QueueHandler::removeNewItens($queueType);
                            }
                        }
                    }
                }


                $this->uniqueExecutionEnd();

                // running 
            } while ($running);

            // Fechar conexao com o banco de dados
            $this->con->close();

            // Remover runner da lista de executores
            KeyValueDatabaseOnPostgresql::delete('runner-on-execution', $this->ueName);

            // Mensagem de sucesso
            $this->success($me);
        }
    }

    public static function setRunnerQueueFunction(Closure $function): void
    {
        self::$runnerQueueFunction = $function;
    }

    private function updateStatistics(): void
    {

        $now = now();
        // obter atual estatistica
        $statistics = $this->con->execQueryAndReturn("SELECT * FROM _nsutil.queue_statistics")[0] ?? null;
        if (null === $statistics) {
            $n = now()->format('american');
            $this->con->executeQuery("INSERT INTO _nsutil.queue_statistics (updated_at) VALUES ('$n')");
            $statistics = $this->con->execQueryAndReturn("SELECT * FROM _nsutil.queue_statistics")[0] ?? [];
        }
        $statistics['pendingItems'] ??= 0;
        $statistics['itemsPerSecond'] ??= 0;
        $statistics['estimatedCompletion'] ??= null;
        $statistics['updatedAt'] ??= now()->sub('10 seconds')->format('american');

        // contar quantos itens estao pendentes na fila
        $queryCounter = "
            SELECT count(*) as qtde
            FROM _nsutil.queue 
            WHERE 
                started_at_queue IS NULL 
                AND init_after_queue <= '" . now()->sub('2 seconds')->format('american') . "'
                AND NOT (data_queue ~@ '__locked_at__')
                AND created_at_queue <= '" . now()->sub('2 seconds')->format('american') . "'
        ";
        $pendingItems = (int) ($this->con->execQueryAndReturn($queryCounter)[0]['qtde'] ?? 0);

        // Nada pendente, zerar e sair
        if ($pendingItems === 0) {
            if (null !== $this->queueStartProcessing && $statistics['pendingItems'] >= 0) {
                $n = now()->format('american');
                $this->con->executeQuery("TRUNCATE TABLE _nsutil.queue_statistics");
                $this->con->executeQuery("INSERT INTO _nsutil.queue_statistics (updated_at) VALUES ('$n')");
            }
            $this->queueStartProcessing = null;
            $this->queueInitProcessing = 0;
            return;
        }

        if ($this->queueStartProcessing === null || $pendingItems > $this->queueInitProcessing) {
            $this->queueInitProcessing = $pendingItems;
            $this->queueStartProcessing = now()->timestamp();
            return;
        }

        // Esta baixando, calcular taxa de processamento
        $timeElapsed = max(1, now()->timestamp() - $this->queueStartProcessing);
        $divisor = $this->queueInitProcessing - $pendingItems;
        if ($divisor <= 0) {
            $rate = -1;
        } else {
            $rate = (float) ($timeElapsed / $divisor);
        }
        $perSecond = (float) round(1 / $rate, 2);
        $eta = round($rate * $pendingItems, 2);
        $updatedAt = $now->format('american');
        $datetime = (new Date($updatedAt))->add((int) $eta . " seconds")->format('american');

        // Aumentou a quantidade a fazer ou eh igual. Aguardar proximo recalculo
        if ($divisor < 0 || $pendingItems > $statistics['pendingItems']) {
            $rate = -1;
            $datetime = null;
        }

        // atualizar estatistica
        $this->con->executeQuery("UPDATE _nsutil.queue_statistics SET 
            updated_at = '$updatedAt', 
            pending_items = $pendingItems, 
            items_per_second = " . $perSecond . "::numeric(12,2), 
            estimated_completion_at = " . ($datetime ? "'$datetime'" : 'NULL') . "
        ");
    }

    private function uniqueExecutionCreate(int $limit): void
    {
        $this->ue->start(max(60, $this->runnerMaxSeconds * ($limit + 1)), true);
        // KeyValueDatabaseOnPostgresql::set('runner-on-execution', $this->ue->getRef(), 'Started at ' . now()->format('american'));
    }

    private function uniqueExecutionEnd(bool $force = false): void
    {
        // parar trava atual
        if ($this->ue instanceof UniqueExecution) {
            $this->ue->end();
            // KeyValueDatabaseOnPostgresql::delete('runner-on-execution', $this->ue->getRef());
        }
        // parar trava de outros processos se for forced
        if ($force) {
            $names = [
                'fromAddhandlerAdd',
                'onlyAssync',
                'onlySync',
                'full'
            ];
            $runnerNameSemQueue = explode(':', $this->runnerName);
            $runnerNameSemQueue = $runnerNameSemQueue[0];
            foreach ($names as $queueName) {
                $ueName = "running-at-{$runnerNameSemQueue}:{$queueName}";
                UniqueExecution::forcedEnd($ueName);
                KeyValueDatabaseOnPostgresql::delete('runner-on-execution', $ueName);
            }
        }
    }

    private function getLimit(): int
    {
        $running = $this->con->execQueryAndReturn("
            SELECT
                count(*) as running
            FROM
                _nsutil.queue
            WHERE
                finished_at_queue IS NULL
                AND running_on_queue= '{$this->runnerName}'
        ");


        if ($this->reloadRuntimeLimit && now()->timestamp() > $this->nextReloadEnv) {
            EnvFile::applyEnvVariables();
            $this->runnerLimit = (int) env('QUEUE_LIMIT_PER_WORKER', Cpu::count());
            $this->nextReloadEnv = now()->add('30 seconds')->timestamp();
        }

        $limit = $this->runnerLimit - $running[0]['running'];

        return max(0, $limit);
    }

    /**
     * Conta quantas filas estao sendo executadas no momento neste runner
     * @return int
     */
    public function countRunning(): int
    {
        $this->connect();

        $query = "
            SELECT count(*) as qtde
            FROM _nsutil.queue 
            WHERE 
                finished_at_queue is null 
                AND running_on_queue ~* '{$this->runnerName}'
        ";
        $ret = $this->con->execQueryAndReturn($query);

        return $ret[0]['qtde'] ?? 0;
    }

    /**
     * Retry queues that are locked and started but not finished within expected time.
     * Dead queues are those that started execution but did not finish within the maximum time limit.
     * @return void
     */
    private function retryDeadQueues(): void
    {
        $query = "
            SELECT * 
            FROM _nsutil.queue q 
            WHERE 
                running_on_queue IS NOT NULL
                AND is_assync_queue = 'true'
                AND started_at_queue IS NOT NULL 
                AND finished_at_queue IS NULL
                AND NOT data_queue ~@ '__error__'
                AND (data_queue->>'__locked_at__')::timestamp <= ?
        ";

        // Retry assync queues
        $limit = now()->sub("{$this->runnerMaxSeconds} seconds")->format('american');
        $list = $this->con->execQueryAndReturnPrepared($query, [$limit]);
        $this->setToRetry($list, 'Dead queue');

        // Invalidates sync queue
        $query = str_replace("is_assync_queue = 'true'", "is_assync_queue = 'false'", $query);
        $list = $this->con->execQueryAndReturnPrepared($query, [$limit]);
        array_map(
            fn($queue) => QueueHandler::handleException($queue['idQueue'], new Exception('Queue started but not completed within expected time. Considered locked.')),
            $list
        );
    }


    /**
     * Retry queues that are locked (__locked_at__) and not started.
     * @param int $seconds
     * @return void
     */
    private function retryLockedQueues(int $seconds = 60): void
    {
        $query = "
            SELECT * 
            FROM _nsutil.queue q 
            WHERE 
                is_assync_queue = 'true'
                AND started_at_queue IS NULL 
                AND finished_at_queue IS NULL
                AND (data_queue->>'__locked_at__')::timestamp <= ?
        ";

        // Retry assync queues
        // $limit = now()->sub("{$seconds} seconds")->format('american');
        $limit = now()->sub("{$this->runnerMaxSeconds} seconds")->format('american');
        $list = $this->con->execQueryAndReturnPrepared($query, [$limit]);
        $this->setToRetry($list, 'Locked queue');

        // Invalidates sync queue
        $query = str_replace("is_assync_queue = 'true'", "is_assync_queue = 'false'", $query);
        $list = $this->con->execQueryAndReturnPrepared($query, [$limit]);
        foreach ($list as $queue) {
            QueueHandler::handleException($queue['idQueue'], new Exception('Queue started but not completed within expected time. Considered locked.'));
        }
    }

    private function setToRetry(array $list, string $errorMessage): void
    {
        foreach ($list as $queue) {
            QueueHandler::setQueueToRetrie($queue['idQueue'], $errorMessage, new Date($queue['initAfterQueue']));
        }
    }

    public function stopRunner(bool $force = false): void
    {
        $this->connect();

        KeyValueDatabaseOnPostgresql::set($this->queueNameToStop, $this->runnerNameBase, 'true');

        $secondsWaiting = 0;

        echo "\n";

        try {
            if (!$force) {
                do {
                    $pending = $this->countRunning();
                    if ($pending > 0) {
                        $label = "Waiting running queues ($pending). Waiting {$secondsWaiting}s of " . ($this->runnerMaxSeconds * $pending) . " max seconds";
                        echo "\r\033[K";
                        printf("%-40s%-20s", $label, '🏃‍♂️ Running ...');
                        sleep(1);
                        $secondsWaiting++;
                        if ($secondsWaiting > ($this->runnerMaxSeconds * $pending)) {
                            throw new Exception("Stop runner timeout. All queues proccess has forced to stop. Check Queues.");
                        }
                    }
                } while ($pending > 0);
            }
            $runner = "Stop Runner" . ($force ? ' (forced)' : '');
            echo "\r\033[K";
            printf("%-40s%-20s", $runner, '✔ Done');
            echo "\n";
            KeyValueDatabaseOnPostgresql::delete($this->queueNameToStop, $this->runnerNameBase);
        } catch (Exception $ex) {
            throw $ex;
        } finally {
            $force = true;
            $this->uniqueExecutionEnd($force);
            KeyValueDatabaseOnPostgresql::delete($this->queueNameToStop, $this->runnerNameBase);
        }
    }


    public static function getRunnerName(): string
    {
        return Helper::sanitize(trim(shell_exec('hostname')));
    }

    public static function checkOnlyOneRunnerSync(): void
    {

        $runners = KeyValueDatabaseOnPostgresql::list('runner-on-execution');
        $candidates = array_filter(
            $runners,
            fn($runner) => stripos($runner['key'], ':full') !== false || stripos($runner['key'], ':onlySync') !== false
        );
        if (count($candidates) >= 1) {
            $message = "\nOnly one runner for sync queues is allowed.\n"
                . ConsoleTable::arrayToTableConsole($runners);
            throw new Exception($message);
        }
    }

    public static function getRunners(?string $type = null): array
    {
        $runners = KeyValueDatabaseOnPostgresql::list('runner-on-execution');
        $candidates = $runners;
        if (null !== $type) {
            $candidates = array_filter(
                $runners,
                fn($runner) => stripos($runner['key'], ":$type") !== false
            );
        }
        return $candidates;
    }
}
