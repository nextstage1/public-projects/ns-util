<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\Commands\Abstracts\Command;
use NsUtil\Services\Redis;

class CacheClear extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "cache:clear";

    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));
        Redis::clearAll();
        $this->success($me);
    }
}
