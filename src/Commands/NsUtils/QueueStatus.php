<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\Commands\Abstracts\Command;
use NsUtil\Connection\KeyValueDatabaseOnPostgresql;
use NsUtil\ConnectionPostgreSQL;
use NsUtil\ConsoleTable;
use NsUtil\Queue\QueueHandler;

use function NsUtil\now;

class QueueStatus extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "queue:status";

    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));

        $con = ConnectionPostgreSQL::getConnectionByEnv();

        QueueHandler::migrate();

        // Guarda a posição inicial do cursor
        echo "\033[s";

        do {
            // Retorna o cursor para a posição salva e limpa tudo abaixo
            $out =  "\033[u\033[J";

            $out .= "\n\n";

            // Queues Failed
            $result = $con->execQueryAndReturn("SELECT * FROM _nsutil.queues_failed");
            $out .= ConsoleTable::arrayToTableConsole($result, '💥 Failed queues', 'red', '💥 No failed queues', 'green');
            $out .= "\n";

            // Queues Pending
            $pending = $con->execQueryAndReturn("SELECT handler, priority, assync, count(*) as counter FROM _nsutil.queues_pending WHERE init_after <= '" . now()->format('american') . "' GROUP BY 1,2,3");
            $out .= ConsoleTable::arrayToTableConsole($pending, '⌛ Pending queues', 'yellow', '⌛ No pending queues', 'green');
            $out .= "\n";

            // Runners
            $runners = KeyValueDatabaseOnPostgresql::list('runner-on-execution');
            $out .= ConsoleTable::arrayToTableConsole($runners, '🤖 Runners', 'green', '🤖 No runners in execution', 'red');
            $out .= "\n";

            // Running queues
            if (count($runners) > 0) {
                $result = $con->execQueryAndReturn("SELECT * FROM _nsutil.queues_running");
                $out .= ConsoleTable::arrayToTableConsole($result, '▶️ Queues running', 'green', '▶️ No queues running', 'info');
                $out .= "\n";
            }

            echo $out;

            sleep(2);
        } while (true);

        // $this->success($me);
    }
}
