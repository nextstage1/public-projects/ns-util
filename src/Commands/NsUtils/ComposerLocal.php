<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\Commands\Abstracts\Command;
use NsUtil\Config;

use function NsUtil\dd;
use function NsUtil\env;

class ComposerLocal extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "composer:local";

    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));

        $config = (new Config())->loadEnvFile(__DIR__ . '/../../../.env');


        $LOCAL_PROJECTS = explode(';', $config->get('LOCAL_COMPOSER', ''));

        if (count($LOCAL_PROJECTS) > 0) {
            $src = realpath(__DIR__ . '/../../../src');
            (new \NsUtil\LocalComposer())($src, $LOCAL_PROJECTS);
        }

        $this->success($me);
    }
}
