<?php

namespace NsUtil\Commands\NsUtils;

use Exception;
use NsUtil\Commands\Abstracts\Command;
use NsUtil\Connection\KeyValueDatabaseOnPostgresql;
use NsUtil\ConsoleTable;
use NsUtil\Helper;
use NsUtil\Helpers\BashUtils;
use PhpParser\Node\Name\FullyQualified;
use function NsUtil\dd;
use function NsUtil\nsCommand;

class QueueWorker extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "queue:worker";

    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));

        // Vou checar se existe runner registrado porem sem operacao
        BashUtils::checkIfProcessIsRunning(
            'queue:run',
            // Se existir, nao faz nada
            null,
            // Se nao existir, mata o runner e sobe um novo. Assim garantir que nao existem runners zumbi registrados.
            function () {
                $this->info('Runner registred but not in operation. Killed force...');
                echo nsCommand('queue:stop force');
            }
        );

        // chamadas daqui sempre tem o tercero parametro true para manter o worker ativo apos concluir as filas
        $args[0] ??= null;

        // Possiveis entradas: onlyAssync, onlySync ou full
        switch ($args[0]) {
            case 'onlyAssync':
                $args[0] = 'onlyAssync';
                break;
            case 'onlySync':
                QueueRun::checkOnlyOneRunnerSync();
                $args[0] = 'onlySync';
                break;
            default:
                $args[0] = null;
                break;
        }

        if (null !== $args[0]) {
            nsCommand('queue:run -1 ' . trim($args[0]) . ' true', null, true);
            $me .= " - " . trim($args[0]);
        } else {

            // Gerenciar construcao da filas sincrona. Deve existir apenas um runner para a database
            try {
                QueueRun::checkOnlyOneRunnerSync();
                nsCommand('queue:run -1 onlySync true', null, true);
                $me .= " - onlySync";
            } catch (Exception $e) {
            }

            // sempre subir um assync extra. A gestao do UniqueExecution nao deixara subir dois na mesma VM.
            nsCommand('queue:run -1 onlyAssync true', null, true);
            $me .= " - onlyAssync";
        }
        $me .= PHP_EOL
            . ConsoleTable::arrayToTableConsole(KeyValueDatabaseOnPostgresql::list('runner-on-execution'));

        $this->success($me);
    }
}
