<?php

namespace NsUtil\Commands\NsUtils;

use Exception;
use NsUtil\Helper;
use NsUtil\Template;
use NsUtil\ConsoleTable;
use NsUtil\Commands\Abstracts\Command;
use NsUtil\Commands\Utils\Util;

use function NsUtil\env;

class MakeDto extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'make:dto';

    /**
     * Handles the execution of the command.
     *
     * @param mixed $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        // Extrair dados da entrada
        $np = Util::prepareNamespace('DTOs', 'NSUTIL_DTO_NAMESPACE', $args[1] ?? null);
        [$className, $filename, $namespace] = Util::prepareNameToCreate($args[0], 'DTO', $np);

        $ret = Util::writeOnTemplate(__DIR__ . '/../Templates/dto.php', $filename, [
            'namespace' => $namespace,
            'classname' => $className
        ]);

        if ($ret) {
            $this->success("File $filename was created!");
        } else {
            $this->error("File $filename was not created");
        }
    }
}
