<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\Commands\Abstracts\Command;
use NsUtil\Services\Redis;

class CacheList extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "cache:list";

    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));
        echo PHP_EOL;
        foreach (Redis::list() as $item) {
            echo PHP_EOL;
            echo $item;
        }
        echo PHP_EOL;
        $this->success($me);
    }
}
