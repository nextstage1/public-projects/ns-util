<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\Commands\Abstracts\Command;
use NsUtil\Connection\KeyValueDatabaseOnPostgresql;

class QueueStop extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "queue:stop";

    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));

        // stop graceful all runners
        (new QueueRun())->stopRunner($args[0] ?? '' === 'force');

        // kill all queue:run
        exec('pkill -f "queue:run"');

        $this->success($me);
    }
}
