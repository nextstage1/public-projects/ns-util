<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\Helper;
use NsUtil\Template;
use NsUtil\ConsoleTable;
use NsUtil\Commands\Abstracts\Command;
use NsUtil\Commands\Utils\Util;

use function NsUtil\env;
use function NsUtil\nsCommand;

class MakeMiddleware extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'make:middleware';

    /**
     * Handles the execution of the command.
     *
     * @param mixed $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {

        // Extrair dados da entrada
        $np = Util::prepareNamespace('Middlewares', 'NSUTIL_MIDDLEWARES_NAMESPACE', $args[1] ?? null);
        [$className, $filename, $namespace] = Util::prepareNameToCreate($args[0], 'Middleware', $np);

        $ret = Util::writeOnTemplate(__DIR__ . '/../Templates/middleware.php', $filename, [
            'namespace' => $namespace,
            'classname' => $className
        ]);

        if ($ret) {
            $this->success("File $filename was created!");
        } else {
            $this->error("File $filename was not created");
        }


        // if (null === $args[0]) {
        //     throw new \Exception('Name of new router was not informed. Use: php nsutil make:router RouterName');
        // }


        // // create dir
        // $namespaceFromArg = env('NSUTIL_MIDDLEWARES_NAMESPACE') ?? $args[1] ?? 'Middlewares';
        // $pathToCommands = Helper::getPathApp() . '/src/' . $namespaceFromArg;
        // Helper::mkdir($pathToCommands);

        // $className = ucwords(Helper::name2CamelCase($args[0]));
        // $className = str_replace(['Middleware', 'middleware'], '', $className);
        // $commandName = str_ireplace(['_middleware', 'middleware'], '', Helper::sanitize(Helper::reverteName2CamelCase($className)));
        // $namespace = Helper::getPsr4Name() . '\\' . str_replace('/', '\\', $namespaceFromArg);
        // $signature = str_ireplace(['_', 'command'], ['-', ''], $commandName);
        // $filename = $pathToCommands . "/{$className}Middleware.php";

        // // arquivos padrão para determinadas rotas 
        // $libraryTemplateFile = __DIR__ . '/../Templates/middleware.php';
        // $template = include $libraryTemplateFile;

        // $content = (new Template($template, [
        //     'namespace' => $namespace,
        //     'classname' => $className,
        //     'signature' => $signature,
        // ]))->render();


        // if (file_exists($filename)) {
        //     throw new \Exception("Error: file $filename was exists");
        // }

        // if (!Helper::saveFile($filename, false, $content)) {
        //     throw new \Exception("It is not possible to create the file $filename");
        // }

        // chmod($filename, 0777);

        // $this->success("Middleware $filename was created!");
    }
}
