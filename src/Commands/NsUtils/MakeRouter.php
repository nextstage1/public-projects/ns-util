<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\EnvFile;
use NsUtil\Helper;
use NsUtil\Template;
use NsUtil\ConsoleTable;
use NsUtil\Commands\Abstracts\Command;
use NsUtil\Commands\Utils\Util;

use function NsUtil\env;
use function NsUtil\nsCommand;

class MakeRouter extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'make:router';

    /**
     * Handles the execution of the command.
     *
     * @param mixed $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        // Reload env from file if exists
        EnvFile::applyEnvVariables();

        // Extrair dados da entrada
        $np = Util::prepareNamespace('Controllers/Api', 'NSUTIL_ROUTERAPI_NAMESPACE', $args[1] ?? null);
        [$className, $filename, $namespace] = Util::prepareNameToCreate($args[0], 'Router', $np);

        // Template
        $libraryTemplateFile = file_exists(__DIR__ . "/../Templates/library/{$className}Router.php")
            ? __DIR__ . "/../Templates/library/{$className}Router.php"
            : __DIR__ . '/../Templates/router.php';
        $template = include $libraryTemplateFile;

        // Ignored by config
        $ignoreByconfig = array_map('trim', explode(',', env('BUILD_IGNORE_ROUTERS', '__notdefined__')));
        if (array_search($className, $ignoreByconfig) !== false) {
            $msg = "Router '$className' was ignored by configurations (check BUILD_IGNORE_ROUTERS in .env)";
            throw new \Exception($msg);
        }


        $ret = Util::writeOnTemplate($template, $filename, [
            'namespace' => $namespace,
            'classname' => $className,
            'NSUTIL_MODELS_NAMESPACE' => Helper::getPsr4Name() . '\\' . str_replace('/', '\\', env('NSUTIL_MODELS_NAMESPACE')),
            'NSUTIL_RESOURCES_NAMESPACE' => Helper::getPsr4Name() . '\\' . str_replace('/', '\\', env('NSUTIL_RESOURCES_NAMESPACE')),
        ]);

        if ($ret) {
            $this->success("File $filename was created!");
            // Dependencias da rota 
            echo nsCommand('make:resource "' . $args[0] . '" "' . $args[1] . '"');
        } else {
            $this->error("File $filename was not created");
        }






        // if (null === $args[0]) {
        //     throw new \Exception('Name of new router was not informed. Use: php nsutil make:router RouterName');
        // }


        // // create dir
        // $namespaceFromArg = env('NSUTIL_ROUTERAPI_NAMESPACE') ?? $args[1] ?? 'Controllers/Api';
        // $pathToCommands = Helper::getPathApp() . '/src/' . $namespaceFromArg;
        // Helper::mkdir($pathToCommands);

        // $className = ucwords(Helper::name2CamelCase($args[0]));
        // $className = str_replace(['Controller', 'controller'], '', $className);
        // $commandName = str_ireplace(['_router', 'router'], '', Helper::sanitize(Helper::reverteName2CamelCase($className)));
        // $namespace = Helper::getPsr4Name() . '\\' . str_replace('/', '\\', $namespaceFromArg);
        // $signature = str_ireplace(['_', 'command'], [':', ''], $commandName);
        // $filename = $pathToCommands . "/{$className}Router.php";

        // $ignoreByconfig = array_map('trim', explode(',', env('BUILD_IGNORE_ROUTERS', '__notdefined__')));
        // if (array_search($className, $ignoreByconfig) !== false) {
        //     $msg = "Router '$className' was ignored by configurations (check BUILD_IGNORE_ROUTERS in .env)";
        //     throw new \Exception($msg);
        // }


        // // arquivos padrão para determinadas rotas 
        // $libraryTemplateFile = file_exists(__DIR__ . "/../Templates/library/{$className}Router.php")
        //     ? __DIR__ . "/../Templates/library/{$className}Router.php"
        //     : __DIR__ . '/../Templates/router.php';
        // $template = include $libraryTemplateFile;

        // $content = (new Template($template, [
        //     'namespace' => $namespace,
        //     'classname' => $className,
        //     'signature' => $signature,
        //     'NSUTIL_MODELS_NAMESPACE' => Helper::getPsr4Name() . '\\' . str_replace('/', '\\', env('NSUTIL_MODELS_NAMESPACE')),
        //     'NSUTIL_RESOURCES_NAMESPACE' => Helper::getPsr4Name() . '\\' . str_replace('/', '\\', env('NSUTIL_RESOURCES_NAMESPACE')),
        // ]))->render();


        // if (file_exists($filename)) {
        //     throw new \Exception("Error: file $filename was exists");
        // }

        // if (!Helper::saveFile($filename, false, $content)) {
        //     throw new \Exception('It is not possible to create the file ' . $filename);
        // }

        // // Dependencias da rota 
        // nsCommand('make:resource ' . $args[0]);

        // chmod($filename, 0777);

        // $this->success("File $filename was created!");
    }
}
