<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\Commands\Abstracts\Command;
use NsUtil\Connection\SQLite;
use NsUtil\ConnectionPostgreSQL;
use NsUtil\Helper;
use NsUtil\Queue\QueueHandler;
use NsUtil\UniqueExecution;

use function NsUtil\env;
use function NsUtil\json_decode;
use function NsUtil\now;
use function NsUtil\nsCommand;

/**
 * Class QueueReprocess
 * Command to reprocess failed queues by moving them back to the main queue table
 * 
 * This command allows reprocessing of one or multiple queues by their IDs
 * The IDs can be passed as comma or semicolon separated values
 */
class QueueReprocess extends Command
{
    private $con;
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "queue:reprocess";

    public function __construct() {}

    /**
     * Handle the queue reprocessing command
     * 
     * @param array $args Command arguments where $args[0] contains queue IDs
     * @throws \Exception When no queue ID is provided
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));

        if (strlen($args[0]) == 0) {
            throw new \Exception('Set id_queue to reprocess as first argument');
        }

        $errors = [];

        $con = ConnectionPostgreSQL::getConnectionByEnv();

        switch (true) {
            case stripos($args[0], ',') !== false:
                $queues = array_map('trim', explode(',', $args[0]));
                break;
            case stripos($args[0], ';') !== false:
                $queues = array_map('trim', explode(';', $args[0]));
                break;
            default:
                $queues = [$args[0]];
        }

        foreach ($queues as $idQueue) {

            $queue = $con->execQueryAndReturnPrepared("SELECT * FROM _nsutil.queue_fails WHERE id_queue = ?", [$idQueue])[0]
                ?? $con->execQueryAndReturnPrepared("SELECT * FROM _nsutil.queue WHERE id_queue = ?", [$idQueue])[0]
                ?? null;

            if (!isset($queue['idQueue'])) {
                $errors[] = "Queue $idQueue not found";
                continue;
            }

            $con->begin_transaction();
            $data['queue'] = json_decode($queue['dataQueue'], true);
            unset($data['queue']['__locked_at__']);
            unset($data['queue']['__queueExecution']);
            $query = "INSERT INTO _nsutil.queue (id_queue, created_at_queue, init_after_queue, handler_queue, remove_queue, priority_queue, is_assync_queue, data_queue) 
            VALUES (?, ?, ?, ?, ?, ?, ?, ?) 
            ON CONFLICT (id_queue) 
            DO UPDATE SET 
                created_at_queue = EXCLUDED.created_at_queue,
                started_at_queue = null,
                running_on_queue = null,
                init_after_queue = EXCLUDED.init_after_queue,
                handler_queue = EXCLUDED.handler_queue,
                remove_queue = EXCLUDED.remove_queue,
                priority_queue = EXCLUDED.priority_queue,
                is_assync_queue = EXCLUDED.is_assync_queue,
                data_queue = EXCLUDED.data_queue";

            $con->execQueryAndReturnPrepared($query, [
                $queue['idQueue'],
                $queue['createdAtQueue'],
                $queue['initAfterQueue'],
                $queue['handlerQueue'],
                (string) Helper::compareString($queue['removeQueue'], 'true') ? 'true' : 'false',
                $queue['priorityQueue'],
                (string) Helper::compareString($queue['isAssyncQueue'], 'true') ? 'true' : 'false',
                json_encode($data['queue'])
            ]);
            $con->executeQuery("DELETE FROM _nsutil.queue_fails WHERE id_queue= $idQueue");
            $con->commit();
        }

        unset($con);

        if (empty($errors)) {
            $this->success("$me - Successfully added for reprocessing");
        } else {
            $this->error("Errors: \n" . implode("\n", $errors));
        }
    }
}
