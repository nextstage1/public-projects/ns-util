<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\Commands\Abstracts\Command;
use NsUtil\Config;
use NsUtil\ConnectionPostgreSQL;
use NsUtil\Helper;

use function NsUtil\dd;
use function NsUtil\env;
use function NsUtil\nsCommand;

class QueueInstall extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "queue:install";

    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));

        $content = $this->getQueryToCreateTables();

        // criar migration 
        $output = nsCommand('make:migration create-queue-nsutil');
        if (preg_match('/File (.+) was created/', $output, $matches)) {
            $filePath = $matches[1];
            $fileMigration = trim($filePath);

            Helper::saveFile($fileMigration, false, $content, 'SOBREPOR');
            nsCommand('migrate');
        }

        // add crotab 
        nsCommand('crontab:add "* * * * * cd /var/www/html && php nsutil queue:worker"');


        $this->success($me);
    }

    public function getQueryToCreateTables(): string
    {

        return <<<EOF
create schema if not exists _nsutil;

drop table if exists _nsutil.queue cascade;

CREATE TABLE
    _nsutil.queue (
        id_queue bigserial NOT NULL,
        created_at_queue timestamp not null default now (),
        init_after_queue timestamp,
        started_at_queue timestamp,
        finished_at_queue timestamp,
        running_on_queue text,
        handler_queue text NOT NULL,
        remove_queue boolean not null default 'true',
        priority_queue smallint not null default 1,
        is_assync_queue boolean not null default 'false',
        data_queue jsonb NULL,
        CONSTRAINT queue_pk PRIMARY KEY (id_queue)
    );

drop table if exists _nsutil.queue_fails cascade;

CREATE TABLE
    _nsutil.queue_fails (
        id_queue int8 NOT NULL,
        created_at_queue timestamp not null default now (),
        init_after_queue timestamp,
        started_at_queue timestamp,
        finished_at_queue timestamp,
        running_on_queue text,
        handler_queue text NOT NULL,
        remove_queue boolean not null default 'true',
        priority_queue smallint not null default 1,
        is_assync_queue boolean not null default 'false',
        data_queue jsonb NULL,
        error_message text,
        CONSTRAINT queue_fails_pk PRIMARY KEY (id_queue)
    );

drop table if exists _nsutil.queue_success cascade;

CREATE TABLE
    _nsutil.queue_success (
        id_queue int8 NOT NULL,
        created_at_queue timestamp not null default now (),
        init_after_queue timestamp,
        started_at_queue timestamp,
        finished_at_queue timestamp,
        running_on_queue text,
        handler_queue text NOT NULL,
        remove_queue boolean not null default 'true',
        priority_queue smallint not null default 1,
        is_assync_queue boolean not null default 'false',
        data_queue jsonb NULL,
        CONSTRAINT queue_success_pk PRIMARY KEY (id_queue)
    );
EOF;
    }
}
