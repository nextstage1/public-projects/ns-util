<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\Commands\Abstracts\Command;
use NsUtil\Commands\Utils\Util;
use NsUtil\Helper;
use NsUtil\Template;

class MakeQueueHandler extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "make:queue-handler";

    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));

        // Extrair dados da entrada
        $np = Util::prepareNamespace('QueueHandlers', 'NSUTIL_QUEUE_HANDLERS_NAMESPACE', $args[1] ?? null);
        [$className, $filename, $namespace] = Util::prepareNameToCreate($args[0], 'QueueHandler', $np);

        $ret = Util::writeOnTemplate(__DIR__ . '/../Templates/queueHandler.php', $filename, [
            'namespace' => $namespace,
            'classname' => $className,
        ]);

        if ($ret) {
            $this->success("File $filename was created!");
        } else {
            $this->error("File $filename was not created");
        }
    }
}
