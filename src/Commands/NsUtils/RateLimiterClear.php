<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\Commands\Abstracts\Command;
use NsUtil\ConsoleTable;
use NsUtil\Services\RateLimiter;

use function NsUtil\dd;

class RateLimiterClear extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'rate:clear';

    /**
     * Handles the execution of the command.
     *
     * @param mixed $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {

        $appkey = $args[0] ?? null;

        $me = basename(str_replace('\\', "/", self::class));

        if (null !== $appkey) {
            RateLimiter::clear($appkey);
            $this->success($me);
        } else {
            $this->error('Required param 1: appkey or IP to clear');
        }
    }
}
