<?php

namespace NsUtil\Commands\NsUtils;

use Exception;
use NsUtil\Commands\Abstracts\Command;
use NsUtil\Helper;

use function NsUtil\now;

class CrontabAdd extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "crontab:add";

    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $className = basename(str_replace('\\', "/", self::class));
        if (!isset($args[0])) {
            throw new Exception('Missing first parameter: crontab expression');
        }
        $expression = $args[0];

        $content = '';

        $file = Helper::getPathApp() . '/cron/crontab';
        if (file_exists($file)) {
            $content = file_get_contents($file);
        }

        $content = trim($content) .  "\n# added at " . now()->format('american') . "\n$expression\n\n";

        Helper::saveFile($file, false, $content, 'SOBREPOR');

        $this->success($className);
    }
}
