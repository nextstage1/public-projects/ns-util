<?php

namespace NsUtil\Commands\NsUtils;

use Exception;
use NsUtil\Exceptions\FileAlreadyExistsException;
use NsUtil\Helper;
use NsUtil\Template;
use NsUtil\ConsoleTable;
use NsUtil\Commands\Abstracts\Command;
use NsUtil\Commands\Utils\Util;

use function NsUtil\env;

class MakeResource extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'make:resource';

    /**
     * Handles the execution of the command.
     *
     * @param mixed $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {

        // Extrair dados da entrada
        $np = Util::prepareNamespace('Resources', 'NSUTIL_RESOURCES_NAMESPACE', $args[1] ?? null);
        [$className, $filename, $namespace] = Util::prepareNameToCreate($args[0], 'Resource', $np);

        $ret = Util::writeOnTemplate(__DIR__ . '/../Templates/resource.php', $filename, [
            'namespace' => $namespace,
            'classname' => $className,
        ]);

        if ($ret) {
            $this->success("File $filename was created!");
        } else {
            $this->error("File $filename was not created");
        }
    }
}
