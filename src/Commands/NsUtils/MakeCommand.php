<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\ConsoleTable;
use NsUtil\Helper;
use NsUtil\Template;
use NsUtil\Commands\Abstracts\Command;
use NsUtil\Commands\Utils\Util;

class MakeCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'make:command';

    /**
     * Handles the execution of the command.
     *
     * @param mixed $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        // Extrair dados da entrada
        [$className, $filename, $namespace] = Util::prepareNameToCreate($args[0], 'Command', 'Console/Commands');

        $ret = Util::writeOnTemplate(__DIR__ . '/../Templates/command.php', $filename, [
            'namespace' => $namespace,
            'classname' => $className,
            'signature' => str_ireplace(['_', 'command'], ['-', ''], Helper::sanitize(Helper::reverteName2CamelCase($className)))
        ]);

        if ($ret) {
            $this->success("File $filename was created!");
        } else {
            $this->error("File $filename was not created");
        }
    }
}
