<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\Config;
use NsUtil\Helper;
use NsUtil\ConsoleTable;
use NsUtil\ConnectionPostgreSQL;
use NsUtil\Databases\Migrations;
use NsUtil\Commands\Abstracts\Command;
use NsUtil\Helpers\Prompt;

use function NsUtil\env;

class MigrateFresh extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'migrate:fresh';

    /**
     * Handles the execution of the command.
     *
     * @param mixed $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        echo "\n";
        $force = $args[0] === 'force';
        if ($force || Prompt::handle('All your data in database ' . env('DBHOST') . ' will be removed. Confirm? ', 'yes')) {

            Helper::deleteDir('/tmp/nsutil-migrations');

            // Remover tudo
            $con = ConnectionPostgreSQL::getConnectionByEnv();
            $con->begin_transaction();
            $con->executeQuery("DROP SCHEMA IF EXISTS _dbupdater CASCADE");
            $con->executeQuery("DROP SCHEMA IF EXISTS _nsutil CASCADE");

            // Remover todas as tabelas
            $dropAllTablesQuery = "
                SELECT
                    'DROP TABLE IF EXISTS ' || quote_ident(schemaname) || '.' || quote_ident(tablename) || ' CASCADE;' AS cmd
                FROM
                    pg_tables
                WHERE
                    schemaname NOT IN ('pg_catalog', 'information_schema')
                    AND tablename NOT IN ('spatial_ref_sys');
            ";
            $querys = $con->execQueryAndReturn($dropAllTablesQuery);
            foreach ($querys as $item) {
                $con->executeQuery($item['cmd']);
            }
            $con->commit();

            (new Migrate())->handle([]);

            $this->success('Migrate Fresh');
        } else {
            echo "Aborted\n";
        }
    }
}
