<?php

namespace NsUtil\Commands\NsUtils;

use NsUtil\Commands\Abstracts\Command;
use NsUtil\Commands\Utils\Util;
use NsUtil\Helper;
use NsUtil\Template;
use function NsUtil\env;

class MakeLoadCSVClosure extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "make:loadcsv-closure";

    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));


        // Extrair dados da entrada
        $np = Util::prepareNamespace('LoadCSVClosures', 'NSUTIL_LOADCSV_CLOSURES_NAMESPACE', $args[1] ?? null);
        [$className, $filename, $namespace] = Util::prepareNameToCreate($args[0], 'LoadCSVClosure', $np);

        $ret = Util::writeOnTemplate(__DIR__ . '/../Templates/loadCSVClosure.php', $filename, [
            'namespace' => $namespace,
            'classname' => $className
        ]);

        if ($ret) {
            $this->success("File $filename was created!");
        } else {
            $this->error("File $filename was not created");
        }

        // if (null === $args[0]) {
        //     throw new \Exception('Name of new handler was not informed');
        // }


        // $className = ucwords(Helper::name2CamelCase($args[0]));
        // $file = Helper::getPathApp() . "/src/LoadCSVClosures/{$className}LoadCSVClosure.php";
        // $namespace = Helper::getPsr4Name() . '\LoadCSVClosures';

        // $template = include __DIR__ . '/../Templates/loadCSVClosure.php';
        // $content = (new Template($template, [
        //     'namespace' => $namespace,
        //     'classname' => $className,
        // ]))->render();


        // Helper::saveFile($file, false, $content);

        // chmod($file, 0777);

        // $this->success("File $file was created!");
    }
}
