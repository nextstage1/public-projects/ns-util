<?php

namespace NsUtil\Commands\Utils;

use NsUtil\EnvFile;
use NsUtil\Helper;
use NsUtil\Helpers\Template;
use function NsUtil\env;

class Util
{

    public static function prepareNamespace($default, ?string $envname, ?string $namespace): string
    {
        if ($namespace !== null) {
            $namespace = str_replace('\\', '/', $namespace);
            $namespace = str_replace(["/{$default}", $default], '', $namespace);
            return strlen($namespace) > 0 ? "$namespace/$default" : $default;
        } else {
            $default = env($envname, $default);
            return $default;
        }
    }

    public static function prepareNameToCreate(string $filename, string $sufixo, string $namespace): array
    {
        if (null === $filename) {
            throw new \Exception('Name of file was not informed');
        }

        $filename = str_replace($sufixo, '', $filename);

        $namespace = str_replace([Helper::getPsr4Name(), '/'], ['', '\\'], $namespace);

        switch (true) {
            case stripos($filename, ' ') !== false:
                $filename = Helper::name2CamelCase(
                    strtolower(preg_replace('/[A-Z]/', ' $0', $filename))
                );
                $path = Helper::getPathApp() . '/src/' . $namespace;
                $namespace = Helper::getPsr4Name() . '\\' . $namespace;
                break;
            case stripos($filename, '/') !== false:
                $dirname = dirname(str_replace(['/src/', 'src/'], '', $filename));
                $path = Helper::getPathApp() . "/src/$dirname";
                $filename = basename($filename);
                $namespace = Helper::getPsr4Name() . str_replace('/', '\\', $dirname);
                break;
            default:
                // reverter todos caracteres maiusculos depois da primeira para espaco e o caracteres minusculo
                $filename = Helper::name2CamelCase(
                    strtolower(preg_replace('/[A-Z]/', ' $0', $filename))
                );
                $path = Helper::getPathApp() . '/src/' . $namespace;
                $namespace = Helper::getPsr4Name() . '\\' . $namespace;
                break;
        }


        $className = ucwords($filename);
        $namespace = str_replace('/', '\\', $namespace);
        $path = str_replace('\\', '/', $path);
        $filename = "{$path}/{$className}{$sufixo}.php";

        return [$className, $filename, $namespace];
    }

    public static function writeOnTemplate(string $template, string $filename,  array $data): bool
    {
        if (is_file($template)) {
            $template = require $template;
        }

        $content = (new Template($template, $data))->render();

        if (Helper::saveFile($filename, false, $content)) {
            chmod($filename, 0777);
            return true;
        }

        return false;
    }
}
