<?php

return '<?php

namespace {namespace};

use NsLibrary\Controller\ApiRest\AbstractApiRestController;
use NsUtil\Api;

class HealthcheckRouter extends AbstractApiRestController
{

    public function __construct(Api $api)
    {
        $this->init($api);
    }

    public function list(): void
    {
        $this->response(["status"=>"Running"], 200);
    }
}
';
