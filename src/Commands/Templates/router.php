<?php

return '<?php

namespace {namespace};

use NsUtil\Api;
use NsUtil\Validate;
use NsLibrary\Config;
use NsLibrary\Controller\ApiRest\AbstractApiRestController;
use {NSUTIL_MODELS_NAMESPACE}\{classname} as Model;
use {NSUTIL_RESOURCES_NAMESPACE}\{classname}Resource as Resource;

class {classname}Router extends AbstractApiRestController
{

    private $entitieName="{classname}";

    public function __construct(Api $api) {

        $this->init($api);

        $entidadeName = $this->entitieName;
        $entidadeObject = new Model();
        $poderesGrupo = $this->entitieName;
        $poderesSubGrupo = $this->entitieName;
        $camposDate = Config::getData("entitieConfig")[$this->entitieName]["camposDate"];
        $camposDouble = Config::getData("entitieConfig")[$this->entitieName]["camposDouble"];
        $camposJson = Config::getData("entitieConfig")[$this->entitieName]["camposJson"];

        $this->controllerInit($entidadeName, $entidadeObject, $poderesGrupo, $poderesSubGrupo, $camposDate, $camposDouble, $camposJson);
    }

    private function responseResource($item, int $code = 200)
    {
        $this->response((new Resource($item)), $code);
    }

    public function list(): void
    {
        $list = $this->ws_getAll($this->dados);
        $this->responseResource($list, 200);
    }

    public function read(int $id): void
    {
        $item = $this->ws_getById(["id" => $id]);
        $this->responseResource($item, 200);
    }

    public function create(): void
    {
        $item = $this->ws_save($this->dados);
        $this->responseResource($item, 201);
    }

    public function update(): void
    {
        $this->create();
    }

    public function delete(): void
    {
        $item = $this->ws_remove($this->dados);
        $this->response([], 204);
    }


}
    ';
