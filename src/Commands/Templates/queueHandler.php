<?php

return '<?php

namespace {namespace};

use NsUtil\Queue\AbstractQueueHandler;

class {classname}QueueHandler extends AbstractQueueHandler
{

    public static function addByDTO($param1, $param2): int
    {
    
        $remove = true;
        $priority = 1;
        $assync = true;
        $initAt = null;

        $data = [
            "param1" => $param1,
            "param2" => $param2,
        ];
        
        // dont change this line
        return self::add($data, $remove, $priority, $assync, $initAt);
    }

    public static function handle(array $data): void
    {
        // your rules to run
    }

}
    ';
