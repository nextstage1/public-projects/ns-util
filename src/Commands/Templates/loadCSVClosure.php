<?php

return '<?php

namespace {namespace};

use NsUtil\Assync\LoadCSV\LoadCSVClosuresAbstract;

class {classname}LoadCSVClosure extends LoadCSVClosuresAbstract
{
    public function handle(array $data): array
    {
        $list = $this->filterOnlyArray($data);

        foreach ($list as &$item) {
            // $this->formatCep($item, [\'field_name_cep\']);
            // $this->formatFone($item, [\'field_name_fone\']);
            // $this->formatDate($item, [\'field_name_date\']);
            // $this->formatDatetime($item, [\'field_name_datetime\']);
            // $this->formatInt($item, [\'field_name_int\']);
        }

        return $list;
    }
}
';
