<?php

return "<?php

namespace {namespace};

use NsLibrary\Resource\AbstractResource;
                
class {classname}Resource extends AbstractResource
{

    public function __construct(\$resource)
    {
        parent::__construct(\$resource);

        // Use this to ignore fields from resource
        // \$this->fieldsToIgnore = [];

        // Only this fields will be returned. 
        // To rename fields, use ['new_name' => 'nameOnTable']. 
        // To get same name, use ['nameOnTable'].
        // \$this->fillable = null;

        // Use this to parse fields to another type. Use mapped name fields.
        // \$this->parser = ['field' => 'type'];

        // Use this to add extra fields to resource. Use original name fields from resource.
        // \$this->extras = ['field' => fn(\$item) => (int) \$item['field'] ];
    }
}
";
