<?php

return "<?php

namespace {namespace};

use NsUtil\Middlewares\AbstractMiddleware;

class {classname}Middleware extends AbstractMiddleware
{
    public function check(): bool
    {
        // Your rules to check

        // If Error, throw an exception
        // throw new \Exception(\$message);

        // If Success
        return true;
    }
}";
