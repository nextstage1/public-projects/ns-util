<?php

return "<?php

namespace {namespace};

use NsUtil\DTOs\AbstractDTO;

/**
 * {classname} DTO class.
 * 
 * Represents a Data Transfer Object (DTO) for {classname}.
 */
class {classname}DTO extends AbstractDTO
{
    /**
     * Constructor to initialize {classname} DTO.
     *
     * @param mixed \$env1 The value for environment variable 1.
     * @param mixed \$env2 The value for environment variable 2.
     */
    public function __construct(\$env1, \$env2)
    {
        \$this->data = [
            'env1' => \$env1, 
            'env2' => \$env2
        ];
    }
}
";
