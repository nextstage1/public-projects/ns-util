<?php

namespace NsUtil;

use Closure;
use Exception;
use NsUtil\Helper;
use PDO;
use stdClass;

class ConnectionPostgreSQL
{

    private $con; // garantir o singleton
    private $config;
    public $query;
    public $result;
    public $numRows;
    public $error;
    public $dd;
    public $lastInsertId;
    private static $transaction_in_progress;
    private $nullas = '';
    private $logfile = null;

    public function __construct($host, $user, $pass, $port, $database, string $logfile = null)
    {
        $this->logfile = $logfile;
        $this->config = new stdClass();
        $this->config->host = $host;
        $this->config->port = $port;
        $this->config->database = $database;
        $this->config->user = $user;
        $this->config->pwd = $pass;
        $this->open();
    }

    public function getConfig(): stdClass
    {
        return $this->config;
    }

    private function getPID(): int
    {
        if (null === $this->con) {
            return -1;
        }

        return (int) $this->execQueryAndReturn("SELECT pg_backend_pid() as pid")[0]['pid'] ?? -1;
    }

    private function log($message)
    {
        if (null !== $this->logfile) {
            Log::logTxt(
                $this->logfile,
                '[' . $this->con->pgsqlGetPid() . '] ' .
                    preg_replace('/(\\s)+/', ' ', $message),
                true
            );
        }
    }

    public static function getConnectionByEnv(): ConnectionPostgreSQL
    {
        return new ConnectionPostgreSQL(
            getenv('DBHOST'),
            getenv('DBUSER'),
            getenv('DBPASS'),
            getenv('DBPORT'),
            getenv('DBNAME')
        );
    }

    public function open()
    {
        if (!$this->con) {
            try {
                $stringConnection = "pgsql:host=" . $this->config->host . ";port=" . $this->config->port . ";dbname=" . $this->config->database . ";user=" . $this->config->user . ";password=" . $this->config->pwd;
                $this->con = new PDO($stringConnection);
                $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->log('Connection started');
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
        }

        if (null !== env('PRE_QUERY_AUDIT')) {
            array_map(fn($item) => $this->con->exec($item), json_decode(env('PRE_QUERY_AUDIT'), true));
        }
    }

    public function getConn(): ?PDO
    {
        return $this->con;
    }

    public function close(): void
    {
        $this->con = null;
    }

    public function isAlive(): bool
    {
        return $this->con !== null;
    }

    public function begin_transaction(): void
    {
        if (!self::$transaction_in_progress) {
            $this->executeQuery('START TRANSACTION');
            self::$transaction_in_progress = true;
            register_shutdown_function(array($this, "__shutdown_check"));
        }
    }

    public function __shutdown_check(): void
    {
        $this->con = null;
        if (self::$transaction_in_progress) {
            $this->rollback();
        }
    }

    public function commit(): void
    {
        $this->executeQuery("COMMIT");
        self::$transaction_in_progress = false;
    }

    public function rollback(): void
    {
        try {
            $this->executeQuery('ROLLBACK');
        } catch (Exception $exc) {
        }
        self::$transaction_in_progress = false;
    }

    public function autocommit(bool $boolean): void
    {
        $this->con->setAttribute(PDO::ATTR_AUTOCOMMIT, $boolean);
    }

    public function executeQuery($query, $params = null)
    {
        $this->open();
        $res = false;
        $this->numRows = 0;
        $this->result = false;
        $this->error = false;
        $this->query = $query;
        $this->log($query);
        try {
            $this->result = $this->con->prepare($query);
            if (null !== $params && is_array($params)) {
                $res = $this->result->execute($params);
            } else {
                $res = $this->result->execute();
            }
            if (!$res) {
                $this->error = $this->result->errorInfo()[2];
                $this->log('   >> ERROR: ' . $this->result->errorInfo()[0] . $this->result->errorInfo()[2]);
                throw new Exception($this->result->errorInfo()[0] . $this->result->errorInfo()[2], 0);
            }
            $this->numRows = $this->result->rowCount();
        } catch (Exception $exc) {
            $this->result = false;
            $this->result = false;
            if (stripos($exc->getMessage(), 'ERROR:  function unaccent') > -1) {
                die('DEV:  A EXTENSÃO UNNACCENT NÃO FOI INSTALADA');
            }
            $this->log('   >> ERROR: ' . $exc->getMessage());
            throw new Exception($exc->getMessage());
        }
        return $res;
    }

    public function next()
    {
        try {
            if ($this->result) {
                $dados = $this->result->fetch(PDO::FETCH_ASSOC);
                if (is_array($dados)) {
                    return $dados;
                } else {
                    $this->result = false;
                    return false;
                }
            }
        } catch (Exception $e) {
            return false;
        }
    }

    private function parseToCamelCase(array $data): array
    {
        if (count($data) === 0) {
            return $data;
        }

        // Converter as chaves para camelCase
        $keys = [];
        foreach (array_keys($data[0]) as $item) {
            $keys[] = ['original' => $item, 'camel' => Helper::name2CamelCase($item)];
        }

        // Popular o objeto de saida
        $out = [];
        foreach ($data as $item) {
            $outItem = [];
            foreach ($keys as $key) {
                $outItem[$key['camel']] = $item[$key['original']];
            }
            $out[] = $outItem;
        }

        return $out;
    }

    /**
     * Executa e retonar a query formatada com nameCase
     *
     * @param string $query
     * @param boolean $log
     * @param boolean $keyCamelCaseFormat
     * @return array
     */
    public function execQueryAndReturn(string $query, bool $log = true, bool $keyCamelCaseFormat = true): array
    {
        return $this->execQueryAndReturnPrepared($query, null, $keyCamelCaseFormat);
    }

    /**
     * Executa e retonar a query formatada com nameCase
     *
     * @param string $query
     * @param boolean $log
     * @param boolean $keyCamelCaseFormat
     * @return array
     */
    public function execQueryAndReturnPrepared(string $query, ?array $params, bool $keyCamelCaseFormat = true): array
    {
        $this->open();
        $out = [];
        $this->executeQuery($query, $params);

        while ($dd = $this->next()) {
            $out[] = $dd;
        }

        if ($keyCamelCaseFormat && count($out) > 0) {
            $out = $this->parseToCamelCase($out);
        }

        return $out;
    }

    /**
     * Define o que será utilizado em nullas ao executar o insertByCopy
     * @param string $nullas
     */
    public function setNullAs($nullAs = '')
    {
        $this->nullas = $nullAs;
    }

    private function sanitizeToInsert(string $string): string
    {
        // Definindo as substituições
        $replace_pairs = [
            // Remover caracteres de controle ASCII 0-31 e 127
            "\x00" => '', // NUL
            "\x01" => '',
            "\x02" => '',
            "\x03" => '',
            "\x04" => '',
            "\x05" => '',
            "\x06" => '',
            "\x07" => '',
            "\x08" => '',
            "\x09" => '',
            "\x0A" => '',
            "\x0B" => '',
            "\x0C" => '',
            "\x0D" => '',
            "\x0E" => '',
            "\x0F" => '',
            "\x10" => '',
            "\x11" => '',
            "\x12" => '',
            "\x13" => '',
            "\x14" => '',
            "\x15" => '',
            "\x16" => '',
            "\x17" => '',
            "\x18" => '',
            "\x19" => '',
            "\x1A" => '',
            "\x1B" => '',
            "\x1C" => '',
            "\x1D" => '',
            "\x1E" => '',
            "\x1F" => '',
            "\x7F" => '', // DEL

            // Normalizar ou remover espaços indesejados
            "\xA0" => ' ', // Substitui espaço não separável por espaço normal

            // Tratamento para novas linhas, tabs e etc
            "\\r\\n" => "\n", // Normaliza CRLF para LF (nova linha)
            "\\r" => "\n",   // Substitui CR (carriage return) por LF
            "\\t" => ' ',    // Substitui tabs por espaços

            // Tratamento para aspas e barras invertidas (não substitui, mas pode ser necessário em contextos específicos)
            // Exemplo: você pode querer adicionar escapes adicionais ou substituir conforme necessário:
            // "'" => "\'", // Escapa aspas simples
            // '"' => '\"', // Escapa aspas duplas
            // "\\" => "\\\\", // Escapa barras invertidas
        ];

        // Aplica as substituições
        $sanitized_string = strtr($string, $replace_pairs);

        return $sanitized_string;
    }

    /**
     * Ingest data using copy strategy postgresql
     *
     * @param string $toTable
     * @param array $fields
     * @param array $records
     * @return void
     */
    public function insertByCopy($toTable, array $fields, array $records): bool
    {
        $this->open();
        static $delimiter = "\t";
        $nullAs = $this->nullas;
        $rows = [];
        foreach ($records as $record) {
            $row = [];
            foreach ($fields as $key => $field) {

                switch (true) {
                    case !isset($record[$key]):
                    case is_null($record[$key]):
                        $record[$key] = $nullAs;
                        break;
                    case is_bool($record[$key]):
                        $record[$key] = $record[$key] ? 't' : 'f';
                        break;
                    case is_array($record[$key]):
                        $record[$key] = json_encode($record[$key], JSON_HEX_QUOT | JSON_HEX_APOS | JSON_UNESCAPED_UNICODE);
                        break;
                    default:
                        $record[$key] = $record[$key] ?? $nullAs;
                }

                $record[$key] = str_replace($delimiter, ' ', $record[$key]);
                // $record[$key] = $this->sanitizeToInsert($record[$key]);
                $record[$key] = str_replace("\0", "", $record[$key]);

                // Convert multiline text to one line.
                $record[$key] = addcslashes($record[$key], "\0..\37");
                $row[] = $record[$key];
            }
            $rows[] = implode($delimiter, $row) . "\n";
        }

        if (!empty($rows)) {
            $this->con->pgsqlCopyFromArray($toTable, $rows, $delimiter, addslashes($nullAs), implode(',', $fields));
        }

        unset($rows);

        return true;
    }

    public function queryRunWithLoader($querys, $label, $showQueryOnLabel = false, $showQtde = false)
    {
        $querys = array_values($querys);
        $loader = new StatusLoader(count($querys), $label);
        $loader->done(1);
        $loader->setShowQtde($showQtde);
        for ($i = 0; $i < count($querys); $i++) {
            if ($showQueryOnLabel) {
                $loader->setLabel($querys[$i]);
            }
            $this->executeQuery($querys[$i]);
            $loader->done($i + 1);
        }
        return $loader->getLastStatusBar();
    }

    /**
     *Executara um update na tabela com prepared. Os nomes do campos já devem estar no formato da tabela, sem camelcase
     *
     * @param string $table
     * @param array $array
     * @param string $nomeCpoId
     * @param string $onConflict
     * @return bool
     */
    public function insert($table, $array, $nomeCpoId, $onConflict = '')
    {
        $preValues = $update = $valores = [];
        foreach ($array as $key => $value) {
            $keys[] = '"' . $key . '"';
            $preValues[] = '?';
            $valores[] = $value;
        }
        $query = "INSERT INTO $table (" . implode(',', $keys) . ") VALUES (" . implode(',', $preValues) . ")"
            . " $onConflict "
            . " returning $nomeCpoId as nsnovoid";
        $this->open();
        $res = false;
        $this->numRows = 0;
        $this->result = false;
        $this->error = false;
        $this->log($query);
        try {
            $this->result = $this->con->prepare($query);
            if (!$this->result->execute($valores)) {
                $this->error = $this->result->errorInfo()[2];
                $this->log('   >> ERROR: ' . $this->result->errorInfo()[0] . $this->result->errorInfo()[2]);
                throw new Exception($this->result->errorInfo()[0] . $this->result->errorInfo()[2], 0);
            }
            return $res;
        } catch (Exception $exc) {
            $this->result = false;
            $this->log('   >> ERROR: ' . $exc->getMessage());
            throw new Exception($exc->getMessage());
        }
    }

    /**
     * Executara um update na tabela com prepared. Os nomes do campos já devem estar no formato da tabela, sem camelcase
     * @param string $table
     * @param array $array
     * @param string $cpoWhere
     * @return boolean
     * @throws Exception
     */
    public function update($table, $array, $cpoWhere)
    {
        $update = $valores = [];
        $idWhere = $array[$cpoWhere];
        unset($array[$cpoWhere]);
        foreach ($array as $key => $value) {
            $valores[] = $value;
            $update[] = "\"$key\"=?";
        }
        // where
        $valores[] = $idWhere;
        $query = "update $table set " . implode(',', $update) . " where $cpoWhere=?";
        $this->open();
        $res = false;
        $this->numRows = 0;
        $this->result = false;
        $this->error = false;
        $this->log($query);
        try {
            $this->result = $this->con->prepare($query);
            if (!$this->result->execute($valores)) {
                $this->error = $this->result->errorInfo()[2];
                $this->log('   >> ERROR: ' . $this->result->errorInfo()[0] . $this->result->errorInfo()[2]);
                throw new Exception($this->result->errorInfo()[0] . $this->result->errorInfo()[2], 0);
            }
            return $res;
        } catch (Exception $exc) {
            $this->result = false;
            $this->log('   >> ERROR: ' . $exc->getMessage());
            throw new Exception($exc->getMessage());
        }
    }

    /**
     * Set the value of logfile
     *
     * @return  self
     */
    public function setLogfile($logfile)
    {
        $this->logfile = $logfile;
        return $this;
    }


    public function __sleep()
    {
        // Excluir a conexão PDO da serialização
        return ['config', 'query', 'numRows', 'error', 'dd', 'lastInsertId', 'nullas', 'logfile'];
    }

    public function __wakeup()
    {
        // Reestabelecer a conexão ao desserializar
        $this->open();
    }
}
