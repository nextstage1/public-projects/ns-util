<?php

namespace NsUtil;

use Closure;
use Exception;
use NsUtil\Helpers\Validate;
use NsUtil\Helpers\JsonParser;
use NsUtil\Services\RateLimiter;
use NsUtil\Interfaces\MiddlewareInterface;
use NsUtil\Exceptions\ModelNotFoundException;
use NsUtil\Exceptions\TooManyRequestException;
use NsUtil\Exceptions\RedisConnectionException;

class Api
{

    private $body = [];
    private $headers;
    private $responseData = ['content' => [], 'error' => false];
    private $eficiencia;
    private $responseCode = 200;
    private $config = [];
    private $router;
    private $simpleReturn = false; // Utilizado para deinfir se aapenas retornar o conteudo ou encerrar a aplicação
    private ?Closure $successCallback = null;
    private ?Closure $errorCallback = null;
    private $onResponse;
    private $authRequired;
    private $validators = [];

    // [Informational 1xx]
    const HTTP_CONTINUE = 100;
    const HTTP_SWITCHING_PROTOCOLS = 101;
    // [Successful 2xx]
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_ACCEPTED = 202;
    const HTTP_NONAUTHORITATIVE_INFORMATION = 203;
    const HTTP_NO_CONTENT = 204;
    const HTTP_RESET_CONTENT = 205;
    const HTTP_PARTIAL_CONTENT = 206;
    // [Redirection 3xx]
    const HTTP_MULTIPLE_CHOICES = 300;
    const HTTP_MOVED_PERMANENTLY = 301;
    const HTTP_FOUND = 302;
    const HTTP_SEE_OTHER = 303;
    const HTTP_NOT_MODIFIED = 304;
    const HTTP_USE_PROXY = 305;
    const HTTP_UNUSED = 306;
    const HTTP_TEMPORARY_REDIRECT = 307;
    // [Client Error 4xx]
    const errorCodesBeginAt = 400;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_PAYMENT_REQUIRED = 402;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_METHOD_NOT_ALLOWED = 405;
    const HTTP_NOT_ACCEPTABLE = 406;
    const HTTP_PROXY_AUTHENTICATION_REQUIRED = 407;
    const HTTP_REQUEST_TIMEOUT = 408;
    const HTTP_CONFLICT = 409;
    const HTTP_GONE = 410;
    const HTTP_LENGTH_REQUIRED = 411;
    const HTTP_PRECONDITION_FAILED = 412;
    const HTTP_REQUEST_ENTITY_TOO_LARGE = 413;
    const HTTP_REQUEST_URI_TOO_LONG = 414;
    const HTTP_UNSUPPORTED_MEDIA_TYPE = 415;
    const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
    const HTTP_EXPECTATION_FAILED = 417;
    const HTTP_TOO_MANY_REQUESTS = 429;
    // [Server Error 5xx]
    const HTTP_INTERNAL_SERVER_ERROR = 500;
    const HTTP_NOT_IMPLEMENTED = 501;
    const HTTP_BAD_GATEWAY = 502;
    const HTTP_SERVICE_UNAVAILABLE = 503;
    const HTTP_GATEWAY_TIMEOUT = 504;
    const HTTP_VERSION_NOT_SUPPORTED = 505;

    public static array $allowHeadersDefault = ['Data', 'Cache-Control', 'Referer', 'User-Agent', 'Origin', 'Accept', 'X-Requested-With', 'Content-Type', 'Access-Control-Request-Method', 'Access-Control-Request-Headers', 'Token', 'Authorization'];
    public static array $allowMethodsDefault = ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'];

    public static bool $ignoreContentOnError = false;

    public function __construct(?string $typeOut = 'json', $bodyTreated = null, ?Router $router = null)
    {
        $this->setTypeOut($typeOut);

        // Obtenção dos headers. Chaves sempre minusculas
        $this->headers = $this->getAllHeaders();

        // Obtenção do verbo
        $metodo = $_SERVER['REQUEST_METHOD'] ?? 'NULL';

        if (null === $bodyTreated) {
            $this->body = [];
            switch ($metodo) {
                case 'PUT':
                case 'POST':
                    // Obtenção do body
                    $this->body = $_POST;
                    $dd = json_decode(file_get_contents('php://input'), true);
                    $dd = is_array($dd) ? $dd : [];
                    $this->body = array_merge($_POST, $_GET, $dd);
                    break;
                case 'GET':
                    $this->body = $_GET;
                    break;
                default:
            }

            // forcar setar este item para evitar erros ao obter a pagina atual 
            $this->body['page'] ??= $this->body['pagina'] ?? 0;
            $this->body['pagina'] ??= $this->body['page'] ?? 0;

            $bodyOrigin = $this->body;
            Helper::recebeDadosFromView($this->body);
        } else {
            $bodyOrigin = $bodyTreated;
            $this->setBodyTreated($bodyTreated);
        }

        $this->eficiencia = new \NsUtil\Eficiencia();

        // Config para aplicação
        $router ??= new Router('');
        $this->router = $router;

        // Variaveis adicionadas
        $rest = [
            'method' => (string) $metodo,
            'id' => (int) $router->getAllParam(2),
            'resource' => (string) $router->getAllParam(1),
            'action' => (string) $router->getAllParam(3),
        ];
        $this->config = [
            'rest' => $rest,
            'bodyOrigin' => $bodyOrigin,
            'headers' => $this->getHeaders(),
            'rota' => $router->getAllParam(1) . (($router->getAllParam(2)) ? '/' . $router->getAllParam(2) : ''),
            // '/' . $router->getAllParam(2),
            'acao' => 'ws_' . $router->getAllParam(2),
            'controller' => ucwords((string) $router->getAllParam(1)),
            'entidade' => $router->getEntidade(),
            'includeFile' => $router->getIncludeFile(),
            'ParamsRouter' => $router->getAllParam(),
            'dados' => array_merge($this->getBody(), [
                'idFromRoute' => (int) $router->getAllParam(3),
                '_rest' => $rest
            ]),
        ];

        // Definições API Rest
        switch ($metodo) {
            case 'GET':
                if (!$this->config['rest']['action']) {
                    $this->config['rest']['action'] = $this->config['rest']['id'] > 0 ? 'read' : 'list';
                }
                break;
            case 'DELETE':
                $this->config['rest']['action'] = 'delete';
                break;
            case 'PUT':
            case 'POST':
                if (!$this->config['rest']['action']) {
                    $this->config['rest']['action'] = $this->config['rest']['id'] > 0 ? 'update' : 'save';
                }
                break;
            default:
                $this->config['rest']['action'] = 'METHOD_NOT_FOUND';
        }
    }

    private function getAllHeaders()
    {
        if (!function_exists('getallheaders')) {
            $headers = [];
            foreach ($_SERVER as $name => $value) {
                if (mb_substr((string) $name, 0, 5) == 'HTTP_') {
                    $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr((string) $name, 5)))))] = $value;
                }
            }
            return Helper::filterSanitize($headers);
        } else {
            return getallheaders();
        }
    }

    /**
     * Retorna os dados enviados pela api restfull
     *
     * @return object
     */
    public function getRest(): object
    {
        return (object) $this->config['rest'];
    }

    /**
     * ATENÇÃO: Retorna o dado conforme foi enviado, sem nenhum tratamento de segurança. Use com atenção.
     * @param string $key
     */
    public function getOriginalData($key)
    {
        // Obtenção do body
        $b = array_merge($_POST, $_GET);
        $dd = json_decode(file_get_contents('php://input'), true);
        if (is_array($dd)) {
            $b = array_merge($_POST, $dd);
        }
        return $b[$key];
    }

    /**
     * Adiciona a chave de error para response da Api
     * @param mixed $mensagem
     * @param int $code
     * @return $this
     */
    public function setError($mensagem, $code = 200)
    {
        $this->responseData['error'] = $mensagem;
        $this->responseCode = $code;
        return $this;
    }

    /**
     * Dado um array, faz o merge com o response body atual
     * @param array $response
     * @return $this
     */
    public function responseMerge(array $response)
    {
        $this->responseData = array_merge($this->responseData, $response);
        return $this;
    }

    /**
     * Adiciona ao body de saída uma chave => valor
     * @param string $chave
     * @param mixed $valor
     * @return self
     */
    public function addResponse($chave, $valor)
    {
        $this->responseMerge([$chave => $valor]);
        return $this;
    }

    /**
     * Irá retornar o body em array ao inves de imrprimr e encerrar
     * @return void
     */
    public function getResponse()
    {
        return $this->response([], 0);
    }

    /**
     * Responde a requisição, encerrando o script
     * @param array $response
     * @param int $responseCode
     */
    public function response(array $response = [], int $responseCode = 0)
    {
        // Setar o codigo final de saida
        if ($responseCode > 0) {
            $this->responseCode = $responseCode;
        }

        if (count($response) > 0) {
            // caso content não venha nada, vou  colocar por padrão
            if (!isset($response['content']) && !isset($this->responseData['content'])) {
                //$response['content'] = false;
            }

            // Adicionar parametros default
            $this->responseMerge($response);
        }


        // Prepara os dados de forma padrão a ser entregue
        $this->getResponseData();

        // Saida
        http_response_code($this->responseCode);

        // Executar função anonima caso exista
        if (is_callable($this->errorCallback) && $this->responseCode > 399) {
            $this->successCallback = null;
            call_user_func($this->errorCallback, $this->responseData, $this->responseCode, $this);
        }

        // Caso seja um desses códigos, nem imprimir nada
        if (array_search($this->responseCode, [501]) === false) {

            // Executar função anonima caso exista
            if (is_callable($this->onResponse)) {
                call_user_func($this->onResponse, $this->responseData, $this->responseCode, $this);
            }

            echo json_encode($this->responseData, JSON_UNESCAPED_SLASHES);

            // Executar função anonima caso exista
            if (is_callable($this->successCallback)) {
                call_user_func($this->successCallback, $this->responseData, $this->responseCode, $this);
            }
        }

        die();
    }

    /**
     * responde a aplicação com um erro
     * @param mixed $mensagem
     * @param int $code
     */
    public function error($mensagem, int $code = 200)
    {
        $this->setError($mensagem, $code);
        $this->response();
    }

    /**
     * Retorna o body da requisição
     * @return array
     */
    function getBody(): array
    {
        return $this->body;
    }

    /**
     * Retorna o headers da requisição
     * @return array
     */
    function getHeaders($keysToLower = false): array
    {
        if ($keysToLower) {
            foreach ($this->headers as $key => $val) {
                unset($this->headers[$key]);
                $this->headers[mb_strtolower($key)] = $val;
            }
        }
        return $this->headers;
    }
    /**
     * Retorna os dados de configuração.
     *
     * Este método decodifica a string JSON armazenada na propriedade $config.
     *
     * @param string $format Formato desejado para os dados retornados. Aceita 'array' para retornar os dados como um array associativo ou qualquer outro valor para retornar um objeto.
     * @return mixed Retorna os dados de configuração como um array associativo se $format for 'array', ou como um objeto se $format for qualquer outro valor.
     */
    public function getConfigData(string $format = 'array')
    {
        return JsonParser::handle($this->config, $format === 'array');
    }

    public function getRouter(): Router
    {
        return $this->router;
    }

    /**
     * Estaticamente, cria uma instancia da API e responde o body com o código citado
     * @param int $code
     * @param array $response
     */
    public static function result(int $code, array $response, $type = 'json')
    {
        $api = new Api();
        $api->response($response, $code);
    }

    /**
     * Recebe um array com as configuração e seta a configuração estatica de Config 
     * @param array $config
     * @param string $page404
     */
    public function setConfig(array $config = [], $page404 = ''): Api
    {
        $router = new Router($page404);

        // Config para aplicação
        $this->config = array_merge($this->config, $config);
        Config::init($this->config);

        return $this;
    }

    /**
     * Retorna configurações da rota API
     * @return string
     */
    public function getRota()
    {
        return $this->config['rota'];
    }

    /**
     * Retorna um array contento username e password enviado. 
     * 
     * Espera uma string em base64_encode contendo {username}:{password} no headers
     * @return array
     */
    public function getUsernameAndPasswordFromAuthorizationHeaders(): array
    {
        $dt = explode(':', base64_decode(mb_substr((string) $this->getHeaders(true)['authorization'], 6)));
        Helper::recebeDadosFromView($dt);
        return [
            'username' => $dt[0] ?? null,
            'password' => $dt[1] ?? null
        ];
    }

    /**
     * Retorna a string enviada como Token no cabeçalho Authorization
     * @return string
     */
    public function getTokenFromAuthorizationHeaders(): ?string
    {
        $headers = $this->getHeaders(true);
        $auth = $headers['authorization'] ?? null;
        if (null !== $auth) {
            return (string) trim(substr($auth, 6));
        } else {
            return null;
        }
    }

    /**
     *
     * @return array
     */
    function getResponseData(): array
    {
        // Preparar saida padrão
        if (!isset($this->responseData['content'])) {
            $this->responseData['content'] = [];
        }
        $this->responseMerge([
            'status' => $this->responseCode,
            // 'elapsedTime' => $this->eficiencia->end()->time,
        ]);

        // Sanitização
        $this->responseData['error'] = (($this->responseData['error'] !== false) ? $this->responseData['error'] : false);

        if ($this->responseCode > 401 && stripos(json_encode($this->responseData), 'SQLSTATE') === false) {
            $this->responseData = ['error' => $this->responseData['error'], 'content' => []];
        }

        if (self::$ignoreContentOnError === true && ($this->responseData['error'] !== false || $this->responseCode > 399 || $this->responseCode === 0)) {
            unset($this->responseData['content']);
        }

        return $this->responseData;
    }

    /**
     *
     * @return integer
     */
    function getResponseCode(): int
    {
        return $this->responseCode;
    }

    /**
     * Undocumented function
     *
     * @param integer $responseCode
     * @return self
     */
    function setResponseCode(int $responseCode): self
    {
        $this->responseCode = (int) $responseCode;
        return $this;
    }

    /**
     * Observa se a requisição é do tipo options e encerra respondendo as options
     * @param string $allowOrigin
     * @param string $allowMethods
     * @param string $allowHeaders
     * @return void
     */
    public static function options(?string $allowOrigin = null, ?string $allowMethods = null, ?string $allowHeaders = null): void
    {
        $allowOrigin ??= '*';
        $allowHeaders ??= implode(',', self::$allowHeadersDefault);
        $allowMethods ??= implode(',', self::$allowMethodsDefault);

        $allowHeaders = array_map('trim', explode(',', $allowHeaders));
        $allowMethods = array_map('trim', explode(',', $allowMethods));

        ## CORS
        \header('Access-Control-Allow-Origin: ' . $allowOrigin);
        \header("Access-Control-Allow-Methods: " . implode(',', $allowMethods));
        \header("Access-Control-Allow-Headers: " . implode(',', $allowHeaders));
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            exit(0);
        }
    }


    /**
     * Verifica se a classe existe no path indicado, cria o controller padrão e entrega conforme os verbos para execução
     * @param string $namespace
     * string $allowOrigin = '*', string $allowMethods = 'GET,PUT,POST,DELETE,OPTIONS', string $allowHeaders = 'Data,Cache-Control,Referer,User-Agent,Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Token,Authorization'
     */
    public static function restFull(string $namespace, Api $api = null): void
    {
        self::options();
        if (null === $api) {
            $api = new Api();
        }
        $api->setConfig();

        $namespace = str_replace('/', '\\', $namespace);

        // Executar função anonima caso exista. Só existe função em caso de erro. O teste da funcao enviada é feito na chamada do metodo validator()
        foreach ($api->validators as $validator) {
            if (is_callable($validator)) {
                $api->successCallback = \null;
                call_user_func($validator, $api);
                die();
            }
        }

        $rest = (object) $api->getConfigData()['rest'];
        $class_name = $namespace . '\\' . ucwords((string) Helper::name2CamelCase($rest->resource));
        $class_name_controller = $class_name . 'Controller';
        $class_name_router = $class_name . 'Router';
        $oldController = ucwords($rest->resource) . 'Controller';
        switch (true) {
            case (class_exists($class_name)):
                (new $class_name($api))();
                break;
            case (class_exists($class_name_controller)):
                (new $class_name_controller($api))();
                break;
            case (class_exists($class_name_router)):
                (new $class_name_router($api))();
                break;
            case (class_exists($oldController)):
                $aliases = [
                    'read' => 'getById',
                    'list' => 'getAll',
                    'create' => 'save',
                    'delete' => 'remove',
                    'update' => 'save',
                    'search' => 'getAll',
                    'new' => 'getNew'
                ];
                switch (true) {
                    case method_exists($oldController, "ws_" . $rest->action):
                        $action = "ws_" . $rest->action;
                        break;
                    case method_exists($oldController, "ws_" . $aliases[$rest->action]):
                        $action = "ws_" . $aliases[$rest->action];
                        break;
                    default:
                        $api->error('', Api::HTTP_NOT_IMPLEMENTED);
                        break;
                }
                $response = [];

                $code = 200;
                $controller = new $oldController();
                $data = array_merge(['id' => $rest->id], $api->getBody());
                $response['content'] = $controller->$action($data);

                if (isset($response['content']['error'])) {
                    $response['error'] = $response['content']['error'];
                }

                // se for getNew, remover os erros               
                if (Helper::compareString('ws_read', $action) || Helper::compareString('ws_getById', $action)) {
                    $response['error'] = false;
                    $response['content']['error'] = false;
                }

                $api->response($response, $code);

                break;

            default:
                http_response_code(Api::HTTP_NOT_IMPLEMENTED);
                die();
        }
    }

    /**
     * Undocumented function
     *
     * @param Closure $successCallback
     * @return self
     */
    public function setSuccessCallback(Closure $successCallback): self
    {
        $this->successCallback = $successCallback;
        return $this;
    }

    /**
     * Undocumented function
     *
     * @param Closure $successCallback
     * @return self
     */
    public function onSuccess(Closure $successCallback): self
    {
        $this->successCallback = $successCallback;
        return $this;
    }

    /**
     * Undocumented function
     *
     * @param Closure $successCallback
     * @return self
     */
    public function onError(Closure $errorCallback): self
    {
        $this->errorCallback = $errorCallback;
        return $this;
    }

    /**
     * Validates a rule and handles middleware execution.
     *
     * @param string $message The error message to use if validation fails.
     * @param int $code The error code to use if validation fails.
     * @param mixed $rule The validation rule, which can be a callable or a class name implementing MiddlewareInterface.
     * @return self Returns the current instance for method chaining.
     * @throws Exception If the rule is not a callable or a class implementing MiddlewareInterface.
     */
    public function validator(string $message, int $code, $rule): self
    {
        // Check if the parameter is an instance of a class that implements MiddlewareInterface
        if (is_callable($rule)) {
            $ret = call_user_func($rule);
        } else {
            if (is_string($rule) && class_exists($rule)) {
                $class = new $rule();
                if ($class instanceof MiddlewareInterface) {
                    $ret = $class->handle($this);
                } else {
                    throw new Exception('Only middleware interface was accepted');
                }
            } else {
                throw new Exception('Only closure or middleware interface was accepted');
            }
        }

        if ($ret !== true) {
            $this->validators[] = function ($api) use ($code, $message) {
                $api->error($message, $code);
            };
        }
        return $this;
    }


    /**
     * Provides a compatibility method for middleware based on the validator method.
     *
     * @param string $message        The error message to display upon validation failure
     * @param int $code              The error code
     * @param Closure|MiddlewareInterface $rule The closure or Middleware class instance to perform the validation
     *
     * @return self
     * @throws Exception             Thrown if a closure or Middleware class is not provided
     */
    public function middleware(string $message, int $code, $rule): self
    {
        return self::validator($message, $code, $rule);
    }

    /**
     * Applies a set of middleware rules and optionally stops on failure.
     *
     * @param array $rules An array of middleware rules to apply. Each rule will be passed to the validator method.
     * @param bool $stopOnFail If true, the method will stop processing further rules and execute any error callback if validation fails.
     * @return self Returns the current instance for method chaining.
     */
    public function middlewares($rules): self
    {
        foreach ($rules as $rule) {
            $this->validator('', 0, $rule);
        }

        return $this;
    }


    /**
     * Undocumented function
     *
     * @param string $namespace
     * @return void
     */
    public function rest(string $namespace): void
    {
        try {
            self::restFull($namespace, $this);
        } catch (ModelNotFoundException $exc) {
            $this->error($exc->getMessage() ?? 'Not found', $exc->getCode() ?? 404);
        } catch (Exception $exc) {
            $this->error($exc->getMessage(), $exc->getCode() ?? self::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Set the value of onResponse
     *
     * @return  self
     */
    public function onResponse(Closure $onResponse)
    {
        $this->onResponse = $onResponse;
        return $this;
    }

    public function rateLimit(int $maxCallsLimit = 120, int $secondsInterval = 60, ?string $apikey = null): self
    {
        try {
            if (null !== $apikey) {
                RateLimiter::byKey($apikey, $maxCallsLimit, $secondsInterval, $this->getConfigData()['rest']['resource']);
            } else {
                RateLimiter::byIP($maxCallsLimit, $secondsInterval, $this->getConfigData()['rest']['resource']);
            }
        } catch (RedisConnectionException $exc) {
            $this->error($exc->getMessage(), $exc->getCode());
        } catch (TooManyRequestException $exc) {
            $this->error($exc->getMessage(), $exc->getCode());
        } catch (Exception $exc) {
            $this->error($exc->getMessage(), $exc->getCode());
        }

        return $this;
    }

    /**
     * Body treated. Attention: it s will be updated the original body requested
     *
     * @param mixed $data
     * @return self
     */
    public function setBodyTreated($data): self
    {
        $this->body = $data;
        return $this;
    }

    public function setOriginalBodyTreated($data): self
    {
        $this->config['bodyOrigin'] = $data;
        return $this;
    }

    public function getOriginalBody(): array
    {
        return $this->config['bodyOrigin'] ?? [];
    }

    /**
     * Get the value of request
     */
    public function request(?string $key = null, $value = null, bool $merge = true)
    {
        if (null !== $value && $merge === false) {
            $this->body = [];
        }

        if (null !== $value && null !== $key) {
            $this->body[$key] = $value;
        }

        return null !== $key ? ($this->body[$key] ?? null) : $this->body;
    }

    public function setTypeOut(?string $typeOut): self
    {
        if (null === $typeOut) {
            return $this;
        }

        switch ($typeOut) {
            case 'json':
                header('Content-Type:application/json');
                break;
            case 'html':
                header('Content-Type: text/html; charset=utf-8');
                break;
            default:
                header("Content-Type: {$typeOut}; charset=utf-8");
                break;
        }

        return $this;
    }

    public function getRestResource(): string
    {
        return (string) ($this->getConfigData()['rest']['resource'] ?? '');
    }

    public function getRestAction(): string
    {
        return (string) ($this->getConfigData()['rest']['action'] ?? '');
    }

    public function getRestId(): ?int
    {
        return (int) ($this->getConfigData()['rest']['id'] ?? null);
    }

    /**
     * Validate the body and if fail, the Api will response with the error code defined.
     * @param mixed $keysToValidate key|type|msg
     * @return void
     */
    public function validateOrFail($keysToValidate = [])
    {
        Validate::validateOrFail($this, $keysToValidate);
    }
}
