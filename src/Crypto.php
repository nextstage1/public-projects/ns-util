<?php

namespace NsUtil;

use Exception;
use function mb_substr;

/**
 * @deprecated Use \NsUtil\Security\Crypto instead
 * @see \NsUtil\Security\Crypto
 * @package NsUtil
 */
class Crypto extends \NsUtil\Security\Crypto {}
