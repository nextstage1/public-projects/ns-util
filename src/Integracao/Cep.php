<?php

namespace NsUtil\Integracao;

use NsUtil\Helper;
use NsUtil\Resources\CepResource;

use function NsUtil\json_decode;

/**
 * Class Cep
 * Handles the retrieval of address information based on CEP (Brazilian postal code).
 */
class Cep
{
    private static string $apiUrl = 'https://viacep.com.br/ws/';

    /**
     * Retrieves address information for a given CEP.
     *
     * @param string $cep The CEP to search for.
     * @param string $lang The language for the response.
     * @return array The address information.
     * @throws \InvalidArgumentException If the CEP is invalid.
     * @throws \Exception If the API cannot be accessed or the CEP is not found.
     */
    public static function getAddress(string $cep, string $lang = 'pt'): array
    {
        $cep = preg_replace('/[^0-9]/', '', $cep);

        if (strlen($cep) !== 8) {
            throw new \InvalidArgumentException('Invalid CEP.');
        }

        return self::__brasilApi($cep)->toArray($lang);
    }

    private static function __brasilApi(string $cep): CepResource
    {
        $url = "https://brasilapi.com.br/api/cep/v2/{$cep}";
        $response = Helper::myFileGetContents($url);
        $data = json_decode($response, true);
        return new CepResource(
            $data['cep'] ?? '',
            $data['street'] ?? '',
            $data['complement'] ?? '',
            $data['unit'] ?? '',
            $data['neighborhood'] ?? '',
            $data['city'] ?? '',
            $data['state'] ?? '',
            '', // region is not provided by the API
            $data['ibge'] ?? '',
            $data['gia'] ?? '',
            $data['ddd'] ?? '',
            $data['siafi'] ?? '',
            $data['location'] ?? []
        );
    }

    private static function __viacep(string $cep): CepResource
    {
        $url = "https://viacep.com.br/ws/{$cep}/json/";
        $response = Helper::myFileGetContents($url);
        $data = json_decode($response, true);
        return new CepResource(
            $data['cep'] ?? '',
            $data['street'] ?? '',
            $data['complement'] ?? '',
            $data['unit'] ?? '',
            $data['neighborhood'] ?? '',
            $data['city'] ?? '',
            $data['state'] ?? '',
            '', // region is not provided by the API
            $data['ibge'] ?? '',
            $data['gia'] ?? '',
            $data['ddd'] ?? '',
            $data['siafi'] ?? '',
            $data['location'] ?? []
        );
    }
}
