<?php

namespace NsUtil\Integracao;

use Exception;
use NsUtil\Helper;
use NsUtil\EnvFile;
use function NsUtil\env;
use function NsUtil\json_decode;

class Dify
{
    private string $API_KEY;
    private string $BASE_URL;

    public function __construct()
    {
        $this->API_KEY = env('DIFY_APIKEY');
        $this->BASE_URL = env('DIFY_BASEURL');

        if (null === $this->API_KEY) {
            throw new Exception('API KEY not found to use Dify');
        }
    }

    private function fetch(string $route, string $text, string $method = 'POST'): string
    {
        $url = "{$this->BASE_URL}/{$route}";
        $header = [
            "Authorization: Bearer {$this->API_KEY}",
            'Content-Type: application/json',
        ];
        $ssl = true;
        $timeout = 30;
        $params = [
            'inputs' => [],
            'query' => $text,
            'response_mode' => 'streaming',
            'user' => 'develop-bot',
            'conversation_id' => '',
            'files' => []
        ];
        $response =  Helper::curlCall($url, $params, $method, $header, $ssl, $timeout);

        $data = json_decode($response->content, true);

        $lines = explode("\n", $response->content);
        $response = '';
        foreach ($lines as $line) {
            if (strlen($line) <= 5) {
                continue;
            }
            $line = str_replace('data: ', '', $line);
            $data = json_decode($line, true);
            // "data: {"event": "agent_message", "conversation_id": "d1bc9e5a-2451-4f3d-a5c6-934b38c8e7a2", "message_id": "b7410d11-f0e6-48d0-baf0-963f86a5c69f", "created_at": 1734087420, "task_id": "16462150-9df2-4a2d-839a-7e140d2208a1", "id": "b7410d11-f0e6-48d0-baf0-963f86a5c69f", "answer": "TITLE"}"
            if ($data['event'] === 'agent_message') {
                $response .= str_replace('"', '', $data['answer']);
            }
        }

        //         "data: {"event": "agent_thought", "conversation_id": "1bc3c7d6-c6d0-4d45-a5a3-6660c25a50b9", "message_id": "8a358c47-d30b-4c8c-8c81-6d96feee425c", "created_at": 1734087073, "task_id": "4b9a01e4-cfa1-4350-9633-ee940a1449b2", "id": "9d17d450-9090-4fbf-95e1-584dd8e94b7f", "position": 1, "thought": "", "observation": "", "tool": "", "tool_labels": {}, "tool_input": "", "message_files": []}

        // data: {"event": "agent_message", "conversation_id": "1bc3c7d6-c6d0-4d45-a5a3-6660c25a50b9", "message_id": "8a358c47-d30b-4c8c-8c81-6d96feee425c", "created_at": 1734087073, "task_id": "4b9a01e4-cfa1-4350-9633-ee940a1449b2", "id": "8a358c47-d30b-4c8c-8c81-6d96feee425c", "answer": "Ok"}

        // data: {"event": "agent_message", "conversation_id": "1bc3c7d6-c6d0-4d45-a5a3-6660c25a50b9", "message_id": "8a358c47-d30b-4c8c-8c81-6d96feee425c", "created_at": 1734087073, "task_id": "4b9a01e4-cfa1-4350-9633-ee940a1449b2", "id": "8a358c47-d30b-4c8c-8c81-6d96feee425c", "answer": ". Por favor, forne\u00e7a a descri\u00e7\u00e3o do erro.  Estou pronto para analisar"}

        // data: {"event": "agent_message", "conversation_id": "1bc3c7d6-c6d0-4d45-a5a3-6660c25a50b9", "message_id": "8a358c47-d30b-4c8c-8c81-6d96feee425c", "created_at": 1734087073, "task_id": "4b9a01e4-cfa1-4350-9633-ee940a1449b2", "id": "8a358c47-d30b-4c8c-8c81-6d96feee425c", "answer": " e identificar as poss\u00edveis causas.  Ap\u00f3s a sua descri\u00e7\u00e3o,  fornecere"}

        // data: {"event": "agent_message", "conversation_id": "1bc3c7d6-c6d0-4d45-a5a3-6660c25a50b9", "message_id": "8a358c47-d30b-4c8c-8c81-6d96feee425c", "created_at": 1734087073, "task_id": "4b9a01e4-cfa1-4350-9633-ee940a1449b2", "id": "8a358c47-d30b-4c8c-8c81-6d96feee425c", "answer": "i um resumo seguindo o formato solicitado.\n"}

        // data: {"event": "agent_thought", "conversation_id": "1bc3c7d6-c6d0-4d45-a5a3-6660c25a50b9", "message_id": "8a358c47-d30b-4c8c-8c81-6d96feee425c", "created_at": 1734087073, "task_id": "4b9a01e4-cfa1-4350-9633-ee940a1449b2", "id": "9d17d450-9090-4fbf-95e1-584dd8e94b7f", "position": 1, "thought": "Ok. Por favor, forne\u00e7a a descri\u00e7\u00e3o do erro.  Estou pronto para analisar e identificar as poss\u00edveis causas.  Ap\u00f3s a sua descri\u00e7\u00e3o,  fornecerei um resumo seguindo o formato solicitado.\n", "observation": "", "tool": "", "tool_labels": {}, "tool_input": "", "message_files": []}

        // data: {"event": "message_end", "conversation_id": "1bc3c7d6-c6d0-4d45-a5a3-6660c25a50b9", "message_id": "8a358c47-d30b-4c8c-8c81-6d96feee425c", "created_at": 1734087073, "task_id": "4b9a01e4-cfa1-4350-9633-ee940a1449b2", "id": "8a358c47-d30b-4c8c-8c81-6d96feee425c", "metadata": {"usage": {"prompt_tokens": 164, "prompt_unit_price": "0.00", "prompt_price_unit": "0.000001", "prompt_price": "0E-7", "completion_tokens": 18, "completion_unit_price": "0.00", "completion_price_unit": "0.000001", "completion_price": "0E-7", "total_tokens": 182, "total_price": "0E-7", "currency": "USD", "latency": 3.4609977930012974}}, "files": null}


        return $response;
    }

    public function sendMessage(string $message): string
    {
        return $this->fetch('chat-messages', $message);
    }
}
