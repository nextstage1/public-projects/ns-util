<?php

namespace NsUtil\Integracao\Gitlab;

/**
 * Class to handle GitLab issue time estimates
 * 
 * This class provides methods to convert between time estimates and labels
 * for GitLab issues. It handles different difficulty levels from minimal
 * to high, with corresponding time ranges.
 */
class Estimate
{

    private const DEFAULT_CONFIG = [
        'pt_BR' => [
            'MINIMAL' => 'Dificuldade: nenhuma (-10m)',
            'VERY_LOW' => 'Dificuldade: baixíssima (até 1hr)',
            'LOW' => 'Dificuldade: baixa (1-4hs)',
            'MEDIUM' => 'Dificuldade: média (4-8hs)',
            'HIGH' => 'Dificuldade: alta (+8hs)'
        ],
        'en' => [
            'MINIMAL' => 'Estimate: Minimal (-15m)',
            'VERY_LOW' => 'Estimate: Very low (-1hr)',
            'LOW' => 'Estimate: Low (1-4hs)',
            'MEDIUM' => 'Estimate: Medium (4-8hs)',
            'HIGH' => 'Estimate: High (+8hs)'
        ],
    ];
    /**
     * Get label name based on estimated time in seconds
     *
     * @param integer $estimate Time estimate in seconds
     * @param array|null $config Optional custom configuration array to override default labels
     * @return string|null Label corresponding to the time estimate, or null if no match
     */
    public static function getEstimateLabelByEstimateTime(int $estimate, string $language = 'pt_BR', array $config = [])
    {
        // validar language
        if (!array_key_exists($language, self::DEFAULT_CONFIG)) {
            $language = 'pt_BR';
        }

        $config = array_merge(self::DEFAULT_CONFIG[$language], $config);

        switch (true) {
            case $estimate > 0 && $estimate <= (15 * 60):
                $label = $config['MINIMAL'];
                break;
            case $estimate <= (60 * 60):
                $label = $config['VERY_LOW'];
                break;
            case $estimate <= (60 * 60 * 4):
                $label = $config['LOW'];
                break;
            case $estimate <= (60 * 60 * 8):
                $label = $config['MEDIUM'];
                break;
            case $estimate > (60 * 60 * 8):
                $label = $config['HIGH'];
                break;
            default:
                $label = null;
        }

        return $label;
    }

    /**
     * Convert a label string to time estimate in seconds
     *
     * @param string $label The difficulty label to convert
     * @return integer Time estimate in seconds
     * 
     * Conversion rules:
     * - "10m" -> 600 seconds
     * - "15m" -> 900 seconds
     * - "1hr" -> 1800 seconds
     * - "1-4hs" -> 9000 seconds (2.5 hours)
     * - "4-8hs" -> 21600 seconds (6 hours)
     * - default -> 43200 seconds (12 hours)
     */
    public static function getEstimateByLabel(string $label)
    {
        switch (true) {
            case stripos($label, '10m'):
                $estimate = 10;
                break;
            case stripos($label, '15m'):
                $estimate = 15;
                break;
            case stripos($label, '1hr'):
                $estimate = 30;
                break;
            case stripos($label, '1-4hs'):
                $estimate = 60 * 2.5;
                break;
            case stripos($label, '4-8hs'):
                $estimate = 60 * 6;
                break;
            default:
                $estimate = 12 * 60;
        }

        // seconds
        return $estimate * 60;
    }
}
