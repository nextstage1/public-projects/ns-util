<?php

namespace NsUtil\Integracao\Gitlab;

use Exception;
use NsUtil\Exceptions\ModelNotFoundException;
use NsUtil\Helper;
use NsUtil\Integracao\Gitlab;
use NsUtil\Services\Redis;

use function NsUtil\dd;
use function NsUtil\json_decode;

/**
 * Class to handle GitLab Issues operations
 * Manages issue data, updates, comments and relationships with GitLab API
 */
class Issue
{

    private $issue = [];
    private $client;
    private $iid;
    private $defaultNotesInternal = false;


    /**
     * Creates a new Issue instance
     * 
     * @param int|string $idProject GitLab project ID
     * @param int|string $issue_iid Issue IID within the project
     * @param Gitlab $client GitLab API client instance
     * @param array $issueData Optional initial issue data
     */
    public function __construct($idProject, $issue_iid, Gitlab $client, array $issueData = [])
    {
        $this->client = $client;
        $this->client->setIdProject($idProject);
        $this->iid = $issue_iid;

        $issueData['projectId'] ??= $idProject;
        $issueData['project_id'] ??= $idProject;
        $issueData['issue_id'] ??= $issue_iid;
        $issueData['iid'] ??= $issue_iid;
        if (Redis::checkHasServer()) {
            $redisKey = "gitlab-issue-{$issueData['project_id']}-{$issueData['iid']}";
            $this->issue = Redis::cacheArray(
                $redisKey,
                fn() => $this->load()->getData(),
                Redis::MINUTE
            );
        } else {
            $this->load();
        }
        $this->setIssue($issueData, true);
    }
    public function setDefaultNotesInternal(bool $defaultNotesInternal): self
    {
        $this->defaultNotesInternal = $defaultNotesInternal;
        return $this;
    }

    /**
     * Loads issue data from GitLab API
     * 
     * @throws ModelNotFoundException When issue is not found
     * @return self
     */
    public function load()
    {
        try {
            $data = $this->client->read(
                "projects/" . $this->client->getIdProject() . "/issues",
                $this->iid
            );

            if ([] === $data || null === $data) {
                throw new Exception('Issue not found: ' . $this->iid . ' on project ' . $this->client->getIdProject());
            }

            $this->setIssue($data, false);
        } catch (Exception $exc) {
            throw new ModelNotFoundException($exc->getMessage());
        }

        return $this;
    }

    /**
     * Returns the current issue data
     * 
     * @return array
     */
    public function getData()
    {
        return $this->issue;
    }

    /**
     * Updates issue data in GitLab
     * 
     * @param array $data Data to update
     * @return self
     */
    public function update(array $data)
    {
        $this->client->issueEdit($this->iid, $data);
        return $this->load();
    }

    /**
     * Sets issue data locally
     * 
     * @param array $data Issue data to set
     * @param boolean $merge Whether to merge with existing data
     * @return self
     */
    public function setIssue(array $data, bool $merge = true): self
    {
        $this->issue = $merge
            ? array_merge($this->issue, $data)
            : $data;

        // set correction labels from webhooks
        if (isset($this->issue['labels'][0]['title'])) {
            $actualLabels = $this->issue['labels'] ?? [];
            $this->issue['labels'] = array_map(fn($label) => (string) $label['title'], $actualLabels);
        }

        // set project data
        $this->setProjectDataOnIssue();

        return $this;
    }

    private function setProjectDataOnIssue(): void
    {
        // Esta cacheado no service com Redis caso exista
        $this->issue['project'] = $this->client->projectRead();
    }

    /**
     * Updates issue labels in GitLab
     * 
     * @param array $labels Labels to set
     * @param boolean $merge Whether to merge with existing labels
     * @return self
     */
    public function updateLabels(array $labels, bool $merge = false, array $removeByPrefix = []): self
    {
        if ($merge) {
            $this->load();
            $labelsLoaded = json_decode($this->getData()['labels'], true);
            // remvoer by preefixo
            if (count($removeByPrefix) > 0) {
                foreach ($labelsLoaded as $key => $label) {
                    foreach ($removeByPrefix as $prefix) {
                        // 0 para obter somente no inicio, pois a regra eh prefixo.
                        $has = stripos($label, $prefix);
                        if ($has === 0 && $has !== false) {
                            unset($labelsLoaded[$key]);
                        }
                    }
                }
            }
            // merge
            $labels = array_merge($labelsLoaded, $labels);
        }

        $this->client->issueEdit(
            $this->issue['iid'],
            ['labels' => implode(',', array_values($labels))]
        );
        return $this->load();
    }
    /**
     * Adds a comment to the issue
     * 
     * @param string $comments Comment text to add
     * @return self
     */
    public function addComments(string $comments, ?bool $internal = null): self
    {
        $internal ??= $this->defaultNotesInternal;
        $this->client->addComments(
            $this->issue['iid'],
            $comments,
            null,
            $internal
        );
        return $this;
    }

    /**
     * Sets time estimate for the issue
     * 
     * @param int $estimateInSeconds Time estimate in seconds
     * @return self
     */
    public function setEstimate(int $estimateInSeconds)
    {
        $this->client->setEstimate($this->issue['iid'], $estimateInSeconds . 's');
        return $this->load();
    }

    /**
     * Links this issue to another issue
     * 
     * @param integer $targetIssueIid Target issue IID to link to
     * @param int|string|null $targetProjectId Project ID containing target issue
     * @return self
     */
    public function setLinkedIssue(int $targetIssueIid, $targetProjectId): self
    {
        $targetProjectId ??= $this->issue['project_id'];

        $resource = 'projects/' . $this->issue['project_id']
            . '/issues/' . $this->issue['iid']
            . '/links';

        $payload = [
            "link_type" => "relates_to",
            "target_project_id" => (string) $targetProjectId,
            'target_issue_iid' => (string) $targetIssueIid
        ];

        try {
            $this->client->fetch($resource, $payload, 'POST');
        } catch (Exception $exc) {
            // 409 diz que já existe
            if ($exc->getCode() !== 409) {
                throw new Exception($exc->getMessage(), $exc->getCode());
            }
        }

        return $this;
    }

    public function setMilestoneByLabel(): self
    {
        $this->load();

        // Obter a label Milestone: <milestone>
        $milestoneLabel = array_values(array_filter($this->getData()['labels'], function ($label) {
            return strpos($label, 'Milestone:') === 0;
        }))[0] ?? null;

        if (null === $milestoneLabel) {
            return $this;
        }

        $milestoneName = trim(explode('Milestone: ', (string) $milestoneLabel)[1]);

        // Obter Milestone no Group
        $listOfMilestones = $this->client->fetch("groups/" . (int) $this->getData()['project']['namespace']['id'] . "/milestones", ['title' => $milestoneName]);
        $milestone = ($listOfMilestones->status === 200 && isset($listOfMilestones->content[0]['id']))
            ? $listOfMilestones->content[0]
            : $this->client->milestoneAdd($milestoneName, '', 'groups');

        // Check if milestone is closed
        $username = $this->getData()['assignee']['username'] ?? 'not.assigned';
        if ($milestone['state'] === 'closed') {
            $this->addComments("@{$username} The milestone '{$milestone['title']}' is closed and does not allow new issues", true);
            $this->update(['milestone_id' => 0]);
            return $this;
        }

        // Adicionar milestone a issue
        if (!Helper::compareString($this->getData()['milestone']['id'] ?? 0, $milestone['id'] ?? 0)) {
            $data = $this->client->issueEdit($this->getData()['iid'], ['milestone_id' => $milestone['id']]);
            $this->setIssue(json_decode($data, true));
        }

        return $this;
    }

    public function alterStateOfRelatedMilestone(string $state): self
    {
        // validate state: only activate or close
        if (!in_array($state, ['activate', 'close'])) {
            throw new Exception('Invalid state');
        }

        $groupId = $this->getData()['milestone']['group_id'] ?? null;

        if (!Helper::compareString($this->getData()['milestone']['state'], $state)) {
            $this->client->fetch(
                "groups/$groupId/milestones/" . $this->getData()['milestone']['id'],
                ['state_event' => $state],
                'PUT'
            );
        }

        return $this;
    }

    public function getNotes(string $sort = 'asc', int $page = 1, int $perPage = 100): array
    {
        // validat sort: asc ou desc
        if (!in_array($sort, ['asc', 'desc'])) {
            throw new Exception('Invalid sort');
        }

        $list = $this->client->fetch(
            "projects/" . $this->getData()['project_id'] . "/issues/" . $this->getData()['iid'] . "/notes",
            ['sort' => $sort, 'per_page' => $perPage, 'page' => $page]
        );
        return $list->content ?? [];
    }
}
