<?php

namespace NsUtil\Integracao\Gitlab;

class Variable
{
    private $items = [];

    public function __construct()
    {
    }

    public function add(string $key, string|int $value, string $description = '')
    {
        $this->items[$key] =  [
            "key" =>  $key,
            "value" =>  $value,
            "description" => $description
        ];
    }

    public function getItems(): array
    {
        return $this->items;
    }
}
