<?php

namespace NsUtil\OFXParser\Contracts;

use NsUtil\OFXParser\DTOs\OFXData;


interface OFXParserInterface
{
    public function parse(string $content): OFXData;
}
