<?php

namespace NsUtil\OFXParser\DTOs;

class BankTransaction
{
    /** @var string */
    public $type;

    /** @var \DateTime */
    public $date;

    /** @var float */
    public $amount;

    /** @var string */
    public $fitid;

    /** @var string */
    public $memo;

    public function __construct(
        string $type,
        \DateTime $date,
        float $amount,
        string $fitid,
        string $memo
    ) {
        $this->type = $type;
        $this->date = $date;
        $this->amount = $amount;
        $this->fitid = $fitid;
        $this->memo = $memo;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getFitid(): string
    {
        return $this->fitid;
    }

    public function getMemo(): string
    {
        return $this->memo;
    }
}
