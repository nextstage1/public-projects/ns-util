<?php

namespace NsUtil\OFXParser\DTOs;

class OFXData
{
    /** @var string */
    public $bankId;

    /** @var string */
    public $accountId;

    /** @var BankTransaction[] */
    public $transactions;

    /**
     * @param BankTransaction[] $transactions
     */
    public function __construct(
        string $bankId,
        string $accountId,
        array $transactions
    ) {
        $this->bankId = $bankId;
        $this->accountId = $accountId;
        $this->transactions = $transactions;
    }

    public function getBankId(): string
    {
        return $this->bankId;
    }

    public function getAccountId(): string
    {
        return $this->accountId;
    }

    /**
     * @return BankTransaction[]
     */
    public function getTransactions(): array
    {
        return $this->transactions;
    }
}
