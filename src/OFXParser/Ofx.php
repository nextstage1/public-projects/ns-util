<?php

namespace NsUtil\OFXParser;

use NsUtil\OFXParser\Contracts\OFXParserInterface;
use NsUtil\OFXParser\DTOs\BankTransaction;
use NsUtil\OFXParser\DTOs\OFXData;
use NsUtil\OFXParser\Parser\Parser;
use NsUtil\OFXParser\Parser\Utils;

/**
 * Example of use:
 * 
 * $parser = new Parser();
 * $ofxData = $parser->parse(file_get_contents('arquivo.ofx'));
 * 
 * foreach ($ofxData->getTransactions() as $transaction) {
 *     echo $transaction->getDescription() . ': ' . $transaction->getAmount() . PHP_EOL;
 * }
 */


class Ofx implements OFXParserInterface
{
    /**
     * Parse OFX file content and return typed object
     */
    public function parse(string $content): OFXData
    {
        if (empty($content)) {
            throw new \Exception('Content is empty');
        }
        $xml = (new Parser())->loadFromString($content);
        return $this->extractData($xml);
    }

    public static function parseFromFile(string $filePath): OFXData
    {
        if (!file_exists($filePath)) {
            throw new \Exception('File not found');
        }
        $content = file_get_contents($filePath);
        return self::parseFromString($content);
    }

    public static function parseFromString(string $content): OFXData
    {
        return (new self())->parse($content);
    }

    /**
     * Extract data from XML to typed object
     */
    private function extractData(\SimpleXMLElement $xml): OFXData
    {
        $bankTransactions = [];

        if (isset($xml->BANKMSGSRSV1->STMTTRNRS->STMTRS->BANKTRANLIST->STMTTRN)) {
            foreach ($xml->BANKMSGSRSV1->STMTTRNRS->STMTRS->BANKTRANLIST->STMTTRN as $transaction) {
                $bankTransactions[] = new BankTransaction(
                    (string)$transaction->TRNTYPE,
                    Utils::createDateTimeFromStr((string)$transaction->DTPOSTED),
                    (float) Utils::createAmountFromStr((string)$transaction->TRNAMT),
                    (string)$transaction->FITID,
                    (string)$transaction->MEMO
                );
            }
        }

        return new OFXData(
            (string)$xml->BANKMSGSRSV1->STMTTRNRS->STMTRS->BANKACCTFROM->BANKID,
            (string)$xml->BANKMSGSRSV1->STMTTRNRS->STMTRS->BANKACCTFROM->ACCTID,
            $bankTransactions
        );
    }
}
