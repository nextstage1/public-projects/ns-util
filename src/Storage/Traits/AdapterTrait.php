<?php
namespace NsUtil\Storage\Traits;

use Exception;
use League\Flysystem\Filesystem;

trait AdapterTrait
{
    private Filesystem $fs;
    private $adapter;
    private string $bucket;

    public function setBucket($bucket): void
    {
        $this->bucket = $bucket;
    }

    public function getFs(): Filesystem
    {
        return $this->fs;
    }

    public function getAdapter()
    {
        return $this->adapter;
    }


    private function checkIfAdapterIsRead()
    {
        if ($this->adapter === null) {
            throw new Exception('NSUtil Storage: Error on set the adapter');
        }

    }

}