<?php

namespace NsUtil\Storage;

use Aws\S3\S3Client;
use League\Flysystem\Filesystem;
use NsUtil\Storage\Adapters\AdapterInterface;
use NsUtil\Storage\Traits\AdapterTrait;

/**
 * @deprecated This class is marked as deprecated since version 1.66 and will be removed in version 2.0
 *             Use \NsUtil\Storage\Adapters\S3Adapter() insted.
 *
 */
class S3 implements AdapterInterface
{
    use AdapterTrait;

    public $endpoint;
    private $key, $secret, $region, $version, $s3Client;

    public function __construct($key, $secret, $bucket, $region = 'us-east-2', $version = '2006-03-01')
    {
        $this->key = $key;
        $this->secret = $secret;
        $this->bucket = $bucket;
        $this->region = $region;
        $this->version = $version;
        $this->init();
        $this->checkIfAdapterIsRead();
    }

    public function setBucket($bucket): void
    {
        $this->bucket = $bucket;
        $this->init();
    }

    public function getFs(): Filesystem
    {
        return $this->fs;
    }

    public function getAdapter()
    {
        return $this->adapter;
    }

    private function init()
    {
        $this->s3Client = new S3Client([
            'credentials' => [
                'key' => $this->key,
                'secret' => $this->secret
            ],
            'region' => $this->region,
            'version' => $this->version,
        ]);
        $this->endpoint = "https://" . $this->bucket . ".s3-" . $this->region . ".amazonaws.com";

        $adapters = [
            \League\Flysystem\AwsS3v3\AwsS3Adapter::class,
            \League\Flysystem\AwsS3V3\AwsS3V3Adapter::class
        ];
        foreach ($adapters as $adapter) {
            if (class_exists($adapter)) {
                $this->adapter = new $adapter($this->s3Client, $this->bucket);
            }
        }

        $this->fs = new Filesystem($this->adapter);
    }

    public function getUrlSigned(string $item, int $minutes = 10, array $options = []): string
    {
        try {
            $cmd = $this->s3Client->getCommand('GetObject', array_merge(
                $options,
                [
                    'Bucket' => $this->bucket,
                    'Key' => $item
                ]
            ));
            $request = $this->s3Client->createPresignedRequest($cmd, '+' . $minutes . ' minutes');
            return (string) $request->getUri();
        } catch (\Exception $exc) {
            return 'Error on get url signed to item: ' . $exc->getMessage();
        }
    }

    public function temporaryUrl(string $item, int $minutes = 10, array $options = []): string
    {
        return $this->getUrlSigned($item, $minutes);
    }
}
