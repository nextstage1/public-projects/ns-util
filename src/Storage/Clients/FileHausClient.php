<?php

namespace NsUtil\Storage\Clients;

use NsUtil\Helper;
use NsUtil\Http\Http;
use function NsUtil\env;
use function NsUtil\now;
use NsUtil\Helpers\Date;
use NsUtil\Security\Crypto;
use NsUtil\Services\JWTService;
use function NsUtil\json_decode;
use NsUtil\Exceptions\FileNotFoundException;
use NsUtil\Services\Redis;
use NsUtil\Storage\Exceptions\AuthException;

class FileHausClient
{

    private string $baseurl;
    private string $username;
    private string $password;
    private string $access_token;
    private $access_token_expires_at = 0;
    private array $fileInOperation = [];
    private string $keyToPersistToken;

    public function __construct(string $baseurl, string $username, string $password)
    {
        $this->baseurl = $baseurl;
        $this->username = $username;
        $this->password = $password;
        $this->keyToPersistToken = 'nsfh-' . hash("sha256", "{$this->username}-{$this->password}-token");
        $this->access_token = $this->getAccessToken();
    }

    /**
     * Return a valid access token from Redis
     * @return object
     */
    private function authByRedis(): object
    {
        $data = Redis::cacheArray($this->keyToPersistToken, fn() => json_decode($this->authGetAccessToken(), true), Redis::MINUTE * 55);
        $data = json_decode($data);
        $this->access_token = (string) $data->access_token;
        return $data;
    }

    /**
     * Return a valid access token from File
     * @return object
     */
    private function authByFile(): object
    {
        // read if exists and is valid
        $crypto = new Crypto($this->keyToPersistToken);
        if (file_exists($this->keyToPersistToken)) {
            $data = json_decode($crypto->decrypt(file_get_contents($this->keyToPersistToken)));
            $valid = new \DateTime($data->valid_at ?? '1999-01-01', new \DateTimeZone('UTC'));
            $now = now('UTC')->sub('5 minutes')->timestamp();
            if ($valid->getTimestamp() > $now) {
                $this->access_token = (string) $data->access_token;
                return $data;
            }
        }
        // get new access token and persist     
        $data = $this->authGetAccessToken();
        $stringToSave = $crypto->encrypt(json_encode($data));
        Helper::saveFile($this->keyToPersistToken, '', $stringToSave, 'OVERWRITE');
        chmod($this->keyToPersistToken, 0777);
        $this->access_token = (string) $data->access_token;

        return $data;
    }

    /**
     * Return a valid access token from Redis or File
     * @return object
     */
    private function auth(): object
    {
        return Redis::checkHasServer() ? $this->authByRedis() : $this->authByFile();
    }

    /**
     * Get a new access token on server
     * @return object
     */
    private function authGetAccessToken(): object
    {
        // Proceed to get a new token
        $url = "{$this->baseurl}/api/login";
        $header = ['Content-Type: application/json'];
        $header[] = 'Authorization: Basic ' . base64_encode("{$this->username}:{$this->password}");
        $ssl = (bool) env('FILEHAUS_SSL', true);
        $response =  Http::call($url, ['minutes' => 60], 'POST', $header, $ssl);

        // Throw exception if not success
        if ($response->getHttpCode() !== 200) {
            throw new AuthException($response->getBodyAsArray()['error'] ?? 'Error', $response->getHttpCode());
        }

        return json_decode($response->getBodyAsArray()['content']);
    }

    public function getAccessToken(): string
    {
        $this->auth();
        return $this->access_token;
    }


    public function fetch(string $resource, array $params = [], string $method = 'GET', int $timeout = 30)
    {
        $this->auth();

        $url = "{$this->baseurl}/{$resource}";
        $header = [
            'Content-Type: application/json',
            "Authorization: Bearer {$this->access_token}"
        ];
        $ssl = (bool) env('FILEHAUS_SSL', true);

        $response =  Http::call($url, $params, $method, $header, $ssl, $timeout);

        // Nesses casos tudo eh erro
        if ($response->getErrorCode() > 399) {
            throw new \Exception($response->getError(), $response->getErrorCode());
        }

        return $response;
    }

    public function list(?string $search = null, ?string $path = null, int $limit = 100): array
    {
        $params = [];
        if ($search) {
            $params['search'] = $search;
        }
        if ($path) {
            $params['path'] = $path;
        }
        $params['limit'] = $limit;
        $response =  $this->fetch('file', $params);
        $list = $response->getBodyAsArray()['content'] ?? [];
        return $list;
    }

    public function getMetadata(string $path): array
    {
        $response =  $this->fetch("file", ['path' => $path]);
        $this->fileInOperation = $response->getBodyAsArray()['content'][0] ?? [];
        if (isset($this->fileInOperation['id'])) {
            return $this->fileInOperation;
        }
        throw new FileNotFoundException($path);
    }

    public function has(string $path): bool
    {
        try {
            $this->getMetadata($path);
            return true;
        } catch (FileNotFoundException $e) {
            return false;
        }
    }

    public function rename(string $path, string $name): void
    {
        $file = $this->getMetadata($path);
        $idFile = $file['id'];
        $response =  $this->fetch("file/{$idFile}/rename", ['name' => $name]);
        $response->getBodyAsArray()['content'] ?? [];
    }

    public function move(string $path, string $newpath): void
    {
        $file = $this->getMetadata($path);
        $idFile = $file['id'];
        $this->fetch("file/{$idFile}/move", ['path' => $newpath]);
    }

    public function read(string $path): string
    {
        $file = $this->getMetadata($path);
        $idFile = $file['id'];
        $response = $this->fetch("file/{$idFile}/download");
        $body = $response->getBodyAsArray()['content']['body'] ?? null;
        if ($body) {
            return base64_decode($body);
        }
        throw new FileNotFoundException($path);
    }
    public function write(string $path, string $localfile, bool $overwrite = false): array
    {

        if (!file_exists($localfile)) {
            throw new FileNotFoundException("Local file not found: $localfile");
        }

        $params = ['path' => $path];
        $params['overwrite'] = $overwrite;

        // $curl = "curl -k -X POST --header 'Authorization: Bearer $this->access_token' -T $localfile "
        //     . $this->baseurl . '/api?'
        //     . http_build_query(['path' => $path]);

        $curl = "curl -k -X POST --header 'Authorization: Bearer {$this->access_token}' -F 'file=@$localfile' "
            . $this->baseurl . '/file?'
            . http_build_query($params);
        $response = shell_exec($curl);
        $ret = json_decode($response, true);

        if ($ret['status'] !== 201) {
            throw new \Exception("Failed to upload file: " . $ret['error']);
        }

        return $ret['content'];
    }

    public function delete(string $path): void
    {
        $file = $this->getMetadata($path);
        $idFile = $file['id'];
        $this->fetch("file/{$idFile}", [], 'DELETE');
    }

    public function temporaryUrl(string $path, ?int $minutes = null, bool $download = false, bool $isDir = false): string
    {

        if ($isDir) {
            $response = $this->fetch(
                "weblink",
                ['path' => $path, 'minutes' => $minutes ?? 15, 'download' => $download, 'permanent' => $minutes < 0],
                'POST'
            );
            return $response->getBodyAsArray()['content']['url'] ?? '';
        }

        // Auth data
        $auth = $this->auth();
        $privateKey = $auth->self_signed->private_key;
        $publicKey = $auth->self_signed->public_key;
        $url = $auth->self_signed->baseurl;

        // JWT Token
        $jwt = (new JWTService($privateKey))->generateToken([
            'path' => base64_encode($path),
            'download' => $download ? '1' : '0',
        ], now('UTC')->add("$minutes minutes"));

        // Params
        $params = http_build_query([
            'wia' => $publicKey,
            'tkn' => $jwt
        ]);

        // URL
        return "{$url}?{$params}";
    }

    public function createDir(string $path): array
    {
        $response = $this->fetch("directory", [
            'path' => $path
        ], 'POST');
        return $response->getBodyAsArray()['content'] ?? [];
    }
}
