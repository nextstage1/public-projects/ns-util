<?php

namespace NsUtil\Storage;

use Closure;
use Exception;
use League\Flysystem\Filesystem;

use NsUtil\Exceptions\FileNotFoundException;
use function NsUtil\dd;

class NsFilesystem
{
    private $fs;
    private $adapter;
    private Closure $temporaryUrlClosure;
    public function __construct($adapter, ?Closure $temporaryUrl = null, array $config = [])
    {
        $this->fs = new Filesystem($adapter, $config);
        $this->adapter = $adapter;
        $this->temporaryUrlClosure = $temporaryUrl;
    }

    /**
     * Get the value of adapter
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    public function __call($method, $args)
    {

        if (method_exists($this->fs, $method)) {
            return $this->fs->$method(...$args);
        }


        $compatibility = [
            'rename' => ['rename', 'move'],
            'update' => ['update', 'write'],
            'put' => ['put', 'write'],
            'updateStream' => ['updateStream', 'writeStream'],
            'putStream' => ['putStream', 'writeStream'],
            'getTimestamp' => ['getTimestamp', 'lastModified'],
            'has' => ['has', 'fileExists'],
            'getMimetype' => ['getMimetype', 'mimeType'],
            'getSize' => ['getSize', 'fileSize'],
            'getVisibility' => ['getVisibility', 'visibility']
        ];

        // Adiciona inversos automaticamente para evitar repetição
        foreach ($compatibility as $key => $values) {
            foreach ($values as $value) {
                $compatibility[$value][] = $key;
            }
        }

        if (isset($compatibility[$method])) {
            foreach ($compatibility[$method] as $methodMapped) {
                if (method_exists($this->fs, $methodMapped)) {
                    return $this->fs->$methodMapped(...$args);
                }
            }
        }

        throw new Exception("Method $method not found!");
    }



    public function listContents($directory = '', $recursive = false)
    {
        $itens = $this->fs->listContents($directory, $recursive);
        $type = gettype($itens);

        switch ($type) {
            // version 1.0
            case 'array':
                break;
            // version 2.0+
            case 'object':
                $itens = $itens
                    // ->filter(fn($attributes) => $attributes->isFile())
                    ->map(fn($attributes) => $attributes->jsonSerialize())
                    ->toArray();
                break;
            default:
                $itens = [];
                break;
        }

        return array_map(
            fn($item) => $item['type'] === 'file'
                ? array_merge($item, [
                    'filename' => $item['path'] ?? '',
                    'timestamp' => $item['last_modified'] ?? $item['lastModified'] ?? '',
                    'size' => $item['file_size'] ?? $item['fileSize'] ?? '',
                    'storageclass' => $item['extra_metadata']['StorageClass'] ?? '',
                    'etag' => $item['extra_metadata']['ETag'] ?? $item['extraMetadata'] ?? '',
                    'visibility' => $item['visibility'] ?? '',
                    'mime' => $item['mimeType'] ?? ''
                ])
                : [
                    'dirname' => $item['path'] ?? '',
                    'basename' => $item['path'] ?? '',
                    'filename' => $item['path'] ?? '',
                ],
            $itens
        );
    }

    public function temporaryUrl(string $item, int $minutes = 10, array $options = []): string
    {
        return is_callable($this->temporaryUrlClosure)
            ? call_user_func($this->temporaryUrlClosure, $item, $minutes, $options)
            : 'URL not disponibile in this system';
    }

    public function upload(string $localFile, string $remotePath, string $visibility = 'private'): string
    {
        $localFile = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $localFile);

        if (!file_exists($localFile)) {
            throw new FileNotFoundException("File $localFile not found!");
        }

        $content = file_get_contents($localFile);
        $filename = trim($remotePath, '/')
            . '/'
            . basename($localFile);

        $this->put($filename, $content);

        if ($visibility === 'public') {
            $this->setVisibility($filename, $visibility);
        }

        return $filename;
    }

    public function setCors(
        array $allowedMethods = ['GET', 'OPTIONS'],
        array $allowedHeaders = ['Content-Type', 'Access-Control-Allow-Origin'],
        array $allowedOrigins = ['*'],
        int $maxAge = 3600
    ): void {
        call_user_func($this->adapter->setCors, $allowedMethods, $allowedHeaders, $allowedOrigins, $maxAge);
    }
}
