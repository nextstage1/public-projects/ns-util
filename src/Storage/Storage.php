<?php

namespace NsUtil\Storage;

use Exception;
use NsUtil\Storage\Adapters\FileHausAdapter;
use NsUtil\Storage\Adapters\S3Adapter;
use NsUtil\Storage\Adapters\LocalAdapter;
use NsUtil\Storage\Adapters\SambaAdapter;

use function NsUtil\env;

class Storage
{
    public static function getDrive(?string $bucketName = null, ?string $drive = null): NsFilesystem
    {

        $drive ??= env('STORAGE_DRIVE', 'STORAGE_DRIVE_NOT_DEFINED');
        $bucketName ??= env('BUCKET_NAME', 'BUCKET_NAME_NOT_DEFINED');
        switch (mb_strtolower($drive)) {
            case 'local':
                $st = new LocalAdapter($bucketName);
                break;
            case 's3':
                $st = new S3Adapter(
                    env('AWS_KEY', 'S3_KEY_NOT_DEFINED'),
                    env('AWS_SECRET', 'S3_SECRET_NOT_DEFINED'),
                    $bucketName,
                    env('S3_REGION', 'us-east-1'),
                    env('S3_VERSION', '2006-03-01')
                );
                break;
            case 'samba':
                $st = new SambaAdapter(
                    env('SMB_SERVICE'),
                    env('SMB_USERNAME'),
                    env('SMB_PASSWORD'),
                    env('SMB_DOMAIN', ''),
                    env('SMB_VERSION', ''),
                    env('SMB_TMP_PATH', '/tmp')
                );
                $st->setBucket($bucketName);
                break;
            case 'filehaus':
                $st =  new FileHausAdapter(
                    env('FILEHAUS_URL'),
                    env('FILEHAUS_USERNAME'),
                    env('FILEHAUS_PASSWORD'),
                    $bucketName
                );
                break;
            default:
                throw new Exception("Drive $drive not enabled");
        }

        return new NsFilesystem(
            $st->getAdapter(),
            fn(string $item, int $minutes = 10, array $options = []) => $st->temporaryUrl($item, $minutes, $options)
        );
    }
}
