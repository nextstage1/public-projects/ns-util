<?php

namespace NsUtil\Storage\Adapters\Samba;

use ArrayIterator;
use League\Flysystem\FileAttributes;
use League\Flysystem\FilesystemAdapter;
use NsUtil\Storage\Clients\SmbClient;

use function NsUtil\dd;

class SambaAdapterV3 implements FilesystemAdapter
{

    public $service, $username, $password, $smbver, $tmp_dir;
    private $smbclient;
    private $pathCache = [];

    /**
     * @var string|null path prefix
     */
    protected $pathPrefix;

    /**
     * @var string
     */
    protected $pathSeparator = '/';



    public function __construct($service, $username, $password, $domain = '', $smbver = "", $tmp_dir = '/tmp')
    {
        $this->service = $service;
        $this->username = $username;
        $this->password = $password;
        $this->smbver = $smbver;
        $this->tmp_dir = $tmp_dir;
        $this->smbclient = new SmbClient($service, $username, $password, $domain, $smbver);
    }

    public function setPathPrefix($prefix)
    {
        $prefix = (string) $prefix;

        if ($prefix === '') {
            $this->pathPrefix = null;

            return;
        }

        $this->pathPrefix = rtrim($prefix, '\\/') . $this->pathSeparator;
    }

    public function getPathPrefix()
    {
        return $this->pathPrefix;
    }

    public function applyPathPrefix($path)
    {
        return $this->getPathPrefix() . ltrim($path, '\\/');
    }

    public function removePathPrefix($path)
    {
        return substr($path, strlen((string) $this->getPathPrefix()));
    }


    public function fileExists(string $path): bool
    {
        $ret = $this->getMetadata($path);
        return isset($ret['type']) ? true : false;
    }

    public function directoryExists(string $path): bool
    {
        $ret = $this->getMetadata($path);
        return isset($ret['type']) ? true : false;
    }
    public function write(string $path, string $contents, \League\Flysystem\Config $config): void
    {
        $this->upload($path, $contents);
    }
    public function writeStream(string $path, $contents, \League\Flysystem\Config $config): void
    {
        $this->upload($path, $contents);
    }
    public function read(string $path): string
    {
        if ($this->has($path)) {
            $path = $this->applyPathPrefix($path);
            $local = $this->tmp_dir . DIRECTORY_SEPARATOR . md5((string) $path);
            $this->smbclient->get($path, $local);
            $this->pathCache[md5((string) $path)]['contents'] = '';
            if (file_exists($local)) {
                $this->pathCache[md5((string) $path)]['contents'] = file_get_contents($local);
                unlink($local);
            }
        }
        return $this->pathCache[md5((string) $path)]['contents'] ?? '';
    }
    public function readStream(string $path): string
    {
        return $this->read($path);
    }

    public function delete(string $path): void
    {
        $path = $this->applyPathPrefix($path);
        $this->smbclient->del($path);
    }
    public function deleteDirectory(string $path): void
    {
        $path = $this->applyPathPrefix($path);
        $this->smbclient->del($path);
    }
    public function createDirectory(string $path, \League\Flysystem\Config $config): void
    {
        $path = $this->applyPathPrefix($path);
        $this->smbclient->mkdir($path);
    }
    public function setVisibility(string $path, string $visibility): void {}
    public function visibility(string $path): FileAttributes
    {
        $meta = $this->getMetadata($path);
        return new FileAttributes($path, $meta['size'], $meta['visibility'], $meta['timestamp'], $meta['mimetype'], $meta);
    }
    public function mimeType(string $path): FileAttributes
    {
        return $this->visibility($path);
    }
    public function lastModified(string $path): FileAttributes
    {
        return $this->visibility($path);
    }
    public function fileSize(string $path): FileAttributes
    {
        return $this->visibility($path);
    }
    public function listContents(string $path, bool $deep): iterable
    {
        $path = $this->applyPathPrefix($path);
        $list = $this->smbclient->dir($path);
        $out = [];
        foreach ($list as $item) {
            $meta = $this->mapFile($item, 'qwqco');
            $out[] = new FileAttributes($path, $meta['size'], $meta['visibility'], $meta['timestamp'], $meta['mimetype'], $meta);
        }
        return new ArrayIterator($out);
    }
    public function move(string $source, string $destination, \League\Flysystem\Config $config): void
    {
        $source = $this->applyPathPrefix($source);
        $destination = $this->applyPathPrefix($destination);
        $this->smbclient->rename($source, $destination);
    }

    public function copy(string $source, string $destination, \League\Flysystem\Config $config): void
    {
        $source = $this->applyPathPrefix($source);
        $destination = $this->applyPathPrefix($destination);
        // $this->smbclient->copy($source, $destination);
    }


    protected function mapFile($returnOfDir, $path): array
    {
        $this->pathCache[md5((string) $path)] = [
            'type' => isset($returnOfDir['isdir']) ? 'dir' : 'file',
            'path' => $returnOfDir['filename'],
            //'contents' => '',
            //'stream' => '',
            'visibility' => 'public',
            'timestamp' => (int) ($returnOfDir['mtime'] ?? 0),
            'size' => (int) (isset($returnOfDir['isdir']) ? $returnOfDir['size'] : 0),
            'mimetype' => \NsUtil\Storage\libs\Mimes::getMimeType($returnOfDir['filename'])
        ];
        return $this->pathCache[md5((string) $path)];
    }


    public function getMetadata($path)
    {
        $path = $this->applyPathPrefix($path);
        if (!isset($this->pathCache[md5((string) $path)]['type'])) {
            $dir_ret = $this->smbclient->dir('', $path);
            if (isset($dir_ret[0])) {
                $this->mapFile($dir_ret[0], $path);
            }
        }
        return $this->pathCache[md5((string) $path)] ?? [];
    }

    protected function upload($path, $contents)
    {
        $path = $this->applyPathPrefix($path);
        // salvar o contents em um arquivo local
        $local = $this->tmp_dir . '/' . md5((string) $path);
        file_put_contents(realpath($local), $contents);
        unlink($local);
        return $this->smbclient->put($local, $path);
    }

    public function has($path)
    {
        $ret = $this->getMetadata($path);
        return isset($ret['type']) ? true : false;
    }

    public function download($path, $pathToSave)
    {
        if ($this->has($path)) {
            $path = $this->applyPathPrefix($path);
            $this->smbclient->get($path, $pathToSave);
            return (bool) file_exists($pathToSave) && filesize($pathToSave) > 0;
        } else {
            throw new \Exception('File not found: ' . $path);
        }
    }
}
