<?php

namespace NsUtil\Storage\Adapters;

use League\Flysystem\Filesystem;
use NsUtil\Helper;
use NsUtil\Services\JWTService;
use NsUtil\Storage\Adapters\AdapterInterface;
use NsUtil\Storage\Traits\AdapterTrait;

use NsUtil\Storage\Utils\StorageUtil;
use function NsUtil\env;
use function NsUtil\now;

class LocalAdapter implements AdapterInterface
{

    use AdapterTrait;


    public function __construct(string $path)
    {
        // tentar criar se nao existir
        if (!is_dir($path)) {
            Helper::mkdir($path);
        }

        $this->setBucket($path);

        $adapter = StorageUtil::switchV1orV3FilesystemAdapter(
            '\League\Flysystem\Adapter\Local',
            '\League\Flysystem\Local\LocalFilesystemAdapter'
        );

        $this->adapter = new $adapter($path);

        $this->fs = new Filesystem($this->adapter);

        $this->checkIfAdapterIsRead();
    }


    public function temporaryUrl(string $item, int $minutes = 10, array $options = []): string
    {
        if (!env('STORAGE_LOCAL_URL_BASE')) {
            throw new \Exception('STORAGE_LOCAL_URL_BASE is not set');
        }


        if (!$this->adapter->has($item)) {
            throw new \Exception('File not found');
        }

        $token = (new JWTService(env('STORAGE_LOCAL_SECRET_KEY')))->generateToken([
            // Vou remover para avaliar onde esta sendo utilizado
            // 'item' => $item,
            'path' => $item,
            'minutes' => $minutes,
            'options' => $options
        ], now()->add("$minutes minutes"));

        return env('STORAGE_LOCAL_URL_BASE') . "?tkn=$token";
    }

    public function setCors(
        array $allowedMethods = ['GET', 'OPTIONS'],
        array $allowedHeaders = ['Content-Type', 'Access-Control-Allow-Origin'],
        array $allowedOrigins = ['*'],
        int $maxAge = 3600
    ): void {}

    public static function openTemporaryURL($jwt)
    {
        // Get payload from path
        return (array) (new JWTService(env('STORAGE_LOCAL_SECRET_KEY')))->decodeToken($jwt);
    }
}
