<?php

namespace NsUtil\Storage\Adapters;

use League\Flysystem\Filesystem;
use NsUtil\Storage\Adapters\AdapterInterface;
use NsUtil\Storage\Traits\AdapterTrait;
use NsUtil\Storage\Utils\StorageUtil;

class FileHausAdapter implements AdapterInterface
{

    use AdapterTrait;


    public function __construct(string $baseurl, string $username, string $password, string $path)
    {
        $this->setBucket($path);

        $adapter = StorageUtil::switchV1orV3FilesystemAdapter(
            '\NsUtil\Storage\Adapters\FileHaus\FileHausAdapterV1',
            '\NsUtil\Storage\Adapters\FileHaus\FileHausAdapterV3'
        );

        $this->adapter = new $adapter($baseurl, $username, $password, $path);

        $this->fs = new Filesystem($this->adapter);

        $this->checkIfAdapterIsRead();
    }

    public function temporaryUrl(string $item, int $minutes = 10, array $options = []): string
    {
        return $this->adapter->temporaryUrl($item, $minutes, $options);
    }

    public function setCors(
        array $allowedMethods = ['GET', 'OPTIONS'],
        array $allowedHeaders = ['Content-Type', 'Access-Control-Allow-Origin'],
        array $allowedOrigins = ['*'],
        int $maxAge = 3600
    ): void {}
}
