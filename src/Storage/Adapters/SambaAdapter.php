<?php

namespace NsUtil\Storage\Adapters;

use Exception;
use League\Flysystem\Filesystem;
use NsUtil\Storage\Adapters\AdapterInterface;
use NsUtil\Storage\Adapters\Samba\SambaAdapterV1;
use NsUtil\Storage\Adapters\Samba\SambaAdapterV3;
use NsUtil\Storage\Traits\AdapterTrait;

class SambaAdapter implements AdapterInterface
{

    use AdapterTrait;


    public function __construct($service, $username, $password, $domain = '', $smbver = "", $tmp_dir = '/tmp')
    {

        $ret = shell_exec('type smbclient');
        if (stripos($ret, 'not found') > -1) {
            throw new Exception('smbclient is not installed');
        }


        // v3
        if (class_exists(\League\Flysystem\Local\LocalFilesystemAdapter::class)) {
            $this->adapter = new SambaAdapterV3($service, $username, $password, $domain, $smbver, $tmp_dir);
        }
        // old version
        else {
            $this->adapter = new SambaAdapterV1($service, $username, $password, $domain, $smbver, $tmp_dir);
        }

        $this->checkIfAdapterIsRead();
    }

    public function setBucket($bucket): void
    {
        $this->bucket = $bucket;
        $this->adapter->setPathPrefix($bucket);
    }

    public function temporaryUrl(string $item, int $minutes = 10, array $options = []): string
    {
        return 'This adapter no have temporaryUrl enabled';
    }

    public function setCors(
        array $allowedMethods = ['GET', 'OPTIONS'],
        array $allowedHeaders = ['Content-Type', 'Access-Control-Allow-Origin'],
        array $allowedOrigins = ['*'],
        int $maxAge = 3600
    ): void {
    }
}
