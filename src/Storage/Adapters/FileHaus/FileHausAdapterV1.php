<?php

namespace NsUtil\Storage\Adapters\FileHaus;

use League\Flysystem\Adapter\AbstractAdapter;
use League\Flysystem\Config;
use NsUtil\Exceptions\FileNotFoundException;
use NsUtil\Helper;
use NsUtil\Security;
use NsUtil\Security\Crypto;
use NsUtil\Storage\Clients\FileHausClient;
use NsUtil\Storage\libs\Mimes;
use Tymon\JWTAuth\Facades\JWTAuth;

use function NsUtil\now;

class FileHausAdapterV1 extends AbstractAdapter
{

    private $baseurl;
    private $path;
    private $jwtToken;
    private FileHausClient $client;

    public function __construct(string $baseurl, string $username, string $password, string $path)
    {
        $this->baseurl = $baseurl;
        $this->path = $path;
        $this->setPathPrefix($path);
        $this->client = new FileHausClient($baseurl, $username, $password);
    }

    public function copy($path, $saveToLocal): bool
    {
        $location = $this->applyPathPrefix($path);
        throw new \Exception('Not implemented: copy');
    }


    public function createDir($dirname, Config $config)
    {
        $location = $this->applyPathPrefix($dirname);
        return $this->client->createDir($location);
    }

    public function delete($path): bool
    {
        $location = $this->applyPathPrefix($path);
        try {
            $this->client->delete($location);
        } catch (FileNotFoundException $e) {
            return true;
        } catch (\Exception $e) {
            throw new \Exception('Failed to delete file: ' . $e->getMessage());
        }
        return true;
    }

    public function deleteDir($dirname): bool
    {
        $location = $this->applyPathPrefix($dirname);
        throw new \Exception('Not implemented: deleteDir');
    }

    public function getMetadata($path)
    {
        $path = $this->applyPathPrefix($path);
        $file = $this->client->getMetadata($path);
        return $this->mapFile($file);
    }

    public function getMimetype($path)
    {
        return $this->getMetadata($path);
    }

    public function getSize($path)
    {
        return $this->getMetadata($path);
    }

    public function getTimestamp($path)
    {
        return $this->getMetadata($path);
    }

    public function getVisibility($path)
    {
        return $this->getMetadata($path);
    }

    public function has($path)
    {
        $path = $this->applyPathPrefix($path);
        return $this->client->has($path);
    }

    public function listContents($directory = '', $recursive = false): array
    {
        if (strlen($directory) > 0) {
            $directory = $this->applyPathPrefix($directory);
        }
        $list = $this->client->list($directory);
        $out = [];
        foreach ($list as $item) {
            $out[] = $this->mapFile($item);
        }
        return $out;
    }

    public function read($path)
    {
        $path = $this->applyPathPrefix($path);
        $contents = $this->client->read($path);
        return ['contents' => $contents];
    }

    public function readStream($path)
    {
        return $this->read($path);
    }

    public function rename($path, $newpath): bool
    {
        $path = $this->applyPathPrefix($path);
        $newpath = $this->applyPathPrefix($newpath);
        $this->client->rename($path, $newpath);
        return true;
    }

    public function setVisibility($path, $visibility)
    {
        return true;
    }

    public function update($path, $contents, Config $config)
    {
        $this->upload($path, $contents);
    }

    public function updateStream($path, $resource, Config $config)
    {
        die('not implements');
    }

    public function write($path, $contents, Config $config)
    {
        $this->upload($path, $contents);
    }

    public function writeStream($path, $resource, Config $config)
    {
        die('not implements');
    }

    private function generateTmpFileToUpload($contents)
    {
        $file = Helper::getTmpDir() . '/' .  Helper::generateUuidV4() . '.tmp';
        file_put_contents($file, $contents);
        return $file;
    }

    public function uploadFile($path, $file, $remove = false): void
    {
        $this->client->write($path, $file);
        if ($remove) {
            unlink($file);
        }
    }

    protected function upload($path, $contents)
    {
        $path = $this->applyPathPrefix($path);

        $file = $this->generateTmpFileToUpload($contents);

        $this->uploadFile($path, $file, true);

        return $this->has($path);
    }

    protected function mapFile($file): array
    {
        $mappedFile = [
            'type' => $file['metadata']['type'] ?? 'file',
            'path' => $file['path'],
            'visibility' => 'public',
            'timestamp' => (int) $file['metadata']['createdAt'] ?? 0,
            'size' => (int) $file['metadata']['size'] ?? 0,
            'mimetype' => Mimes::$_MIMES[$file['metadata']['mimetype']] ?? pathinfo($file['path'], PATHINFO_EXTENSION),
            'mimetype-full' => $file['metadata']['mimetype']
        ];

        return $mappedFile;
    }

    public function download($path, $pathToSave, $sobrepor = true)
    {
        $path = $this->applyPathPrefix($path);
        $contents = $this->client->read($path);
        Helper::saveFile($pathToSave, '', $contents, $sobrepor ? 'SOBREPOR' : '');
        if (file_exists($pathToSave) && filesize($pathToSave) > 0) {
            return true;
        }
        throw new \Exception("Failed to download file: {$pathToSave}");
    }

    public function temporaryUrl(string $item, int $minutes = 10, array $options = []): string
    {
        $item = $this->applyPathPrefix($item);
        return $this->client->temporaryUrl($item, $minutes, $options['download'] ? 1 : 0, isset($options['isDir']));
    }
}
