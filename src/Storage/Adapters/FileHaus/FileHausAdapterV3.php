<?php

namespace NsUtil\Storage\Adapters\FileHaus;

use ArrayIterator;
use League\Flysystem\FileAttributes;
use League\Flysystem\FilesystemAdapter;
use NsUtil\Storage\Adapters\Abstract\AbstractV3Adapter;
use NsUtil\Storage\Clients\FileHausClient;


class FileHausAdapterV3 extends AbstractV3Adapter
{

    private $baseurl;
    private $access_token;
    private $path;
    private FileHausClient $client;

    public function __construct(string $baseurl, string $access_token, string $path)
    {
        $this->baseurl = $baseurl;
        $this->access_token = $access_token;
        $this->path = $path;
        $this->client = new FileHausClient($baseurl, $access_token);
    }


    protected function mapFile($file, $path): array
    {
        $mappedFile = [
            'type' => $file['metadata']['type'] ?? 'file',
            'path' => $file['path'],
            'visibility' => 'public',
            'timestamp' => (int) $file['metadata']['createdAt'] ?? 0,
            'size' => (int) $file['metadata']['size'] ?? 0,
            'mimetype' => \NsUtil\Storage\libs\Mimes::getMimeType($file['metadata']['mimetype'])
        ];

        return $mappedFile;
    }
}
