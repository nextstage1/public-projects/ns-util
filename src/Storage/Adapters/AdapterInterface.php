<?php

namespace NsUtil\Storage\Adapters;

use League\Flysystem\Filesystem;

interface AdapterInterface
{


    public function setBucket($bucket): void;

    public function getFs(): Filesystem;

    public function getAdapter();



    public function temporaryUrl(string $item, int $minutes = 10, array $options = []): string;
}
