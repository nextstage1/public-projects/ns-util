<?php

namespace NsUtil\Storage\Utils;

use Exception;

class StorageUtil
{

    public static function switchV1orV3FilesystemAdapter($v1ClassName, $v3ClassName)
    {
        $v3Class = '\League\Flysystem\Local\LocalFilesystemAdapter';
        $switchedClass = class_exists($v3Class) ? $v3ClassName : $v1ClassName;

        if (class_exists($switchedClass)) {
            return $switchedClass;
        }

        throw new Exception("Class $switchedClass not found");
    }
}
