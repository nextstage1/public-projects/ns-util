<?php

namespace NsUtil\Resources;

class CepResource
{
    private $zipCode;
    private $street;
    private $complement;
    private $unit;
    private $neighborhood;
    private $city;
    private $state;
    private $region;
    private $ibge;
    private $gia;
    private $ddd;
    private $siafi;

    private $getLocation;
    /**
     * Constructor for CepResource.
     *
     * @param string $zipCode
     * @param string $street
     * @param string $complement
     * @param string $unit
     * @param string $neighborhood
     * @param string $city
     * @param string $state
     * @param string $region
     * @param string $ibge
     * @param string $gia
     * @param string $ddd
     * @param string $siafi
     */
    public function __construct(
        string $zipCode = '',
        string $street = '',
        string $complement = '',
        string $unit = '',
        string $neighborhood = '',
        string $city = '',
        string $state = '',
        string $region = '',
        string $ibge = '',
        string $gia = '',
        string $ddd = '',
        string $siafi = '',
        array $getLocation = []
    ) {
        $this->setZipCode($zipCode);
        $this->setStreet($street);
        $this->setComplement($complement);
        $this->setUnit($unit);
        $this->setNeighborhood($neighborhood);
        $this->setCity($city);
        $this->setState($state);
        $this->setRegion($region);
        $this->setIbge($ibge);
        $this->setGia($gia);
        $this->setDdd($ddd);
        $this->setSiafi($siafi);
        $this->setLocation($getLocation);
    }

    /**
     * Get the zip code.
     *
     * @return string
     */
    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    /**
     * Set the zip code.
     *
     * @param string $zipCode
     */
    public function setZipCode(string $zipCode = ''): void
    {
        $this->zipCode = $zipCode;
    }

    /**
     * Get the street.
     *
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * Set the street.
     *
     * @param string $street
     */
    public function setStreet(string $street = ''): void
    {
        $this->street = $street;
    }

    /**
     * Get the complement.
     *
     * @return string
     */
    public function getComplement(): string
    {
        return $this->complement;
    }

    /**
     * Set the complement.
     *
     * @param string $complement
     */
    public function setComplement(string $complement = ''): void
    {
        $this->complement = $complement;
    }

    /**
     * Get the unit.
     *
     * @return string
     */
    public function getUnit(): string
    {
        return $this->unit;
    }

    /**
     * Set the unit.
     *
     * @param string $unit
     */
    public function setUnit(string $unit = ''): void
    {
        $this->unit = $unit;
    }

    /**
     * Get the neighborhood.
     *
     * @return string
     */
    public function getNeighborhood(): string
    {
        return $this->neighborhood;
    }

    /**
     * Set the neighborhood.
     *
     * @param string $neighborhood
     */
    public function setNeighborhood(string $neighborhood = ''): void
    {
        $this->neighborhood = $neighborhood;
    }

    /**
     * Get the city.
     *
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * Set the city.
     *
     * @param string $city
     */
    public function setCity(string $city = ''): void
    {
        $this->city = $city;
    }

    /**
     * Get the state.
     *
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * Set the state.
     *
     * @param string $state
     */
    public function setState(string $state = ''): void
    {
        $this->state = $state;
    }

    /**
     * Get the region.
     *
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }

    /**
     * Set the region.
     *
     * @param string $region
     */
    public function setRegion(string $region = ''): void
    {
        $this->region = $region;
    }

    /**
     * Get the IBGE code.
     *
     * @return string
     */
    public function getIbge(): string
    {
        return $this->ibge;
    }

    /**
     * Set the IBGE code.
     *
     * @param string $ibge
     */
    public function setIbge(string $ibge = ''): void
    {
        $this->ibge = $ibge;
    }

    /**
     * Get the GIA code.
     *
     * @return string
     */
    public function getGia(): string
    {
        return $this->gia;
    }

    /**
     * Set the GIA code.
     *
     * @param string $gia
     */
    public function setGia(string $gia = ''): void
    {
        $this->gia = $gia;
    }

    /**
     * Get the DDD code.
     *
     * @return string
     */
    public function getDdd(): string
    {
        return $this->ddd;
    }

    /**
     * Set the DDD code.
     *
     * @param string $ddd
     */
    public function setDdd(string $ddd = ''): void
    {
        $this->ddd = $ddd;
    }

    /**
     * Get the SIAFI code.
     *
     * @return string
     */
    public function getSiafi(): string
    {
        return $this->siafi;
    }

    /**
     * Set the SIAFI code.
     *
     * @param string $siafi
     */
    public function setSiafi(string $siafi = ''): void
    {
        $this->siafi = $siafi;
    }

    public function setLocation(array $getLocation = []): void
    {
        $this->getLocation = $getLocation;
    }

    public function getLocation(): array
    {
        return $this->getLocation;
    }

    /**
     * Convert the object to an array.
     *
     * @param string $lang
     * @return array
     */
    public function toArray(string $lang = 'pt'): array
    {
        $data = [
            'zipCode' => $this->getZipCode(),
            'street' => $this->getStreet(),
            'complement' => $this->getComplement(),
            'unit' => $this->getUnit(),
            'neighborhood' => $this->getNeighborhood(),
            'city' => $this->getCity(),
            'state' => $this->getState(),
            'region' => $this->getRegion(),
            'ibge' => $this->getIbge(),
            'gia' => $this->getGia(),
            'ddd' => $this->getDdd(),
            'siafi' => $this->getSiafi(),
            'geoLocation' => $this->getLocation(),
        ];

        if ($lang === 'en') {
            return $data;
        }

        return [
            'cep' => $data['zipCode'],
            'logradouro' => $data['street'],
            'complemento' => $data['complement'],
            'unidade' => $data['unit'],
            'bairro' => $data['neighborhood'],
            'localidade' => $data['city'],
            'uf' => $data['state'],
            'regiao' => $data['region'],
            'ibge' => $data['ibge'],
            'gia' => $data['gia'],
            'ddd' => $data['ddd'],
            'siafi' => $data['siafi'],
            'geoLocation' => $this->getLocation(),
        ];
    }
}
