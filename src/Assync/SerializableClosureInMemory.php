<?php

namespace NsUtil\Assync;

use Closure;
use Laravel\SerializableClosure\Contracts\Serializable;
use Laravel\SerializableClosure\Support\ReflectionClosure;

class SerializableClosureInMemory implements Serializable
{
    /**
     * The closure to be serialized/unserialized.
     *
     * @var \Closure
     */
    protected $closure;

    /**
     * The closure's reflection.
     *
     * @var \Laravel\SerializableClosure\Support\ReflectionClosure|null
     */
    protected $reflector;

    /**
     * Creates a new serializable closure instance.
     *
     * @param  \Closure  $closure
     * @return void
     */
    public function __construct(Closure $closure)
    {
        $this->closure = $closure;
    }

    /**
     * Resolve the closure with the given arguments.
     *
     * @return mixed
     */
    public function __invoke()
    {
        return call_user_func_array($this->closure, func_get_args());
    }

    /**
     * Gets the closure.
     *
     * @return \Closure
     */
    public function getClosure()
    {
        return $this->closure;
    }

    /**
     * Get the serializable representation of the closure.
     *
     * @return array
     */
    public function __serialize()
    {
        $reflector = $this->getReflector();
        $code = $reflector->getCode();
        $use = $reflector->getUseVariables();

        return [
            'code' => $code,
            'use' => $use,
        ];
    }

    /**
     * Restore the closure after serialization.
     *
     * @param  array  $data
     * @return void
     */
    public function __unserialize($data)
    {
        $code = $data['code'];
        $use = $data['use'];

        extract($use, EXTR_OVERWRITE | EXTR_REFS);

        $this->closure = eval('return ' . $code . ';');
    }

    /**
     * Gets the closure's reflector.
     *
     * @return \Laravel\SerializableClosure\Support\ReflectionClosure
     */
    protected function getReflector()
    {
        if ($this->reflector === null) {
            $this->reflector = new ReflectionClosure($this->closure);
        }

        return $this->reflector;
    }
}
