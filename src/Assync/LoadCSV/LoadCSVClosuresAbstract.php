<?php

namespace NsUtil\Assync\LoadCSV;

use NsUtil\Helpers\Format;

abstract class LoadCSVClosuresAbstract implements LoadCSVClosuresInterface
{
    private Format $format;

    public function handle(array $data): array
    {
        return $data;
    }

    public function getValues(array $data): array
    {
        $data = $this->handle($data);
        return array_map(fn($item) => array_values($item), $data);
    }

    protected function filterOnlyArray(array $data): array
    {
        return array_filter($data, fn($item) => is_array($item));
    }


    protected function getFormat(): Format
    {
        $this->format ??= new Format();

        return $this->format;
    }

    protected function formatDatetime(array &$item, array $fields, bool $isDatetime = true): void
    {
        foreach ($fields as $field) {
            $item[$field] = (strlen($item[$field]) < 8)
                ? ''
                : $this->getFormat()->setString($item[$field])->date('arrumar', $isDatetime);
        }
    }

    protected function formatDate(array &$item, array $fields): void
    {
        $this->formatDatetime($item, $fields, false);
    }

    protected function formatInt(array &$item, array $fields): void
    {
        foreach ($fields as $field) {
            $item[$field] = $this->getFormat()->setString($item[$field])->parseInt();
        }
    }

    protected function formatCep(array &$item, array $fields): void
    {
        foreach ($fields as $field) {
            $item[$field] = $this->getFormat()->setString($item[$field])->parseInt();
            $item[$field] = $item[$field] === '00000000' ? '' : $item[$field];
        }
    }

    protected function formatFone(array &$item, array $fields): void
    {
        foreach ($fields as $field) {
            $item[$field] = str_replace(' ', '', $this->getFormat()->setString($item[$field])->fone());
        }
    }
}
