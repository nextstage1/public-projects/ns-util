<?php

namespace NsUtil\Assync\LoadCSV;

use Exception;
use NsUtil\Assync\Assync;
use NsUtil\Helper;
use NsUtil\LoadCSVToArray;

class LoadCSVRuntime
{
    public function __construct() {}

    public function handle(array $param)
    {
        $file = $param['file'];
        $runners = $param['runners'];

        // Restauracao da conexao PDO 
        $con = unserialize(file_get_contents($param['pdo']));
        $con->open();

        // descarga parcial para evitar sobrecarga de memoria 
        $lines = (new LoadCSVToArray($file))->run();

        // Campos extras 
        if (isset($runners['extrasFields'])) {
            foreach ($lines as $key => $item) {
                foreach ($runners['extrasFields'] as $i) {
                    $lines[$key][] = '';
                }
            }
        }

        // processamento de funcao por linha 
        if (strlen($runners['closure'] ?? '') > 0) {

            $keys = array_map('mb_strtolower', $param['fields']);
            $combine = array_map(fn($item) => array_combine($keys, $item), $lines);

            // verifica se a closure é callable
            $ClosureReceive = $runners['closure'] ?? '__none__';
            $closure = Assync::decodeTask($ClosureReceive);

            if (!$closure && class_exists($ClosureReceive)) {
                // verifica se a classe existe
                $runner = new $ClosureReceive();
                // verifica se a classe implementa a interface LoadCSVClosuresInterface
                if ($runner instanceof LoadCSVClosuresInterface) {
                    $lines = $runner->getValues($combine);
                } else {
                    throw new Exception("Class {$closure} does not implement the LoadCSVClosuresInterface interface");
                }
            } elseif (is_callable($closure)) {
                // se for callable, executa a closure
                $lines = array_map(
                    fn($item) => array_values(call_user_func($closure, $item)),
                    array_filter($combine, fn($item) => is_array($item))
                );
            } else {
                throw new Exception("Closure or class {$closure} is not callable or does not exist");
            }

            unset($combine);
        }

        Helper::deleteFile($file);

        if (is_array($lines) && count($lines) > 0) {
            $con->insertByCopy($param['tableSchema'], $param['fields'], $lines);
        }

        return true;
    }
}
