<?php

namespace NsUtil\Assync\LoadCSV;

interface LoadCSVClosuresInterface
{
    public function handle(array $data): array;

    public function getValues(array $data): array;
}
