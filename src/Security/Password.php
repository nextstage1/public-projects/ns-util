<?php

namespace NsUtil\Security;

/**
 * Class Password
 * 
 * Provides methods for password encoding, generation, verification, and strength assessment.
 */
class Password
{
    /**
     * Encodes a password with a token and encryption.
     * The constant TOKEN is defined in _config.php.
     * 
     * @param string $senha The password to encode.
     * @return string The encoded password.
     */
    public static function codificaSenha(string $senha): string
    {
        self::addToken($senha);
        return self::generateHash($senha);
    }

    /**
     * Generates a new password.
     * 
     * @return string The generated password.
     */
    public static function geraNovaSenha(): string
    {
        $senha = substr(md5((string) microtime()), 0, 12);
        return $senha;
    }

    /**
     * Verifies a password against a hash.
     * 
     * @param string $senha The password to verify.
     * @param string $hash The hash to verify against.
     * @return bool True if the password matches the hash, false otherwise.
     */
    public static function verify(string $senha, string $hash): bool
    {
        self::addToken($senha);
        return password_verify($senha, $hash);
    }

    /**
     * Adds a token to the password.
     * 
     * @param string $senha The password to add the token to.
     * @param string $token The token to add (optional).
     */
    private static function addToken(string &$senha, string $token = '-'): void
    {
        $senha = hash_hmac('sha256', trim($senha), $token);
    }

    /**
     * Generates a hash for the password.
     * 
     * @param string $senha The password to hash.
     * @return string The hashed password.
     */
    private static function generateHash(string $senha): string
    {
        return password_hash($senha, PASSWORD_DEFAULT);
    }

    /**
     * Updates the password if the old password is verified.
     * 
     * @param string $oldSenha The old password.
     * @param string $newSenha The new password.
     * @param string $hash The hash of the old password.
     * @return string|bool The new hashed password or false if verification fails.
     */
    public static function updatePassword(string $oldSenha, string $newSenha, string $hash)
    {
        if (self::verify($oldSenha, $hash)) {
            return self::codificaSenha($newSenha);
        }
        return false;
    }

    /**
     * Assesses the strength of a password.
     * 
     * @param string $senha The password to assess.
     * @return int The strength score of the password.
     */
    public static function forcaSenha(string $senha): int
    {
        $len = strlen($senha);
        if ($len < 6) {
            return -1;
        }
        $count = 0;
        if ($len >= 8) {
            $count++;
        }
        $array = ['/[a-z]/', '/[A-Z]/', '/[0-9]/', '/[!#_-]/'];
        foreach ($array as $a) { // for each positive match, add a point
            if (preg_match($a, $senha)) {
                $count++;
            }
        }
        return $count;
    }
}
