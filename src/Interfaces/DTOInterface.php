<?php

namespace NsUtil\Interfaces;

use NsUtil\Api;

interface DTOInterface
{
    /**
     * Retorna os dados do DTO como um array associativo.
     *
     * @return array
     */
    public function toArray(): array;
}
