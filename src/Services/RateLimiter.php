<?php

namespace NsUtil\Services;

use NsUtil\Api;
use NsUtil\Date;
use NsUtil\Exceptions\TooManyRequestException;
use NsUtil\Format;
use NsUtil\Helper;
use NsUtil\Services\Redis;

use function NsUtil\dd;
use function NsUtil\json_decode;
use function NsUtil\now;

class RateLimiter
{
    private static ?Redis $redis = null;

    public const PER_DAY = -1;
    public const PER_HOUR = 60 * 60;
    public const PER_MINUTE = 60;
    public const PER_SECOND = 1;


    private function __construct() {}

    private static function init()
    {
        if (null === self::$redis) {
            self::$redis = new Redis();
        }
    }

    private static function getIpAddress(): string
    {
        return Helper::getIP();
    }

    public static function byIP(int $maxCallsLimit = 120, int $secondsInterval = self::PER_MINUTE, ?string $route = null): void
    {
        self::handle($maxCallsLimit, $secondsInterval, null, $route);
    }

    public static function byKey(string $key, int $maxCallsLimit = 100, int $secondsInterval = self::PER_HOUR, ?string $route = null): void
    {
        self::handle($maxCallsLimit, $secondsInterval, $key, $route);
    }



    private static function handle(
        int $maxCallsLimit = 3,
        int $secondsInterval = 10,
        ?string $appkey = null,
        ?string $route = null
    ): void {
        self::init();

        // per_day
        if ($secondsInterval === -1) {
            $mezzanotte = new Date(date('Y-m-d H:i:s', mktime(23, 59, 59, date('m'), date('d'), date('Y'))));
            $secondsInterval = $mezzanotte->timestamp() - now()->timestamp();
        }

        $rateKey = 'rate-limiter-' . ($appkey ?? (self::getIpAddress() . '-' . ($route ?? 'default-router')));
        $currentTime = now()->timestamp();

        // Recupera a lista de timestamps das requisições
        $requestTimestamps = self::$redis::get($rateKey) ?? [];
        $requestTimestamps = json_decode($requestTimestamps, true) ?: [];

        // Remove timestamps que estão fora do intervalo de tempo
        $requestTimestamps = array_filter($requestTimestamps, function ($timestamp) use ($currentTime, $secondsInterval) {
            return ($currentTime - $timestamp) < $secondsInterval;
        });

        // Adiciona o timestamp atual à lista
        $requestTimestamps[] = $currentTime;

        // Verifica se o número de requisições excede o limite
        $humanLimit = $secondsInterval === -1
            ? 'day'
            : (new Format($secondsInterval))->humanTimeFromSeconds();

        if (count($requestTimestamps) > $maxCallsLimit) {
            throw new TooManyRequestException(
                "Limit exceed (" . count($requestTimestamps) . "). Max. $maxCallsLimit per " . $humanLimit
            );
        }

        // Atualiza a lista de timestamps no Redis
        self::$redis::set($rateKey, json_encode($requestTimestamps), $secondsInterval);
    }

    public static function clear(?string $appkey = null, string $route = null): void
    {
        self::init();
        $rateKey = 'rate-limiter-' . ($appkey ?? (self::getIpAddress() . '-' . ($route ?? 'default-router')));
        self::$redis::clearKey($rateKey);
    }
}
