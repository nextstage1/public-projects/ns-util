<?php

namespace NsUtil\Services;

use Exception;
use NsUtil\Log;
use NsUtil\Helper;
use Predis\Client;
use function NsUtil\env;

use function NsUtil\json_decode;
use NsUtil\Exceptions\RedisConnectionException;

class Redis
{
    private static ?Client $client = null;
    public const FOREVER = (60 * 60 * 24 * 3650);
    public const HOUR = (60 * 60);
    public const DAY = (60 * 60 * 24);
    public const MINUTE = 60;


    private static function init()
    {
        try {
            if (null === self::$client) {


                $options = [
                    'scheme' => 'tcp',
                    'host' => env('REDIS_HOST', 'host.docker.internal'),
                    'port' => (int) env('REDIS_PORT', 6379)
                ];

                if (env('REDIS_PASSWORD')) {
                    $options['password'] = env('REDIS_PASSWORD');
                }

                self::$client = new Client($options);
            }

            self::$client->connect(); // Tenta conectar ao Redis
        } catch (Exception $e) {
            Log::logTxt(Helper::getTmpDir() . '/redis-error.log', [
                'error-message' => $e->getMessage(),
                'env-config' => [
                    'REDIS_HOST' => env('REDIS_HOST', 'host.docker.internal'),
                    'REDIS_PORT' => (int) env('REDIS_PORT', 6379)
                ]
            ]);
            throw new RedisConnectionException('Check your cache server configuration', 400);
        }
    }

    public static function checkHasServer(): bool
    {
        try {
            self::init();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    private static function prepareKey($key): string
    {
        $prefix = 'default';
        $envs = ['HOSTNAME', 'COMPOSE_PROJECT_NAME', 'AMBIENTE', 'ENVIRONMENT'];
        foreach ($envs as $envName) {
            $value = env($envName);
            if ($value !== null) {
                $prefix = $value;
                break;
            }
        }
        return '_' . Helper::sanitize($prefix) . ':' . Helper::sanitize($key);
    }

    /**
     * Set a cached value
     *
     * @param string $key Chave de recuperação
     * @param mixed $value Valor a ser cacheado
     * @param integer $timeInSeconds Valor em segundos da duração do cache
     * @return void
     */
    public static function set(string $key, $value, int $timeInSeconds = self::FOREVER)
    {
        self::init();
        $keyPrepared = self::prepareKey($key);

        $timeInSeconds = $timeInSeconds < 5 ? 5 : $timeInSeconds;

        if (is_callable($value)) {
            $ret = call_user_func($value);
            self::$client->set($keyPrepared, $ret);
        } else {
            self::$client->set($keyPrepared, $value);
        }

        self::$client->expire($keyPrepared, $timeInSeconds);

        // list of keys the app
        $ret = self::$client->get(self::prepareKey('application-keys'));
        if (strlen((string) $ret) === 0) {
            $ret = '[]';
        }
        self::$client->set(self::prepareKey('application-keys'), json_encode(array_merge(
            json_decode($ret, true),
            [$key]
        )));
    }

    public static function expire($key, $timeInSeconds = self::FOREVER)
    {
        self::init();
        $keyPrepared = self::prepareKey($key);
        self::$client->expire($keyPrepared, $timeInSeconds);
    }

    public static function get($key, $value = null, int $timeInSeconds = self::FOREVER)
    {

        self::init();
        $keyPrepared = self::prepareKey($key);

        $ret = self::$client->get($keyPrepared);

        if (strlen((string) $ret) === 0 && $value !== null) {
            self::set($key, $value, $timeInSeconds);
            $ret = self::$client->get($keyPrepared);
        }
        return strlen((string) $ret) === 0 ? null : $ret;
    }

    /**
     * Check if key exists and is valid (not expired)
     * @param string $key Key to check
     * @return bool
     */
    public static function has($key): bool
    {
        self::init();
        $keyPrepared = self::prepareKey($key);
        return self::$client->exists($keyPrepared) && self::$client->ttl($keyPrepared) > 0;
    }


    public static function getAppKeys(): array
    {
        self::init();
        return json_decode(self::$client->get(self::prepareKey('application-keys')), true) ?? [];
    }

    /**
     * Caso encontre uma parte da string em uma das chaves ira remove-la
     *
     * @param [type] $keyToRemove
     * @return void
     */
    public static function removeBySimilarity($keyToRemove)
    {
        $keys = self::getAppKeys();
        foreach ($keys as $k => $item) {
            if (stripos($item, $keyToRemove) !== false) {
                self::clearKey($item);
                unset($keys[$k]);
            }
        }
        self::$client->set(self::prepareKey('application-keys'), json_encode(
            array_values($keys)
        ));
    }


    public static function clearAll()
    {
        self::init();
        self::$client->flushall();
    }

    public static function clearKey($key): void
    {
        self::init();
        $key = self::prepareKey($key);
        if (self::$client->exists($key)) {
            self::$client->del($key);
        }
    }

    /**
     *  Only for compatibility
     * @param mixed $key
     * @return void
     */
    public static function delete($key): void
    {
        self::clearKey($key);
    }

    /**
     * Set a cached value
     *
     * @param string $key Chave de recuperação
     * @param mixed $value Valor a ser cacheado
     * @param integer $timeInSeconds Valor em segundos da duração do cache
     * @return void
     */
    public static function update(string $key, $value)
    {
        $originalKey = $key;
        self::init();
        $key = self::prepareKey($key);

        $ret = self::$client->get($key);
        if (strlen((string) $ret) === 0) {
            throw new Exception("Redis: key '$originalKey' not found");
        }

        if (is_callable($value)) {
            $ret = call_user_func($value);
            self::$client->set($key, $ret);
        } else {
            self::$client->set($key, $value);
        }
    }

    public static function incr(string $key): void
    {
        $originalKey = $key;
        self::init();
        $key = self::prepareKey($key);
        if (!self::$client->exists($key)) {
            throw new Exception("Redis: key '$originalKey' not found");
        }

        self::$client->incr($key);
    }

    public static function cacheArray($key, $value = null, int $timeInSeconds = self::FOREVER)
    {
        $content = self::get($key, function () use ($value) {
            return (string) json_encode(is_callable($value) ? call_user_func($value) : $value);
        }, $timeInSeconds);

        return json_decode($content, true);
    }

    public static function cacheJson($key, $value = null, int $timeInSeconds = self::FOREVER)
    {
        $ret = self::cacheArray($key, $value, $timeInSeconds);

        return json_decode($ret);
    }

    public function cache($key, $value = null, int $timeInSeconds = self::FOREVER)
    {
        return self::get($key, function () use ($value) {
            return is_callable($value) ? call_user_func($value) : $value;
        }, $timeInSeconds);
    }

    public static function list(): array
    {
        return self::getAppKeys();
    }
}
