<?php

namespace NsUtil\Services;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use NsUtil\Date;

class JWTService
{
    private string $secretKey;
    private string $algorithm;

    public function __construct(string $secretKey, string $algorithm = 'HS256')
    {
        $this->secretKey = $secretKey;
        $this->algorithm = $algorithm;
    }

    /**
     * Gera um token JWT.
     *
     * @param array $payload Dados a serem incluídos no token.
     * @param int $expiration Tempo de expiração em segundos.
     * @return string Token JWT gerado.
     */
    public function generateToken(array $payload, Date $expire): string
    {
        $payload['exp'] = $expire->timestamp();
        return JWT::encode($payload, $this->secretKey, $this->algorithm);
    }

    /**
     * Valida e decodifica um token JWT.
     *
     * @param string $token Token JWT a ser validado.
     * @return object Dados decodificados do token.
     * @throws \Exception Se o token for inválido ou expirado.
     */
    public function decodeToken(string $token): object
    {
        return JWT::decode($token, new Key($this->secretKey, $this->algorithm));
    }
}
