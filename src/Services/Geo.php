<?php

namespace NsUtil\Services;

use Exception;
use NsUtil\Http\Http;

class Geo
{

    public static function get($ip)
    {
        $url = "http://www.geoplugin.net/php.gp?ip={$ip}";
        $ret = Http::call($url, [], 'GET', ['Content-Type:application/json'], false, 15);
        $geoplugin = unserialize($ret->getBody());
        if (!is_array($geoplugin)) {
            throw new Exception("Invalid response from url $url");
        }

        $out = [];
        foreach ($geoplugin as $key => $geo) {
            $key = str_replace('geoplugin_', '', $key);
            $out[$key] = $geo;
        }
        unset($out['credit']);

        return $out;
    }
}
