<?php

namespace NsUtil\Services\Gitlab;

use Config;
use NsUtil\Exceptions\ModuleNotEnabledByFeatureFlags;
use NsUtil\Integracao\Gitlab;

use function NsUtil\env;

class FeatureFlag
{
    private Gitlab $gl;
    private function __construct()
    {
        $this->gl = new Gitlab(
            env('GITLAB_TOKEN', '-'),
            env('GITLAB_URL', 'https://gitlab.com/api/v4'),
        );
    }

    public static function check(string $featureFlagName): bool
    {
        $me = new self();
        return $me->gl
            ->setIdProject((int)env('GITLAB_ID_PROJECT', -1))
            ->loadFeatureFlag($featureFlagName);
    }

    public static function checkOrFail(string $featureFlagName): void
    {
        if (!self::check($featureFlagName)) {
            throw new ModuleNotEnabledByFeatureFlags($featureFlagName);
        }
    }
}
