<?php

namespace NsUtil;

use Exception;
use NsUtil\Exceptions\FileNotFoundException;
use NsUtil\Exceptions\InvalidEnvfileException;


class EnvFile
{
    private $config = [];
    private $envFilePath;

    public function __construct(?string $env)
    {

        $this->envFilePath = $env ??  Helper::getPathApp() . '/.env';

        if (!file_exists($this->envFilePath)) {
            Helper::saveFile($this->envFilePath, false, '');
            chmod($this->envFilePath, 0777);
        }

        $this->config = file($this->envFilePath, FILE_IGNORE_NEW_LINES);
    }

    public function update(array $updated, bool $sort = true): void
    {
        $newContent = [];
        $lastKey = '';
        foreach ($updated as $key => $newValue) {

            $keyFound = false;
            // Verifica se newValue contém espaços ou é uma variável referenciada
            if (strpos($newValue, ' ') !== false || strpos($newValue, '$') !== false) {
                $newValue = '"' . $newValue . '"';
            }

            foreach ($this->config as $index => $line) {
                // Ignora linhas de comentários
                if (strpos(trim($line), '#') === 0 || empty(trim($line))) {
                    continue;
                }

                if (strpos($line, $key . '=') === 0) {
                    $this->config[$index] = $key . '=' . $newValue;
                    $keyFound = true;
                    break;
                }
            }

            if (!$keyFound) {
                $this->config[] = $key . '=' . $newValue;
            }
        }

        // adicionar uma linha entre as configurações parecidas
        $this->config = array_filter($this->config, fn($item) => strlen($item) > 0);

        if ($sort) {
            sort($this->config);


            $tempArray = [];
            $lastPrefix = '';
            foreach ($this->config as $item) {
                $prefix = substr($item, 0, 2); // explode('_', $item)[0];
                if ($lastPrefix !== '' && $prefix !== $lastPrefix) {
                    $tempArray[] = "";
                }

                $tempArray[] = $item;
                $lastPrefix = $prefix;
            }
            $newContent = implode("\n", $tempArray);
        } else {
            $newContent = implode("\n", $this->config);
        }


        file_put_contents($this->envFilePath, $newContent);
    }

    /**
     * Carrega um arquivo de variáveis de ambiente (.env) e retorna seu conteúdo como um array.
     *
     * @param string $envFilePath O caminho para o arquivo .env.
     * @return array Um array associativo contendo as variáveis de ambiente.
     * @throws \Exception Se o arquivo não for encontrado ou se a configuração estiver incorreta.
     */
    public static function loadEnvFile(?string $envFilePath = null): array
    {
        $envFilePath ??= Helper::getPathApp() . '/.env';

        if (file_exists($envFilePath)) {
            $_CONFIG = parse_ini_file($envFilePath);
            if (!is_array($_CONFIG)) {
                throw new InvalidEnvfileException("Incorrect file configuration for .env type in file '$envFilePath'");
            }
            return $_CONFIG;
        } else {
            throw new FileNotFoundException('Envfile not found: ' . $envFilePath);
        }
    }

    /**
     * Carrega variáveis de ambiente de um arquivo .env e define essas variáveis no ambiente atual.
     *
     * @param string $envFilePath O caminho para o arquivo .env.
     * @param array $extras Array chave=>valor com os demais itens a serem disponibilzados em variavel de ambiente
     * @return void
     * @throws \Exception Se o arquivo conter configuração invalida. Ignora arquivo inexistente.
     */
    public static function applyEnvVariables(?string $envFilePath = null, array $extras = []): void
    {
        $envs = [];

        try {
            $envs = self::loadEnvFile($envFilePath);
        } catch (FileNotFoundException $exc) {
            $envs = [];
        } catch (InvalidEnvfileException $exc) {
            if (null !== $envFilePath) {
                throw $exc;
            }
        }


        $envs = array_merge($envs, $extras);
        array_map(
            fn($key, $item) => putenv("$key=$item"),
            array_keys($envs),
            array_values($envs)
        );
    }
}
