<?php

namespace NsUtil;

use Closure;

/**
 * Class Eficiencia
 *
 * The Eficiencia class is designed to measure the efficiency of code execution by calculating
 * the elapsed time and peak memory usage between the instantiation and the end of its execution.
 *
 * @package NsUtil
 */
class Eficiencia
{

    /**
     * @var float Start time of code execution.
     */
    private $start;

    /**
     * @var float End time of code execution.
     */
    private $end;

    /**
     * @var Closure|null A closure to be executed after the code execution ends.
     */
    private ?Closure $fnAfterEnding;

    /**
     * Eficiencia constructor.
     *
     * Initializes the start time and sets the closure to be executed after code execution ends.
     *
     * @param Closure|null $fnAfterEnding A closure to be executed after code execution ends.
     */
    public function __construct(?Closure $fnAfterEnding = null)
    {
        list($usec, $sec) = explode(' ', microtime());
        $this->start = (float) $sec + (float) $usec;

        $this->fnAfterEnding = $fnAfterEnding;
    }

    /**
     * Registers a shutdown function to ensure that the end method is called before script termination.
     */
    public function __shutdown_check()
    {
        $this->end();
    }

    /**
     * Ends the measurement of code execution and returns an object containing relevant information.
     *
     * @return object An object containing the following information:
     *   - text: A string with information about elapsed time and peak memory.
     *   - time: Elapsed time in seconds.
     *   - memory: Peak memory usage.
     */
    public function end()
    {
        list($usec, $sec) = explode(' ', microtime());
        $this->end = (float) $sec + (float) $usec;

        $elapsed_time = round($this->end - $this->start, 5);
        $time = gmdate("H:i:s", (int) $elapsed_time);
        $seconds = round($this->end - $this->start, 5);
        $memory = round(((memory_get_peak_usage(true) / 1024) / 1024), 2) . 'MB';

        // Run closure
        if (is_callable($this->fnAfterEnding)) {
            call_user_func_array($this->fnAfterEnding, [$seconds, $memory, $time]);
        }

        return (object) [
            'text' => "Elapsed time: $time. Memory peak: $memory",
            'time' => $seconds,
            'memory' => $memory
        ];
    }

    /**
     * Set a closure to be executed after the code execution ends.
     *
     * @param  Closure|null  $fnAfterEnding  A closure to be executed after the code execution ends.
     *
     * @return  self
     */
    public function setFnAfterEnding(?Closure $fnAfterEnding)
    {
        $this->fnAfterEnding = $fnAfterEnding;

        return $this;
    }
}
