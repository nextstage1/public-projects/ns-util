<?php

namespace NsUtil\Helpers;

use DateTime;
use DateTimeZone;
use Exception;

/**
 * Class Format
 * Provides various formatting utilities for dates, phone numbers, postal codes, and more.
 */
class Format
{
    private string $timezone;
    private ?string $string;

    /**
     * Format constructor.
     * @param string|null $string
     * @param string|null $timezone
     */
    public function __construct(?string $string = null, ?string $timezone = null)
    {
        $this->timezone = $timezone ?? 'America/Sao_Paulo';
        $this->string = $string;
    }

    /**
     * Set the string to be formatted.
     * @param string $string
     * @return $this
     */
    public function setString(?string $string): self
    {
        $this->string = (string) $string;
        return $this;
    }

    /**
     * Format a date string.
     * @param string $format
     * @param bool $includeTime
     * @param bool $changeTimeZone
     * @return mixed
     */
    public function date(string $format = 'arrumar', bool $includeTime = false, bool $changeTimeZone = false)
    {
        $data = (string) $this->string;
        if ($data !== 'NOW') {
            if (strlen($data) < 6) {
                return '';
            }
            $data = str_replace('"', '', $data);
            $t = explode('.', $data);
            $data = str_replace("T", " ", $t[0]);
            $hora = '12:00:00';
            $t = explode(' ', $data);
            if (count($t) > 1) {
                $data = $t[0];
                $hora = $t[1];
            }
            $c = substr($data, 2, 1);
            if (!is_numeric($c)) {
                $data = substr($data, 6, 4) . '-' . substr($data, 3, 2) . '-' . substr($data, 0, 2);
            }
            $data = $data . 'T' . $hora . '-00:00';
        }

        try {
            $date = new DateTime($data);
            if ($changeTimeZone) {
                $date->setTimezone(new DateTimeZone($this->timezone));
            }
        } catch (Exception $e) {
            return '';
        }

        switch ($format) {
            case 'arrumar':
                $out = $includeTime ? $date->format('Y-m-d H:i:s') : $date->format('Y-m-d');
                break;
            case 'mostrar':
                $out = $includeTime ? $date->format('d/m/Y H:i:s') : $date->format('d/m/Y');
                break;
            case 'iso8601':
            case 'c':
                $out = $date->format('c');
                break;
            case 'extenso':
                $out = $date->format('d \d\e F \d\e Y');
                break;
            case 'timestamp':
                $out = $date->getTimestamp();
                break;
            case 'age':
                $currentDate = new DateTime();
                $difference = $currentDate->diff($date);
                $out = [$difference->y, $difference->m, $difference->d];
                break;
            default:
                $out = $date->format('Y-m-d');
                break;
        }

        return $out;
    }

    /**
     * Format a phone number.
     * @return string
     */
    public function fone(): string
    {
        $fone = $this->parseInt();
        $ddd = '(' . substr($fone, 0, 2) . ') ';
        $fone = substr($fone, 2);
        $out = $ddd . substr($fone, 0, 4) . substr($fone, 4);
        if (strlen($fone) === 9) { // nono digito
            $out = $ddd . substr($fone, 0, 5) . substr($fone, 5);
        }
        return $out;
    }

    /**
     * Format a postal code (CEP).
     * @return string
     */
    public function cep(): string
    {
        $cep = $this->parseInt();
        return substr($cep, 0, 5) . '-' . substr($cep, 5);
    }

    /**
     * Return the absolute value of the string formatted as a US decimal.
     * @return float|string
     */
    public function decimal(bool $returnAsString = true)
    {
        $var = $this->string;
        if (stripos((string) $var, ',') > -1) { // se achar virgula, veio da view, com formato. da base, nao vem virgula
            $var = $this->parseInt(true);
            $var = substr((string) $var, 0, strlen((string) $var) - 2)
                . "."
                . substr((string) $var, strlen((string) $var) - 2, 2);
        }
        return $returnAsString ? (string) $var : (float) $var;
    }

    /**
     * Remove all characters is not a number
     *
     * @param mixed $var
     * @return ?string
     */
    public function parseInt($isNumber = false)
    {
        $str = (string) $this->string;

        // mantém e considera o traço um valor negativo
        if ($isNumber) {
            // remover virgulas
            $str = str_replace([',', '.'], '', $str);
            $str = str_replace(['R$', ' ', '-'], ['', '', '-'], $str); // Remove 'R$', espaços e mantém o sinal negativo
            $str = str_replace(',', '', $str); // Remove a vírgula

            // Remove any remaining non-numeric characters except for the negative sign
            $str = preg_replace("/[^-0-9]/", "", $str);
            if (preg_match('/-?\d+/', $str, $matches)) {
                return $matches[0];
            }
        } else {
            // Remove any remaining non-numeric characters
            $str = preg_replace("/[^0-9]/", "", $str);
            if (preg_match('/\d+/', $str, $matches)) {
                return $matches[0];
            }
        }

        return null;
    }

    /**
     * Convert a date string to a Unix timestamp.
     * @return int
     */
    public function dateToMktime(): int
    {
        $date = (string) $this->string;
        if (strlen($date) < 6) {
            return time();
        }
        $date = $this->date('arrumar', true);
        $dt = DateTime::createFromFormat('Y-m-d H:i:s', $date);
        return $dt ? $dt->getTimestamp() : time();
    }

    /**
     * Subtract or add days to the current date.
     * @param int $days
     * @param string $operation
     * @return string
     */
    public function subDays(int $days, string $operation = '-'): string
    {
        $d = $this->date();
        $date = new Date($d);
        if ($operation === '-') {
            $date->sub("$days days");
        } else {
            $date->add("$days months");
        }
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * Subtract or add months to the current date.
     * @param int $months
     * @param string $operation
     * @return string
     */
    public function subMonths(int $months, string $operation = '-'): string
    {
        $d = $this->date();
        $date = new Date($d);
        if ($operation === '-') {
            $date->sub("$months months");
        } else {
            $date->add("$months months");
        }
        return $date->format('Y-m-d H:i:s');
    }


    /**
     * Format a number for display.
     * @param bool $signAtEnd
     * @param string|bool $prefix
     * @param bool $color
     * @return string
     */
    public function formatNumber(bool $signAtEnd = false, $prefix = true, bool $color = true): string
    {
        $number = $this->string;

        if ($prefix) {
            $prefix = '<strong><small>' .
                (($prefix !== true) ? $prefix : 'R$')
                . '</small> </strong> ';
        }
        $out = $prefix . number_format((float) $number, 2, ',', '.');
        if ($signAtEnd) {
            $out = $prefix . number_format(abs($number), 2, ',', '.') . (($number < 0) ? '-' : '+');
        }
        if ($color && $number < 0) {
            $out = '<span class="text-red">' . $out . '</span>';
        }

        return $out;
    }

    /**
     * Format a CPF or CNPJ number.
     * @return string
     */
    public function cpfCnpj(): string
    {
        $var = $this->parseInt();

        if (strlen($var) === 11) { // cpf
            $out = substr($var, 0, 3) . '.' . substr($var, 3, 3) . '.' . substr($var, 6, 3) . '-' . substr($var, 9, 2);
        } else if (strlen($var) === 14) { // cnpj
            $out = substr($var, 0, 2) . '.' . substr($var, 2, 3) . '.' . substr($var, 5, 3) . '/' . substr($var, 8, 4) . '-' . substr($var, 12, 2);
        } else {
            $out = $var;
        }
        return $out;
    }

    /**
     * Convert seconds to a human-readable time format.
     * @return string
     */
    public function humanTimeFromSeconds(): string
    {
        $seconds = (int) $this->string;

        $days = floor($seconds / 86400);
        $hours = floor(($seconds % 86400) / 3600);
        $minutes = floor(($seconds % 3600) / 60);
        $remainingSeconds = $seconds % 60;

        $format = '';
        $separator = '';

        if ($days > 0) {
            $format .= $days . 'd';
            $separator = ' ';
        }

        if ($hours > 0) {
            $format .= $separator . $hours . 'h';
            $separator = ' ';
        }

        if ($minutes > 0) {
            $format .= $separator . $minutes . 'm';
            $separator = ' ';
        }

        if ($remainingSeconds > 0) {
            $format .= $separator . $remainingSeconds . 's';
        }

        return $format;
    }
}
