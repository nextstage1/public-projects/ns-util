<?php

namespace NsUtil\Helpers;

use DateTime;
use stdClass;
use Exception;
use NsUtil\Http\Ssl;
use NsUtil\Http\Http;
use NsUtil\Helpers\Filter;
use NsUtil\Helpers\Format;
use NsUtil\Helpers\Packer;
use function NsUtil\json_decode;
use NsUtil\Helpers\Traits\FileTrait;
use NsUtil\Helpers\Traits\ArrayTrait;
use NsUtil\Helpers\Traits\StringTrait;
use NsUtil\Helpers\DirectoryManipulation;
use NsUtil\Integracao\Cep as IntegracaoCep;
use NsUtil\Helpers\Traits\CompatibilityTrait;

class Utils
{
    use StringTrait, ArrayTrait, FileTrait, CompatibilityTrait;

    // public static function nsIncludeConfigFile(string $dirArquivoSample): ?array
    // {
    //     if (!file_exists($dirArquivoSample)) {
    //         throw new Exception("File does not exist: $dirArquivoSample");
    //     }
    //     $dirArquivoSample = realpath($dirArquivoSample);
    //     $temp = explode(DIRECTORY_SEPARATOR, $dirArquivoSample);
    //     $configName = array_pop($temp);

    //     $t = explode(DIRECTORY_SEPARATOR, __DIR__);
    //     $file = 'nao-deve-achar.json';
    //     array_pop($t);
    //     array_pop($t);
    //     while (!file_exists($file)) {
    //         array_pop($t);
    //         $dir = implode(DIRECTORY_SEPARATOR, $t) . DIRECTORY_SEPARATOR;
    //         $file = $dir . 'composer.json';
    //     }
    //     self::mkdir($dir . DIRECTORY_SEPARATOR . 'nsConfig', 0600);
    //     $config = $dir . DIRECTORY_SEPARATOR . 'nsConfig' . DIRECTORY_SEPARATOR . $configName;
    //     if (!file_exists($config)) {
    //         copy($dirArquivoSample, $config);
    //         echo "<h1>nsConfig: É necessário criar o arquivo de configuração '[DIR_COMPOSER]/nsConfig/$configName'. <br/>Tentei gravar um modelo. Caso não esteja, existe um padrão na raiz da aplicação.</h1>";
    //         die();
    //     }
    //     include_once $config;
    //     $var = str_replace('.php', '', $configName);
    //     return $$var ?? null;
    // }

    /**
     * @deprecated Method is disabled. Use class Eficiencia()
     */
    public static function eficiencia_init()
    {
        return "Method is disabled. Use class Eficiencia()";
    }

    /**
     * @deprecated Method is disabled. Use class Eficiencia()
     */
    public static function endEficiencia()
    {
        return "Method is disabled. Use class Eficiencia()";
    }

    public static function directorySeparator(&$var): void
    {
        $var = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $var);
    }

    public static function deleteDir($pasta)
    {
        return DirectoryManipulation::deleteDirectory($pasta);
    }

    public static function packerAndPrintJS($js): void
    {
        echo Packer::jsPack($js, true, false);
    }

    public static function base64_to_jpeg($base64_string, $output_file)
    {
        $ifp = fopen($output_file, 'wb');
        $data = explode(',', $base64_string);
        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);
        return $output_file;
    }

    public static function extractDataAtributesFromHtml($html)
    {
        $list = explode("data-", $html);
        unset($list[0]);
        $out = [];
        foreach ($list as $item) {
            $atribute = explode('"', $item);
            $out[str_replace('=', '', $atribute[0])] = $atribute[1];
        }
        return $out;
    }

    /**
     * Detecta se a chamada foi feita de um dispositivo mobile
     * @return boolean
     */
    public static function isMobile()
    {
        return Validate::isMobile();
    }

    public static function isJson($jsonString): bool
    {
        return Validate::isJson($jsonString);
    }

    /**
     * Retorna o IP em uso pelo cliente
     *
     * @return string
     */
    public static function getIP(): string
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && strlen($_SERVER['HTTP_X_FORWARDED_FOR']) > 0) {
            $parts = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = trim(end($parts));
        } else {
            $ip = $_SERVER['HTTP_CLIENT_IP'] ?? $_SERVER['REMOTE_ADDR'] ?? '-';
        }
        return Filter::string($ip);
    }


    public static function getSO()
    {
        return mb_strtolower(explode(' ', php_uname())[0]);
    }

    /**
     * Remove all characters is not a number
     *
     * @param mixed $var
     * @param bool $isNumber
     * @return ?int
     */
    public static function parseInt($var, bool $isNumber = false)
    {
        return (new Format($var))->parseInt($isNumber);
        // $str = (string) $var;

        // // mantém e considera o traço um valor negativo
        // if ($isNumber) {
        //     // remover virgulas
        //     $str = str_replace([',', '.'], '', $str);
        //     $str = str_replace(['R$', ' ', '-'], ['', '', '-'], $str); // Remove 'R$', espaços e mantém o sinal negativo
        //     $str = str_replace(',', '', $str); // Remove a vírgula

        //     if (preg_match('/-?\d+/', $str, $matches)) {
        //         return (int) $matches[0];
        //     }

        //     return null;
        // } else {
        //     return preg_replace("/[^0-9]/", "", (string) $var);
        // }
    }

    /**
     * Ira buscar o path da aplicação, antes da pasta /vendor
     */
    public static function getPathApp(): string
    {
        $vendorDir = DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR;
        $baseDir = strstr(__DIR__, $vendorDir, true);
        return $baseDir ?: __DIR__;
    }

    /**
     * Retorna um valor baseado em um tipo
     * @param mixed $string
     * @param string $type
     */
    public static function getValByType($string, string $type)
    {
        switch ($type) {
            case 'int':
            case 'serial':
            case 'serial4':
            case 'serial8':
                $out = filter_var($string, FILTER_VALIDATE_INT);
                if ($out === false) {
                    $out = null;
                }
                break;
            case 'double':
                $out = filter_var($string, FILTER_VALIDATE_FLOAT);
                if ($out === false) {
                    $out = null;
                }
                break;
            case 'html':
                $out = $string;
                break;
            case 'bool':
            case 'boolean':
                $out =
                    self::compareString($string, 'true')
                    || self::compareString($string, true)
                    || self::compareString($string, '(true)')
                    || self::compareString($string, '1')
                    || self::compareString($string, 1);
                break;
            default:
                $out = null === $string ? null : Filter::string($string);
                break;
        }
        return $out;
    }

    public static function formatDate($date, $escolha = 'arrumar', $datahora = false, $alterarTimeZone = false, $timezone = 'America/Sao_Paulo')
    {
        return (new Format($date, $timezone))->date($escolha, $datahora, $alterarTimeZone);
    }

    public static function formatCep($cep)
    {
        return (new Format($cep))->cep();
    }

    public static function formatCpfCnpj($var)
    {
        return (new Format($var))->cpfCnpj();
    }


    public static function getPsr4Name(?string $dir = null)
    {
        $dir ??= self::getPathApp();

        $composer = file_get_contents(self::fileSearchRecursive('composer.json', $dir));
        $composer = \json_decode($composer, true);

        return substr(
            str_replace('\\\\', '\\', key($composer['autoload']['psr-4'])),
            0,
            -1
        );
    }


    public static function getPhpUser()
    {
        $user = get_current_user();
        return $user ? ['name' => $user] : -1;
    }

    // Define uma função que poderá ser usada para validar e-mails usando regexp
    public static function validaEmail($email)
    {
        return Validate::validaEmail($email);
    }

    /**
     * Verifica se a string é uma base64_encoded valida
     * @param string $data
     * @return boolean
     */
    public static function isBase64Encoded($string)
    {
        return Validate::isBase64Encoded($string);
    }

    /**
     * Prepara um valor para o formato americano decimal
     *
     * @param mixed $var
     * @return float|string
     */
    public static function decimalFormat($var, bool $returnAsString = false)
    {
        return (new Format($var))->decimal($returnAsString);
    }

    /**
     * Retorna um array com as variaveis para paginação de resultados
     * @param int $page Pagina atual
     * @param int $limitPerPage limite por página
     * @param int $totalRegs total de registros do request
     * @return array
     */
    public static function pagination(int $atualPage, int $limitPerPage, int $totalRegs): array
    {
        $ret = [];
        $ret['atualPage'] = $atualPage;
        $ret['nextPage'] = ((($limitPerPage * ($atualPage + 1)) < $totalRegs) ? ($atualPage + 1) : false);
        $ret['prevPage'] = (($atualPage > 0) ? $atualPage - 1 : false);
        $ret['totalPages'] = ceil(($totalRegs / $limitPerPage));
        return $ret;
    }



    /**
     * Return de icon name
     *
     * @param string $filename
     * @return string
     */
    public static function getThumbsByFilename(string $filename): string
    {
        $t = explode('.', $filename);
        $extensao = array_pop($t);
        $out = '';
        switch (mb_strtoupper((string) $extensao)) {
            case 'XLSX':
            case 'XLS':
                $out = 'file-excel-o';
                break;
            case 'PDF':
                $out = 'file-pdf-o';
                break;
            case 'PNG':
            case 'JPG':
            case 'GIF':
            case 'JPEG':
                $out = 'file-image-o';
                break;
            case 'ZIP':
                $out = 'file-archive-o';
                break;
            case 'MP3':
            case 'AAC':
                $out = 'file-audio-o';
                break;
            case 'AVI':
            case 'MP4':
                $out = 'file-video-o';
                break;
            default:
                $out = 'file';
        }
        return $out;
    }

    /**
     * Executa uma busca no viacep e retorna
     * @param string $cep
     * @return stdClass
     */
    public static function buscacep(string $cep): stdClass
    {
        return json_decode(IntegracaoCep::getAddress($cep));
    }

    public static function setPaginacao(int $registros, int $limit, array &$out, ?array $dados = null): void
    {
        $paginas = (int) ($registros / $limit);
        $dados ??= ['page' => 0];
        $out['pagination'] = [];
        $out['pagination']['page'] ??= 0;
        $out['pagination']['atualPage'] = (int) ($dados['page'] ?? 0);
        $out['pagination']['totalPages'] = ($paginas * $limit < $registros) ? $paginas + 1 : $paginas;
        $out['pagination']['totalItens'] = $registros;
        $out['pagination']['nextPage'] = $out['pagination']['page'] + 1 < $out['pagination']['totalPages'] ? $out['pagination']['page'] + 1 : null;
        $out['pagination']['previusPage'] = $out['pagination']['page'] - 1 >= 0 ? $out['pagination']['page'] - 1 : null;
        $out['pagination']['initalPage'] = 0;
        $out['pagination']['lastPage'] = $out['pagination']['totalPages'];
    }

    /**
     * @return string
     */
    public static function getTmpDir(): string
    {
        return File::getTmpDir();
    }

    /**
     * @return string
     */
    public static function getHost(): string
    {
        return php_uname('n');
    }

    public static function addConditionFromAPI(array &$condition, array $dados): void
    {
        if (isset($dados['conditions']) && is_array($dados['conditions'])) {
            $newConditions = [];
            foreach ($dados['conditions'] as $key => $val) {
                $isJson = json_decode($val, true);
                if ($isJson) {
                    $val = $isJson;
                }
                $newConditions[$key . '_addedConditionFromAPI'] = $val;
            }
            $condition = array_merge($condition, $newConditions);
        }
    }

    public static function httpsForce()
    {
        if ($_SERVER["HTTPS"] != "on") {
            header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
            die();
        }
    }

    public static function generateCode($string)
    {
        return strtoupper(substr($string, 0, 1) . substr($string, -1) . substr(md5($string), 0, 2));
    }

    public static function shellExec($command, $suppressOutput = false)
    {
        $descriptorspec = [
            0 => ['pipe', 'r'], // stdin is a pipe that the child will read from
            1 => ['pipe', 'w'], // stdout is a pipe that the child will write to
            2 => ['pipe', 'w'], // stderr is a pipe that the child will write to
        ];

        $process = proc_open($command, $descriptorspec, $pipes);

        if (is_resource($process)) {
            fclose($pipes[0]); // Close the stdin pipe, since we won't be using it

            // Read the output from stdout and stderr pipes
            $output = '';
            while (($buffer = fgets($pipes[1])) !== false || ($buffer = fgets($pipes[2])) !== false) {
                $output .= $buffer;
                if (!$suppressOutput) {
                    echo $buffer; // Display the output in real-time
                    flush(); // Force the output to be sent to the browser

                    // If PHP output buffering is enabled, force it to be sent to the browser
                    if (ob_get_length() > 0) {
                        ob_flush();
                    }
                }
            }

            fclose($pipes[1]);
            fclose($pipes[2]);

            $return_value = proc_close($process);

            // echo "Command returned: $return_value\n";
        }
    }

    public static function commandPrintHeader($text, $size = 40, $suppressOutput = false)
    {
        $cmd = "header=\"$text\" && width=$size && padding=\$(((\$width - \${#header}) / 2)) && printf '%*s\n' \"\${COLUMNS:-40}\" \"\" | tr ' ' '-' | cut -c 1-\"\${width}\" && printf \"|%*s%s%*s|\n\" \$padding \"\" \"\$header\" \$padding \"\" && printf '%*s\n' \"\${COLUMNS:-80}\" \"\" | tr ' ' '-' | cut -c 1-\"\${width}\"";
        self::shellExec($cmd, $suppressOutput);
    }

    public static function uniqueHash($prefix = '')
    {
        return md5($prefix . uniqid(rand(), true));
    }

    /**
     * Obtém a data de vecimento do certificado anexado a URL ou null caso não exista
     *
     * @param string $url
     * @return ?int
     */
    public static function sslGetEndDate(string $url)
    {
        return Ssl::getEndDate($url);
    }

    public static function generateUuidV4(): string
    {
        $data = random_bytes(16);

        // Define os bytes conforme a especificação do UUID v4
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // Versão 4
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // Variante RFC 4122

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    /**
     * Get the contents of a file with optional SSL verification.
     *
     * @param string $url
     * @param bool $ssl
     * @param int $timeout
     * @return string
     */
    public static function myFileGetContents(string $url, bool $ssl = false, int $timeout = 30): string
    {
        return Http::getUrlContents($url, $ssl, $timeout);
    }

    public static function curlCall(string $url, array $params = [], string $method = 'GET', array $header = ['Content-Type: application/json'], bool $ssl = true, int $timeout = 30): object
    {
        $response = Http::call($url, $params, $method, $header, $ssl, $timeout);

        return (object) [
            'content' => $response->getBody(),
            'errorCode' => $response->getErrorCode(),
            'error' => $response->getError(),
            'status' => $response->getHttpCode(),
            'http_code' => $response->getHttpCode(),
            'headers' => $response->getHeaders(),
            'url' => $response->getUrl()
        ];
    }

    public static function feriado($date)
    {
        $url = 'https://syncpay.usenextstep.com.br/api/util/feriado/' . self::parseInt($date);
        $response = Http::call($url, [], 'GET', ['Content-Type: application/json'], false, 10);
        return json_decode($response->getBody(), true)['content'] ?? null;
    }

    public static function calculaVencimentoUtil(string $vencimento, int $prazo = 0)
    {
        if ($prazo > 0) {
            // rotina para varrer somente dias úteis
            $count = 0;
            while ($count < $prazo) {
                $ret = self::feriado($vencimento);
                $vencimento = $ret['proxDiaUtil'];
                $count++;
            }
        }
        return $vencimento;
    }

    public static function hasContent($value, $type = 'string')
    {
        return Validate::hasContent($value, $type);
    }

    /**
     * Retorna um array com errors
     * @param array $dadosObrigatorios Array contendo array: ['value' => $dados['idCurso'], 'msg' => 'Informe a data inicial', 'type' => 'int'],
     */
    public static function validarCamposObrigatorios(array $dadosObrigatorios)
    {
        return Validate::validarCamposObrigatorios($dadosObrigatorios);
    }

    public static function sizeToHumanReader(int $size)
    {
        $unidades = array('B', 'KB', 'MB', 'GB', 'TB');
        $i = 0;

        while ($size >= 1024 && $i < count($unidades) - 1) {
            $size /= 1024;
            $i++;
        }

        return round($size, 2) . ' ' . $unidades[$i];
    }


    /**
     * Aplica um namespace específico ao conteúdo de um arquivo.
     *
     * Esta função substitui o namespace original no conteúdo do arquivo pelo namespace fornecido.
     *
     * @param string $file O caminho do arquivo ao qual o namespace será aplicado.
     * @param string $originalNamespace O namespace original a ser substituído no conteúdo do arquivo.
     *                                  O valor padrão é 'NsUtil'.
     * @return void
     */
    public static function applyNamespace($file, string $originalNamespace = 'NsUtil'): void
    {
        // Obtém o namespace atual
        $namespace = self::getPsr4Name();

        // Lê o conteúdo do arquivo
        $fileContent = file_get_contents($file);

        // Substitui o namespace original pelo novo namespace no conteúdo do arquivo
        $content = str_replace(
            ["namespace $originalNamespace"],
            ["namespace $namespace"],
            $fileContent
        );

        // Salva o arquivo com o novo conteúdo
        // O parâmetro 'SOBREPOR' indica que o arquivo será sobrescrito com o novo conteúdo
        self::saveFile($file, false, $content, 'SOBREPOR');
    }

    public static function isRunningUnderNohup(): bool
    {
        if (function_exists('posix_getppid')) {
            $parentPid = posix_getppid();
            $parentProcess = shell_exec("ps -p $parentPid -o comm="); // Get the command name of the parent process
            return strpos($parentProcess, 'nohup') !== false; // Check if 'nohup' is in the parent process name
        }
        return false;
    }
}
