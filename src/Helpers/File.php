<?php

namespace NsUtil\Helpers;

use Exception;
use finfo;

/**
 * Class File
 * 
 * Provides utility functions for file handling.
 */
class File
{
    public function __construct() {}

    /**
     * Returns the MIME type of the file.
     *
     * @param string $file The path to the file.
     * @param bool $encoding Whether to include encoding in the MIME type.
     * @return string The MIME type of the file.
     * @throws Exception If the file does not exist, is not readable, or finfo_open function is not enabled.
     */
    public static function getMimeType(string $file, bool $encoding = true): string
    {
        if (!file_exists($file)) {
            throw new Exception("File $file does not exist");
        }
        if (!function_exists('finfo_open')) {
            throw new Exception("finfo_open function is not enabled");
        }

        if (is_file($file) && is_readable($file)) {
            $finfo = new finfo($encoding ? FILEINFO_MIME : FILEINFO_MIME_TYPE);
            $out = explode(';', $finfo->file($file))[0];
        } else {
            throw new Exception("File $file is not readable");
        }

        return str_replace('.', '-', $out);
    }

    /**
     * Returns the system's temporary directory.
     *
     * @return string Path to the temporary directory.
     */
    public static function getTmpDir(): string
    {
        return sys_get_temp_dir()
            ?? $_SERVER['TMP']
            ?? $_SERVER['TEMP']
            ?? $_SERVER['TMPDIR']
            ?? '/tmp';
    }

    public static function sendFileToBrowser($path, bool $download = true, bool $cache = true)
    {
        if (!file_exists($path)) {
            throw new Exception("File $path does not exist");
        }

        // Limpa qualquer saída anterior
        if (ob_get_level()) {
            ob_end_clean();
        }

        // Headers default para embed ou download forced
        header('Content-Security-Policy: frame-ancestors *');
        header('X-Frame-Options: ALLOW-FROM *');

        // Metadados
        header('Content-Type: ' . self::getMimeType($path));
        header('Content-Length: ' . filesize($path));
        header('Content-Transfer-Encoding: binary');

        // Headers de otimização
        header('Accept-Ranges: bytes');
        header('X-Accel-Buffering: no');
        header('X-Content-Type-Options: nosniff');

        // Headers de cache
        if ($cache) {
            $etag = md5($path);
            header('ETag: "' . $etag . '"');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
            header('Cache-Control: public, max-age=31536000');
        }

        // Remove headers que podem atrasar
        header_remove('Pragma');
        header_remove('Expires');

        $disposition = $download ? 'attachment' : 'inline';
        if ($download) {
            header('Content-Description: File Transfer');
        }
        header('Content-Disposition: ' . $disposition . '; filename="' . basename($path) . '"');

        // Transmita o arquivo para o cliente
        readfile($path);
        exit;
    }
}
