<?php

/**
 * NsUtil namespace.
 */

namespace NsUtil\Helpers;

/**
 * Cpu class.
 *
 * This class provides functionalities related to the CPU.
 */
class Cpu
{
    /**
     * Count the number of processors in the system.
     *
     * @return int Number of processors.
     */
    public static function count(): int
    {
        if (is_file('/proc/cpuinfo')) {
            $cpuinfo = file_get_contents('/proc/cpuinfo');
            // Match processor entries in the file
            preg_match_all('/^processor/m', $cpuinfo, $matches);
            $processors = count($matches[0]);
        }

        return $processors ?? 1;
    }

    /**
     * Get the CPU usage.
     *
     * @return float|null The CPU usage percentage or null.
     */
    public static function usage(): ?float
    {
        return self::getServerLoad();
    }

    /**
     * Extracts and returns data about CPU load from the "/proc/stat" file on Linux.
     *
     * @return array|null Returns an array containing User, Nice, System, and Idle values or null if data couldn't be extracted.
     */
    private static function _getServerLoadLinuxData(): ?array
    {
        // Check if file is readable
        if (is_readable("/proc/stat")) {
            $stats = @file_get_contents("/proc/stat");
            if ($stats !== false) {
                // Normalize spaces and split into lines
                $stats = preg_replace("/[[:blank:]]+/", " ", $stats);
                $stats = str_replace(array("\r\n", "\n\r", "\r"), "\n", $stats);
                $stats = explode("\n", $stats);

                // Extract the main CPU load data
                foreach ($stats as $statLine) {
                    $statLineData = explode(" ", trim($statLine));

                    // If the data line is for the main CPU
                    if (
                        (count($statLineData) >= 5) &&
                        ($statLineData[0] == "cpu")
                    ) {
                        return [
                            (int)$statLineData[1],
                            (int)$statLineData[2],
                            (int)$statLineData[3],
                            (int)$statLineData[4],
                        ];
                    }
                }
            }
        }

        return null;
    }

    /**
     * Retrieves the server load.
     *
     * On Windows, it uses the "wmic" command to get the CPU load percentage.
     * On other OSes (primarily Linux), it extracts data from "/proc/stat".
     *
     * @return float|null The server load percentage as a number (without the percent sign) or null.
     */
    private static function getServerLoad(): ?float
    {
        $load = null;
        $os = PHP_OS;
        $isWindows = stristr(PHP_OS, "win");
        $isLinux = stristr(PHP_OS, "linux");
        $isMac = stristr(PHP_OS, "darwin");

        switch (true) {
            case $isWindows:
                $cmd = "wmic cpu get loadpercentage /all";
                @exec($cmd, $output);
                if ($output) {
                    foreach ($output as $line) {
                        if ($line && preg_match("/^[0-9]+\$/", $line)) {
                            $load = (float)$line;
                            break;
                        }
                    }
                }
                break;

            case $isLinux:
                // Tenta primeiro o comando top
                $cmd = "top -bn1 | grep 'Cpu(s)' | sed 's/.*, *\\([0-9.]*\\)%* id.*/\\1/' | awk '{print 100 - $1}'";
                @exec($cmd, $output);

                if ($output && isset($output[0])) {
                    $load = (float)$output[0];
                    break;
                }
                // Se top falhar, tenta mpstat
                $cmd = "mpstat 1 1 | grep 'Average' | awk '{print $12}'";
                @exec($cmd, $output);

                if ($output && isset($output[0])) {
                    $load = (float)$output[0];
                    break;
                }

                // Se ainda falhar, tenta uptime
                $cmd = "uptime | awk -F'[a-z]:' '{ print $2}'";
                @exec($cmd, $output);

                if ($output && isset($output[0])) {
                    $values = array_map('trim', explode(',', $output[0]));
                    $load = (float)$values[0];
                }
                break;

            case $isMac:
                $cmd = "top -l 1 | grep 'CPU usage' | awk '{print $3}'";
                @exec($cmd, $output);
                if ($output) {
                    $load = (float)$output[0];
                }

                break;

            default:
                break;
        }

        return $load;
    }
}
