<?php

namespace NsUtil\Helpers;

class JsonParser
{
    /**
     * Parses a JSON string or array/object into a PHP array or object.
     *
     * @param mixed $json The JSON data to parse. Can be a string, array, or object.
     * @param bool $assoc When true, returned objects will be converted into associative arrays.
     * @param int $depth User specified recursion depth.
     * @param int $options Bitmask of JSON decode options.
     * @return mixed The parsed JSON data.
     */
    public static function handle($json, bool $assoc = false, int $depth = 512, int $options = 0)
    {
        if (is_string($json)) {
            $json = str_replace(['&#34;'], ['\"'], trim($json));
        }

        switch (true) {
            case null === $json:
                $data = \json_decode('{}', $assoc, $depth, $options);
                break;
            case is_array($json):
                $data = (count($json) === 0)
                    ? \json_decode('{}', $assoc, $depth, $options)
                    : $json;
                break;
            case is_object($json):
                $data = $json;
                break;
            case !self::isValidJson($json):
                $data = null;
                break;
            default:
                $jsonTransformed = str_replace(['&#34;'], ['\"'], $json);
                $data = \json_decode($jsonTransformed, $assoc, $depth, $options);
                break;
        }

        return \json_decode(
            \json_encode($data),
            $assoc,
            $depth,
            $options
        );
    }

    /**
     * Validates if a string is a valid JSON without full parsing
     * 
     * @param mixed $string
     * @return bool
     */
    public static function isValidJson($string): bool
    {
        if (!is_string($string)) {
            return false;
        }

        $length = strlen($string);
        $depthKey = 0;
        $depthArray = 0;
        $inString = false;
        $escaped = false;
        $startChar = $string[0] ?? '';
        $endChar = $string[$length - 1] ?? '';

        // Verifica se começa com { ou [ e termina com } ou ]
        if (!(($startChar === '{' && $endChar === '}') ||
            ($startChar === '[' && $endChar === ']'))) {
            return false;
        }

        for ($i = 0; $i < $length; $i++) {
            $char = $string[$i];

            if ($inString) {
                if ($escaped) {
                    $escaped = false;
                } elseif ($char === '\\') {
                    $escaped = true;
                } elseif ($char === '"') {
                    $inString = false;
                }
                continue;
            }

            switch ($char) {
                case '"':
                    $inString = true;
                    break;
                case '{':
                    $depthKey++;
                    break;
                case '[':
                    $depthArray++;
                    break;
                case '}':
                    $depthKey--;
                    if ($depthKey < 0) {
                        return false;
                    }
                    break;
                case ']':
                    $depthArray--;
                    if ($depthArray < 0) {
                        return false;
                    }
                    break;
            }
        }

        $ret =  $depthKey === 0 && $depthArray === 0 && !$inString;

        return $ret;
    }
}
