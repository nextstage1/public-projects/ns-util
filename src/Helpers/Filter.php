<?php

namespace NsUtil\Helpers;

class Filter
{

    public function __construct() {}

    /**
     * Reproduces the behavior of the deprecated FILTER_SANITIZE_STRING
     * @param string $string
     * @return string
     */
    public static function string(?string $string): string
    {
        $string ??= '';
        $str = preg_replace('/\x00|<[^>]*>?/', '', $string);
        return (string) $str;
    }

    /**
     * Validates and converts a string to an integer
     * @param mixed $string
     * @return int
     */
    public static function integer($string): int
    {
        return (int) filter_var($string, FILTER_VALIDATE_INT);
    }
}
