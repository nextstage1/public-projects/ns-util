<?php

namespace NsUtil\Helpers;

use Exception;

use function NsUtil\now;

class DirectoryManipulation
{
    public array $total;
    public string $dir;

    public function __construct()
    {
        $this->total = [];
    }

    /**
     * Recursively calculates the size of files in a directory.
     *
     * @param string $dir The directory to calculate the size of.
     * @return void
     */
    public function getSizeOf(string $dir): void
    {
        if ($diretorio = opendir($dir)) {
            while (false !== ($file = readdir($diretorio))) {
                $path = $dir . DIRECTORY_SEPARATOR . $file;
                if (is_dir($path) && ($file != ".") && ($file != "..")) {
                    $this->getSizeOf($path);
                } elseif (is_file($path)) {
                    $t = explode('.', $file);
                    $type = mb_strtolower(array_pop($t));
                    if (!isset($this->total[$type])) {
                        $this->total[$type]['count'] = 0;
                        $this->total[$type]['size'] = 0;
                    }
                    $this->total[$type]['count']++;
                    $this->total[$type]['size'] += filesize($path);
                }
            }
            closedir($diretorio);
        }
    }

    /**
     * Gets the size of files of a specific type in a directory.
     *
     * @param string $dir The directory to calculate the size of.
     * @param string $type The file type to calculate the size of.
     * @return array The size and count of files of the specified type.
     */
    public function getSize(string $dir, string $type): array
    {
        $this->getSizeOf($dir);
        return $this->total[mb_strtolower($type)];
    }

    /**
     * Reads a directory and returns the files contained within it. Not recursive.
     *
     * @param string $dir The directory to read.
     * @return array The list of files in the directory.
     * @throws Exception If the parameter is not a directory.
     */
    public static function openDir(string $dir): array
    {
        if (!is_dir($dir)) {
            throw new Exception("'$dir' is not a directory");
        }
        $out = [];
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." && stripos($file, '__NEW__') === false) {
                    $out[] = $file;
                }
            }
            closedir($handle);
        }
        return $out;
    }

    /**
     * Copies a directory recursively.
     *
     * @param string $src The source directory.
     * @param string $dst The destination directory.
     * @param bool $sobrepor Whether to overwrite existing files.
     * @return void
     */
    public static function recursiveDirectoryCopy(string $src, string $dst, bool $sobrepor = true): void
    {
        self::recurseCopy($src, $dst, $sobrepor);
    }

    /**
     * Helper function to copy directories recursively.
     *
     * @param string $src The source directory.
     * @param string $dst The destination directory.
     * @param bool $sobrepor Whether to overwrite existing files.
     * @return void
     */
    public static function recurseCopy(string $src, string $dst, bool $sobrepor = true): void
    {
        $dir = opendir($src);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    self::recurseCopy($src . '/' . $file, $dst . '/' . $file, $sobrepor);
                } else {
                    Utils::createTreeDir($dst . '/' . $file);
                    $origem = $src . '/' . $file;
                    $destino = $dst . '/' . $file;
                    Utils::directorySeparator($origem);
                    Utils::directorySeparator($destino);

                    if (!file_exists($origem)) {
                        echo "File $origem not found \n";
                    } else {
                        if (file_exists($destino) && $sobrepor === false) {
                            continue;
                        } else {
                            copy($origem, $destino);
                        }
                    }
                }
            }
        }
        closedir($dir);
    }

    /**
     * Removes all contents of a directory. Does not remove the directory itself.
     *
     * @param string $dir The directory to clear.
     * @param int $days The number of days to keep files.
     * @return void
     */
    public static function clearDir(string $dir, int $days = 7): void
    {
        $files = self::openDir($dir);
        foreach ($files as $file) {
            $filename = $dir . DIRECTORY_SEPARATOR . $file;
            if (is_dir($filename)) {
                self::clearDir($filename, $days);
                // check if the directory is empty
                if (count(scandir($filename)) <= 2) {
                    self::deleteDirectory($filename);
                }
            } else {
                $createdAt = filemtime($filename);
                $removeAt = now()->sub("$days days")->timestamp();
                if ($createdAt <= $removeAt) {
                    unlink($filename);
                }
            }
        }
    }

    /**
     * Deletes a directory and its contents.
     *
     * @param string $pasta The directory to delete.
     * @return bool True if the directory was deleted, false otherwise.
     */
    public static function deleteDirectory(string $pasta): bool
    {
        Utils::directorySeparator($pasta);
        if (!is_dir($pasta)) {
            return true;
        }

        $iterator = new \RecursiveDirectoryIterator($pasta, \FilesystemIterator::SKIP_DOTS);
        $rec_iterator = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($rec_iterator as $file) {
            $file->isFile() ? unlink($file->getPathname()) : rmdir($file->getPathname());
        }

        rmdir($pasta);
        return !is_dir($pasta);
    }

    /**
     * Gets the timestamp of the last file created in a directory.
     *
     * @param string $path The directory to check.
     * @return int|null The timestamp of the last file created, or null if no files found.
     */
    public static function getLastFileCreated(string $path): ?int
    {
        $arquivos = scandir($path);
        if ($arquivos === false) {
            return null;
        }

        $arquivos = array_diff($arquivos, array('.', '..'));
        $dataUltimoArquivo = null;

        foreach ($arquivos as $arquivo) {
            $caminhoCompleto = $path . DIRECTORY_SEPARATOR . $arquivo;
            if (is_file($caminhoCompleto)) {
                $dataModificacao = filemtime($caminhoCompleto);
                if ($dataUltimoArquivo === null || $dataModificacao > $dataUltimoArquivo) {
                    $dataUltimoArquivo = $dataModificacao;
                }
            }
        }

        return $dataUltimoArquivo;
    }
}
