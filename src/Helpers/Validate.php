<?php

namespace NsUtil\Helpers;

use Exception;
use NsUtil\Api;
use NsUtil\Exceptions\ValidateException;
use NsUtil\Log;

class Validate
{
    private $obrigatorios = [
        'list' => []
    ];

    private static function multiplica_cnpj($cnpj, $posicao = 5)
    {
        // Variável para o cálculo
        $calculo = 0;
        // Laço para percorrer os item do cnpj
        for ($i = 0; $i < strlen((string) $cnpj); $i++) {
            // Cálculo mais posição do CNPJ * a posição
            $calculo = $calculo + ($cnpj[$i] * $posicao);
            // Decrementa a posição a cada volta do laço
            $posicao--;
            // Se a posição for menor que 2, ela se torna 9
            if ($posicao < 2) {
                $posicao = 9;
            }
        }
        // Retorna o cálculo
        return $calculo;
    }

    public static function isCep(string $cep): bool
    {
        return preg_match('/^\d{8}$/', $cep) === 1;
    }


    public static function validaCpfCnpj($val)
    {
        $val = is_array($val) || is_object($val) ? null : $val;
        $val = str_replace(['-', '.', '/', ' '], '', (string) $val);
        if (strlen($val) === 11) {
            return self::validaCPF($val);
        }
        if (strlen($val) === 14) {
            return self::validaCnpj($val);
        }
        return 'Preencha corretamente CPF/CNPJ';
    }

    private static function validaCPF($cpf = null)
    {
        // Verifica se um número foi informado
        if (empty($cpf) || $cpf === '') {
            return 'CPF Inválido: Vazio';
        }
        // Elimina possivel mascara
        $cpf = str_replace(['-', '.', '/', ' '], '', (string) $cpf);
        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cpf) != 11) {
            return 'CPF Inválido: Menor que 11 digitos';
        }
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if (
            $cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999'
        ) {
            return 'CPF Inválido: Número Sequencial';
            // Calcula os digitos verificadores para verificar se o
            // CPF é válido
        } else {

            for ($t = 9; $t < 11; $t++) {

                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf[$c] * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf[$c] != $d) {
                    return 'CPF Inválido: Digito verificador não é válido';
                }
            }
            return true;
        }
    }

    private static function validaCnpj($cnpj = null)
    {
        $cnpj = str_replace(['-', '.', '/', ' '], '', (string) $cnpj);
        if (empty($cnpj) || $cnpj === '') {
            return 'CNPJ Inválido: Vazio';
        }
        if (strlen((string) $cnpj) != 14) {
            return 'CNPJ Inválido: Menor que 14 digitos';
        }
        if ($cnpj === '00000000000000') {
            return 'CNPJ Inválido: Número sequencial';
        }
        $cnpj = (string) $cnpj;
        $cnpj_original = $cnpj;
        $primeiros_numeros_cnpj = substr((string) $cnpj, 0, 12);


        // Faz o primeiro cálculo
        $primeiro_calculo = self::multiplica_cnpj($primeiros_numeros_cnpj);

        // Se o resto da divisão entre o primeiro cálculo e 11 for menor que 2, o primeiro
        // Dígito é zero (0), caso contrário é 11 - o resto da divisão entre o cálculo e 11
        $primeiro_digito = ($primeiro_calculo % 11) < 2 ? 0 : 11 - ($primeiro_calculo % 11);

        // Concatena o primeiro dígito nos 12 primeiros números do CNPJ
        // Agora temos 13 números aqui
        $primeiros_numeros_cnpj .= $primeiro_digito;

        // O segundo cálculo é a mesma coisa do primeiro, porém, começa na posição 6
        $segundo_calculo = self::multiplica_cnpj($primeiros_numeros_cnpj, 6);
        $segundo_digito = ($segundo_calculo % 11) < 2 ? 0 : 11 - ($segundo_calculo % 11);

        // Concatena o segundo dígito ao CNPJ
        $cnpj = $primeiros_numeros_cnpj . $segundo_digito;

        // Verifica se o CNPJ gerado é idêntico ao enviado
        if ($cnpj === $cnpj_original) {
            return true;
        } else {
            return 'CNPJ Inválido: Cálculo do dígito verificador inválido';
        }
    }

    // Define uma função que poderá ser usada para validar e-mails usando regexp
    public static function validaEmail($email)
    {
        $er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
        if (preg_match($er, $email)) {
            return true;
        } else {
            return false;
        }
    }


    public static function isBase64Encoded($string)
    {
        if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', (string) $string)) {
            return true;
        } else {
            return false;
        }
    }

    public static function isJson($jsonString): bool
    {
        return JsonParser::isValidJson($jsonString);
        // $isJson = false;
        // $jsonString = Filter::string($jsonString);

        // // Verifica se o conteúdo começa e termina com colchetes ou chaves
        // if (
        //     (strpos($jsonString, '[') === 0 && strrpos($jsonString, ']') === strlen($jsonString) - 1) ||
        //     (strpos($jsonString, '{') === 0 && strrpos($jsonString, '}') === strlen($jsonString) - 1)
        // ) {
        //     // Verifica se o conteúdo é um JSON válido usando json_decode()
        //     json_decode($jsonString);
        //     $isJson = json_last_error() === JSON_ERROR_NONE;
        // }

        // return $isJson;
    }

    public static function isMobile(): bool
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", (string) ($_SERVER["HTTP_USER_AGENT"] ?? '-')) === 1;
    }

    public static function hasContent($value, $type = 'string')
    {
        if (is_array($value)) {
            foreach ($value as $item) {
                if (!self::hasContent($item, $type)) {
                    return false;
                }
            }
            return true;
        }
        $out = false;
        switch ($type) {
            case 'int':
                $t = (int) $value;
                $out = $t > 0;
                break;
            case 'array':
                $out = is_array($value);
                break;
            case 'email':
                $out = is_string($value) && strlen((string) $value) > 0 && self::validaEmail((string) $value);
                break;
            case 'cpf':
            case 'cnpj':
                $out = is_string($value) && strlen((string) $value) > 0 && self::validaCpfCnpj((string) $value) === true;
                break;
                // default mode
            default:
                $out = strlen((string) $value) > 0;
                break;
        }
        return $out;
    }

    public static function validarCamposObrigatorios(array $dadosObrigatorios)
    {
        $error = [];
        foreach ($dadosObrigatorios as $item) {
            $has = self::hasContent($item['value'], $item['type'] ?? 'string');
            if ($has === false) {
                if ($item['key']) {
                    $error[$item['key']] = $item['msg'];
                } else {
                    $error[] = $item['msg'];
                }
            }
        }
        return $error;
    }

    public function addCampoObrigatorio(string $key, ?string $msg = null, string $type = 'string'): Validate
    {

        $this->obrigatorios['list'][] = [
            'key' => $key,
            'msg' => $msg ?? "Param '$key' not found or invalid",
            'type' => $type
        ];
        return $this;
    }

    public function getValidadeAsArray(array $data): array
    {
        $campos = [];
        foreach ($this->obrigatorios['list'] ?? [] as $item) {
            $value = $data;
            $keys = explode('.', $item['key']);

            // Navega através das chaves aninhadas
            foreach ($keys as $k) {
                $value = is_array($value) ? ($value[$k] ?? null) : null;
            }

            $campos[] = [
                'key' => $item['key'],
                'value' => $value,
                'msg' => $item['msg'],
                'type' => $item['type']
            ];
        }
        return self::validarCamposObrigatorios($campos);
    }

    public static function getValidateDataModel(string $key, ?string $msg = null, string $type = 'string'): array
    {
        return ['key' => $key, 'msg' => $msg, 'type' => $type];
    }

    /**
     * If fail, the ValidateException will be thrown.
     * @param array $data
     * @param array $keysToValidate
     * @return void
     */
    public static function validateOrException(array $data, array $keysToValidate = [])
    {
        $val = new Validate();
        foreach ($keysToValidate as $item) {
            $parts = explode('|', $item);
            $val->addCampoObrigatorio(
                $parts[0],
                $parts[2] ?? null,
                $parts[1] ?? 'string'
            );
        }

        $error = $val->getValidadeAsArray($data);
        if (count($error) > 0) {
            throw new ValidateException(json_encode($error));
        }
    }


    /**
     * Valida os dados informados em $data. Caso não seja satisfeito, retorna o codigo definido
     * @param array $data
     * @param \NsUtil\Api $api
     * @param int $errorCode
     * @return void
     */
    public function runValidateData(array $data, Api $api, $errorCode = 200)
    {
        $error = $this->getValidadeAsArray($data);
        if (count($error) > 0) {
            $api->error($error, $errorCode);
        }
    }

    /**
     * If fail, the Api will response with the error code defined.
     * @param mixed $api
     * @param array $keysToValidate
     * @param int $errorCode
     * @return void
     */
    public static function validateOrFail(?Api $api = null, array $keysToValidate = [], int $errorCode = 400)
    {
        $api ??= new Api();
        $val = new Validate();
        foreach ($keysToValidate as $item) {
            $parts = explode('|', $item);
            $val->addCampoObrigatorio(
                $parts[0],
                $parts[2] ?? null,
                $parts[1] ?? 'string'
            );
        }
        $val->runValidateData($api->getBody(), $api, $errorCode);
    }


    /**
     * Adiciona as chaves para validação
     *
     * @param array $keysToValidate
     * @param array $valuesToValidate
     * @return array
     */
    public static function validate(array $keysToValidate, array $valuesToValidate, $throwException = false): array
    {
        $v = new Validate();
        foreach ($keysToValidate as $item) {
            $v->addCampoObrigatorio($item);
        }
        $errors = $v->getValidadeAsArray($valuesToValidate);
        if (count($errors) > 0 && $throwException) {
            Log::logTxt('/tmp/nsutil-send-with-aws.log', 'VALIDATE: ' . json_encode($errors, JSON_PRETTY_PRINT));
            throw new Exception(json_encode($errors));
        }

        return $errors;
    }
}
