<?php

namespace NsUtil\Helpers\Traits;

use Exception;
use NsUtil\Helpers\Filter;
use NsUtil\Helpers\JsonParser;
use stdClass;

use function NsUtil\json_decode;

trait ArrayTrait
{
    use FileTrait;
    /**
     * Orders an array by a specific element.
     *
     * @param array $array
     * @param string $element
     * @param string $sort
     * @return void
     */
    public static function arrayOrderBy(array &$array, string $element, string $sort = 'ASC'): void
    {
        usort($array, function ($a, $b) use ($element, $sort) {
            if ($sort === 'ASC') {
                return $a[$element] <=> $b[$element];
            } else {
                return $b[$element] <=> $a[$element];
            }
        });
    }

    /**
     * Searches an array by a specific key and value.
     *
     * @param array $array
     * @param string $chave
     * @param string $valor
     * @return array
     * @throws Exception
     */
    public static function arraySearchByKey(array &$array, string $chave, string $valor): array
    {
        $key = array_search($valor, array_column($array, $chave));
        if (false !== $key) {
            return $array[$key];
        } else {
            return [];
        }
    }

    /**
     * Maps an array based on a given mapping array.
     *
     * @param array $depara
     * @param array $dados
     * @param bool $retornaSomenteDepara
     * @return array
     */
    public static function depara(array $depara, array $dados, bool $retornaSomenteDepara = true): array
    {
        $out = $retornaSomenteDepara ? [] : $dados;
        foreach ($depara as $key => $val) {
            $out[$key] = $dados[$val] ?? null;
        }
        return $out;
    }

    /**
     * Converts an array to CSV format and optionally saves it to a file.
     *
     * @param array $array 
     * @param string $filepath if false, returns as text
     * @param bool $withBom
     * @return mixed
     */
    public static function array2csv(array $array, ?string $filepath = null, bool $withBom = true, string $delimiter = ',')
    {
        // Manter o padrão entre as chaves
        $trataed = [];
        $keys = array_keys($array[0]);
        foreach ($array as $val) {
            $ni = [];
            foreach ($keys as $k) {
                $ni[$k] = $val[$k] ?? '';
            }
            $trataed[] = $ni;
        }
        $array = $trataed;

        if (null !== $filepath) {

            // garantir que o arquivo exista
            self::saveFile($filepath, false, '', 'SOBREPOR');

            if (!file_exists($filepath) || !is_writable($filepath)) {
                throw new Exception("Failed to create or write to the file: $filepath");
            }

            // BOM
            if ($withBom) {
                file_put_contents($filepath, chr(0xEF) . chr(0xBB) . chr(0xBF), FILE_APPEND);
            }

            // Gravar o cabeçalho
            self::myFPutsCsv($filepath, array_keys($array[0]));

            // Gravar dados
            foreach ($array as $linha) {
                foreach ($linha as $key => $val) {
                    if (is_array($val) || is_object($val)) {
                        $linha[$key] = json_encode($val, JSON_HEX_QUOT | JSON_HEX_APOS | JSON_UNESCAPED_UNICODE);
                    }
                }
                self::myFPutsCsv($filepath, $linha, $delimiter);
            }
            // fclose($fp);
            return file_exists($filepath);
        } else {
            $handle = fopen('php://temp', 'r+');
            foreach ($array as $line) {
                foreach ($line as $key => $val) {
                    if (is_array($val) || is_object($val)) {
                        $line[$key] = json_encode($val, JSON_HEX_QUOT | JSON_HEX_APOS | JSON_UNESCAPED_UNICODE);
                    }
                }
                fputcsv($handle, $line, $delimiter, '"');
            }
            rewind($handle);
            $contents = '';
            while (!feof($handle)) {
                $contents .= fread($handle, 8192);
            }
            fclose($handle);
            return $contents;
        }
    }

    /**
     * Converts a PHP array to a ".env" file format.
     *
     * @param array $config
     * @return array
     */
    public static function array2env(array $config): array
    {
        $out = [];
        foreach ($config as $key => $value) {
            if (is_object($value)) {
                $value = json_decode(json_encode($value), true);
            }
            if (is_array($value)) {
                $out[] = "\n[$key]";
                $temp = self::array2env($value);
                $out[] = implode("\t\n", $temp);
                $out[] = "\n";
            } else {
                $out[] = "$key=\"$value\"";
            }
        }
        return $out;
    }

    /**
     * Returns an array containing only the selected keys.
     *
     * @param array $origem
     * @param array $chaves
     * @return array
     */
    public static function arrayReduceKeys(array $origem, array $chaves): array
    {
        $out = [];
        foreach ($chaves as $val) {
            $out[$val] = $origem[$val] ?? null;
        }
        return $out;
    }

    /**
     * Receives data from a view and applies filters.
     *
     * @param array $dados
     * @return void
     */
    public static function recebeDadosFromView(array &$dados): void
    {
        if (is_array($dados)) {
            foreach ($dados as $key => $value) {
                if (is_array($value)) {
                    self::recebeDadosFromView($dados[$key]);
                } else {
                    if ($value === 'undefined' || $value === 'null' || $value === null) {
                        unset($dados[$key]);
                        continue;
                    }

                    // Trata booleanos
                    if (is_bool($value) || $value === 'true' || $value === 'false') {
                        $dados[$key] = filter_var($value, FILTER_VALIDATE_BOOLEAN);
                        continue;
                    }

                    if (is_int($value)) {
                        $dados[$key] = filter_var($value, FILTER_VALIDATE_INT);
                        continue;
                    }

                    // Trata números decimais
                    if (is_float($value)) {
                        $dados[$key] = (float) filter_var($value, FILTER_VALIDATE_FLOAT);
                        continue;
                    }

                    // Trata JSON strings
                    if (JsonParser::isValidJson($value)) {
                        $dados[$key]  = JsonParser::handle($value, true);
                        continue;
                    }

                    $dados[$key] = Filter::string($value);
                    $dados[$key] = str_replace(['NS21', '&#34;'], ['&', '"'], $dados[$key]);
                }
                if (substr((string) $key, 0, 2) === 'id') {
                    $dados[$key] = Filter::integer($value);
                }
            }
        }
    }

    /**
     * Reescrito pois a funcao nativa estava causando problemas entre o delimitar e ele cosntar no corpo de um item.
     * @param mixed $handle
     * @param mixed $delimiter
     * @param mixed $enclosure
     * @param mixed $escape
     * @return array|bool
     */
    private static function ns_fgetcsv($handle, $delimiter  = ',', $enclosure = '"', $escape = "\\")
    {
        if (($line = fgets($handle)) === false) {
            return false;
        }

        $fields = [];
        $currentField = '';
        $insideEnclosure = false;
        $len = strlen($line);

        for ($i = 0; $i < $len; $i++) {
            $char = $line[$i];
            $nextChar = ($i < $len - 1) ? $line[$i + 1] : null;

            // Tratamento de escape
            if ($char === $escape && $nextChar === $enclosure) {
                $currentField .= $enclosure;
                $i++;
                continue;
            }

            // Tratamento de enclosure
            if ($char === $enclosure) {
                $insideEnclosure = !$insideEnclosure;
                continue;
            }

            // Tratamento de delimitador
            if ($char === $delimiter && !$insideEnclosure) {
                $fields[] = $currentField;
                $currentField = '';
                continue;
            }

            // Caracteres normais
            $currentField .= $char;
        }

        // Adiciona o último campo
        $fields[] = rtrim($currentField); // Remove quebra de linha do último campo

        // Tratamento de codificação e limpeza
        $fields = array_map(function ($field) {
            $field = trim($field);
            return mb_convert_encoding($field, "UTF-8");
        }, $fields);

        return $fields;
    }

    /**
     * Returns an associative array from a handle opened via fopen.
     *
     * @param mixed $handle
     * @param string $explode
     * @return mixed
     */
    public static function myFGetsCsv($handle, $explode = ';', $blockSize = 0, $enclosure = "\"", $escape = "\\")
    {
        // $data = fgetcsv($handle, $blockSize, trim($explode), $enclosure, $escape);
        $data = self::ns_fgetcsv($handle, $explode, $enclosure, $escape);
        if ($data === false || $data === null) {
            return false;
        }

        $substituions = [
            '|EN|' => $explode,
            chr(10) => "___NS_EOL___",
            "\\n" => "___NS_EOL___",
            "\\t" => ' ',
            "\\r" => '',
            "0x0d" => '',
            ';' => ' ',
            "'" => '',
        ];

        $line = (string) trim(
            (string)
            implode('[_|M|_]', $data)
        );

        $line = str_replace(
            array_keys($substituions),
            array_values($substituions),
            $line
        );

        // devolver as novas linhas
        $line = str_replace('\\', '', $line);
        $line = str_replace(['___NS_EOL___'], ["\\n"], $line);

        $line = mb_convert_encoding($line, "UTF-8");
        $data = explode('[_|M|_]', $line);

        return $data;
    }

    /**
     * Writes an array of fields to a CSV file.
     *
     * @param string $file_path
     * @param array $fields
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape_char
     * @return void
     */
    public static function myFPutsCsv(string $file_path, array $fields, string $delimiter = ',', string $enclosure = '"', string $escape_char = "\\")
    {
        if (!file_exists($file_path)) {
            self::saveFile($file_path, '', '');
        }

        $escaped_fields = [];

        // Trata cada campo
        foreach ($fields as $field) {
            // Escapa aspas dentro do campo
            $field = str_replace($enclosure, $escape_char . $enclosure, $field);

            // Substitui quebras de linha reais por literais \n e \r
            $field = str_replace(["\n", "\r"], ["\\n", "\\r"], $field);

            // Envolve o campo com delimitadores se contiver o delimitador ou a quebra de linha escapada
            if (strpos($field, $delimiter) !== false || strpos($field, "\n") !== false || strpos($field, $enclosure) !== false) {
                $field = "{$enclosure}{$field}{$enclosure}";
            }

            $escaped_fields[] = $field;
        }

        // Cria a linha CSV
        $csv_line = implode((string) $delimiter, $escaped_fields) . "\n"; // Cast delimiter to string

        // Escreve no arquivo
        file_put_contents($file_path, $csv_line, FILE_APPEND);
    }

    /**
     * json_decode with treatment for some characters that cause issues.
     * @param mixed $json
     * @return null|array
     */
    public static function jsonToArrayFromView($json): ?array
    {
        return json_decode($json, true);
    }

    /**
     * Converts a PHP object to an array.
     * @param stdClass $object
     * @return array
     */
    public static function objectPHP2Array(stdClass $object)
    {
        return json_decode($object, true);
    }

    /**
     * Método que retorna um array com as diferenças entre dois arrays
     *
     * @param array $arrayNew
     * @param array $arrayOld
     * @param array $keysToIgnore
     * @return array
     */
    public static function arrayDiff(array $arrayNew, array $arrayOld, array $keysToIgnore = [])
    {
        $out = [];

        $alteradosNovo = array_diff_assoc($arrayNew, $arrayOld);
        $alteradosAntigo = array_diff_assoc($arrayOld, $arrayNew);
        unset($alteradosNovo['error']);

        if (count($alteradosNovo) > 0) {
            foreach ($alteradosNovo as $key => $value) {

                if (array_search($key, $keysToIgnore) !== false) {
                    continue;
                }

                $json = is_string($value) ? json_decode((string) $value, true) : null;

                if (is_array($json) && is_string($alteradosAntigo[$key])) {
                    if (array_search($key, $keysToIgnore) !== false) {
                        continue;
                    }
                    $jsonOLD = json_decode((string) $alteradosAntigo[$key], true);
                    $out = array_merge($out, self::arrayDiff($json, $jsonOLD));
                } else {
                    $out[] = [
                        'field' => $key,
                        'old' => isset($alteradosAntigo[$key]) ? $alteradosAntigo[$key] : null,
                        'new' => $value
                    ];
                }
            }
        }

        return $out;
    }
}
