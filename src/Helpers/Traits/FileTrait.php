<?php

namespace NsUtil\Helpers\Traits;

use Exception;
use NsUtil\Helpers\LoadCSVToArray;
use NsUtil\Log;

trait FileTrait
{
    /**
     * Create a directory if it does not exist.
     *
     * @param string $path
     * @param int $perm
     * @return void
     */
    public static function mkdir(string $path, int $perm = 0777): void
    {
        if (!is_dir($path) && !is_file($path)) {
            @mkdir($path, $perm, true);
        }
    }

    /**
     * Save content to a file.
     *
     * @param string $filename
     * @param string|false $name
     * @param string $template
     * @param string $mode
     * @return bool
     */
    public static function saveFile(string $filename, $name = false, string $template = '<?php Header("Location:/");', string $mode = "w+"): bool
    {

        $filename .= strlen((string) $name) > 0 ? "/{$name}" : '';
        $file = self::createTreeDir($filename);
        if (file_exists($filename) && $mode !== 'SOBREPOR' && $mode !== 'OVERWRITE') {
            $file->name = "__NEW__{$file->name}";
        }
        $save = str_replace('/', DIRECTORY_SEPARATOR, $file->path . DIRECTORY_SEPARATOR . $file->name);
        unset($filename);
        file_put_contents($save, $template);
        $saved = (bool) file_exists($save);
        if (!$saved) {
            Log::error("Error on save file {$save} on disk (FTT47)");
        } else {
            chmod($save, 0777);
        }

        return $saved;
    }

    /**
     * Delete a file or directory.
     *
     * @param string $filepath
     * @param bool $apagarDiretorio
     * @param bool $trash
     * @return bool
     */
    public static function deleteFile(string $filepath, bool $apagarDiretorio = false, bool $trash = false): bool
    {
        $filename = str_replace('/', DIRECTORY_SEPARATOR, $filepath);

        if (is_dir($filename)) {
            $dir = dir($filename);
            while ($arquivo = $dir->read()) {
                if ($arquivo != '.' && $arquivo != '..') {
                    self::deleteFile($filename . DIRECTORY_SEPARATOR . $arquivo, false, $trash);
                }
            }
            $dir->close();
            if ($apagarDiretorio) {
                rmdir($filename);
            }
        } else {
            if (file_exists($filename)) {
                unlink($filename);
            }
        }

        return !file_exists($filename);
    }



    /**
     * Get the encoding of a file.
     *
     * @param string $filename
     * @return string
     * @throws Exception
     */
    public static function fileGetEncoding(string $filename): string
    {
        $so = php_uname();
        $cod = '';
        if (stripos($so, 'linux') > -1) {
            $cmd = 'file -bi ' . $filename . ' | sed -e "s/.*[ ]charset=//"';
            $cod = shell_exec($cmd);
        } else {
            throw new Exception('getFileEncoding only works on Linux systems. Yours is ' . $so);
        }
        return trim($cod);
    }

    /**
     * Convert a file to UTF-8 encoding.
     *
     * @param string $filepath
     * @param string|false $output
     * @return string|void
     * @throws Exception
     */
    public static function fileConvertToUtf8(string $filepath, $output = false)
    {
        if (file_exists($filepath)) {
            $enc = self::fileGetEncoding($filepath);
            if (strlen($enc) > 0 && $enc !== 'utf-8' && stripos($enc, 'ascii') === false) {
                $output = $output ? $output : $filepath;
                $cmd = "iconv -f $enc -t utf-8 -o $output $filepath ";
                $ret = shell_exec($cmd);
                if (strlen($ret) > 0) {
                    throw new Exception("Error converting file $filepath to UTF-8: " . $ret);
                }
            }
        } else {
            return 'File not exists';
        }
    }

    /**
     * Search for a file recursively up to a specified depth.
     *
     * @param string $file_name
     * @param string $dir_init
     * @param int $deep
     * @return string|false
     */
    public static function fileSearchRecursive(string $file_name, string $dir_init, int $deep = 10)
    {
        $dirarray = explode(DIRECTORY_SEPARATOR, rtrim($dir_init, DIRECTORY_SEPARATOR));
        $count = 0;

        while ($count < $deep) {
            $filename = implode(DIRECTORY_SEPARATOR, $dirarray) . DIRECTORY_SEPARATOR . $file_name;

            if (file_exists($filename)) {
                return realpath($filename);
            }

            if (empty($dirarray)) {
                break;
            }

            array_pop($dirarray);
            $count++;
        }

        return false;
    }

    /**
     * Read the contents of a gzipped file.
     *
     * @param string $filename
     * @return string
     */
    public static function gzReader(string $filename): string
    {
        $out = '';
        $buffer_size = 4096;
        $file = gzopen($filename, 'rb');

        while (!gzeof($file)) {
            $out .= gzread($file, $buffer_size);
        }

        gzclose($file);
        return $out;
    }

    /**
     * Cria a arvore de diretorios
     * @param string $filename
     * @return object
     */
    public static function createTreeDir($filename)
    {
        $path = str_replace('/', DIRECTORY_SEPARATOR, (string) $filename);
        $parts = explode(DIRECTORY_SEPARATOR, $path);
        $file = array_pop($parts);
        $dir = implode(DIRECTORY_SEPARATOR, $parts);
        self::mkdir($dir, 0777);
        // @mkdir($dir, 0777, true);
        return (object) ['path' => $dir, 'name' => $file];
    }

    /**
     * Conta a quantidade linhas em um arquivo CSV ou TXT
     * @param string $file
     * @return int
     */
    public static function linhasEmArquivo(string $file)
    {
        if (!file_exists($file)) {
            throw new Exception("File $file not found");
        }

        $l = 0;
        if ($f = fopen($file, "r")) {
            while ($d = fgets($f, 1000)) {
                $l++;
            }
        }
        unset($d);
        fclose($f);
        return $l;
    }

    public static function fileCSVToArray(string $csvFile, ?string $explode = null, int $blockSize = 0, string $enclosure = '"'): array
    {
        return (new LoadCSVToArray($csvFile))->run();
    }
}
