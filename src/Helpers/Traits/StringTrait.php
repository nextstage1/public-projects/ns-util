<?php

namespace NsUtil\Helpers\Traits;

use NsUtil\Helpers\Filter;

trait StringTrait
{
    public static array $prefixo = ['mem_', 'sis_', 'anz_', 'aux_', 'app_', 'nsl_'];

    private static array $cache = [];

    /**
     * Sanitize function
     *
     * @param string $str
     * @return string
     */
    public static function sanitize(string $str): string
    {
        $from = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
        $to = "aaaaeeiooouucAAAAEEIOOOUUC";
        $keys = [];
        $values = [];
        preg_match_all('/./u', $from, $keys);
        preg_match_all('/./u', $to, $values);
        $mapping = array_combine($keys[0], $values[0]);
        $str = strtr($str, $mapping);
        $str = preg_replace("/[^A-Za-z0-9]/", "_", $str);
        return $str;
    }

    public static function convertAscii(string $string): string
    {

        $search = ['“', '”', '‘', '’', '–', '—', '•', '…', 'é', 'è', 'ê', 'ë', 'á', 'à', 'â', 'ä', 'í', 'ì', 'î', 'ï', 'ó', 'ò', 'ô', 'ö', 'ú', 'ù', 'û', 'ü', 'ç', 'ñ'];
        $replace = ['"', '"', "'", "'", '-', '--', '*', '...', 'e', 'e', 'e', 'e', 'a', 'a', 'a', 'a', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'c', 'n'];

        $string = str_replace($search, $replace, $string);

        // Remove any remaining non-ASCII characters
        return preg_replace('/[^\x20-\x7E]/', '', $string);
    }

    /**
     * Convert a string to camelCase
     *
     * @param mixed $string
     * @param array|null $prefixo
     * @return mixed
     */
    public static function name2CamelCase($string, ?array $prefixo = null)
    {

        $prefixo ??= null;

        if (is_array($string)) {
            $out = [];
            foreach ($string as $key => $value) {
                $out[self::name2CamelCase($key)] = $value;
            }
            return $out;
        }

        if (is_array($prefixo)) {
            $string = str_replace($prefixo, "", $string);
        }

        // cache
        if (isset(self::$cache[$string])) {
            return self::$cache[$string];
        }

        // Processamento otimizado
        $string = mb_strtolower($string);
        $string = str_replace(['_', '-'], ' ', $string);
        $out = lcfirst(str_replace(' ', '', ucwords($string)));

        // Armazena em cache
        self::$cache[$string] = $out;

        return $out;

        // $prefixo = $prefixo ?? self::$prefixo;

        // if (is_array($string)) {
        //     $out = [];
        //     foreach ($string as $key => $value) {
        //         $out[self::name2CamelCase($key)] = $value;
        //     }
        //     return $out;
        // }

        // if (is_array($prefixo)) {
        //     foreach ($prefixo as $val) {
        //         $string = str_replace($val, "", $string);
        //     }
        // }

        // $string = str_replace(['_', '-'], ' ', $string);
        // $out = lcfirst(str_replace(' ', '', ucwords($string)));
        // return $out;
    }

    /**
     * Convert a camelCase string to snake_case
     *
     * @param string $string
     * @return string
     */
    public static function reverteName2CamelCase(string $string): string
    {
        $out = '';
        $length = strlen($string);

        for ($i = 0; $i < $length; $i++) {
            $currentChar = $string[$i];

            if (ctype_upper($currentChar) && $string[$i] !== '.' && $string[$i] !== '|') {
                $out .= $i > 0 ? '_' : '';
                $currentChar = strtolower($currentChar);
            }
            $out .= $currentChar;
        }
        return $out;
    }

    /**
     * Convert a string to kebab-case
     *
     * @param string $string
     * @return string
     */
    public static function name2KebabCase(string $string): string
    {
        if (stripos($string, '_') === false) {
            $string = self::reverteName2CamelCase($string);
        }

        return str_replace('_', '-', $string);
    }

    /**
     * Highlight a search term in a text
     *
     * @param string $texto
     * @param string $search
     * @return string
     */
    public static function highlightText(string &$texto, string $search): void
    {
        $searchsan = self::sanitize($search);
        $textosan = self::sanitize($texto);
        $inicio = stripos($textosan, $searchsan);
        if ($inicio !== false) {
            $trecho = mb_substr($texto, $inicio, strlen($search));
            $texto = str_replace($trecho, '<span class="ns-highlight-text">' . $trecho . '</span>', $texto);
        }
    }

    /**
     * Convert a plural word to its singular form
     *
     * @param string $word
     * @return string
     */
    public static function singularize(string $word): string
    {
        $pluralEndings = [
            '/(alias|address)es$/i' => '\1',
            '/([^aeiouy])ies$/i' => '\1y',
            '/(ss)$/i' => 'ss',
            '/(n)ews$/i' => '\1ews',
            '/(r)ice$/i' => '\1ice',
            '/(children)$/i' => 'child',
            '/(m)en$/i' => '\1an',
            '/(t)eeth$/i' => '\1ooth',
            '/(f)eet$/i' => '\1oot',
            '/(g)eese)$/i' => '\1oose',
            '/(m)ice$/i' => '\1ouse',
            '/(x|ch|ss|sh)es$/i' => '\1',
            '/(m)ovies$/i' => '\1ovie',
            '/(s)eries$/i' => '\1eries',
            '/([^aeiouy]|qu)ies$/i' => '\1y',
            '/([lr])ves$/i' => '\1f',
            '/(tive)s$/i' => '\1',
            '/(hive)s$/i' => '\1',
            '/(pri)ces$/i' => '\1ce',
            '/(b)uses$/i' => '\1us',
            '/(shoe)s$/i' => '\1',
            '/(o)es$/i' => '\1',
            '/(ax|test)es$/i' => '\1is',
            '/(octop|vir)i$/i' => '\1us',
            '/(status)$/i' => '\1',
            '/(alias)es$/i' => '\1',
            '/s$/i' => '',
        ];

        foreach ($pluralEndings as $pattern => $replacement) {
            if (preg_match($pattern, $word)) {
                return preg_replace($pattern, $replacement, $word);
            }
        }

        return $word;
    }

    /**
     * Convert a singular word to its plural form
     *
     * @param string $word
     * @return string
     */
    public static function pluralize(string $word): string
    {
        $singularEndings = [
            '/(quiz)$/i' => '\1zes',
            '/(matr|vert|ind)ix|ex$/i' => '\1ices',
            '/(x|ch|ss|sh)$/i' => '\1es',
            '/(r|t|h|s|z)$/i' => '\1es',
            '/([^aeiouy]|qu)ies$/i' => '\1y',
            '/(m)ovies$/i' => '\1ovie',
            '/(s)eries)$/i' => '\1eries',
            '/(n)ews)$/i' => '\1ews',
            '/(child)$/i' => '\1ren',
            '/(bus)$/i' => '\1es',
            '/(woman)$/i' => '\1women',
            '/(man)$/i' => '\1men',
            '/(tooth)$/i' => '\1teeth',
            '/(foot)$/i' => '\1feet',
            '/(person)$/i' => '\1people',
            '/(goose)$/i' => '\1geese',
            '/(mouse)$/i' => '\1mice',
            '/(cactus)$/i' => '\1cacti',
            '/(knife)$/i' => '\1knives',
            '/(leaf)$/i' => '\1leaves',
            '/(life)$/i' => '\1lives',
            '/(wife)$/i' => '\1wives',
            '/(hero)$/i' => '\1heroes',
            '/(potato)$/i' => '\1potatoes',
            '/(tomato)$/i' => '\1tomatoes',
            '/(buffalo)$/i' => '\1buffaloes',
            '/(index)$/i' => '\1indices',
            '/(alias)$/i' => '\1aliases',
            '/(status)$/i' => '\1status',
            '/(radius)$/i' => '\1radii',
            '/(syllabus)$/i' => '\1syllabi',
            '/(focus)$/i' => '\1foci',
            '/(fungus)$/i' => '\1fungi',
            '/(datum)$/i' => '\1data',
            '/(appendix)$/i' => '\1appendices',
            '/(bacterium)$/i' => '\1bacteria',
            '/(curriculum)$/i' => '\1curricula',
            '/^(compan)y$/i' => '\1ies',
        ];

        foreach ($singularEndings as $pattern => $replacement) {
            if (preg_match($pattern, $word)) {
                return preg_replace($pattern, $replacement, $word);
            }
        }

        return "{$word}s";
    }

    /**
     * Sanitize an array of input data based on identified types
     *
     * @param mixed $var
     * @return mixed
     */
    public static function filterSanitize($var)
    {
        if (is_array($var)) {
            foreach ($var as $key => $value) {
                if (is_array($value)) {
                    $var[$key] = self::filterSanitize($value);
                } else {
                    if (substr($key, 0, 2) === 'id') {
                        $var[$key] = filter_var($value, FILTER_VALIDATE_INT);
                    }
                    if (stripos($key, 'email') > -1) {
                        $var[$key] = filter_var($value, FILTER_VALIDATE_EMAIL);
                    } else {
                        $var[$key] = Filter::string($value);
                    }
                }
            }
            return $var;
        } else {
            return Filter::string($var);
        }
    }

    /**
     * Compare two strings for equality
     *
     * @param mixed $str1
     * @param mixed $str2
     * @param bool $case
     * @return bool
     */
    public static function compareString($str1, $str2, bool $case = false): bool
    {
        // Pequenas conversões para facilitar a comparação
        if (is_bool($str1)) {
            $str1 = $str1 ? 'true' : 'false';
        }
        if (is_bool($str2)) {
            $str2 = $str2 ? 'true' : 'false';
        }

        if (is_array($str1)) {
            $str1 = implode(',', $str1);
        }

        if (is_array($str2)) {
            $str2 = implode(',', $str2);
        }

        if (is_object($str1)) {
            $str1 = (string)$str1;
        }

        if (is_object($str2)) {
            $str2 = (string)$str2;
        }

        if (!$case) {
            return mb_strtoupper((string)$str1) === mb_strtoupper((string)$str2);
        }
        return $str1 === $str2;
    }

    /**
     * Format a string such that the first letter of each word is uppercase and the rest are lowercase,
     * except for certain words which are always lowercase.
     *
     * @param string $str The input string to format.
     * @return string The formatted string.
     */
    public static function formatTextAllLowerFirstUpper(string $str): string
    {
        $alwaysLowercase = ['a', 'ao', 'da', 'de', 'do', 'dos', 'das', 'ante', 'até', 'após', 'desde', 'em', 'entre', 'com', 'contra', 'para', 'por', 'perante', 'sem', 'sobe', 'sob'];
        $string = ucwords(mb_strtolower($str));
        foreach ($alwaysLowercase as $value) {
            $string = str_ireplace(" $value ", " " . mb_strtolower($value) . " ", $string);
        }
        return $string;
    }
}
