<?php

namespace NsUtil\Helpers\Traits;

use NsUtil\Helpers\JsonParser;

trait CompatibilityTrait
{
    public static function explode($delimiter, $string): array
    {
        return explode((string) $delimiter, (string) $string) ?? [];
    }

    public static function json_decode($json, bool $assoc = false, int $depth = 512, int $options = 0)
    {
        return JsonParser::handle($json, $assoc, $depth, $options);
    }

    public static function str_replace($search, $replace, $subject)
    {
        return str_replace((string) $search, (string) $replace, (string) $subject);
    }

    public static function str_ireplace($search, $replace, $subject)
    {
        return str_ireplace((string) $search, (string) $replace, (string) $subject);
    }
    public static function stripos($haystack, $needle, $offset = 0)
    {
        return stripos((string) $haystack, (string) $needle, (int) $offset);
    }
}
