<?php

namespace NsUtil\Helpers;

/**
 * Class to check and manage queue processes
 */
class BashUtils
{
    /**
     * Check if a process is running and execute an action if not
     * 
     * @param string $processName Process name to check (exact match)
     * @param callable|null $callback Action to execute if no process is running
     * @return bool Returns true if process is not running and callback was executed
     */
    public static function checkIfProcessIsRunning(
        string $processName,
        ?callable $processFoundCallback = null,
        ?callable $noProcessFoundCallback = null
    ): bool {
        // Usando ps para ter o comando completo e grep para match exato
        $command = "ps aux | grep -F " . escapeshellarg($processName) . " | grep -v grep";
        exec($command, $output, $returnCode);

        // Se não encontrou nenhum processo
        if (empty($output)) {
            if ($noProcessFoundCallback !== null) {
                $noProcessFoundCallback();
            }
            return true;
        }

        if ($processFoundCallback !== null) {
            $processFoundCallback();
        }

        return false;
    }

    /**
     * Kill a process by its name
     * 
     * @param string $processName Process name to kill
     * @return bool Returns true if process was killed
     */
    public static function killProcess(string $processName): bool
    {
        exec("pkill -f " . escapeshellarg($processName), $output, $returnCode);
        return $returnCode === 0;
    }
}
