<?php

namespace NsUtil\Helpers;


/**
 * Class Prompt
 * 
 * This class provides a method to handle user prompts and compare their input to an expected value.
 */
class Prompt
{
    /**
     * Handle the user prompt and compare the input to the expected value.
     *
     * @param string $prompt The message to display to the user.
     * @param string $assertValue The value to compare the user's input against.
     * @return bool Returns true if the user's input matches the expected value, false otherwise.
     */
    public static function handle(string $prompt, string $assertValue): bool
    {
        echo $prompt . " ($assertValue)";
        $handle = fopen("php://stdin", "r");
        $response = strtolower(trim(fgets($handle)));
        fclose($handle);
        return Utils::compareString($response, $assertValue);
    }
}
