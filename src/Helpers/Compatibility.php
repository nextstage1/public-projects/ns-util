<?php

namespace NsUtil\Helpers;

use NsUtil\Helpers\DirectoryManipulation;

/**
 * Checks PHP code compatibility with the current PHP version (Lint)
 */
class Compatibility
{
    /**
     * Runs the compatibility check on the specified directory.
     *
     * @param string $dir The directory to check.
     */
    public static function run(string $dir): void
    {
        echo "### Check Lint on PHP Version: " . PHP_VERSION . PHP_EOL;
        self::check($dir);
        echo "\nFinished! (Only errors are displayed) \n";
    }

    /**
     * Recursively checks PHP files in the specified directory for syntax errors.
     *
     * @param string $dir The directory to check.
     * @return bool True if the check completes.
     */
    private static function check(string $dir): bool
    {
        if (!is_dir($dir)) {
            throw new \Exception("Directory does not exist: $dir");
        }

        $files = DirectoryManipulation::openDir($dir);
        foreach ($files as $file) {
            $filePath = $dir . DIRECTORY_SEPARATOR . $file;
            if (is_dir($filePath)) {
                self::check($filePath);
                continue;
            } else {
                if (stripos($file, '.php') !== false) {
                    $ret = shell_exec('php -l ' . escapeshellarg($filePath));
                    if (strpos($ret, 'No syntax errors detected') === false) {
                        echo $ret;
                    }
                }
            }
        }
        return true;
    }
}
