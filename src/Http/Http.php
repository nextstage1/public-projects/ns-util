<?php

namespace NsUtil\Http;

use NsUtil\Helpers\File;

class Http
{
    /**
     * Makes an HTTP request to a specified URL.
     *
     * @param string $url The URL to which the request will be made.
     * @param array $params Request parameters (optional, default: []).
     * @param string $method HTTP method (GET, POST, PUT, DELETE, etc.) (optional, default: 'GET').
     * @param array $header HTTP headers (optional, default: ['Content-Type: application/json']).
     * @param bool $ssl Verify SSL (optional, default: true).
     * @param int $timeout Request timeout in seconds (optional, default: 30).
     * @return Response An object containing the request response, including:
     *                  - body: Response content.
     *                  - error_code: cURL error code.
     *                  - error: cURL error message.
     *                  - http_code: HTTP status code.
     *                  - headers: Response headers.
     *                  - url: Effective request URL.
     */
    public static function call(
        string $url,
        array $params = [],
        string $method = 'GET',
        array $header = ['Content-Type: application/json'],
        bool $ssl = true,
        int $timeout = 30
    ): Response {
        // Remove excess cookie
        $cookiefile = File::getTmpDir() . DIRECTORY_SEPARATOR . 'NsHttpCookie' . md5((string) date('Ymd')) . '.txt';

        $options = [
            CURLOPT_URL => trim($url),
            CURLOPT_CUSTOMREQUEST => strtoupper($method), // Set the HTTP method (GET, POST, PUT, DELETE, etc.)
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0',
            CURLOPT_COOKIEFILE => $cookiefile,
            CURLOPT_COOKIEJAR => $cookiefile,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_AUTOREFERER => true,
            CURLOPT_CONNECTTIMEOUT => $timeout,
            CURLOPT_TIMEOUT => $timeout,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => $ssl,
            // CURLOPT_SSL_VERIFYSTATUS => $ssl, // OSCP desabilitei por que causa muitos erros
            CURLOPT_HEADER => true,
            CURLOPT_VERBOSE => false,
            CURLOPT_HTTPHEADER => $header,
        ];

        if ($ssl) {
            $options[CURLOPT_CAINFO] = __DIR__ . '/resources/cacert.pem';
        }

        // Configure request data according to the HTTP method
        switch (strtoupper($method)) {
            case 'POST':
                $options[CURLOPT_POST] = true;
                $options[CURLOPT_POSTFIELDS] = self::buildRequestBody($params, $header);
                break;
            case 'PUT':
            case 'DELETE':
                $options[CURLOPT_CUSTOMREQUEST] = strtoupper($method);
                $options[CURLOPT_POSTFIELDS] = self::buildRequestBody($params, $header);
                break;
            case 'GET':
                $options[CURLOPT_HTTPGET] = true;
                if (count($params) > 0) {
                    $options[CURLOPT_URL] .= '?' . http_build_query($params, '', '&', PHP_QUERY_RFC3986);
                }
                break;
            default:
                // Additional methods can be added here if needed
                break;
        }

        // Initialize cURL
        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $output = (string) curl_exec($ch);

        // Capture request information
        $urlInfo = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($output, $header_size);

        // Get response headers
        $headers = [];
        $responseHeaders = substr($output, 0, $header_size);
        foreach (explode("\n", trim($responseHeaders)) as $line) {
            $parts = explode(':', $line, 2);
            if (count($parts) === 2) {
                $headers[trim($parts[0])] = trim($parts[1]);
            }
        }

        $ret = new Response(
            $body,
            curl_errno($ch),
            curl_error($ch) ?: curl_errno($ch),
            curl_getinfo($ch, CURLINFO_HTTP_CODE),
            $headers,
            $urlInfo
        );

        // Close cURL session
        curl_close($ch);

        return $ret;
    }

    /**
     * Builds the request body based on the parameters and headers.
     *
     * @param array $params Request parameters.
     * @param array $header HTTP headers.
     * @return string|array The request body in JSON format or query string.
     */
    private static function buildRequestBody(array $params, array $header)
    {
        // Function to check if the header indicates JSON
        $isJsonRequest = array_filter(
            $header,
            fn($headerLine) => preg_match('/\bcontent-type\s*:\s*application\/json\b/i', $headerLine)
        );

        // check if the header indicates multipart/form-data
        $isMultipartFormDataRequest = array_filter(
            $header,
            fn($headerLine) => preg_match('/\bcontent-type\s*:\s*multipart\/form-data\b/i', $headerLine)
        );

        // Check if the function found any header matching the pattern
        if (!empty($isJsonRequest)) {
            return json_encode($params);
        } else if (!empty($isMultipartFormDataRequest)) {
            return $params;
        } else {
            return http_build_query($params, '', '&', PHP_QUERY_RFC3986);
        }
    }

    /**
     * Get the contents of a file with optional SSL verification.
     *
     * @param string $url
     * @param bool $ssl
     * @param int $timeout
     * @return string
     */
    public static function getUrlContents(string $url, bool $ssl = false, int $timeout = 30): string
    {
        $config = [
            'http' => [
                'timeout' => $timeout
            ],
            'ssl' => [
                'verify_peer' => $ssl,
                'verify_peer_name' => $ssl
            ]
        ];
        $context = stream_context_create($config);
        return file_get_contents($url, false, $context);
    }
}
