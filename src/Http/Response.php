<?php

namespace NsUtil\Http;

use NsUtil\Helpers\JsonParser;


class Response
{
    public string $body;
    public int $error_code;
    public string $error;
    public int $http_code;
    public array $headers;
    public string $url;

    public function __construct(
        string $body,
        int $error_code,
        string $error,
        int $http_code,
        array $headers,
        string $url
    ) {
        $this->body = $body;
        $this->error_code = $error_code;
        $this->error = $error;
        $this->http_code = $http_code;
        $this->headers = $headers;
        $this->url = $url;
    }

    /**
     * Get the body of the response.
     *
     * @param string $format The format in which to return the body ('string', 'object', 'array').
     * @return mixed The body of the response in the specified format.
     */
    public function getBody(string $format = 'string')
    {
        switch ($format) {
            case 'object':
                return $this->getBodyAsObject();
            case 'array':
                return $this->getBodyAsArray();
            case 'string':
            default:
                return $this->body;
        }
    }

    public function getBodyAsObject(): object
    {
        return JsonParser::handle($this->body) ?? new \stdClass();
    }

    public function getBodyAsArray(): array
    {
        return JsonParser::handle($this->body, true) ?? [];
    }

    public function getErrorCode(): int
    {
        return $this->error_code;
    }

    public function getError(): string
    {
        return $this->error;
    }

    public function getHttpCode(): int
    {
        return $this->http_code;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function toArray(): array
    {
        return [
            'body' => $this->body,
            'error_code' => $this->error_code,
            'error' => $this->error,
            'http_code' => $this->http_code,
            'headers' => $this->headers,
            'url' => $this->url
        ];
    }
}
