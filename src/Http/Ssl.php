<?php

namespace NsUtil\Http;

use DateTime;
use Exception;
use NsUtil\Helpers\Utils;

class Ssl
{
    /**
     * Gets the expiration date of the certificate attached to the URL or null if it does not exist
     *
     * @param string $url
     * @return ?int
     */
    public static function getEndDate(string $url): ?int
    {
        if (Utils::getSO() !== 'linux') {
            throw new Exception('Only Linux System');
        }
        // sanitize
        $url = str_replace(['https://', 'http://', ':443', '/'], '', $url);
        // sh
        $sh = "echo | openssl s_client -connect $url:443 -servername $url 2>/dev/null | openssl x509 -noout -enddate | awk -F= '{print $2}'";
        $return = null;

        $date = shell_exec($sh);
        if (strlen($date) > 0) {
            $date = new DateTime($date);
            $return = $date->getTimestamp() ?? null;
        }

        return $return;
    }
}
