<?php

namespace NsUtil\DTOs;

use NsUtil\Interfaces\DTOInterface;

/**
 * Abstract class representing a Data Transfer Object (DTO).
 */
abstract class AbstractDTO implements DTOInterface
{
    /**
     * The data storage array for the DTO.
     *
     * @var array
     */
    protected array $data = [];

    /**
     * Returns the DTO data as an associative array.
     *
     * @return array The data of the DTO.
     */
    public function toArray(): array
    {
        return $this->data;
    }
}
