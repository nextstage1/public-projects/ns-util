<?php

namespace NsUtil;

use InvalidArgumentException;
use RuntimeException;

/*
 * This file is part of the Text_Template package.
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * A simple template engine.
 *
 * @since Class available since Release 1.0.0
 */
class Template
{

    /**
     * @var string
     */
    protected $template = '';

    /**
     * @var string
     */
    protected $openDelimiter = '{';

    /**
     * @var string
     */
    protected $closeDelimiter = '}';

    /**
     * @var array
     */
    protected $values = array();

    /**
     * Constructor.
     *
     * @param  string                   $file
     * @throws InvalidArgumentException
     */
    public function __construct($template_file_or_html, $values = [], $openDelimiter = '{', $closeDelimiter = '}')
    {
        switch (true) {
            case strlen($template_file_or_html) === 0:
                $this->template = 'TEMPLATE IS NOT DEFINED';
                break;
            case strlen($template_file_or_html) > 1000:
                $this->template = $template_file_or_html;
                break;
            case file_exists($template_file_or_html):
                $this->setFile($template_file_or_html);
                break;
            default:
                $this->template = $template_file_or_html;
                break;
        }

        $this->openDelimiter = $openDelimiter;
        $this->closeDelimiter = $closeDelimiter;
        $this->setVar($values);
    }

    public function setTemplate($text)
    {
        $this->template = $text;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Sets the template file.
     *
     * @param  string                   $file
     * @throws InvalidArgumentException
     */
    public function setFile($file)
    {
        $distFile = $file;
        if (file_exists($file)) {
            $this->template = file_get_contents($file);
        } else if (file_exists($distFile)) {
            $this->template = file_get_contents($distFile);
        } else {
            throw new InvalidArgumentException(
                'Template file could not be loaded.'
            );
        }
    }

    /**
     * Sets one or more template variables.
     *
     * @param array $values
     * @param bool  $merge
     */
    public function setVar(array $values, $merge = TRUE)
    {
        if (!$merge || empty($this->values)) {
            $this->values = $values;
        } else {
            $this->values = array_merge($this->values, $values);
        }
    }

    /**
     * Renders the template and returns the result.
     *
     * @return string
     */
    public function render()
    {
        $keys = [];
        $values = [];
        foreach ($this->values as $key => $value) {
            if (is_array($value)) {
                continue;
            }
            $keys[] = $this->openDelimiter . $key . $this->closeDelimiter;
            $values[] = $value;
        }
        $this->template = \str_replace($keys, $values, $this->template);
        return $this->template;
    }

    /**
     * Renders the template and writes the result to a file.
     *
     * @param string $target
     * @param boolean $overwrite
     * @return void
     */
    public function renderTo(string $target, bool $overwrite = true)
    {
        Helper::saveFile($target, false, $this->render(), $overwrite ? 'SOBREPOR' : "w+");
    }
}
