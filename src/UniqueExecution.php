<?php

namespace NsUtil;

use Exception;
use NsUtil\Connection\SQLite;

class UniqueExecution
{

    private $con, $ref;
    private string $tablename;

    public function __construct(string $dbName = 'defaultApplication', string $pathToDB = '/tmp')
    {
        $defaultConnection = env('UNIQUE_EXECUTION_DRIVER', 'sqlite');

        switch ($defaultConnection) {
            case 'psql':
            case 'postgresql':
                $this->con = ConnectionPostgreSQL::getConnectionByEnv();
                $this->con->executeQuery("CREATE SCHEMA IF NOT EXISTS _nsutil");
                $this->con->executeQuery("DROP TABLE IF EXISTS public._execution_lock");
                $this->tablename = '_nsutil.unique_execution_lock';
                break;
                //sqlite
            default:
                $pathToDB = $pathToDB === '/tmp' ? Helper::getTmpDir() : $pathToDB;
                $db = "{$pathToDB}/NSUniqueExecution";
                $exists = file_exists($db);
                $this->con = new SQLite($db);
                $this->tablename = '"_unique_execution_lock"';

                // Se foi criado por mim, dar permissão de escrita a qualquer usuario 
                if (!$exists) {
                    chmod($db, 0777);
                }

                break;
        }

        $this->ref = $dbName;

        $this->createDB();
    }

    public static function create(string $dbName = 'defaultApplication', int $secondsLimit = 3600): UniqueExecution
    {
        $ret = new UniqueExecution($dbName);
        $ret->start($secondsLimit, true);
        return $ret;
    }

    // Cria a tabela necessária para execução
    private function createDB(): void
    {
        // em src/Databases/migrations/20241207_120700_initial-nsutil.sql existe a migration base que ja cria essa tabela
        // Util em casos de uso com postgresql
        $query = "CREATE TABLE IF NOT EXISTS {$this->tablename} (
            \"ref\" TEXT PRIMARY KEY,
            \"inited_at\" INTEGER
        )";
        try {
            $this->con->executeQuery($query);
        } catch (Exception $exc) {
            throw new Exception('UNIQUE EXECUTION ERROR - CONNECT DB: ' . $exc->getMessage());
        }
    }

    /**
     * Verifica se uma execução esta em processamento
     *
     * @param integer $timeLimit
     * @param boolean $throwException
     * @return boolean
     */
    public function isRunning(int $timeLimit = 3600, bool $throwException = false): bool
    {
        $query = "SELECT COUNT(*) AS counter FROM {$this->tablename} WHERE ref = :ref AND inited_at > :timeLimit";
        $counter = $this->con->execQueryAndReturnPrepared($query, [
            'ref' => $this->ref,
            'timeLimit' => time() - $timeLimit
        ])[0]['counter'];

        $isRunning = $counter > 0;
        if ($isRunning && $throwException) {
            throw new Exception(
                $this->getDefaultMessageIsRunning($timeLimit)
            );
        }
        return $isRunning;
    }

    /**
     * Registra o inicio do processo unico a ser controlado
     *
     * @param integer $timeTocheckAnotherExecution
     * @param boolean $throwExceptionIfIsRunning
     * @return self
     */
    public function start(int $timeTocheckAnotherExecution = 3600, bool $throwExceptionIfIsRunning = false)
    {
        $this->isRunning($timeTocheckAnotherExecution, $throwExceptionIfIsRunning);

        $query = "INSERT INTO {$this->tablename} (ref, inited_at) VALUES(?, ?) ON CONFLICT(ref) DO UPDATE SET inited_at = ?";

        $initedAt = time();

        $this->con->executeQuery($query, [$this->ref, $initedAt, $initedAt]);

        return $this;
    }

    /**
     * Registra o encerramento do processo unico
     * @return void
     */
    public function end(): void
    {
        $query = "DELETE FROM {$this->tablename} WHERE ref=?";
        $this->con->executeQuery($query, [$this->ref]);
    }

    public static function forcedEnd(string $ref): void
    {
        $me = new self();
        $me->ref = $ref;
        $me->end();
        unset($me);
    }

    /**
     * Retorna o timestamp do inicio do processo em execução, ou 0 caso nenhum esteja em processamento
     * @return int
     */
    public function getStartedAt(): int
    {
        $out = 0;
        $query = "SELECT * FROM {$this->tablename} WHERE ref=?";
        $list = $this->con->execQueryAndReturnPrepared($query, [$this->ref]);
        if (count($list) > 0) {
            $out = (int) $list[0]['initedAt'];
        }
        return $out;
    }

    /**
     * Retorna mensagem padrão contendo a data de inicio do processo atual
     * @return string
     */
    public function getDefaultMessageIsRunning(?int $timeLimit = null): string
    {
        $init = $this->getStartedAt();
        $message = "There is another proccess running.";
        $message .= ' Started at ' . (new Date(date('c', $init)))->format('american', true) . '.';
        if ($timeLimit) {
            $message .= ' Valid until ' . (new Date(date('c', $init)))->add("$timeLimit seconds")->format('american', true);
        }

        return $message;
    }

    public function getRef(): string
    {
        return $this->ref;
    }
}
