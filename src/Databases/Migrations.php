<?php

/**
 * A execução irá ler um arquivo SQL e ira rodar e executar cada instrução se não existir na tabela de controle das migrations
 */

namespace NsUtil\Databases;

use Exception;
use NsUtil\Config;
use NsUtil\ConnectionPostgreSQL;
use NsUtil\ConsoleTable;
use NsUtil\DirectoryManipulation;
use NsUtil\Helper;

class Migrations
{

    private $con, $sqlFilePath;

    public function __construct(string $path, ConnectionPostgreSQL $con)
    {
        $this->sqlFilePath = $path;
        $this->con = $con;
        $this->con instanceof ConnectionPostgreSQL;
        Helper::directorySeparator($this->sqlFilePath);

        // Criar diretorio se não existir
        if (!is_dir($this->sqlFilePath)) {
            Helper::mkdir($path);
        }

        // HTAccess de proteção do apache
        Helper::saveFile($this->sqlFilePath . '/.htaccess', false, "Require all denied", 'SOBREPOR');

        // execucao unica por ambiente
        $file = self::getFilenameToControlMigrations(__DIR__ . '/migrations', 'migrations', $this->con);
        if (!file_exists($file)) {
            // Flag execucao unica por ambiente
            Helper::saveFile($file, '', time(), 'SOBREPOR');

            // Criar estrutura basica. Precisam estar aqui pois serao utilizadas ao iniciar as migracoes.
            $querys = [
                "CREATE SCHEMA IF NOT EXISTS _dbupdater",
                "CREATE SCHEMA IF NOT EXISTS _nsutil",
                "CREATE TABLE IF NOT EXISTS _dbupdater._migrations (name text, hash text NULL,created_at timestamp NULL DEFAULT now())",
                "CREATE OR REPLACE PROCEDURE public.ns_ddl_execute(query text) LANGUAGE plpgsql AS \$procedure\$ BEGIN execute query; END; \$procedure\$;"
            ];
            array_map(fn($query) => $this->con->executeQuery($query), $querys);

            // Criar funcoes basicas
            ob_start();
            $tmpMigrateQueue = Helper::getTmpDir() . '/' . Helper::generateUuidV4();
            $this
                ->createByArray(self::loadFromPath(__DIR__ . '/migrations'))
                ->update();
            Helper::deleteDir($tmpMigrateQueue);
            ob_end_clean();
        }
    }

    private function getHash(string $string): string
    {
        return \hash('sha256', $string);
    }

    public function create(string $name, string $sql, bool $includeDate = true): Migrations
    {
        // Verificar se já existe
        $files = DirectoryManipulation::openDir($this->sqlFilePath);
        asort($files);
        $exists = false;
        $name = str_replace(' ', '-', Helper::sanitize($name));
        foreach ($files as $file) {
            if (stripos($file, $name) !== false) {
                $exists = true;
            }
        }
        if (!$exists) {
            $filename = $this->sqlFilePath . DIRECTORY_SEPARATOR . (($includeDate) ? date('ymdHi') . '_' : '') . $name . '.nsUtilDB';
            file_put_contents($filename, $sql);
            chmod($filename, 0777);
            // echo "\n$name was successfully created!";
        }
        return $this;
    }

    public function createByArray(array $list): Migrations
    {
        $counter = 1;
        foreach ($list as $title => $sql) {
            $prefix = str_pad((string) $counter, 4, '0', STR_PAD_LEFT);
            if (is_file($sql)) {
                $sql = $this->getContentBySQLFileToNsDDLExecute($sql);
            }
            $this->create($prefix . '-' . $title, $sql);
            $counter++;
        }
        return $this;
    }

    public function getContentBySQLFileToNsDDLExecute($filepath): string
    {
        if (!file_exists($filepath)) {
            die("File not exists: $filepath");
        }
        $content = file_get_contents($filepath);
        return "call ns_ddl_execute('" . str_replace("'", "''", $content) . "')";
    }

    private function getHashCommand(string $file): string
    {
        // Verificar se a instrução já foi executada
        $parts = explode('_', $file);
        array_shift($parts);
        return $this->getHash(implode('_', $parts));
    }

    public function update(): array
    {
        $files = array_values(
            array_filter(
                DirectoryManipulation::openDir($this->sqlFilePath),
                function ($item) {
                    if (stripos($item, '.nsUtilDB') !== false) {
                        $hash = $this->getHashCommand($item);
                        $counter = (int) $this->con->execQueryAndReturn("select count(*) as qtde from _dbupdater._migrations where hash= '$hash'")[0]['qtde'];
                        return $counter === 0;
                    }
                    return false;
                }
            )
        );

        if (count($files) === 0) {
            return ['info' => 'Nothing to migrate', 'error' => false];
        }

        asort($files);
        $this->con->begin_transaction();

        echo "\n\nMigrations: Operations to execute: " . count($files) . PHP_EOL;
        foreach ($files as $file) {
            $hash = $this->getHashCommand($file);

            // Executar instrução e registrar na tabela
            $parts = explode('_', $file);
            $fileToPrint = str_replace(
                '_sql.nsUtilDB',
                '',
                implode('_', array_slice($parts, 4))
            );

            $sql = file_get_contents($this->sqlFilePath . DIRECTORY_SEPARATOR . $file);
            try {

                ConsoleTable::printTabular($fileToPrint, "", fn() => $this->con->executeQuery($sql));

                $this->con->executeQuery("INSERT INTO _dbupdater._migrations (name,hash) VALUES ('$file', '$hash')");
            } catch (Exception $exc) {
                $this->con->rollback();
                echo "\n" . ConsoleTable::setColor('ERROR!', 'red') . " $fileToPrint " . "\n";
                return ['error' => $exc->getMessage(), 'details' => $exc->getTraceAsString()];
            }
        }
        $this->con->commit();
        return ['error' => false];
    }

    public static function builder(string $pathAplicacao, array $arrayMigrations, array $conPreQuerys = [])
    {
        $config = new Config(getenv());

        $env = (Helper::fileSearchRecursive('.env', $pathAplicacao));
        if ($env) {
            $config->loadEnvFile($env);
        }

        $con = new ConnectionPostgreSQL($config->get('DBHOST'), $config->get('DBUSER'), $config->get('DBPASS'), $config->get('DBPORT'), $config->get('DBNAME'));
        foreach ($conPreQuerys as $q) {
            $con->executeQuery($q);
        }

        $migrations = (new Migrations($pathAplicacao . '/_migrations', $con))
            ->createByArray($arrayMigrations)
            ->update();

        return $migrations;
    }

    public static function run(string $pathAplicacao, array $conPreQuerys = [], bool $removeDirAfterSuccess = false): array
    {
        try {
            $dirMigrations = \realpath($pathAplicacao) . '/_migrations';
            $migrations = ['error' => "Path is not found $dirMigrations"];
            if (is_dir($dirMigrations)) {
                ob_start();
                $config = new Config(getenv());
                $inifile = Helper::fileSearchRecursive('.env', $pathAplicacao);
                if (file_exists($inifile)) {
                    $config->loadEnvFile($inifile);
                }
                $con = new ConnectionPostgreSQL($config->get('DBHOST'), $config->get('DBUSER'), $config->get('DBPASS'), $config->get('DBPORT'), $config->get('DBNAME'));
                $migrations = (new Migrations($pathAplicacao . '/_migrations', $con))
                    ->update();
                if ($migrations['error'] === false && $removeDirAfterSuccess) {
                    Helper::deleteDir($dirMigrations);
                    if (is_dir($dirMigrations)) {
                        rename($dirMigrations, __DIR__ . '/../../_____rem');
                    }
                }
                ob_end_clean();
            }
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }

        return $migrations;
    }

    public static function loadFromPath(string $path): array
    {
        if (!is_dir($path)) {
            throw new Exception("Path '$path' is not a directory");
        }
        $files = DirectoryManipulation::openDir($path);
        asort($files);
        $migrations = [];
        foreach ($files as $file) {
            $sql = "$path/$file";
            if (pathinfo($file, PATHINFO_EXTENSION) !== 'sql') {
                continue;
            }
            Helper::directorySeparator($sql);
            $migrations[$file] = $sql;
        }
        return $migrations;
    }

    public static function getFilenameToControlMigrations(string $pathToMigrations, string $prefixFile, ?ConnectionPostgreSQL $con = null): string
    {
        $files = DirectoryManipulation::openDir($pathToMigrations);
        $lastMigration = array_pop($files);
        $con ??= ConnectionPostgreSQL::getConnectionByEnv();
        $config = $con->getConfig();
        $key = "{$config->host}-{$config->user}-{$config->port}-{$config->database}";
        $version = md5("{$lastMigration}_$key");
        Helper::mkdir('/tmp/nsutil-migrations');
        return "/tmp/nsutil-migrations/$prefixFile-{$version}.ini";
    }

    public static function migrateAndSetFlagOnFile(string $pathToMigrations, string $prefixFile, ?ConnectionPostgreSQL $con = null): void
    {
        ob_start();
        $flagFile = self::getFilenameToControlMigrations($pathToMigrations, $prefixFile, $con);
        $con ??= ConnectionPostgreSQL::getConnectionByEnv();
        if (!file_exists($flagFile)) {
            $tmpMigrateQueue = Helper::getTmpDir() . '/' . Helper::generateUuidV4();
            $self = new self($tmpMigrateQueue, $con);

            Helper::saveFile($flagFile, '', time(), 'SOBREPOR');

            $self
                ->createByArray(self::loadFromPath($pathToMigrations))
                ->update();

            Helper::deleteDir($tmpMigrateQueue);
        }
        ob_end_clean();
    }
}
