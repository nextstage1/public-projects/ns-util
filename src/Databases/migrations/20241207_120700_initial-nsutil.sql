-- Garantir que os operadores basicos existam
DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_operator WHERE oprname = '~@') THEN
        CREATE OPERATOR ~@ (LEFTARG = jsonb, RIGHTARG = text, PROCEDURE = jsonb_exists);
    END IF;

    IF NOT EXISTS (SELECT 1 FROM pg_operator WHERE oprname = '~@|') THEN
        CREATE OPERATOR ~@| (LEFTARG = jsonb, RIGHTARG = text[], PROCEDURE = jsonb_exists_any);
    END IF;

    IF NOT EXISTS (SELECT 1 FROM pg_operator WHERE oprname = '~@&') THEN
        CREATE OPERATOR ~@& (LEFTARG = jsonb, RIGHTARG = text[], PROCEDURE = jsonb_exists_all);
    END IF;
END
$$;

-- Algumas tabelas basicas para o nsutil
CREATE SCHEMA IF NOT EXISTS _nsutil;
CREATE TABLE IF NOT EXISTS _nsutil.unique_execution_lock (ref TEXT PRIMARY KEY,inited_at INTEGER);
