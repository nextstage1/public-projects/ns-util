<?php

namespace NsUtil;

/**
 * Class Prompt
 * 
 * Provides methods for handling user input from the command line.
 */
class Prompt
{
    /**
     * Prompts the user with a message and returns the input.
     * 
     * @param string $prompt The message to display to the user.
     * @return string The user's input, converted to lowercase and trimmed.
     */
    private static function promter(string $prompt): string
    {
        echo $prompt;
        $handle = fopen("php://stdin", "r");
        $response = strtolower(trim(fgets($handle)));
        fclose($handle);
        return (string) $response;
    }

    /**
     * @deprecated Este método está obsoleto e será removido em uma versão futura.
     * Use o método confirm em vez deste.
     * Handles user input and compares it to an expected value.
     * 
     * @param string $prompt The message to display to the user.
     * @param string $assertValue The value to compare the user's input against.
     * @return bool True if the input matches the expected value, false otherwise.
     */
    public static function handle(string $prompt, string $assertValue): bool
    {
        $response = self::promter("$prompt ($assertValue)");
        return Helper::compareString($response, $assertValue);
    }

    /**
     * Confirms user input by comparing it to an expected value.
     * 
     * @param string $prompt The message to display to the user.
     * @param string $assertValue The value to compare the user's input against.
     * @return bool True if the input matches the expected value, false otherwise.
     */
    public static function confirm(string $prompt, string $assertValue): bool
    {
        $response = self::promter("$prompt ($assertValue)");
        return Helper::compareString($response, $assertValue);
    }

    /**
     * Prompts the user for input and returns it.
     * 
     * @param string $prompt The message to display to the user.
     * @return string The user's input.
     */
    public static function input(string $prompt): string
    {
        return self::promter($prompt);
    }
}
