<?php

namespace NsUtil;

use Exception;
use NsUtil\Connection\SQLite;

class BuscaCep
{

    public static function get($cep)
    {
        $outInsert = [];
        $cep = Helper::parseInt($cep);
        if (strlen((string)$cep) !== 8) {
            throw new Exception('Size to CEP invalid');
        }

        $sqlite = new SQLite('/tmp/buscacep.sqlite');
        $sqlite->execQueryAndReturn('CREATE TABLE IF NOT EXISTS "cep" ("cep" TEXT PRIMARY KEY, "retorno" TEXT)');
        $data = $sqlite->execQueryAndReturn('SELECT * FROM "cep" WHERE "cep"=\'' . $cep . '\' ');
        if (count($data) > 0) {
            return json_decode($data[0]['retorno'], true);
        } else {
            $resultado = file_get_contents("https://viacep.com.br/ws/$cep/json/");
            if (!$resultado) {
                return ['error' => 'CEP not found'];
            }
            $t = \NsUtil\json_decode($resultado);
            // Criar o objeto cep com os valores do objeto $t
            $cep = [
                "cep" => $t->cep,
                "logradouro" => $t->logradouro,
                "complemento" => $t->complemento,
                "bairro" => $t->bairro,
                "localidade" => $t->localidade,
                "uf" => $t->uf,
                "ibge" => $t->ibge,
                "gia" => $t->gia,
                "ddd" => $t->ddd,
                "siafi" => $t->siafi
            ];
            $sqlite->execQueryAndReturnPrepared('INSERT INTO "cep" ("cep", "retorno") VALUES (?,?)', [
                $cep,
                json_encode($cep)
            ]);

            return $cep;
        }
    }
}
