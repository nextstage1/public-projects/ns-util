<?php

namespace NsUtil;

use Closure;
use Exception;
use NsUtil\Assync\Assync;
use NsUtil\Assync\LoadCSV\LoadCSVClosuresInterface;
use NsUtil\Assync\LoadCSV\LoadCSVRuntime;
use ReflectionFunction;
use stdClass;

class PgLoadCSV
{

    private $file, $run, $consoleTable;
    private int $parallelJobs = 1;
    private $csv = [];

    private string $serializabeConnectionFile;

    private array $runners = [];

    private int $splitLines = 50000;

    /**
     * 
     * @param \NsUtil\ConnectionPostgreSQL $con
     * @param string $schema
     * @param bool $schemaDrop
     * @param mixed $truncateTables
     */
    public function __construct(ConnectionPostgreSQL $con, $schema = 'import', bool $schemaDrop = false, $truncateTables = false)
    {
        //$this->file = realpath($file);
        $this->run = new stdClass();
        $this->run->con = $con;
        $this->run->schema = \NsUtil\Helper::sanitize($schema);
        $this->run->delimiter = "\t"; // delimiter to load
        $this->run->nullAs = ''; // null as to load
        $this->run->schemaDrop = $schemaDrop;
        $this->run->truncate = $truncateTables;

        $this->run->schema = $this->sanitizeField($schema);
        if ($this->run->schemaDrop) {
            $this->run->con->executeQuery("DROP SCHEMA IF EXISTS " . $this->run->schema . ' CASCADE');
        }
        $this->run->con->executeQuery("CREATE SCHEMA IF NOT EXISTS " . $this->run->schema);

        $this->consoleTable = new \NsUtil\ConsoleTable();
        $this->consoleTable->setHeaders(['Schema', 'Tabela', 'Linhas', 'Resultado']);
        $this->csv[] = ['Schema', 'Tabela', 'Linhas', 'Tamanho', 'Resultado'];

        // default somente um processador.
        $this->setParallelJobs(1);

        $this->serializabeConnectionFile = tempnam(sys_get_temp_dir(), 'tmp_');
        file_put_contents($this->serializabeConnectionFile, serialize($con));
    }

    public function __destruct()
    {
        unlink($this->serializabeConnectionFile);
    }

    /**
     * Adiciona uma configuração de runner à lista de runners.
     *
     * @param string $filename O nome do arquivo CSV, que será usado para gerar a chave do runner.
     * @param LoadCSVClosuresInterface|Closure $fn A função de fechamento (closure) que será executada pelo runner.
     * @param array $extrasFields Campos extras que serão adicionados ao runner (opcional).
     *
     * @return self Retorna a instância atual para permitir encadeamento de métodos (method chaining).
     */
    public function addRunners(string $filename,  $fn, array $extrasFields = []): self
    {
        $tbl = mb_strtolower(str_replace('.csv', '', $filename));
        $key = $this->sanitizeField(str_replace(['.csv', '.', '-'], ['', '_', '_'], Helper::sanitize($tbl)));

        $this->runners[$key] = [
            'closure' => is_string($fn) ? $fn : Assync::encodeTask($fn),
            'extrasFields' => $extrasFields
        ];

        return $this;
    }

    public function getConsoleTable(): ConsoleTable
    {
        return $this->consoleTable;
    }

    public function getCsv()
    {
        return $this->csv;
    }

    private function consoleTableAddLine($qtdeLinhas, $filesize, $message): void
    {
        $this->csv[$this->file] = [$this->run->schema, $this->run->table, $qtdeLinhas, $filesize, $message];
        $this->consoleTable->addRow([$this->run->schema, $this->run->table, $qtdeLinhas, $message]);
    }

    /**
     * Define o que será utilizado em nullas ao executar o insertByCopy
     * @param string $nullas
     */
    public function setNullAs($nullas = '')
    {
        $this->run->con->setNullAs($nullas);
        return $this;
    }

    private function convertXlsxToCSV(&$file): void
    {
        $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));

        if ($ext === 'xlsx') {
            $ret = shell_exec('type xlsx2csv');
            if (stripos($ret, 'not found') > -1) {
                throw new Exception("ERROR: Library xlsx2csv not found! To install: apt-get install -y xlsx2csv");
            }

            $file_name_without_ext = pathinfo($file, PATHINFO_FILENAME);
            $csv = "/tmp/{$file_name_without_ext}.csv";

            $ret = shell_exec("xlsx2csv $file $csv");
            if (!file_exists($csv)) {
                throw new Exception("An error occurred while converting XLSX file to CSV: $ret");
            }
            $file = $csv;
        }
    }

    /**
     * 
     * @param string $file_or_dir Diretorio ou CSV que deve ser ingerido
     * @param string $tablename - Caso false, será utilizado o nome do arquivo CSV sanitizado
     * @return boolean
     */
    public function run(string $file_or_dir, ?string $tablename = null)
    {
        // looping para tratmento de diretorios
        if (is_dir($file_or_dir)) {
            $types = ['csv', 'xlsx'];
            $files = DirectoryManipulation::openDir($file_or_dir);
            foreach ($files as $filename) {
                $file = $file_or_dir . DIRECTORY_SEPARATOR . $filename;
                $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                if (!in_array($ext, $types)) {
                    continue;
                }

                $this->convertXlsxToCSV($file);

                $this->run($file, $tablename);
            }
            return true;
        }

        // Tratamento de arquivos unicos
        $this->file = realpath($file_or_dir);
        if (!file_exists($this->file)) {
            throw new Exception("File '{$this->file} not found");
        }

        Helper::fileConvertToUtf8($this->file);

        $tbl = pathinfo($this->file, PATHINFO_FILENAME);
        $this->run->table = mb_strtolower(
            $this->sanitizeField(
                str_replace(['.csv', '.', '-'], ['', '_', '_'], Helper::sanitize($tablename ?? $tbl))
            )
        );
        $this->run->tableSchema = $this->run->schema . '.' . $this->run->table;

        $this->head();

        $this->execute();

        return true;
    }

    private function head()
    {
        $fh = fopen($this->file, "rb") or die('not open');
        $data = fgetcsv($fh, 5000, '\\');

        fclose($fh);
        $this->run->explode = ',';
        if (stripos($data[0], ';') > 0) {
            $this->run->explode = ';';
        }
        if (stripos($data[0], "\t") > 0) {
            $this->run->explode = "\t";
        }

        // Remover BOM
        $data[0] = str_replace("\xEF\xBB\xBF", '', $data[0]);


        $head = explode($this->run->explode, $data[0]);
        $this->run->fields = $cpos = $control = [];
        $this->run->originalFields = $head;

        // acrescentar os campos a mais definidos nos runners 
        $head = array_merge($head, $this->runners[$this->run->table]['extrasFields'] ?? []);

        foreach ($head as $key => $val) {
            $val = $this->sanitizeField($val);

            // termos exclusivos do postgres
            $termosReservados = ['references', 'if', 'else', 'case', 'desc', 'asc'];
            if (array_search($val, $termosReservados) !== false) {
                $val = '_' . $val;
            }

            if (strlen((string)$val) === 0) {
                $val = '_' . uniqid('cpo_');
            }
            $cpos[] = "$val text null";
            if (isset($this->run->fields[md5((string)$val)])) {
                $control[md5((string)$val)]++;
                $val = $val . '_' . $control[md5((string)$val)];
            } else {
                $control[md5((string)$val)] = 1;
            }
            $this->run->fields[] = $val;
        }

        //$this->run->fields = implode(',', $this->run->fields);
        $this->run->con->executeQuery("CREATE TABLE IF NOT EXISTS "
            . $this->run->tableSchema
            . " ("
            . implode(',', $cpos)
            . ")");
        if ($this->run->truncate) {
            $this->run->con->executeQuery("TRUNCATE TABLE " . $this->run->tableSchema . " CASCADE");
        }

        $this->run->linhas = \NsUtil\Helper::linhasEmArquivo($this->file);
        $this->run->filesize = filesize($this->file);
    }

    private function execute()
    {
        echo "Table: {$this->run->tableSchema}: " . number_format($this->run->linhas - 1, 0, ',', '.') . " lines\n";

        // splitter
        $headers = implode($this->run->explode, $this->run->originalFields);

        // Comando para remover o cabeçalho do arquivo original
        $tmpFileWithoutHeaders = tempnam(sys_get_temp_dir(), 'tmp_');
        shell_exec("tail -n +2 $this->file > $tmpFileWithoutHeaders");

        // partir o arquivo em varios pedacoes
        $uuid = Helper::generateUuidV4();
        $chunkPath = "/tmp/loadcsv-{$uuid}";
        Helper::deleteDir($chunkPath);
        Helper::mkdir($chunkPath, 0777);

        shell_exec("split -a 4 -l $this->splitLines --additional-suffix=.csv $tmpFileWithoutHeaders $chunkPath/chunck_");

        // remover arquivo temporario
        unlink($tmpFileWithoutHeaders);

        // Processamento assyncrono dos varios pedacos
        $pool = (new Assync())
            ->setLogfile('/tmp/pgloadcsv.log')
            ->setParallelProccess($this->parallelJobs)
            ->setShowLoader($this->run->tableSchema);

        $files = DirectoryManipulation::openDir($chunkPath);

        array_map(
            fn($file_item) => $pool->addClassRunner(md5($file_item), LoadCSVRuntime::class, 'handle', [
                'file' => "$chunkPath/$file_item",
                // 'headers' => $headers,
                'pdo' => $this->serializabeConnectionFile,
                // 'table' => $this->run->table,
                'tableSchema' => $this->run->tableSchema,
                'fields' => $this->run->fields,
                'runners' => $this->runners[$this->run->table] ?? null,
            ]),
            $files
        );

        $pool->run(
            fn($loader) => $this->consoleTableAddLine($this->run->linhas, $this->run->filesize, $loader)
        );

        Helper::deleteDir($chunkPath);



        echo "\n";
    }

    private function sanitizeField($str)
    {
        //$str = preg_replace("/[^A-Za-z0-9]/", "_", $str);

        $str = trim(str_replace('"', '', $str));
        $from = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
        $to = "aaaaeeiooouucAAAAEEIOOOUUC";
        $keys = array();
        $values = array();
        preg_match_all('/./u', $from, $keys);
        preg_match_all('/./u', $to, $values);
        $mapping = array_combine($keys[0], $values[0]);
        $str = strtr($str, $mapping);
        $str = preg_replace("/[^A-Za-z0-9]/", "_", $str);

        if (is_numeric($str[0])) {
            $str = '_' . $str;
        }
        return $str;
    }

    /**
     * Set the value of parallelJobs
     *
     * @return  self
     */
    public function setParallelJobs(?int $parallelJobs = null)
    {
        if (null === $parallelJobs) {
            $processors = 1;
            if (is_file('/proc/cpuinfo')) {
                $cpuinfo = file_get_contents('/proc/cpuinfo');
                preg_match_all('/^processor/m', $cpuinfo, $matches);
                $processors = count($matches[0]);
            }
            $parallelJobs = $processors >= 2 ? ($processors - 1) : 1;
        }

        $this->parallelJobs = $parallelJobs;

        return $this;
    }

    public function setFullParallelJobs(): self
    {
        return $this->setParallelJobs();
    }

    /**
     * Set the value of splitLines
     *
     * @return  self
     */
    public function setSplitLines(int $splitLines = 50000): self
    {
        $this->splitLines = $splitLines;

        return $this;
    }
}
