<?php

namespace NsUtil;

use stdClass;
use ZipArchive;

class Log
{

    public function __construct() {}

    public static function getDefaultPathNSUtil()
    {
        $defaultPathNSUtil = '/var/log/ns-util';
        return is_dir($defaultPathNSUtil)
            ? $defaultPathNSUtil
            : '/tmp';
    }

    /**
     * 
     * @param string $file Path absoluto do arquivo
     * @param int $maxSize Em MB, tamanho máximo do arquivo para rotacionar
     * @return string
     */
    public static function rotate(string $file, int $maxSize = 10, ?string $newPathPrefix = 'default'): void
    {
        Helper::directorySeparator($file);
        $parts = explode(DIRECTORY_SEPARATOR, $file);
        $filename = array_pop($parts);
        $path = implode(DIRECTORY_SEPARATOR, $parts);
        Helper::mkdir($path);
        $pathPrefix = $newPathPrefix === 'default' ? str_replace('.', '-', $filename . '-rotated') : $newPathPrefix;
        $filenameFull = $path . DIRECTORY_SEPARATOR . $filename;
        if (is_file($filenameFull) && filesize($filenameFull) > 1024 * 1024 * $maxSize) {
            $zipname = $filenameFull . '.' . \date('ymdHis') . '.zip';
            $zip = new ZipArchive();
            $zip->open($zipname, ZipArchive::CREATE);
            $zip->addFile($filenameFull);
            $zip->close();
            if (file_exists($zipname)) {
                unlink($filenameFull);
            }
            if (null !== $newPathPrefix && file_exists($zipname)) {
                $parts = explode(DIRECTORY_SEPARATOR, $zipname);
                $filename = array_pop($parts);
                $newPath = implode(DIRECTORY_SEPARATOR, $parts)
                    . DIRECTORY_SEPARATOR
                    . $pathPrefix;
                Helper::mkdir($newPath);
                $newFilename = $newPath
                    . DIRECTORY_SEPARATOR
                    . $filename;
                rename($zipname, $newFilename);
            }
        }
    }

    /**
     * Registra um log de texto em um arquivo determinado
     *
     * @param string $file
     * @param string|array $message
     * @param boolean $ignoreBacktrace
     * @return void
     */
    public static function logTxt(string $file, $message, bool $ignoreBacktrace = false): void
    {
        if (is_array($message) || is_object($message)) {
            $message = var_export($message, true);
        }

        if (!$ignoreBacktrace) {
            $message .= "\n\t" . implode("\n\t", self::getBacktrace());
        }

        // criação do diretorio caso não existe
        self::rotate($file);
        if ($fp = fopen($file, "a")) {
            $date_message = "[" . gmdate('c') . "] " . $message . PHP_EOL;
            fwrite($fp, $date_message);
            fclose($fp);
        } else {
            $message = "[" . gmdate('c') . "] [ERROR] Error on write log file: {$file} \n" . $message;
            file_put_contents('/tmp/php-error.log', $message, FILE_APPEND);
        }
    }


    /**
     * imprimir na tela o texto
     */
    public static function see($var, ?bool $html = null, bool $backtraceShow = true): bool
    {
        switch (true) {
            case is_callable($var):
                $out = var_export($var, true);
                break;
            case $var instanceof stdClass: // Verifica se é um objeto stdClass
                $out = json_encode(Helper::objectPHP2Array($var), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                break;
            case is_array($var):
                $out = json_encode($var, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                break;
            case is_bool($var):
                $out = $var ? 'TRUE' : 'FALSE';
                break;
            case is_string($var) && $var === '':
                $out = 'Variavel não possui nenhum valor';
                break;
            default:
                if (is_object($var) && method_exists($var, 'toArray')) {
                    $out = $var->toArray();
                    self::see($out, $html, $backtraceShow);
                    return true;
                } else {
                    $out = $var;
                }
                break;
        }

        // avalia se esta rodando em cli ou web
        $html ??= php_sapi_name() !== 'cli';

        if ($var === 'ONLY_BACKTRACE') {
            $out = '';
        } else {
            $out = $html
                ? "<hr/><h5>Log visualization:</h5><pre>$out</pre>"
                : ("\n\r## Type: " . gettype($var)
                    . "\n\r## Content\n\r" . str_replace('&#34;', '"', $out));
        }

        $backtrace = $html
            ? "<hr/><h5>Backtrace</h5><pre>" . implode("<br>", self::getBacktrace()) . "</pre><hr/>"
            : "\n\r## Backtrace\n\r" . implode("\n", self::getBacktrace()) . "\n\r";




        echo $out . ($backtraceShow ? $backtrace : '');

        return true;
    }

    /**
     * Retorna o backtrace até aqui
     *
     * @return array
     */
    public static function getBacktrace(int $maxDepth = 20)
    {
        $origem = [];
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 30);
        foreach ($backtrace as $key =>  $item) {
            $item['class'] ??= '';
            if (
                is_null($item)
                || !is_array($item)
                || strlen((string) $item['class']) === 0
            ) {
                continue;
            };

            if ($key > $maxDepth) {
                break;
            }

            $origem[] = ($item['file'] ?? 'file')
                . ':'
                . ($item['line'] ?? -1)
                . ' > '
                . ($item['class'] ?? 'class')
                . '::'
                . ($item['function'] ?? 'function')
                . '()';
        }
        return $origem;
    }

    /**
     * Registra um log de texto em um arquivo determinado
     *
     * @param string $type
     * @param string|array $message
     * @param boolean $ignoreBacktrace
     * @return void
     */
    public static function debug(string $type, $message, bool $ignoreBacktrace = false): void
    {
        $pathToDebugLog = env('DEBUGGER_PATH', '/tmp/php/logs');
        $file = "{$pathToDebugLog}/{$type}.log";
        self::logTxt($file, $message, $ignoreBacktrace);
    }

    public static function error(string $message, bool $ignoreBacktrace = false): void
    {
        $pathToDebugLog = env('DEBUGGER_PATH', '/tmp/php/logs');
        $file = "{$pathToDebugLog}/error.log";
        self::logTxt($file, $message, $ignoreBacktrace);
    }
}
