<?php

namespace NsUtil;

use Exception;
use NsUtil\Financial\Round;
use NsUtil\Financial\Rounders\ABNT_NBR_5891;
use NsUtil\Helpers\JsonParser;
use NsUtil\Storage\NsFilesystem;
use NsUtil\Storage\Storage;

function ns_nextstage(): void
{
    echo "\n### NsUtil functions is loaded! ### \n";
}

/**
 * JsonDecode, com tratamento das regras aplicadas no >php8
 *
 * @param mixed $json
 * @param boolean $assoc
 * @param integer $depth
 * @param integer $options
 * @return mixed
 */
function json_decode($json, bool $assoc = false, int $depth = 512, int $options = 0)
{
    return JsonParser::handle($json, $assoc, $depth, $options);
}

function explode($delimiter, $string)
{
    return Helper::explode((string) $delimiter, (string) $string);
}

function ns_explode($delimiter, $string)
{
    return Helper::explode((string) $delimiter, (string) $string);
}

function stripos($haystack, $needle, $offset = 0)
{
    return Helper::stripos((string) $haystack, (string) $needle, (int) $offset);
}



/**
 * Encrypt the given value.
 *
 * @param array|string $value
 * @param string $passphrase
 * @param string|null $extraKey
 * @return string
 */
function encrypt($value, string $passphrase, string $extraKey = null): string
{
    return (new Crypto($passphrase))->encrypt($value, $extraKey);
}

if (!function_exists('dd')) {

    /**
     * Display and die
     *
     * @param mixed $var
     * @param boolean|null $isHtml
     * @param boolean $showBacktrace
     * @return void
     */

    function dd(...$params): void
    {
        foreach ($params as $var) {
            echo Log::see($var, null, false);
        }
        echo Log::see('ONLY_BACKTRACE');
        die();
    }
}

/**
 * Create a new Carbon instance for the current time.
 *
 * @param  string $tz
 * @return Date
 */
function now($tz = null)
{
    return new Date('NOW', $tz);
}


if (!function_exists('__tr')) {
    /**
     * Get translate of key
     *
     * @param string $key
     * @param string $lang
     * @return string
     */
    function __tr($key, $lang = null)
    {
        $lang ??= env('APP_LANG', 'pt_BR');
        return Translate::get($key, $lang);
    }
}

if (!function_exists('nsCommand')) {

    /**
     * Execute the functions on commands
     *
     * @param string $command
     * @param string $logfile
     * @param boolean $withNohup
     * @return string
     */
    function nsCommand(
        $command,
        $logfile = null,
        $withNohup = false

    ) {
        $nsutilExecutablePath = null;
        $nsutilExecutablePaths = [
            '/var/www/html',
            Helper::getPathApp(),
            realpath(__DIR__ . '/../')
        ];
        foreach ($nsutilExecutablePaths as $path) {
            if (file_exists("{$path}/nsutil")) {
                $nsutilExecutablePath = $path;
                break;
            }
        }
        if (null === $nsutilExecutablePath) {
            throw new Exception("File 'nsutil' not found");
        }

        $logfile = $logfile === false ? null : $logfile;

        if ($withNohup && null === $logfile) {
            $logfile = '/dev/null';
        }

        if ($logfile) {
            Log::rotate($logfile);
        }

        $cmd = "cd {$nsutilExecutablePath} && ";
        $cmd .= $withNohup ? '/usr/bin/nohup ' : '';
        $cmd .= "/usr/bin/php nsutil {$command}";
        $cmd .= $logfile ? " >> $logfile 2>&1 " : '';
        $cmd .= $withNohup ? ' & ' : '';

        $response = shell_exec($cmd);
        return $response;
    }
}

// wrapper pra nsCommand 
if (!function_exists('nsUtil')) {
    /**
     * Execute the functions on commands
     *
     * @param string $command
     * @param string $logfile
     * @param boolean $withNohup
     * @return void
     */
    function nsUtil(
        $command,
        $logfile = null,
        $withNohup = false
    ): string {
        return nsCommand($command, $logfile, $withNohup);
    }
}

if (!function_exists('roundMoneyABNT5891')) {

    /**
     * Execute the functions on commands
     *
     * @param string $command
     * @param string $logfile
     * @param boolean $withNohup
     * @return float
     */
    function roundMoneyABNT5891(
        $value,
        $precision = 2
    ) {
        return Round::handle(
            new ABNT_NBR_5891($value, $precision)
        );
    }
}

if (!function_exists('utf8_encode')) {

    function utf8_encode(string $string)
    {
        mb_convert_encoding($string, 'UTF-8', 'ISO-8859-1');
    }
}

if (!function_exists('utf8_decode')) {

    function utf8_decode(string $string)
    {
        mb_convert_encoding($string, 'ISO-8859-1', 'UTF-8');
    }
}

// if (!function_exists('env')) {
function env(string $key, $default = null)
{
    if (array_key_exists($key, $_ENV) || array_key_exists($key, $_SERVER) || getenv($key) !== false) {
        $value = getenv($key);

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case false:
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }

        return $value;
    }

    return $default;
}
// }

// if (!function_exists('drive')) {
function drive(string $type = 'private'): NsFilesystem
{
    $adapter = $type === 'public' ? env('STORAGE_PUBLIC') : env('STORAGE_PRIVATE');
    $bucket = $type === 'public' ? env('BUCKET_PUBLIC') : env('BUCKET_PRIVATE');

    if (strlen((string) $adapter === 0)) {
        throw new Exception("NSUtil: Adapter $type is not defined");
    }
    if (strlen((string) $bucket === 0)) {
        throw new Exception("NSUtil: Bucket to drive $type is not defined");
    }

    return Storage::getDrive($bucket, $adapter);
}
// }
