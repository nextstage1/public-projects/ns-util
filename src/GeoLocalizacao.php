<?php

namespace NsUtil;

use Exception;

class GeoLocalizacao
{

    public static function get($ip)
    {
        $url = 'http://www.geoplugin.net/php.gp?ip=' . $ip;
        $ret = Helper::curlCall($url, [], 'GET', ['Content-Type:application/json'], false, 15);
        $geoplugin = unserialize($ret->content);
        if (!is_array($geoplugin)) {
            throw new Exception('Invalid response from ' . $url);
        }

        $out = [];
        foreach ($geoplugin as $key => $geo) {
            $key = str_replace('geoplugin_', '', $key);
            $out[$key] = $geo;
        }
        unset($out['credit']);
        return $out;
    }
}
