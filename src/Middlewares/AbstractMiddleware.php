<?php

namespace NsUtil\Middlewares;

use NsUtil\Api;
use NsUtil\Interfaces\MiddlewareInterface;


abstract class AbstractMiddleware implements MiddlewareInterface
{
    protected Api $api;

    public function handle(Api $api): bool
    {
        $this->api = $api;
        return $this->check();
    }

    abstract public function check(): bool;
}
