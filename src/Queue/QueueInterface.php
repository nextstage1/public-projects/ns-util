<?php

namespace NsUtil\Queue;

use NsUtil\Date;

// para identificar os usos: grep -r -i 'implements QueueInterface' /caminho/do/diretorio


interface QueueInterface
{
    public static function add(array $data, bool $remove, int $priority, bool $assync, ?Date $initAt): int;
    public static function handle(array $data): void;
}
