<?php

namespace NsUtil\Queue;

use NsUtil\Api;
use NsUtil\Connection\KeyValueDatabaseOnPostgresql;
use NsUtil\ConnectionPostgreSQL;
use NsUtil\Helpers\Cpu;
use NsUtil\Helpers\Filter;
use NsUtil\Template;

use function NsUtil\now;

class WebViewer
{
    public static function loadData(): array
    {
        $con = QueueHandler::getConnection();
        $out = [
            'faileds' => $con->execQueryAndReturn("SELECT * FROM _nsutil.queues_failed ORDER BY priority DESC"),
            'pending' => $con->execQueryAndReturn("SELECT handler, priority, assync, count(*) as counter FROM _nsutil.queues_pending WHERE init_after <= '" . now()->format('american') . "' GROUP BY 1,2,3 ORDER BY priority DESC"),
            'runners' => KeyValueDatabaseOnPostgresql::list('runner-on-execution'),
            'running' => $con->execQueryAndReturn("SELECT * FROM _nsutil.queues_running"),
            'statistics' => $con->execQueryAndReturn("SELECT * FROM _nsutil.queue_statistics")[0] ?? [],
            'hasNewItens' => QueueHandler::hasNewItens('sync') || QueueHandler::hasNewItens('assync')
        ];
        // Runners
        $out['runners'] = array_map(function ($runner) {
            $runner['startedAt'] = str_replace('Started at ', '', $runner['value']);
            $runner['queueType'] = str_replace('only', '', explode(':', $runner['key'])[1] ?? '');
            $runner['name'] = explode(':', $runner['key'])[0];
            return $runner;
        }, $out['runners']);

        function removeQueueHandlerFromHandlerName(array $queue)
        {
            $name = explode('QueueHandlers\\', $queue['handler']);
            $queue['handler'] = trim(str_replace('Handler', '', $name[1]));
            return $queue;
        }

        // Remove QueueHandler from Handler Name
        $out['faileds'] = array_map(fn($item) => removeQueueHandlerFromHandlerName($item), $out['faileds']);
        $out['pending'] = array_map(fn($item) => removeQueueHandlerFromHandlerName($item), $out['pending']);
        $out['running'] = array_map(fn($item) => removeQueueHandlerFromHandlerName($item), $out['running']);

        // Statistics
        $out['statistics']['pendingItems'] ??= 0;
        $out['statistics']['itemsPerSecond'] ??= -1;
        $out['statistics']['itemsPerSecond'] = $out['statistics']['itemsPerSecond'] < 0 ? 'Calculating...' : $out['statistics']['itemsPerSecond'];
        $out['statistics']['estimatedCompletionAt'] ??= null;
        $out['statistics']['updatedAt'] ??= now()->format('american');

        // dinamic fields
        $out['statistics']['runningItems'] = array_sum(array_column($out['running'], 'counter'));
        $out['statistics']['pendingItems'] = array_sum(array_column($out['pending'], 'counter'));
        if ($out['statistics']['pendingItems'] === 0) {
            $out['statistics']['estimatedCompletionAt'] = null;
            $out['statistics']['itemsPerSecond'] = '';
        }
        $out['statistics']['failedItems'] = array_sum(array_column($out['faileds'], 'counter'));
        $out['statistics']['totalItems'] = $out['statistics']['runningItems'] + $out['statistics']['pendingItems'] + $out['statistics']['failedItems'];
        $out['statistics']['now'] = now('UTC')->format('american');

        // CPU Usage
        $out['usage'] = [
            'cpu' => Cpu::usage(),
            'memory' => memory_get_peak_usage(true)
        ];

        if (count($out['runners']) === 0) {
            $out['statistics']['estimatedCompletionAt'] = null;
            $out['statistics']['itemsPerSecond'] = 'No runners';
        }

        return $out;
    }

    public static function renderHTMLPage(string $urlToLoadData, ?string $title = null): string
    {
        QueueHandler::migrate();
        $html = (new Template(__DIR__ . '/resources/queue-webpage-status.html', ['urlToLoadData' => $urlToLoadData, 'title' => $title ?? 'Application']))->render();
        return $html;
    }

    public static function resolve(string $urlToApi, ?string $title = null)
    {
        $fromAPI = Filter::integer($_GET['_ldt'] ?? 0);
        if ($fromAPI) {
            header('Content-Type:application/json');
            Api::result(200, WebViewer::loadData());
        } else {
            header('Content-Type:text/html; charset=utf-8');
            echo WebViewer::renderHTMLPage("{$urlToApi}?_ldt=1", $title);
        }
    }
}
