<?php

namespace NsUtil\Queue;

use Exception;
use NsUtil\Date;
use NsUtil\Helper;
use NsUtil\Queue\Exceptions\RetryQueueException;
use function NsUtil\env;
use function NsUtil\now;
use NsUtil\UniqueExecution;
use function NsUtil\json_decode;
use NsUtil\ConnectionPostgreSQL;
use NsUtil\Databases\Migrations;
use NsUtil\Commands\NsUtils\QueueRun;
use NsUtil\Connection\KeyValueDatabaseOnPostgresql;
use NsUtil\Log;
use NsUtil\Services\Redis;

class QueueHandler
{
    protected int $id;
    protected array $queueData;
    private static ?ConnectionPostgreSQL $con = null;

    private static $migrated = null;

    private static $queueNewItensString = '/tmp/queueNewItens_%s';

    private function __construct(int $id)
    {
        self::init();
        $this->id = $id;
        $this->queueData = self::$con->execQueryAndReturn("
            SELECT * FROM _nsutil.queue 
            WHERE 
                id_queue= $id 
                AND started_at_queue is null
        ")[0] ?? ['idQueue' => null];
        if (null === $this->queueData['idQueue']) {
            throw new Exception("Queue '$id' not found or not enabled to execute", 404);
        }


        $runnerName = QueueRun::getRunnerName();
        self::$con->executeQuery("UPDATE _nsutil.queue SET 
            started_at_queue= '" . now()->format('american') . "' 
            " . (strlen((string)$this->queueData['runningOnQueue']) === 0 ? ", running_on_queue= '{$runnerName}:full'" : '')
            . " WHERE id_queue= $id");
    }

    private static function init(): void
    {
        if (null === self::$con) {
            self::$con ??= ConnectionPostgreSQL::getConnectionByEnv();
            date_default_timezone_set('UTC');
        }
    }

    public static function getConnection(): ConnectionPostgreSQL
    {
        self::init();
        return self::$con;
    }

    public function handle(): void
    {
        throw new Exception('Handle not implemented');
    }

    public static function migrate(): void
    {
        self::init();
        Migrations::migrateAndSetFlagOnFile(__DIR__ . '/migrations', 'queue-handler', self::$con);
    }

    public static function resolve(int $id): void
    {
        self::init();
        self::migrate();

        if (env('__NSUTIL_QUEUE_TESTS', false)) {
            trigger_error("Queue Handler Local Tests", E_USER_ERROR);
        }

        try {
            $me = new self($id);
            $handler = $me->queueData['handlerQueue'];

            $handler::handle(json_decode($me->queueData['dataQueue'], true));

            if ($me->queueData['removeQueue'] === true) {
                self::$con->executeQuery("DELETE FROM _nsutil.queue WHERE id_queue= $id");
            }
        } catch (RetryQueueException $exc) {
            $me = self::handleRetryQueueException($id, $exc)
                ? null
                : $me;
        } catch (Exception $exc) {
            self::handleException($id, $exc);
        } finally {
            if (($me ?? null) instanceof self &&  $me->queueData['removeQueue'] === false) {
                self::finalizeQueue($id);
            }
            self::$con->close();
        }
    }

    public static function setQueueToRetrie(int $idQueue, string $errorMessage, ?Date $initAfterQueue = null): bool
    {
        self::init();

        // check max retries
        $queue = self::$con->execQueryAndReturn("SELECT * FROM _nsutil.queue WHERE id_queue= $idQueue")[0] ?? [];
        if (!isset($queue['dataQueue'])) {
            // queue not found
            return false;
        }
        $data = json_decode($queue['dataQueue'], true);
        if ((int)($data['__queueExecution']['retries'] ?? 0) >= (int) env('QUEUE_MAX_RETRIES', 5)) {
            QueueHandler::handleException($idQueue, new Exception('Queue retry limit exceeded (' . ($data['__queueExecution']['retries'] ?? 0) . ' retries)'));
            return false;
        }

        // Tratamento dos dados fora, pois na query gerou muitos erros
        unset($data['__locked_at__']);
        $data['__queueExecution'] ??= [];
        $data['__queueExecution']['errors'] ??= [];
        $data['__queueExecution']['errors'][] = [
            'message' => $errorMessage,
            'timestamp' => now()->format('american'),
        ];
        $data['__queueExecution']['retries'] = ($data['__queueExecution']['retries'] ?? 0) + 1;
        $data = json_encode($data);

        // set to retry
        $now = now()->format('american');
        self::$con->execQueryAndReturnPrepared("UPDATE _nsutil.queue SET 
            init_after_queue = ?::timestamp, 
            running_on_queue = null, 
            started_at_queue = null, 
            finished_at_queue = null,
            data_queue = ?::jsonb
        WHERE id_queue= ?", [
            $initAfterQueue ? $initAfterQueue->format('american') : now()->sub('5 seconds')->format('american'),
            $data,
            $idQueue
        ]);

        self::setNewItens(Helper::compareString($queue['isAssyncQueue'], 'true') ? 'assync' : 'sync');

        return true;
    }

    private static function handleRetryQueueException(int $id, RetryQueueException $exc): bool
    {
        self::init();
        $data = json_decode($exc->getMessage(), true);
        return self::setQueueToRetrie($id, $data['errorMessage'], new Date($data['initAfterQueue']));
    }

    public static function handleException(int $id, Exception $exc): void
    {
        self::init();
        $message = str_replace(["'", "\n", "\r", '"'], '', $exc->getMessage());
        self::$con->execQueryAndReturn("UPDATE _nsutil.queue SET finished_at_queue= '" . now()->format('american') . "' WHERE id_queue= $id");
        self::$con->executeQuery("INSERT INTO _nsutil.queue_fails SELECT *, '$message' as error_message FROM _nsutil.queue WHERE id_queue= $id");
        self::$con->executeQuery("DELETE FROM _nsutil.queue WHERE id_queue= $id");
    }

    private static function finalizeQueue(int $id): void
    {
        // Remove the __locked_at__ key from the queue_run in PostgreSQL
        self::$con->execQueryAndReturn("
            UPDATE _nsutil.queue 
            SET 
                data_queue = data_queue - '__locked_at__'::text,
                finished_at_queue= '" . now()->format('american') . "' 
            WHERE id_queue= $id
        ");
        self::$con->executeQuery("INSERT INTO _nsutil.queue_success SELECT * FROM _nsutil.queue WHERE id_queue= $id");
        self::$con->executeQuery("DELETE FROM _nsutil.queue WHERE id_queue= $id");
    }

    public static function add(string $handler, array $data, bool $remove, int $priority, bool $assync, ?Date $initAt = null): int
    {
        self::init();
        self::migrate();

        $data = json_encode($data);
        $initAt = $initAt
            ? $initAt->setTimezone('UTC')
            : now()->sub('5 seconds');

        $query = "INSERT INTO _nsutil.queue (handler_queue, remove_queue, priority_queue, is_assync_queue, data_queue, init_after_queue, created_at_queue) 
            VALUES (?,?,?,?,?,?,?) returning id_queue as id";

        $out = self::$con->execQueryAndReturnPrepared($query, [
            $handler,
            $remove ? 'true' : 'false',
            $priority,
            ($assync ? 'true' : 'false'),
            $data,
            $initAt->format('american'),
            now()->format('american'),
        ])[0]['id'];

        // Avisar via flag
        self::setNewItens($assync ? 'assync' : 'sync');

        // if (env('QUEUE_AUTORUN_ON_ADD', false)) {
        //     try {
        //         UniqueExecution::create('queue-handler:add-call-runners', 10);
        //         shell_exec('cd ' . Helper::getPathApp() . ' && /usr/bin/nohup sh -c "sleep 3 && php nsutil queue:run -1 fromAddhandlerAdd"> /dev/null 2>&1 &');
        //     } catch (Exception $exc) {
        //     }
        // }

        return $out;
    }

    public static function setNewItens(string $queueType = 'assync'): void
    {
        KeyValueDatabaseOnPostgresql::set('queue-new-itens', $queueType, 1);
    }

    public static function removeNewItens(string $queueType = 'assync'): void
    {
        KeyValueDatabaseOnPostgresql::delete('queue-new-itens', $queueType);
    }

    public static function hasNewItens(string $queueType = 'assync'): bool
    {
        return (bool) (KeyValueDatabaseOnPostgresql::get('queue-new-itens', $queueType) ?? false);
    }

    public static function status(int $id): string
    {
        self::init();
        $queue = self::$con->execQueryAndReturn("SELECT * FROM _nsutil.queue WHERE id_queue= $id")[0] ?? ['idQueue' => null];
        $fails = self::$con->execQueryAndReturn("SELECT * FROM _nsutil.queue_fails WHERE id_queue= $id")[0] ?? ['idQueue' => null];
        $complete = self::$con->execQueryAndReturn("SELECT * FROM _nsutil.queue_success WHERE id_queue= $id")[0] ?? ['idQueue' => null];
        $out = 'COMPLETE';
        switch (true) {
            case $complete['idQueue'] !== null:
                $out = 'COMPLETE';
                break;
            case $fails['idQueue'] !== null:
                $out = 'FAILED';
                break;
            case $queue['idQueue'] === null:
                $out = 'COMPLETE';
                break;
            case $queue['runningOnQueue'] === null && $queue['startedAtQueue'] === null:
                $out = 'PENDING';
                break;
            case null !== $queue['startedAtQueue'] && null === $queue['finishedAtQueue'] && null !== (json_decode($queue['dataQueue'], true)['_error'] ?? null):
                $out = 'ERROR';
                break;
            case $queue['runningOnQueue'] !== null && null === $queue['finishedAtQueue']:
                $out = 'RUNNING';
                break;
        }
        return $out;
    }

    public static function getError(int $id): string
    {
        self::init();
        $queue = self::$con->execQueryAndReturn("SELECT * FROM _nsutil.queue WHERE id_queue= $id")[0] ?? [];
        return $queue['dataQueue']['_error'] ?? 'Queue not found';
    }
}
