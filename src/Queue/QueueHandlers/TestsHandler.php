<?php

namespace NsUtil\Queue\QueueHandlers;

use Exception;
use NsUtil\Queue\QueueHandler;
use NsUtil\Queue\AbstractQueueHandler;
use NsUtil\Date;

class TestsHandler extends AbstractQueueHandler
{

    public static function setRemove(bool $remove = false): void
    {
        self::$remove = $remove;
    }

    public static function setPriority(int $priority = 1): void
    {
        self::$priority = $priority;
    }

    public static function setAssync(bool $assync = true): void
    {
        self::$assync = $assync;
    }

    public static function setInitAt(Date $initAt = null): void
    {
        self::$initAt = $initAt;
    }

    public static function addByDTO(
        array $data = [],
        ?bool $throwException = null,
        ?int $sleep = null,
        ?bool $return = null,
        ?bool $runtimeError = null
    ): int {

        $data['_throwException'] = $throwException;
        $data['_sleep'] = $sleep;
        $data['_return'] = $return;
        $data['_runtimeError'] = $runtimeError;

        $response = self::add($data);

        return $response;
    }

    public static function handle(array $data): void
    {
        // Método necessário para os testes. Ira resolver as filas
        if (null !== $data['_throwException']) {
            throw new Exception('Erro de teste');
        }

        if (null !== $data['_sleep']) {
            sleep($data['_sleep']);
        }

        if (null !== $data['_return']) {
            return;
        }

        // to force runtime error
        if (null !== $data['_runtimeError']) {
            trigger_error("Teste de Runtime Error", E_USER_ERROR);
        }

        return;
    }
}
