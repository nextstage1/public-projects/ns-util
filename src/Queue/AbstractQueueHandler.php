<?php

namespace NsUtil\Queue;

use Exception;
use NsUtil\Date;

abstract class AbstractQueueHandler implements QueueInterface
{
    protected static bool $remove = true;
    protected static int $priority = 1;
    protected static bool $assync = true;
    protected static ?Date $initAt = null;

    /**
     * Add a new item to the queue.
     *
     * @param array $data The data to be added to the queue.
     * @param bool $remove Whether to remove the item after processing.
     * @param int $priority The priority of the queue item.
     * @param bool $assync Whether the operation should be asynchronous.
     * @param Date|null $initAt The initial date and time for the queue item.
     * @return int The ID of the added queue item.
     */
    public static function add(array $data, ?bool $remove = null, ?int $priority = null, ?bool $assync = null, ?Date $initAt = null): int
    {
        $remove ??= self::$remove;
        $priority ??= self::$priority;
        $assync ??= self::$assync;
        $initAt ??= self::$initAt;

        return QueueHandler::add(static::class, $data, $remove, $priority, $assync, $initAt);
    }

    public static function handle(array $data): void
    {
        throw new Exception("Method " . __METHOD__ . " was not implemented", 400);
    }
}
