truncate table "_nsutil".queue_statistics;

ALTER TABLE "_nsutil".queue_statistics ALTER COLUMN items_per_second TYPE numeric(12, 2) USING items_per_second::numeric(12, 2);
