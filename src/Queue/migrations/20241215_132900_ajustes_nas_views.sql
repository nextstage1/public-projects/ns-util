DROP VIEW IF EXISTS "_nsutil".queues_status;

DROP VIEW IF EXISTS "_nsutil".queues_running;

DROP VIEW IF EXISTS "_nsutil".queues_failed;

DROP VIEW IF EXISTS "_nsutil".queues_pending;

-- status da fila
CREATE
OR REPLACE VIEW "_nsutil".queues_status AS
SELECT
    handler_queue as handler,
    priority_queue as priority,
    is_assync_queue as assync,
    count(*) as counter
FROM
    "_nsutil".queue
GROUP BY
    1,
    2,
    3;

-- runing queue
CREATE
OR REPLACE VIEW "_nsutil".queues_running AS
SELECT
    handler_queue as handler,
    priority_queue as priority,
    running_on_queue as runner,
    count(*) as counter
FROM
    "_nsutil".queue
WHERE
    running_on_queue IS NOT NULL
GROUP BY
    1,
    2,
    3
ORDER BY
    running_on_queue;

-- failed queues
CREATE
OR REPLACE VIEW "_nsutil".queues_failed AS
SELECT
    handler_queue as handler,
    priority_queue as priority,
    count(*) as counter
FROM
    "_nsutil".queue_fails
GROUP BY
    1,
    2;

-- pending queues
CREATE
OR REPLACE VIEW "_nsutil".queues_pending AS
SELECT
    handler_queue as handler,
    priority_queue as priority,
    is_assync_queue as assync,
    init_after_queue as init_after
FROM
    "_nsutil".queue
WHERE
    running_on_queue IS NULL;