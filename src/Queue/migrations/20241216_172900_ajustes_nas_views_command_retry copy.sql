DROP VIEW IF EXISTS "_nsutil".queues_failed_with_command;

-- failed queues
CREATE
OR REPLACE VIEW "_nsutil".queues_failed_with_command AS
SELECT
    handler_queue,
    error_message,
    'php nsutil queue:reprocess ' || id_queue::TEXT AS command
FROM
    "_nsutil".queue_fails;