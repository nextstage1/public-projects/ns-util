create schema if not exists _nsutil;

CREATE TABLE
    IF NOT EXISTS _nsutil.queue (
        id_queue bigserial NOT NULL,
        created_at_queue timestamp not null default now (),
        init_after_queue timestamp,
        started_at_queue timestamp,
        finished_at_queue timestamp,
        running_on_queue text,
        handler_queue text NOT NULL,
        remove_queue boolean not null default 'true',
        priority_queue smallint not null default 1,
        is_assync_queue boolean not null default 'false',
        data_queue jsonb NULL,
        CONSTRAINT queue_pk PRIMARY KEY (id_queue)
    );

CREATE TABLE
    IF NOT EXISTS _nsutil.queue_fails (
        id_queue int8 NOT NULL,
        created_at_queue timestamp not null default now (),
        init_after_queue timestamp,
        started_at_queue timestamp,
        finished_at_queue timestamp,
        running_on_queue text,
        handler_queue text NOT NULL,
        remove_queue boolean not null default 'true',
        priority_queue smallint not null default 1,
        is_assync_queue boolean not null default 'false',
        data_queue jsonb NULL,
        error_message text,
        CONSTRAINT queue_fails_pk PRIMARY KEY (id_queue)
    );

CREATE TABLE
    IF NOT EXISTS _nsutil.queue_success (
        id_queue int8 NOT NULL,
        created_at_queue timestamp not null default now (),
        init_after_queue timestamp,
        started_at_queue timestamp,
        finished_at_queue timestamp,
        running_on_queue text,
        handler_queue text NOT NULL,
        remove_queue boolean not null default 'true',
        priority_queue smallint not null default 1,
        is_assync_queue boolean not null default 'false',
        data_queue jsonb NULL,
        CONSTRAINT queue_success_pk PRIMARY KEY (id_queue)
    );