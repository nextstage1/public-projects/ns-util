create table
    _nsutil.queue_statistics (
        updated_at timestamp not null,
        pending_items bigint null,
        items_per_second integer null,
        estimated_completion_at timestamp null
    );

INSERT INTO
    "_nsutil".queue_statistics (updated_at)
VALUES
    ('2025-01-10 11:32:00');