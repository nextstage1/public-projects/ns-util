-- status da fila
CREATE
OR REPLACE VIEW "_nsutil".queues_status AS
SELECT
    handler_queue,
    priority_queue,
    is_assync_queue,
    count(*)
FROM
    "_nsutil".queue
GROUP BY
    1,
    2,
    3;

-- runing queue
CREATE
OR REPLACE VIEW "_nsutil".queues_running AS
SELECT
    handler_queue,
    priority_queue,
    running_on_queue,
    count(*)
FROM
    "_nsutil".queue
WHERE
    started_at_queue IS NOT NULL
GROUP BY
    1,
    2,
    3;

-- failed queues
CREATE
OR REPLACE VIEW "_nsutil".queues_failed AS
SELECT
    handler_queue,
    priority_queue,
    count(*)
FROM
    "_nsutil".queue_fails
GROUP BY
    1,
    2;