<?php

namespace NsUtil\Queue\Exceptions;

use Exception;
use NsUtil\Date;

use function NsUtil\now;

class RetryQueueException extends Exception
{
    public function __construct($message = null, $code = 0, $previous = null)
    {
        $data = json_decode($message, true);

        if (!isset($data['initAfterQueue']) || !isset($data['errorMessage'])) {
            die('Invalid message format (RQE14)');
        }

        parent::__construct($message, $code, $previous);
    }

    public static function responseModel(Date $initAfterQueue, string $errorMessage): array
    {
        return [
            'initAfterQueue' => $initAfterQueue,
            'errorMessage' => $errorMessage
        ];
    }

    /**
     * Summary of throwExceptionToRetryWithInitAfterQueueCalculated
     * @param array $data Content of field data_queue of _nsutil.queue
     * @param string $errorMessage Error message to be displayed
     * @throws \NsUtil\Queue\Exceptions\RetryQueueException
     * @return never
     */
    public static function throwExceptionToRetryWithInitAfterQueueCalculated(array $data, string $errorMessage, ?int $seconds = null)
    {
        $retries = $data['__queueExecution']['retries'] ?? 0;
        $minuteToWait = [5, 30, 60, 120, 300, 600, 1800, 3600, 7200];
        // Anotar para retentativa, conforme limites definidos no worker (QUEUE_MAX_RETRIES' or 5)
        $waitSeconds = $seconds ?? ($minuteToWait[$retries] * 60);
        $data = [
            'initAfterQueue' => now()->add("{$waitSeconds} seconds")->format('american'),
            'errorMessage' => $errorMessage
        ];
        throw new self(json_encode($data));
    }
}
