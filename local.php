# configure a .env com o parametro LOCAL_COMPOSER listando os paths absoutos a serem atualizados localmente separados
por ";"
Ex.:
LOCAL_COMPOSER="/home/user/apps/app1;/home/user/apps/app2"

Dentro deste projeto será atualizado a pasta /vendor/nextstage-brasil/ns-util/src com a cópia do conteudo deste projeto.

Para executar:
sudo php nsutil composer:local

Atenção: se executar isso dentro de um docker não ira funcionar, pois os paths não estarão disponiveis.