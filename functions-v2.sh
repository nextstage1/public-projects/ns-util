#!/bin/bash

defineAbsolutePath(){
    local path=$1
    if [[ "$path" == ../* || "$path" == ./* ]]; then
        if [[ "$path" == ./* ]]; then 
            path="${path#./}" # Remove ./
        fi 
        path="$(pwd)/$path"
    fi
    echo $path
}


# check if docker-compose exists 
checkDockerCompose() {
    if command -v docker-compose >/dev/null 2>&1; then
        DOCKERCOMPOSE_COMMAND=docker-compose
    elif docker compose version >/dev/null 2>&1; then
        DOCKERCOMPOSE_COMMAND="docker compose"
    else
        echo "Nenhum comando docker-compose ou docker compose foi encontrado. Por favor, instale o Docker Compose." >&2
        exit 1
    fi
}

dd() {
    echo ""
    echo "## ERROR ## "
    echo -e $1
    echo ""
    exit 2
}

printHeader() {
    local header="$1"
    local width=60
    local padding=$((($width - ${#header}) / 2))

    printf '%*s\n' "${COLUMNS:-60}" "" | tr " " "-" | cut -c 1-"${width}"
    printf "|%*s%s%*s|\n" $padding "" "$header" $padding ""
    printf '%*s\n' "${COLUMNS:-60}" "" | tr " " "-" | cut -c 1-"${width}"
}

printTabular() {
    local message="$1"
    local cmdBuild="$2"
    local cmdPush="$3"
    echo -ne "\r\033[K"
    printf "%-40s%-20s" "$message" '🏃‍♂️ Running ...'
    eval "$cmdBuild"
    if [ "$cmdPush" != "" ]; then
        echo -ne "\r\033[K"
        printf "%-40s%-20s" "$message" '🚀 Push to docker...'
        eval "$cmdPush"
    fi
    echo -ne "\r\033[K"
    printf "%-40s%-20s" "$message" '✔ Done'
    echo ""
}

printContainers() {
    echo ""
    docker ps --format "table {{.Names}}\t{{.Status}}\t{{.Ports}}" | sort -k1
    echo ""
    echo ""
}

clearline() {
    echo -ne "\r\033[K"
}

waitForContainerHealth() {
    echo "Wait for containers up"
    for container_name in $1; do
        while [[ "$(docker inspect -f '{{.State.Health.Status}}' "$container_name")" == "starting" ]]; do
            state=$(docker inspect -f '{{.State.Health.Status}}' "$container_name")
            echo -ne "\r\033[K"
            printf "   - %-20s%-20s" "$container_name" '⏳ Starting ...'
            sleep 3
        done
        clearline
        health=$(docker inspect -f '{{.State.Health.Status}}' "$container_name")
        icon="✅"
        if [ "$health" != "healthy" ]; then
            icon="$health ❌"
        fi
        echo -ne "\r\033[K"
        printf "   - %-20s%-20s" "$container_name" "$icon"
        echo ""
    done
}

createPersistPath() {
    loadEnv

    if ! [ -d ${PERSISTPATH} ]; then
        mkdir -m 1777 -p $PERSISTPATH
        chmod 1777 $PERSISTPATH
    fi

    toCreate=(
        # "${TABLESPACE_INDEX:-NONE}:0777:999:999"  # TABLESPACE_INDEX com permissões 0777 e UID/GID 999/999
        # "${TABLESPACE_TMP:-NONE}:0777:999:999"  # TABLESPACE_TMP com permissões 0777 e UID/GID 999/999
    )

    # Adicionar argumentos adicionais à matriz toCreate, se existirem
    if [ $# -ne 0 ]; then
        for additional_path_with_options in "$@"; do
            toCreate+=("$additional_path_with_options")
        done
    fi

    # if [ -n $PGVERSION ]; then 
    #     toCreate+=("${PERSISTPATH}/conf.d:0777")
    #     toCreate+=("${PERSISTPATH}/pgbouncer:0777")
    # fi

    for path_with_options in "${toCreate[@]}"; do 
        # Quebra a string usando ':' como delimitador
        IFS=':' read -r path permissions uid gid <<< "$path_with_options"  

        if [ "$path" != "NONE" ] && [ "$path" != "" ]; then
            path=$(defineAbsolutePath $path)

            if [ ! -d "$path" ]; then 
                mkdir -p -m "$permissions" "$path"
            fi

            # Defina as permissões e proprietário/grupo
            sudo chmod -R "$permissions" "$path"

            # Define proprietarios 
            if [ -n "$uid" ]; then
                sudo chown -R "$uid:$gid" "$path"
            fi
        fi
    done

    if ! [ -f "${PERSISTPATH}/docker-start.log" ]; then
        touch ${PERSISTPATH}/docker-start.log
    fi

    if [ ! -f ${PERSISTPATH}/pgbouncer/userlist.txt ]; then 
        mkdir -p ${PERSISTPATH}/pgbouncer
        touch ${PERSISTPATH}/pgbouncer/userlist.txt
        chmod -R 0777 ${PERSISTPATH}/pgbouncer/userlist.txt
    fi

    pgConfPathCreate
}


checkRootUser() {
    # Verifica se o sistema operacional é Linux
    if [ "$(uname)" == "Linux" ]; then
        # Verifica se o usuário atual não é root
        if [ ! $(id -u) -eq 0 ]; then
            echo "To continue, run this script with sudo"
            exit 1
        fi
    fi
    # Se o sistema operacional não for Linux (como macOS), não faz nada
}


dockerPrune() {
    printTabular \
        "Cleaning up previous images" \
        "docker system prune -f >/dev/null 2>&1"
}

dockerPull() {
    checkDockerCompose

    printTabular \
        "Getting updated images" \
        "${DOCKERCOMPOSE_COMMAND} pull >/dev/null 2>&1"
}

dockerDown() {
    checkDockerCompose

    printTabular \
        "Stopping old containers" \
        "${DOCKERCOMPOSE_COMMAND} down --volumes --remove-orphans >/dev/null 2>&1"
}

dockerUp() {
    checkDockerCompose
    
    dockerDown

    createPersistPath

    printTabular \
        "Starting containers" \
        "${DOCKERCOMPOSE_COMMAND} up -d --build > ${PERSISTPATH}/docker-start.log 2>&1"

    createPersistPath
}

dockerRestart() {
    checkDockerCompose
    local CONTAINER=$1
    if [ "${CONTAINER:-NONE}" == "NONE" ]; then 
        printTabular \
            "Restarting all containers" \
            "${DOCKERCOMPOSE_COMMAND} restart >/dev/null 2>&1"
    else 
        printTabular \
            "Restarting $CONTAINER" \
            "docker restart $CONTAINER >/dev/null 2>&1"
    fi
}

dockerRemoveAll() {
    checkDockerCompose
    echo ">>> ATENTION!! <<<"
    echo "All containers, images and networks on host will be removed"
    read -p "Confirm remove FULL? (no/yes): " CONFIRM
    if [ ${CONFIRM} == "yes" ]; then
        docker system prune -a --force
        docker kill $(docker ps -q)
        docker rm $(docker ps -a -q)
        docker rmi $(docker images -q)
        dockerPrune
        echo ""
    fi
}

removePersistPath() {
    printHeader "${COMPOSE_PROJECT_NAME} - Reinstall"

    TOREMOVE=$1
    TOREMOVE=${TOREMOVE:-'path-not-informed'}

    if [ ! -d "${TOREMOVE}" ]; then 
        echo "Path: '${TOREMOVE}' not exists"
    else
        echo "Atention!! You will lose all saved data!"
        echo "Path: '${TOREMOVE}'"
        read -p "Do you confirm remove all data from path?: (yes/no) " dd
        DECIDE=${dd:-NO}
        if [ ! $DECIDE = 'yes' ]; then
            echo "Aborted!"
            exit 3
        fi
        echo ""

        # stop application
        dockerDown
        dockerPrune

        rm -R "${TOREMOVE}"

        echo "Persist path was removed. Run start command to reinstall"
    fi
}

dockerPs() {
    printContainers
}

loadEnv() {
    if [ ! -f ./.env ]; then
        dd "To continue, create '.env' with you configurations"
    fi
    set -a # turn on automatic exporting
    . .env # source test.env
    set +a # turn off automatic exporting
    PGVERSION=${PGVERSION:-${DEVELOP_PGVERSION:-16}}
    PERSISTPATH=${PERSISTPATH:-${DEVELOP_PERSISTPATH:-./.persist}}
    PHP_VERSION=${PHP_VERSION:-${DEVELOP_PHP_VERSION:-7.4}}
    WAIT_DATABASE_CREATE=${WAIT_DATABASE_CREATE:-${DEVELOP_WAIT_DATABASE_CREATE}}


    PERSISTPATH=$(defineAbsolutePath $PERSISTPATH)
    # echo "1: $PERSISTPATH"

    # # Ajuste para verificacao de paths relativos
    # if [[ "$PERSISTPATH" == ../* || "$PERSISTPATH" == ./* ]]; then
    #     if [[ "$PERSISTPATH" == ./* ]]; then 
    #         PERSISTPATH="${PERSISTPATH#./}" # Remove ./
    #     fi 

    #     PERSISTPATH="$(pwd)/$PERSISTPATH"
    # fi

    # echo "2: $PERSISTPATH"

    # Criar se nao existir
    if [ ! -d $PERSISTPATH ];then 
        mkdir -m 0777 -p $PERSISTPATH
    fi 

    PERSISTPATH=$(realpath "${PERSISTPATH}")

    # echo "4: $PERSISTPATH";
    
    export PERSISTPATH
}


awsLogin() {
    loadEnv
    if [ ! -z $AWS_ECR ]; then 
        printTabular \
            "AWS Login" \
            "aws ecr get-login-password --region us-east-2 --profile ${AWS_PROFILE:-default} | docker login --username AWS --password-stdin ${AWS_ECR} > \"${PERSISTPATH}/docker-start.log\""
    fi
}

awsLogout() {
    if [ ! -z $AWS_ECR ]; then 
        printTabular \
            "AWS Logout" \
            "docker logout "${AWS_ECR}" >/dev/null 2>&1"
    fi
}


pathPermissions() {
    loadEnv
    for default in main release homolog develop apps-data app; do
        if [ -d "${PERSISTPATH}/$default" ]; then
            chmod -R 1777 "${PERSISTPATH}/$default">/dev/null 2>&1
        fi
    done

    pgConfPathCreate
}

pgConfPathCreate() {
    PG_CONF_PATH=$(defineAbsolutePath $PG_CONF_PATH)
    if [ ! -z $PG_CONF_PATH ]; then 
        if [ ! -d $PG_CONF_PATH ]; then 
            mkdir -p $PG_CONF_PATH
        fi
        chmod -R 0777 $PG_CONF_PATH
    else 
        echo ">> env PG_CONF_PATH is not defined"
    fi
}

checkBranchMain() {
    branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
    if [ "$branch" = 'main' ]; then
        dd "Change to the working branch. The main branch is not permitted on development"
    fi
}

checkEnvExists() {
    if [ ! -f ./.env ]; then
        dd "To continue, create the file '.env' with you configurations"
    fi

}

nsComposer() {
    CONTAINER=$1
    ACTION=$2
    printTabular \
        "Composer $ACTION" \
        "docker exec $CONTAINER sh -c 'composer $ACTION --quiet >/dev/null 2>&1'"
}

nsNpm() {
    CONTAINER=$1
    ACTION=$2
    printTabular \
        "NPM $ACTION" \
        "docker exec $CONTAINER sh -c 'npm $ACTION --silent >/dev/null 2>&1'"
}

waitRestore() {
    sleep 2
    local container=$1
    loadEnv
    WAIT_DATABASE_CREATE=${WAIT_DATABASE_CREATE:-600}
    while docker exec "$container" test -f /tmp/restore.log; do
        echo -ne "\r\033[K"
        printf "%-40s%-20s" "Restoring database" "🏃‍♂️ Predicted time ${WAIT_DATABASE_CREATE} seconds"
        sleep 1
        WAIT_DATABASE_CREATE=$((WAIT_DATABASE_CREATE - 1))

    done
    
    echo -ne "\r\033[K"
    printf "%-40s%-20s" "Waiting database" "✔ Done"
    echo ""
}

pgpass() {
    ACTION=$1
    CONTAINER=$2
    CONTAINER=${CONTAINER:-"ns_pg${PGVERSION}"}
    docker exec $CONTAINER sh -c 'if [ -f ~/.pgpass ]; then rm -R ~/.pgpass; fi'
    if [ ${ACTION:-"NONE"} == "set" ]; then
        docker exec $CONTAINER sh -c 'touch ~/.pgpass'
        docker exec $CONTAINER sh -c 'echo "*:*:*:postgres:${POSTGRES_PASSWORD}" >~/.pgpass'
        docker exec $CONTAINER sh -c 'chmod 0600 ~/.pgpass'
    fi
    CONNECTION_STRING="psql postgresql://postgres@localhost:5432/postgres"
}

pgbouncerUserList() {
    loadEnv
    CONTAINER=$1
    SAVETO=$2
    SAVETO=${SAVETO:-./pgbouncer/config/userlist.txt}
    FULL=$3

    pgpass set $CONTAINER

    # criar o arquivo se nao existir, junto com seu diretorio 
    if [ ! -f "$SAVETO" ]; then
        local dirpath="$(dirname "$SAVETO")"
        if [ ! -d "$dirpath" ]; then 
            mkdir -m 0777 -p "$dirpath" || {
                echo "Erro ao criar diretórios: $dirpath"
                return 1
            }
        fi
        touch "$SAVETO" || {
            echo "Erro ao criar arquivo: $SAVETO"
            return 1
        }

        chmod -R 0777 "$SAVETO"
    fi

    if [ ${FULL:-NONE} == "full" ]; then 
        docker exec $CONTAINER psql -Atq -h localhost -p 5432 -U postgres -d postgres -c "SELECT concat('\"', rolname, '\" \"', rolpassword, '\"')  FROM pg_authid WHERE rolpassword IS NOT NULL and rolname not in ('replicator');" >$SAVETO
    else 
        docker exec $CONTAINER psql -Atq -h localhost -p 5432 -U postgres -d postgres -c "SELECT concat('\"', rolname, '\" \"', rolpassword, '\"')  FROM pg_authid WHERE rolpassword IS NOT NULL and rolname not in ('postgres', 'replicator');" >$SAVETO
    fi

    pgpass NONE $CONTAINER

}

postgresDatabasesList() {
    loadEnv
    pgpass set
    echo ">>> Databases <<<"
    docker exec -it $CONTAINER $CONNECTION_STRING -c "SELECT datname FROM pg_catalog.pg_database where datname not in ('postgres', 'template1', 'template0');"
    pgpass rem
}

pgDump() {
    loadEnv

    SAVETO=${SAVETO:="hd"}

    # Lista de variáveis de ambiente necessárias
    required_env_vars=(
        "HOST"
        "BACKUP_PATH"
        "COMPOSE_PROJECT_NAME"
        "POSTGRES_PASSWORD"
        "PGVERSION"
    )

    # Verifica se cada variável de ambiente está definida
    for env_var in "${required_env_vars[@]}"; do
        if [ -z "${!env_var}" ]; then
            echo "A variável de ambiente $env_var não está definida."
            exit 1
        fi
    done

    docker run --rm \
        -v ${BACKUP_PATH}/${COMPOSE_PROJECT_NAME}/pg-backup:/storage \
        -v ${BACKUP_PATH}/${COMPOSE_PROJECT_NAME}/tmp:/tmp \
        --add-host=host.docker.internal:host-gateway \
        -e APPNAME=${COMPOSE_PROJECT_NAME} \
        -e HOST=${HOST:="172.17.0.1"} \
        -e POSTGRES_USER_PASS=${BACKUP_PGPASSWORD:-${POSTGRES_PASSWORD}} \
        -e PGPORT=${BACKUP_PGPORT:-${PGPORT:-5432}} \
        -e DBIGNORE=${DBIGNORE:="postgres"} \
        -e SAVETO=${SAVETO:="hd"} \
        -e BUCKET_NAME=${BUCKET_NAME:="pg-backup"} \
        -e GCLOUD_JSONFILE=${GCLOUD_JSONFILE:=""} \
        -e AWS_PROFILE=${AWS_PROFILE:="default"} \
        -e AWS_REGION=${AWS_REGION:="us-east-1"} \
        -e AWS_KEY=${AWS_KEY:=""} \
        -e AWS_SECRET=${AWS_SECRET:=""} \
        nextstage/pg-backup:${BACKUP_PGVERSION:-${PGVERSION}} \
        php nsutil app:run
}

postgresStackUp() {
    WAIT_CONTAINERS=$1
    WAIT_RESTORE=$2
    ACTION=$3

    loadEnv
    printHeader "Stack Postgresql: $ACTION"
    echo "Chech logs on file: ${PERSISTPATH}/docker-start.log"
    createPersistPath

    if [ -d ./pgbouncer/config ]; then 
        chmod -R 0777 ./pgbouncer/config
    fi 

    if [[ "$ACTION" == "up" || "$ACTION" == "start" ]]; then
        dockerPull
        pgConfPathCreate
        dockerUp
        createPersistPath
        pgConfPathCreate

        if [ ! -z "${WAIT_RESTORE}" ]; then
            waitRestore "${WAIT_RESTORE}"
        fi

        if [ ! -z "${WAIT_CONTAINERS}" ]; then
            sleep 2
            waitForContainerHealth "${WAIT_CONTAINERS}"
        fi

        pgConfPathCreate
    fi

    if [[ "$ACTION" == "down" || "$ACTION" == "stop" ]]; then
        dockerDown
    fi

    if [ $ACTION == 'restart' ]; then
        dockerUp
    fi

    dockerPrune

    printContainers


}

http() {
    ACTION=$1
    WAIT_CONTAINERS=$2

    loadEnv

    printHeader "${COMPOSE_PROJECT_NAME}: $ACTION"
    echo "Chech logs on file: ${PERSISTPATH}/docker-start.log"
    loadEnv
    createPersistPath

    if [[ "$ACTION" == "up" || "$ACTION" == "start" ]]; then
        dockerPrune
        awsLogin
        dockerPull
        awsLogout
        pgConfPathCreate
        dockerUp
        pathPermissions
        dockerPrune
        if [ -n "${WAIT_CONTAINERS}" ]; then
            sleep 2
            waitForContainerHealth "${WAIT_CONTAINERS}"
        fi
        pgConfPathCreate
    fi

    if [[ "$ACTION" == "down" || "$ACTION" == "stop" ]]; then
        dockerDown
        dockerPrune
    fi

    if [ $ACTION == 'restart' ]; then
        dockerRestart
    fi

    printContainers

}

# List all functions
help() {
    printf "Functions available:\n"
    declare -F | awk '{print $3}'
}

nsDockerStart() {
    checkRootUser

    emp_partial=$1
    app_partial=$2
    docker_up="up"
    
    # Encontrar o diretório e mudar para ele
    LOCAL="/home/$SUDO_USER"
    targets=$(echo ${LOCAL}/${emp_partial}*/*${app_partial}*/)

    for target in $targets; do 
        if [[ "$target" != *"-produto"* ]]; then
            if [ -n "$target" ] && [ -d "$target" ]; then
                application_name=$(basename "$target")
                if [ ${docker_up:="no"} = "up" ]; then 
                    if [ -f "$target/docker/scripts/start.sh" ]; then 
                        echo "[$application_name]: Started!"
                        nohup bash "${target}docker/scripts/start.sh" >/tmp/$application_name.nohup 2>&1 &
                    else 
                        echo "[$application_name]: ERROR: file start.sh not found to up containers"
                    fi
                fi
            else
                echo "[$application_name]: ERROR: path not found"
            fi
        fi
    done
}

configureXdebugPortOnVscode() {
    # check if XDEBUG_CLIENT_PORT is defined
    if [ -z ${XDEBUG_CLIENT_PORT} ]; then 
        # echo "XDEBUG_CLIENT_PORT is not defined"
        return 1
    fi

    LAUNCH_DIR=$(defineAbsolutePath ./.vscode)
    LAUNCH=$(defineAbsolutePath ./.vscode/launch.json)

    if [ ! -d $LAUNCH_DIR ]; then 
        mkdir -p $LAUNCH_DIR
    fi

    echo "{
    \"version\": \"0.2.0\",
    \"configurations\": [
        {
            \"name\": \"Listen for XDebug\",
            \"type\": \"php\",
            \"request\": \"launch\",
            \"port\": ${XDEBUG_CLIENT_PORT:-9003}, 
            \"pathMappings\": {
                \"/var/www/html\": \"\${workspaceRoot}\"
            }
        }
    ]
}
" > $LAUNCH

    echo "Xdebug port configured on $LAUNCH"  

    PHP_VERSION=${PHP_VERSION:-8.3}

    # check if settings.json exists
    echo "{
  \"intelephense.environment.phpVersion\": \"${PHP_VERSION}\",
  \"php.version\": \"${PHP_VERSION}\"
}" > $(defineAbsolutePath ./.vscode/settings.json)

    echo "PHP Version configured on $(defineAbsolutePath ./.vscode/settings.json)"
}

function panel() {
    # Find all start.sh files in subdirectories
    echo "=== Applications Menu ==="
    echo ""

    # Create array with found scripts
    scripts=()
    i=1

    while IFS= read -r script; do
        app_name=$(dirname $(dirname "$script"))
        scripts+=("$script")
        echo "[$i] $app_name"
        ((i++))
    done < <(find */scripts/start.sh -type f 2>/dev/null | sort)

    echo ""
    echo "Enter the application number or 'q' to exit:"

    # Read user input
    read -r choice

    # Execute selected script
    if [[ "$choice" =~ ^[0-9]+$ ]] && [ "$choice" -le "${#scripts[@]}" ]; then
        selected_script="${scripts[$choice-1]}"
        bash "$selected_script"
    elif [ "$choice" = "q" ]; then
        echo "Exiting..."
        exit 0
    else
        echo "Invalid option!"
    fi
}

# Only root user
checkRootUser

# pre load envs
loadEnv

# create persisth Path
createPersistPath

# Development configure
configureXdebugPortOnVscode