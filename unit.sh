#!/bin/bash

# Define a variável __DIR__ com o diretório atual do script
PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

for V in 7.4 8.0 8.1 8.2 8.3; do
    echo "****************************************************"
    echo "Running tests for PHP $V"
    OUTPUT=$(docker run \
        --quiet \
        --add-host=host.docker.internal:host-gateway \
        --env-file $PWD/.env \
        -it \
        --rm \
        -v $PWD:/var/www/html \
        nextstage/php:$V-cli-dev \
        sh -c "cd /var/www/html && composer update > /dev/null 2>&1 && ./vendor/bin/phpunit --configuration phpunit.xml")

    if echo "$OUTPUT" | grep -q "FAILURES"; then
        echo "----------------------------------------"
        echo "Tests failed for PHP $V"
        echo "----------------------------------------"
        exit 1  # Aborta a operação
    fi
    echo ""
done

echo "Testes Conclusos"
