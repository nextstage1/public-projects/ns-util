<?php

use NsUtil\Helpers\Validate;
use PHPUnit\Framework\TestCase;

class ValidateTest extends TestCase
{
    public function testIsCep()
    {
        $this->assertTrue(Validate::isCep('88180970'));
        $this->assertFalse(Validate::isCep('1234567'));
        $this->assertFalse(Validate::isCep('123456789'));
        $this->assertFalse(Validate::isCep('abcdefgh'));
    }

    public function testValidaCpfCnpj()
    {
        $this->assertTrue(Validate::validaCpfCnpj('15885509077'));
        $this->assertTrue(Validate::validaCpfCnpj('158.855.090-77'));
        $this->assertTrue(Validate::validaCpfCnpj('04824265000165'));
        $this->assertTrue(Validate::validaCpfCnpj('04.824.265/0001-65'));
        $this->assertEquals('Preencha corretamente CPF/CNPJ', Validate::validaCpfCnpj('test-15885509077'));
        $this->assertEquals('Preencha corretamente CPF/CNPJ', Validate::validaCpfCnpj('123'));
        $this->assertEquals('CPF Inválido: Digito verificador não é válido', Validate::validaCpfCnpj('15885509076'));
        $this->assertEquals('CPF Inválido: Número Sequencial', Validate::validaCpfCnpj('99999999999'));
        $this->assertEquals('Preencha corretamente CPF/CNPJ', Validate::validaCpfCnpj(''));
        $this->assertEquals('Preencha corretamente CPF/CNPJ', Validate::validaCpfCnpj(null));
        $this->assertEquals('Preencha corretamente CPF/CNPJ', Validate::validaCpfCnpj(false));
        $this->assertEquals('Preencha corretamente CPF/CNPJ', Validate::validaCpfCnpj(true));
        $this->assertEquals('Preencha corretamente CPF/CNPJ', Validate::validaCpfCnpj([]));
        $this->assertEquals('Preencha corretamente CPF/CNPJ', Validate::validaCpfCnpj(new stdClass()));
    }

    public function testValidaEmail()
    {
        $this->assertTrue(Validate::validaEmail('test@example.com'));
        $this->assertFalse(Validate::validaEmail('invalid-email'));
    }

    public function testIsBase64Encoded()
    {
        $this->assertTrue(Validate::isBase64Encoded('dGVzdA=='));
        $this->assertFalse(Validate::isBase64Encoded('invalid base64'));
    }

    public function testIsJson()
    {
        $this->assertTrue(Validate::isJson('{"key": "value"}'));
        $this->assertFalse(Validate::isJson('invalid json'));
    }

    public function testIsMobile()
    {
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1';
        $this->assertTrue(Validate::isMobile());

        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3';
        $this->assertFalse(Validate::isMobile());
    }
}
