<?php

use NsUtil\Helpers\File;
use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
    private $testFilePath;

    protected function setUp(): void
    {
        $this->testFilePath = '/tmp/testfile.txt';
        file_put_contents($this->testFilePath, 'Test content');
    }

    protected function tearDown(): void
    {
        if (file_exists($this->testFilePath)) {
            unlink($this->testFilePath);
        }
    }

    public function testGetMimeType()
    {
        $mimeType = File::getMimeType($this->testFilePath);
        $this->assertEquals('text/plain', $mimeType);
    }

    public function testGetTmpDir()
    {
        $tmpDir = File::getTmpDir();
        $this->assertDirectoryExists($tmpDir);
    }
}
