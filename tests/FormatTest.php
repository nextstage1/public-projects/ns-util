<?php

use NsUtil\Helpers\Format;
use PHPUnit\Framework\TestCase;

class FormatTest extends TestCase
{
    public function testDate()
    {
        $format = new Format('2023-10-05T12:00:00');
        $this->assertEquals('2023-10-05', $format->date('arrumar'));
        $this->assertEquals('05/10/2023', $format->date('mostrar'));
        $this->assertEquals('2023-10-05T12:00:00+00:00', $format->date('c'));
        $this->assertEquals('05 de October de 2023', $format->date('extenso'));
        $this->assertIsInt($format->date('timestamp'));
        $this->assertIsArray($format->date('age'));
    }

    public function testFone()
    {
        $format = new Format('11987654321');
        $this->assertEquals('(11) 987654321', $format->fone());
    }

    public function testCep()
    {
        $format = new Format('12345678');
        $this->assertEquals('12345-678', $format->cep());
    }

    public function testDecimal()
    {
        $format = new Format('1.234,56');
        $this->assertEquals('1234.56', $format->decimal());
    }

    public function testParseInt()
    {
        $format = new Format('R$ 1.234,56');
        $this->assertEquals('123456', $format->parseInt());
    }

    public function testDateToMktime()
    {
        $format = new Format('2023-10-05T12:00:00');
        $this->assertIsInt($format->dateToMktime());
    }

    public function testSubDays()
    {
        $format = new Format('2023-10-05');
        $this->assertEquals('2023-10-03 12:00:00', $format->subDays(2));
    }

    public function testSubMonths()
    {
        $format = new Format('2023-10-05 12:00:00');
        $this->assertEquals('2023-08-05 12:00:00', $format->subMonths(2));
    }

    public function testFormatNumber()
    {
        $format = new Format('1234.56');
        $this->assertEquals('<strong><small>R$</small> </strong> 1.234,56', $format->formatNumber());
    }

    public function testCpfCnpj()
    {
        $format = new Format('12345678909');
        $this->assertEquals('123.456.789-09', $format->cpfCnpj());

        $format = new Format('12345678000195');
        $this->assertEquals('12.345.678/0001-95', $format->cpfCnpj());
    }

    public function testHumanTimeFromSeconds()
    {
        $format = new Format('3661');
        $this->assertEquals('1h 1m 1s', $format->humanTimeFromSeconds());
    }
}
