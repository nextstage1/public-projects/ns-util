<?php

use NsUtil\Helpers\Template;
use PHPUnit\Framework\TestCase;

class TemplateTest extends TestCase
{
    public function testConstructorWithEmptyString()
    {
        $template = new Template('');
        $this->assertEquals('TEMPLATE IS NOT DEFINED', $template->getTemplate());
    }

    public function testConstructorWithLongString()
    {
        $longString = str_repeat('a', 1001);
        $template = new Template($longString);
        $this->assertEquals($longString, $template->getTemplate());
    }

    public function testConstructorWithFile()
    {
        $filePath = '/tmp/test_template.txt';
        file_put_contents($filePath, 'File content');
        $template = new Template($filePath);
        $this->assertEquals('File content', $template->getTemplate());
        unlink($filePath);
    }

    public function testConstructorWithHtmlString()
    {
        $htmlString = '<h1>Hello, World!</h1>';
        $template = new Template($htmlString);
        $this->assertEquals($htmlString, $template->getTemplate());
    }

    public function testSetTemplate()
    {
        $template = new Template('');
        $template->setTemplate('New template content');
        $this->assertEquals('New template content', $template->getTemplate());
    }

    public function testSetVarAndRender()
    {
        $template = new Template('Hello, {name}!');
        $template->setVar(['name' => 'John']);
        $this->assertEquals('Hello, John!', $template->render());
    }

    public function testRenderWithMultipleVariables()
    {
        $template = new Template('Hello, {name}! Welcome to {place}.');
        $template->setVar(['name' => 'John', 'place' => 'Earth']);
        $this->assertEquals('Hello, John! Welcome to Earth.', $template->render());
    }

    public function testRenderToFile()
    {
        $template = new Template('Hello, {name}!');
        $template->setVar(['name' => 'John']);
        $targetFile = '/tmp/output.txt';
        $template->renderTo($targetFile, true);
        $this->assertFileExists($targetFile);
        $this->assertEquals('Hello, John!', file_get_contents($targetFile));
        unlink($targetFile);
    }
}
