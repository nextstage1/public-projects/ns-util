<?php

use NsUtil\Security\Password;
use PHPUnit\Framework\TestCase;

class PasswordTest extends TestCase
{
    public function testCodificaSenha()
    {
        $senha = 'password123';
        $encoded = Password::codificaSenha($senha);
        $this->assertNotEquals($senha, $encoded);
        $this->assertFalse(password_verify($senha, $encoded));
        $this->assertTrue(Password::verify($senha, $encoded));
    }

    public function testGeraNovaSenha()
    {
        $senha = Password::geraNovaSenha();
        $this->assertEquals(12, strlen($senha));
    }

    public function testVerify()
    {
        $senha = 'password123';
        $hash = Password::codificaSenha($senha);
        $this->assertTrue(Password::verify($senha, $hash));
        $this->assertFalse(Password::verify('wrongpassword', $hash));
    }

    public function testUpdatePassword()
    {
        $oldSenha = 'oldpassword';
        $newSenha = 'newpassword123';
        $hash = Password::codificaSenha($oldSenha);
        $newHash = Password::updatePassword($oldSenha, $newSenha, $hash);
        $this->assertNotFalse($newHash);
        $this->assertTrue(Password::verify($newSenha, $newHash));
    }

    public function testForcaSenha()
    {
        $this->assertEquals(-1, Password::forcaSenha('123'));
        $this->assertEquals(2, Password::forcaSenha('password'));
        $this->assertEquals(4, Password::forcaSenha('Password1'));
        $this->assertEquals(5, Password::forcaSenha('Password1!'));
        $this->assertEquals(5, Password::forcaSenha('Password1!#'));
    }

    public function testSecurityEdgeCases()
    {
        // Test with common passwords
        $commonPasswords = ['123456', 'password', '123456789', '12345678', '12345'];
        foreach ($commonPasswords as $commonPassword) {
            $encoded = Password::codificaSenha($commonPassword);
            $this->assertNotEquals($commonPassword, $encoded);
            $this->assertTrue(Password::verify($commonPassword, $encoded));
        }

        // Test with SQL injection attempts
        $sqlInjections = ["' OR '1'='1", "'; DROP TABLE users; --", "' OR 'x'='x"];
        foreach ($sqlInjections as $sqlInjection) {
            $encoded = Password::codificaSenha($sqlInjection);
            $this->assertNotEquals($sqlInjection, $encoded);
            $this->assertTrue(Password::verify($sqlInjection, $encoded));
        }

        // Test with XSS attempts
        $xssAttempts = ["<script>alert('xss');</script>", "<img src='x' onerror='alert(1)'>"];
        foreach ($xssAttempts as $xssAttempt) {
            $encoded = Password::codificaSenha($xssAttempt);
            $this->assertNotEquals($xssAttempt, $encoded);
            $this->assertTrue(Password::verify($xssAttempt, $encoded));
        }

        // Test with special characters
        $specialChars = ['!@#$%^&*()', ')(*&^%$#@!', 'password!@#'];
        foreach ($specialChars as $specialChar) {
            $encoded = Password::codificaSenha($specialChar);
            $this->assertNotEquals($specialChar, $encoded);
            $this->assertTrue(Password::verify($specialChar, $encoded));
        }

        // Test with very long passwords
        $longPassword = str_repeat('a', 1000);
        $encoded = Password::codificaSenha($longPassword);
        $this->assertNotEquals($longPassword, $encoded);
        $this->assertTrue(Password::verify($longPassword, $encoded));
    }
}
