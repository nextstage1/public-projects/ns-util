<?php

namespace Tests;

use Laravel\SerializableClosure\SerializableClosure;
use PHPUnit\Framework\TestCase;

class SerializableClosureTest extends TestCase
{
    public function testSerializeClosure()
    {
        // Criando uma closure básica
        $closure = function ($x) {
            return $x * 2;
        };

        // Serializando a closure
        $serializable = new SerializableClosure($closure);
        $serialized = serialize($serializable);

        // Desserializando
        $unserialized = unserialize($serialized);
        $result = $unserialized(5);

        // Verificando se o resultado está correto
        $this->assertEquals(10, $result);
    }

    public function testSerializeClosureWithUseStatement()
    {
        $multiplier = 3;
        $closure = function ($x) use ($multiplier) {
            return $x * $multiplier;
        };

        $serializable = new SerializableClosure($closure);
        $serialized = serialize($serializable);

        $unserialized = unserialize($serialized);
        $result = $unserialized(5);

        $this->assertEquals(15, $result);
    }
}
