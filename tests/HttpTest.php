<?php

use NsUtil\Http\Http;
use PHPUnit\Framework\TestCase;

class HttpTest extends TestCase
{
    private const BASE_URL = 'https://jsonplaceholder.typicode.com/posts';

    public function testGetRequest()
    {
        $url = self::BASE_URL . '/1';
        $response = Http::call($url, [], 'GET', ['Content-Type: application/json'], false);

        $this->assertEquals(200, $response->getHttpCode());
        $this->assertNotEmpty($response->getBody());
        $this->assertIsArray($response->getHeaders());
    }

    public function testPostRequest()
    {
        $url = self::BASE_URL;
        $params = [
            'title' => 'foo',
            'body' => 'bar',
            'userId' => 1
        ];
        $method = 'POST';
        $header = ['Content-Type: application/json'];

        $response = Http::call($url, $params, $method, $header, false);

        $this->assertEquals(201, $response->getHttpCode());
        $this->assertNotEmpty($response->getBody());
        $this->assertIsArray($response->getHeaders());
    }

    public function testPutRequest()
    {
        $url = self::BASE_URL . '/1';
        $params = [
            'id' => 1,
            'title' => 'foo',
            'body' => 'bar',
            'userId' => 1
        ];
        $method = 'PUT';
        $header = ['Content-Type: application/json'];

        $response = Http::call($url, $params, $method, $header, false);

        $this->assertEquals(200, $response->getHttpCode());
        $this->assertNotEmpty($response->getBody());
        $this->assertIsArray($response->getHeaders());
    }

    public function testDeleteRequest()
    {
        $url = self::BASE_URL . '/1';
        $method = 'DELETE';
        $header = ['Content-Type: application/json'];

        $response = Http::call($url, [], $method, $header, false);
        $this->assertEquals(200, $response->getHttpCode());
        $this->assertJson($response->getBody());
        $this->assertIsArray($response->getHeaders());
    }

    public function testInvalidUrl()
    {
        $url = 'https://invalid.url';
        $response = Http::call($url);

        $this->assertNotEquals(0, $response->getErrorCode());
        $this->assertNotEmpty($response->getError());
    }
}
