<?php

use NsUtil\Helpers\DirectoryManipulation;
use PHPUnit\Framework\TestCase;

class DirectoryManipulationTest extends TestCase
{
    private $testDir = '/tmp';

    protected function setUp(): void
    {
        $this->testDir = __DIR__ . '/testDir';
        mkdir($this->testDir);
        file_put_contents($this->testDir . '/file1.txt', 'Hello World');
        file_put_contents($this->testDir . '/file2.txt', 'Hello Again');
        mkdir($this->testDir . '/subDir');
        file_put_contents($this->testDir . '/subDir/file3.txt', 'Hello SubDir');
    }

    protected function tearDown(): void
    {
        $this->deleteDirectory($this->testDir);
    }

    private function deleteDirectory($dir)
    {
        if (!is_dir($dir)) {
            return;
        }
        $files = array_diff(scandir($dir), ['.', '..']);
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->deleteDirectory("$dir/$file") : unlink("$dir/$file");
        }
        rmdir($dir);
    }

    public function testGetSizeOf()
    {
        $dm = new DirectoryManipulation();
        $dm->getSizeOf($this->testDir);
        $this->assertArrayHasKey('txt', $dm->total);
        $this->assertEquals(3, $dm->total['txt']['count']);
    }

    public function testGetSize()
    {
        $dm = new DirectoryManipulation();
        $result = $dm->getSize($this->testDir, 'txt');
        $this->assertEquals(3, $result['count']);
    }

    public function testOpenDir()
    {
        $files = DirectoryManipulation::openDir($this->testDir);
        $this->assertContains('file1.txt', $files);
        $this->assertContains('file2.txt', $files);
        $this->assertContains('subDir', $files);
    }

    public function testRecursiveDirectoryCopy()
    {
        $destDir = $this->testDir . '/destDir';
        DirectoryManipulation::recursiveDirectoryCopy($this->testDir, $destDir);
        $this->assertDirectoryExists($destDir);
        $this->assertFileExists($destDir . '/file1.txt');
        $this->assertFileExists($destDir . '/subDir/file3.txt');
        $this->deleteDirectory($destDir);
    }

    public function testClearDir()
    {
        DirectoryManipulation::clearDir($this->testDir, 0);
        $files = DirectoryManipulation::openDir($this->testDir);
        $this->assertEmpty($files);
    }

    public function testDeleteDirectory()
    {
        $result = DirectoryManipulation::deleteDirectory($this->testDir);
        $this->assertTrue($result);
        $this->assertDirectoryDoesNotExist($this->testDir);
    }

    public function testGetLastFileCreated()
    {
        $lastFileTime = DirectoryManipulation::getLastFileCreated($this->testDir);
        $this->assertNotNull($lastFileTime);
        $this->assertIsInt($lastFileTime);
    }
}
