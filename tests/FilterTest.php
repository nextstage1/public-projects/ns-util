<?php

use NsUtil\Helpers\Filter;
use PHPUnit\Framework\TestCase;

class FilterTest extends TestCase
{
    public function testStringSanitization()
    {
        $input = "<script>alert('test');</script> Hello\x00 World!";
        $expected = "alert('test'); Hello World!";
        $this->assertEquals($expected, Filter::string($input));
    }

    public function testIntegerValidation()
    {
        $input = "123";
        $expected = 123;
        $this->assertEquals($expected, Filter::integer($input));

        $input = "abc";
        $expected = 0;
        $this->assertEquals($expected, Filter::integer($input));
    }

    public function testTemporaryFileCreation()
    {
        $tempFile = tempnam(sys_get_temp_dir(), 'test_');
        file_put_contents($tempFile, 'Temporary file content');

        $this->assertFileExists($tempFile);
        $this->assertStringEqualsFile($tempFile, 'Temporary file content');

        // Clean up
        unlink($tempFile);
        $this->assertFileDoesNotExist($tempFile);
    }
}
