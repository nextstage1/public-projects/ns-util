<?php

use NsUtil\Helpers\Eficience;
use PHPUnit\Framework\TestCase;

class EficienceTest extends TestCase
{
    public function testEndMethodReturnsCorrectObject()
    {
        $eficience = new Eficience();
        usleep(100000); // Sleep for 0.1 seconds to simulate code execution
        $result = $eficience->end();

        $this->assertTrue(property_exists($result, 'text'));
        $this->assertTrue(property_exists($result, 'time'));
        $this->assertTrue(property_exists($result, 'memory'));
        $this->assertStringContainsString('Elapsed time:', $result->text);
        $this->assertStringContainsString('Memory peak:', $result->text);
    }

    public function testClosureIsCalledWithCorrectParameters()
    {
        $closureCalled = false;
        $closure = function ($seconds, $memory, $time) use (&$closureCalled) {
            $closureCalled = true;
            $this->assertIsFloat($seconds);
            $this->assertIsString($memory);
            $this->assertIsString($time);
        };

        $eficience = new Eficience($closure);
        usleep(100000); // Sleep for 0.1 seconds to simulate code execution
        $eficience->end();

        $this->assertTrue($closureCalled);
    }

    public function testSetFnAfterEnding()
    {
        $eficience = new Eficience();
        $closure = function () {};
        $eficience->setFnAfterEnding($closure);

        $reflection = new ReflectionClass($eficience);
        $property = $reflection->getProperty('fnAfterEnding');
        $property->setAccessible(true);

        $this->assertSame($closure, $property->getValue($eficience));
    }

    public function testEndMethodCalculatesCorrectTimeAndMemory()
    {
        $eficience = new Eficience();
        usleep(100000); // Sleep for 0.1 seconds to simulate code execution
        $result = $eficience->end();

        $this->assertGreaterThan(0, $result->time);
        $this->assertStringEndsWith('MB', $result->memory);
    }
}
