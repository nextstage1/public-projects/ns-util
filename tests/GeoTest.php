<?php

use NsUtil\Services\Geo;
use PHPUnit\Framework\TestCase;

class GeoTest extends TestCase
{
    public function testGetValidIp()
    {
        $result = Geo::get('8.8.8.8');

        $this->assertArrayHasKey('countryName', $result);
        $this->assertArrayHasKey('regionName', $result);
        $this->assertArrayHasKey('city', $result);
        $this->assertArrayHasKey('latitude', $result);
        $this->assertArrayHasKey('longitude', $result);
        $this->assertArrayHasKey('timezone', $result);
        $this->assertArrayHasKey('currencyCode', $result);
        $this->assertArrayHasKey('currencySymbol', $result);

        $this->assertEquals('United States', $result['countryName']);
        $this->assertEquals('North America', $result['continentName']);
        $this->assertEquals('USD', $result['currencyCode']);
        // $this->assertEquals('$', $result['currencySymbol_UTF8']);
    }

    public function testGetInvalidResponse()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Invalid response from url');

        Geo::get('8.8.8.777');
    }
}
