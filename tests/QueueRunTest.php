<?php

namespace Tests\Commands\NsUtils;

use Exception;
use NsUtil\Commands\NsUtils\QueueRun;
use NsUtil\Helper;
use NsUtil\EnvFile;

use function NsUtil\env;
use function NsUtil\nsCommand;
use NsUtil\Queue\QueueHandler;
use PHPUnit\Framework\TestCase;
use NsUtil\ConnectionPostgreSQL;
use NsUtil\ConsoleTable;
use NsUtil\Queue\QueueHandlers\TestsHandler;

class QueueRunTest extends TestCase
{
    private ConnectionPostgreSQL $con;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        echo "\n";
        ConsoleTable::printHeader('QueueRunTest');

        // Silenciar mensagens do Xdebug mantendo a funcionalidade
        ini_set('xdebug.log_level', '0');

        Helper::saveFile(__DIR__ . '/../test/logs/php_error.log', '', '');
        ini_set('error_log', __DIR__ . '/../test/logs/php_error.log');

        // Remover diretorio de migrations se existir
        Helper::deleteDir('/tmp/nsutil-migrations');

        // verificar se postgresql ja esta instalado e instalar se precisar
        // exec("service postgresql status", $output, $return);
        // if ($return !== 0) {
        // exec('curl -fsSL https://www.postgresql.org/media/keys/ACCC4CF8.asc | gpg --dearmor -o /usr/share/keyrings/postgresql-keyring.gpg');
        // exec('echo "deb [signed-by=/usr/share/keyrings/postgresql-keyring.gpg] http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list');
        exec('apt-get update');
        exec('apt-get install postgresql postgresql-contrib -y');
        exec('service postgresql restart');
        // }

        // wait for postgresql to be ready
        sleep(3);

        // create database and password to user postgres
        exec('su - postgres -c "psql -c \\"ALTER USER postgres PASSWORD \'102030\';\\""');
        $terminateQuery = "
            SELECT pg_terminate_backend(pg_stat_activity.pid) 
            FROM pg_stat_activity 
            WHERE 
                pid <> pg_backend_pid() 
                AND pg_stat_activity.datname = 'tests'
        ";
        exec('su - postgres -c "psql -c \\"' . $terminateQuery . '\\""');
        exec("su - postgres -c \"psql -c 'DROP DATABASE IF EXISTS tests;'\"");
        exec("su - postgres -c \"psql -c 'CREATE DATABASE tests;'\"");

        // definir as envs de ambiente
        EnvFile::applyEnvVariables(null, [
            'DBHOST' => 'localhost',
            'DBUSER' => 'postgres',
            'DBPASS' => '102030',
            'DBPORT' => '5432',
            'DBNAME' => 'tests',
            'NSUTIL_QUEUE_AUTORUN_ON_ADD' => false,
            'UNIQUE_EXECUTION_DRIVER' => 'psql',
            'NSUTIL_QUEUE_TESTS' => false
        ]);

        // Chamado aqui para ativar as migrations pre-testes
        QueueHandler::migrate();
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();

        // Remover diretorio de migrations se existir
        Helper::deleteDir('/tmp/nsutil-migrations');
    }

    protected function setUp(): void
    {
        parent::setUp();

        // definir as envs de ambiente
        EnvFile::applyEnvVariables(null, [
            'DBHOST' => 'localhost',
            'DBUSER' => 'postgres',
            'DBPASS' => '102030',
            'DBPORT' => '5432',
            'DBNAME' => 'tests',
            'NSUTIL_QUEUE_AUTORUN_ON_ADD' => false,
            'UNIQUE_EXECUTION_DRIVER' => 'psql',
            'RUNTIME_ERROR' => null,
            '__NSUTIL_QUEUE_TESTS' => null
        ]);


        $this->con = ConnectionPostgreSQL::getConnectionByEnv();

        // Remover diretorio de migrations se existir
        // Helper::deleteDir('/tmp/nsutil-migrations');

        // Chamado aqui para ativar as migrations pre-testes
        // QueueHandler::migrate();

        // Limpar tabelas de trabalho (nsutil)
        $cleared = [
            "TRUNCATE TABLE _nsutil.queue CASCADE",
            "TRUNCATE TABLE _nsutil.queue_fails CASCADE",
            "TRUNCATE TABLE _nsutil.queue_success CASCADE",
            "TRUNCATE TABLE _nsutil.unique_execution_lock CASCADE"
        ];
        array_map(fn($query) => $this->con->executeQuery($query), $cleared);

        // default values
        TestsHandler::setRemove(true);
        TestsHandler::setPriority(1);
        TestsHandler::setAssync(true);
        TestsHandler::setInitAt(null);

        // parar qualquer fila rodando
        nsCommand("queue:stop");
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->con->close();
    }
    /**
     * @test
     */
    public function testCreateQueue()
    {
        $result = TestsHandler::addByDTO(['amount' => 123]);
        $this->assertGreaterThan(0, $result);
    }

    /**
     * @test
     */
    public function testShouldReturnPendingStatus()
    {
        $id = TestsHandler::addByDTO(['amount' => 123]);
        $this->assertEquals('PENDING', QueueHandler::status($id));
    }

    /**
     * @test
     */
    public function testShouldReturnCompleteStatus()
    {
        $id = TestsHandler::addByDTO(['amount' => 123]);
        QueueHandler::resolve($id);
        $this->assertEquals('COMPLETE', QueueHandler::status($id));
    }

    /**
     * @test
     */
    public function testShouldHandleException()
    {
        $id = TestsHandler::addByDTO(['amount' => 123], true);
        QueueHandler::resolve($id);
        $this->assertEquals('FAILED', QueueHandler::status($id));
    }

    /**
     * @test
     */
    public function testShouldReturnRunningStatus()
    {
        $id = TestsHandler::addByDTO(['amount' => 123], null, 2);
        nsCommand("queue:run $id", '/tmp/queueRunTests.log', true);
        sleep(1);
        $this->assertEquals('RUNNING', QueueHandler::status($id));
        sleep(2);
        $this->assertEquals('COMPLETE', QueueHandler::status($id));
    }

    private function __stopQueueExecute(bool $assync = true): void
    {
        // Criar algumas filas em execução
        TestsHandler::setAssync($assync);
        $sleepTime = $assync ? 5 : 1;
        nsCommand("queue:stop");

        $queuesToRunner = array_map(fn($id) => TestsHandler::addByDTO(['amount' => 123 * $id], null, $sleepTime), range(1, 3));

        // Iniciar execução das filas
        sleep(3);
        nsCommand("queue:run");

        // Solicitar parada
        sleep(3);
        nsCommand("queue:stop");

        // Inserir novas filas que devem ficar pendentes
        $queuesToNotRunning = array_map(fn($id) => TestsHandler::addByDTO(['amount' => 123 * $id], null, 2), range(1, 1));

        // Verificar se as filas foram finalizadas
        array_map(fn($status) => $this->assertEquals('COMPLETE', QueueHandler::status($status)), $queuesToRunner);

        // Verificar se as filas novas ficaram pendentes apos pedido de stop
        array_map(fn($status) => $this->assertEquals('PENDING', QueueHandler::status($status)), $queuesToNotRunning);
    }


    /**
     * @test
     */
    public function testShouldStopAssyncsQueue(): void
    {
        $this->__stopQueueExecute(true);
    }

    /**
     * @test
     */
    public function testShouldStopSyncsQueue(): void
    {
        $this->__stopQueueExecute(false);
    }

    private function __retryQueueExecute(): void
    {
        // Test to retry assync queues
        TestsHandler::setAssync(true);
        $id = TestsHandler::addByDTO(['amount' => 123], null, null, null, env('RUNTIME_ERROR', null));
        sleep(3);
        $retries = 0;
        do {
            nsCommand("queue:run");
            sleep(1);
            $retries++;
        } while (QueueHandler::status($id) === 'RUNNING' && $retries < 10);
        $this->assertEquals('FAILED', QueueHandler::status($id));
        $this->assertLessThanOrEqual(3, $retries);

        TestsHandler::setAssync(false);
        $id = TestsHandler::addByDTO(['amount' => 123], null, null, null, env('RUNTIME_ERROR', null));
        sleep(3);
        $retries = 0;
        do {
            nsCommand("queue:run");
            sleep(1);
            $retries++;
        } while (QueueHandler::status($id) === 'RUNNING' && $retries < 3);
        $this->assertEquals('FAILED', QueueHandler::status($id));
        $this->assertLessThanOrEqual(3, $retries);
    }

    /**
     * @test
     */
    public function testShouldRetryDeadQueues(): void
    {
        EnvFile::applyEnvVariables(null, [
            'QUEUE_MAX_SECONDS_PER_QUEUE' => 1,
            'QUEUE_MAX_RETRIES' => 1,
            'RUNTIME_ERROR' => true
        ]);

        $this->__retryQueueExecute();
    }

    /**
     * @test
     */
    public function testShouldRetryLockedQueues(): void
    {
        EnvFile::applyEnvVariables(null, [
            'QUEUE_MAX_SECONDS_PER_QUEUE' => 1,
            'QUEUE_MAX_RETRIES' => 1,
            // usado para injetar o erro no init da classe queueHandler e nao permitir a conexao com o banco de dados
            '__NSUTIL_QUEUE_TESTS' => true
        ]);

        $this->__retryQueueExecute();
    }

    /**
     * @test
     */
    public function testShouldPreventDuplicateWorkerExecution(): void
    {
        // Configurar para execução assíncrona
        TestsHandler::setAssync(true);

        // Criar algumas filas para processamento
        $queues = array_map(
            fn($id) => TestsHandler::addByDTO(['amount' => 123 * $id], null, 2),
            range(1, 3)
        );

        // Iniciar primeiro worker
        nsCommand("queue:worker");
        sleep(2);

        // Tentar iniciar segundo worker (deve lançar exceção)
        nsCommand("queue:run", null, true);
        nsCommand("queue:worker");

        $this->assertEquals(1, count(QueueRun::getRunners('onlyAssync')));
        $this->assertEquals(1, count(QueueRun::getRunners('onlySync')));
        $this->assertEquals(0, count(QueueRun::getRunners('full')));

        // Parar o worker
        nsCommand("queue:stop");
        $this->assertEquals(0, count(QueueRun::getRunners()));

        // Validar existencia do full, e 
        nsCommand("queue:run");
        $this->assertEquals(0, count(QueueRun::getRunners('full')));
    }
}
