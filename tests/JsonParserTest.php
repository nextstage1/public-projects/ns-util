<?php

use NsUtil\Helpers\JsonParser;
use PHPUnit\Framework\TestCase;
use function NsUtil\json_decode;

class JsonParserTest extends TestCase
{

    public function testHandleWithNull()
    {
        $result = JsonParser::handle(null, true);
        $this->assertIsArray($result);
        $this->assertEmpty($result);

        $result = json_decode(null, true);
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }

    public function testJsonWithNewLineOnEnd()
    {
        $json = '{"error":"Invalid parameters","message":"Face with ID=a47ddf875e8aa4d425547882ea6abfdb already enrolled to \\"2|3428|CS-DEVELOP-AWS\\" with app_key 100a131ea56f42fca9a16a250a3dacdc"}
';
        $result = JsonParser::handle($json, true);
        $this->assertIsArray($result);
    }

    public function testJsonWithNewLineOnStart()
    {
        $json = '
{"error":"Invalid parameters","message":"Face with ID=a47ddf875e8aa4d425547882ea6abfdb already enrolled to \\"2|3428|CS-DEVELOP-AWS\\" with app_key 100a131ea56f42fca9a16a250a3dacdc"} ';
        $result = JsonParser::handle($json, true);
        $this->assertIsArray($result);
    }

    public function testJsonWithNewLineOnMiddle()
    {
        $json = '{"error":"Invalid parameters","message":"Face with ID=a47ddf875e8aa4d425547882ea6abfdb already enrolled to \\"2|3428|CS-DEVELOP-AWS\\" with app_key 100a131ea56f42fca9a16a250a3dacdc"}';
        $json = str_replace('with', "\nwith", $json);
        $result = JsonParser::handle($json, true);
        $this->assertEmpty($result);
    }

    public function testHandleWithArray()
    {
        $input = ['key' => 'value'];
        $result = JsonParser::handle($input, true);
        $this->assertIsArray($result);
        $this->assertEquals($input, $result);

        $result = json_decode($input, true);
        $this->assertIsArray($result);
        $this->assertEquals($input, $result);
    }

    public function testHandleWithObject()
    {
        $input = (object) ['key' => 'value'];
        $result = JsonParser::handle($input, false);
        $this->assertIsObject($result);
        $this->assertEquals($input, $result);

        $result = json_decode($input, false);
        $this->assertIsObject($result);
        $this->assertEquals($input, $result);
    }

    public function testHandleWithJsonString()
    {
        $input = '{"key": "value"}';
        $result = JsonParser::handle($input, true);
        $this->assertIsArray($result);
        $this->assertEquals(['key' => 'value'], $result);

        $result = json_decode($input, true);
        $this->assertIsArray($result);
        $this->assertEquals(['key' => 'value'], $result);
    }

    public function testHandleWithInvalidJsonString()
    {
        $input = '{"key": "value"';
        $result = JsonParser::handle($input, true);
        $this->assertNull($result);

        $result = json_decode($input, true);
        $this->assertNull($result);
    }

    // New test cases
    public function testHandleWithEmptyString()
    {
        $input = '';
        $result = JsonParser::handle($input, true);
        $this->assertNull($result);

        $result = json_decode($input, true);
        $this->assertNull($result);
    }

    public function testHandleWithSpecialCharacters()
    {
        $input = '{"key": "value&#34;"}';
        $result = JsonParser::handle($input, true);
        $this->assertIsArray($result);
        $this->assertEquals(['key' => 'value"'], $result);

        $result = json_decode($input, true);
        $this->assertIsArray($result);
        $this->assertEquals(['key' => 'value"'], $result);
    }

    public function testHandleWithDepth()
    {
        $input = '{"key": {"nestedKey": "nestedValue"}}';
        $result = JsonParser::handle($input, true, 1);
        $this->assertNull($result);

        $result = json_decode($input, true, 1);
        $this->assertNull($result);
    }

    public function testHandleWithOptions()
    {
        $input = '{"key": "value"}';
        $result = JsonParser::handle($input, true, 512, JSON_BIGINT_AS_STRING);
        $this->assertIsArray($result);
        $this->assertEquals(['key' => 'value'], $result);

        $result = json_decode($input, true, 512, JSON_BIGINT_AS_STRING);
        $this->assertIsArray($result);
        $this->assertEquals(['key' => 'value'], $result);
    }

    public function testIsValidJsonWithValidJsonObject()
    {
        $input = '{"key": "value"}';
        $this->assertTrue(JsonParser::isValidJson($input));
    }

    public function testIsValidJsonWithValidJsonArray()
    {
        $input = '["value1", "value2"]';
        $this->assertTrue(JsonParser::isValidJson($input));
    }

    public function testIsValidJsonWithInvalidJson()
    {
        $input = '{"key": "value"';
        $this->assertFalse(JsonParser::isValidJson($input));
    }

    public function testIsValidJsonWithNonString()
    {
        $input = ['key' => 'value'];
        $this->assertFalse(JsonParser::isValidJson($input));
    }

    public function testIsValidJsonWithEmptyString()
    {
        $input = '';
        $this->assertFalse(JsonParser::isValidJson($input));
    }

    public function testIsValidJsonWithNestedStructures()
    {
        $input = '{"key": {"nested": "value"}, "array": [1,2,3]}';
        $this->assertTrue(JsonParser::isValidJson($input));
    }

    public function testIsValidJsonWithEscapedQuotes()
    {
        $input = '{"key": "value with \"quotes\""}';
        $this->assertTrue(JsonParser::isValidJson($input));
    }

    public function testIsValidJsonWithUnbalancedBrackets()
    {
        $input = '{"key": [}}';
        $this->assertFalse(JsonParser::isValidJson($input));
    }

    public function testIsValidJsonWithInvalidStart()
    {
        $input = 'not a json {"key": "value"}';
        $this->assertFalse(JsonParser::isValidJson($input));
    }

    public function testIsValidJsonWithNull()
    {
        $this->assertFalse(JsonParser::isValidJson(null));
    }
}
