<?php

use PHPUnit\Framework\TestCase;
use NsUtil\Integracao\Cep;

class CepTest extends TestCase
{
    private string $validCep = '88090000';
    private array $expectedData = [
        'cep' => '88090000',
        'uf' => 'SC',
        'localidade' => 'Florianópolis',
        'bairro' => 'Capoeiras',
        'logradouro' => 'Rua Prefeito Dib Cherem',
    ];

    public function testGetAddressReturnsArray()
    {
        $result = Cep::getAddress($this->validCep);
        $this->assertIsArray($result);
    }

    private function extractKeysFromResult(array $result): array
    {
        $keys = array_keys($this->expectedData);
        $extractedData = [];
        foreach ($keys as $key) {
            $extractedData[$key] = $result[$key];
        }
        return $extractedData;
    }

    public function testGetAddressWithValidCep()
    {
        $result = Cep::getAddress($this->validCep);
        $this->assertEquals($this->expectedData, $this->extractKeysFromResult($result));
    }

    public function testGetAddressWithFormattedCep()
    {
        $result = Cep::getAddress('88090-000');
        $this->assertEquals($this->expectedData, $this->extractKeysFromResult($result));
    }

    public function testGetAddressWithInvalidCep()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid CEP.');
        Cep::getAddress('123');
    }

    public function testGetAddressWithEnglishLanguage()
    {

        $result = Cep::getAddress($this->validCep, 'en');
        // teste itens isolados
        $this->assertEquals($this->expectedData['cep'], $result['zipCode']);
        $this->assertEquals($this->expectedData['uf'], $result['state']);
        $this->assertEquals($this->expectedData['localidade'], $result['city']);
        $this->assertEquals($this->expectedData['bairro'], $result['neighborhood']);
        $this->assertEquals($this->expectedData['logradouro'], $result['street']);
    }
}
