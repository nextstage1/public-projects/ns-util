<?php

use NsUtil\Helpers\Cpu;
use PHPUnit\Framework\TestCase;

class CpuTest extends TestCase
{
    public function testCount()
    {
        $processors = Cpu::count();
        $this->assertIsInt($processors);
        $this->assertGreaterThan(0, $processors);
    }

    public function testUsage()
    {
        $usage = Cpu::usage();
        $this->assertIsFloat($usage);
        $this->assertGreaterThanOrEqual(0, $usage);
        $this->assertLessThanOrEqual(100, $usage);
    }

    public function testGetServerLoadLinuxData()
    {
        if (stristr(PHP_OS, "win")) {
            $this->markTestSkipped('Test skipped on Windows OS.');
        }

        $reflection = new ReflectionClass(Cpu::class);
        $method = $reflection->getMethod('_getServerLoadLinuxData');
        $method->setAccessible(true);
        $data = $method->invoke(null);

        $this->assertIsArray($data);
        $this->assertCount(4, $data);
        foreach ($data as $value) {
            $this->assertIsInt($value);
        }
    }

    public function testGetServerLoad()
    {
        $reflection = new ReflectionClass(Cpu::class);
        $method = $reflection->getMethod('getServerLoad');
        $method->setAccessible(true);
        $load = $method->invoke(null);

        $this->assertIsFloat($load);
        $this->assertGreaterThanOrEqual(0, $load);
        $this->assertLessThanOrEqual(100, $load);
    }
}
