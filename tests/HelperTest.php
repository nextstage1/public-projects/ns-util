<?php

use NsUtil\Helper;
use PHPUnit\Framework\TestCase;

use function NsUtil\json_decode;

class HelperTest extends TestCase
{
    private $json = '[{"id":1,"username":"john.doe","name":"John Doe","state":"active","locked":false,"avatar_url":"https://example.com/avatar/1.png","web_url":"https://example.com/john.doe"},{"id":2,"username":"jane.smith","name":"Jane Smith","state":"active","locked":false,"avatar_url":"https://example.com/avatar/2.png","web_url":"https://example.com/jane.smith"},{"id":3,"username":"alice.johnson","name":"Alice Johnson","state":"active","locked":false,"avatar_url":"https://example.com/avatar/3.png","web_url":"https://example.com/alice.johnson"},{"id":4,"username":"bob.brown","name":"Bob Brown","state":"active","locked":false,"avatar_url":"https://example.com/avatar/4.png","web_url":"https://example.com/bob.brown"},{"id":5,"username":"charlie.davis","name":"Charlie Davis","state":"active","locked":false,"avatar_url":"https://example.com/avatar/5.png","web_url":"https://example.com/charlie.davis"},{"id":6,"username":"david.wilson","name":"David Wilson","state":"active","locked":false,"avatar_url":"https://example.com/avatar/6.png","web_url":"https://example.com/david.wilson"},{"id":7,"username":"eve.miller","name":"Eve Miller","state":"active","locked":false,"avatar_url":"https://example.com/avatar/7.png","web_url":"https://example.com/eve.miller"},{"id":8,"username":"frank.moore","name":"Frank Moore","state":"active","locked":false,"avatar_url":"https://example.com/avatar/8.png","web_url":"https://example.com/frank.moore"},{"id":9,"username":"grace.taylor","name":"Grace Taylor","state":"active","locked":false,"avatar_url":"https://example.com/avatar/9.png","web_url":"https://example.com/grace.taylor"},{"id":10,"username":"henry.jackson","name":"Henry Jackson","state":"active","locked":false,"avatar_url":"https://example.com/avatar/10.png","web_url":"https://example.com/henry.jackson"}]';

    public function testDirectorySeparator()
    {
        $path = 'some/path/to/file';
        Helper::directorySeparator($path);
        $this->assertEquals('some' . DIRECTORY_SEPARATOR . 'path' . DIRECTORY_SEPARATOR . 'to' . DIRECTORY_SEPARATOR . 'file', $path);
    }

    public function testDeleteDir()
    {
        $result = Helper::deleteDir('/path/to/dir');
        $this->assertTrue($result);
    }

    public function testPackerAndPrintJS()
    {
        $res = "<script>eval(function(p,a,c,k,e,d){e=function(c){return c};if(!''.replace(/^/,String)){while(c--){d[c]=k[c]||c}k=[function(e){return d[e]}];e=function(){return'\\\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c])}}return p}('0',1,1,'js_code'.split('|'),0,{}))
</script>";
        $this->expectOutputString($res);
        Helper::packerAndPrintJS('js_code');
    }

    public function testExtractDataAtributesFromHtml()
    {
        $html = '<div data-test="value"></div>';
        $result = Helper::extractDataAtributesFromHtml($html);
        $this->assertArrayHasKey('test', $result);
        $this->assertEquals('value', $result['test']);
    }

    public function testIsMobile()
    {
        $result = Helper::isMobile();
        $this->assertIsBool($result);
    }

    public function testIsJson()
    {
        $jsonString = '{"key": "value"}';
        $result = Helper::isJson($jsonString);
        $this->assertTrue($result);
    }

    public function testGetIP()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $result = Helper::getIP();
        $this->assertEquals('127.0.0.1', $result);
    }

    public function testGetSO()
    {
        $result = Helper::getSO();
        $this->assertIsString($result);
    }

    public function testParseInt()
    {
        $result = Helper::parseInt('123abc');
        $this->assertEquals('123', $result);
    }

    public function testGetPathApp()
    {
        $result = Helper::getPathApp();
        $this->assertIsString($result);
    }

    public function testGetValByType()
    {
        $result = Helper::getValByType('123', 'int');
        $this->assertEquals(123, $result);
    }

    public function testFormatDate()
    {
        $result = Helper::formatDate('2023-01-01');
        $this->assertIsString($result);
    }

    public function testFormatCep()
    {
        $result = Helper::formatCep('12345678');
        $this->assertIsString($result);
    }

    public function testFormatCpfCnpj()
    {
        $result = Helper::formatCpfCnpj('12345678901');
        $this->assertIsString($result);
    }

    public function testGetPsr4Name()
    {
        $result = Helper::getPsr4Name();
        $this->assertIsString($result);
    }

    public function testGetPhpUser()
    {
        $result = Helper::getPhpUser();
        $this->assertIsArray($result);
    }

    public function testValidaEmail()
    {
        $result = Helper::validaEmail('test@example.com');
        $this->assertTrue($result);
    }

    public function testIsBase64Encoded()
    {
        $result = Helper::isBase64Encoded('dGVzdA==');
        $this->assertTrue($result);
    }

    public function testDecimalFormat()
    {
        $result = Helper::decimalFormat('1,234.56');
        $this->assertEquals('1234.56', $result);
    }

    public function testPagination()
    {
        $result = Helper::pagination(1, 10, 100);
        $this->assertIsArray($result);
    }

    public function testGetThumbsByFilename()
    {
        $result = Helper::getThumbsByFilename('file.pdf');
        $this->assertEquals('file-pdf-o', $result);
    }

    public function testBuscacep()
    {
        $result = Helper::buscacep('88090000');
        $this->assertInstanceOf(stdClass::class, $result);
    }

    public function testSetPaginacao()
    {
        $out = [];
        $dados = ['pagina' => 1];
        Helper::setPaginacao(100, 10, $out, $dados);
        $this->assertArrayHasKey('pagination', $out);
    }

    public function testGetTmpDir()
    {
        $result = Helper::getTmpDir();
        $this->assertIsString($result);
    }

    public function testGetHost()
    {
        $result = Helper::getHost();
        $this->assertIsString($result);
    }

    public function testAddConditionFromAPI()
    {
        $condition = [];
        $dados = ['conditions' => ['key' => 'value']];
        Helper::addConditionFromAPI($condition, $dados);
        $this->assertArrayHasKey('key_addedConditionFromAPI', $condition);
    }

    public function testGenerateCode()
    {
        $result = Helper::generateCode('test');
        $this->assertEquals('TT' . substr(md5('test'), 0, 2), $result);
    }


    public function testShellExec()
    {
        // Capture the output of the shell command
        ob_start();
        Helper::shellExec('echo "."'); // Suppress output
        $output = ob_get_clean();

        // Assert that the output is as expected
        $this->assertEquals('', trim($output));
    }

    public function testCommandPrintHeader()
    {
        $this->expectOutputString('');
        Helper::commandPrintHeader('Header', 40);
    }

    public function testUniqueHash()
    {
        $result = Helper::uniqueHash('prefix');
        $this->assertIsString($result);
    }

    public function testSslGetEndDate()
    {
        $result = Helper::sslGetEndDate('https://google.com');
        $this->assertIsInt($result);
    }

    public function testGenerateUuidV4()
    {
        $result = Helper::generateUuidV4();
        $this->assertIsString($result);
    }

    public function testMyFileGetContents()
    {
        $result = Helper::myFileGetContents('https://google.com', false, 5);
        $this->assertIsString($result);
    }

    public function testCurlCall()
    {
        $result = Helper::curlCall('https://google.com', [], 'GET', ['Content-Type: application/json'], false, 5);
        $this->assertIsObject($result);
    }

    public function testFeriado()
    {
        $result = Helper::feriado('2023-01-01');
        $this->assertIsArray($result);
    }

    public function testCalculaVencimentoUtil()
    {
        $result = Helper::calculaVencimentoUtil('2023-01-01', 5);
        $this->assertIsString($result);
    }

    public function testArrayOrderBy()
    {
        $array = [
            ['name' => 'John', 'age' => 30],
            ['name' => 'Jane', 'age' => 25],
        ];
        Helper::arrayOrderBy($array, 'age', 'ASC');
        $this->assertEquals('Jane', $array[0]['name']);
    }

    public function testArraySearchByKey()
    {
        $array = [
            ['name' => 'John', 'age' => 30],
            ['name' => 'Jane', 'age' => 25],
        ];
        $result = Helper::arraySearchByKey($array, 'name', 'Jane');
        $this->assertEquals(['name' => 'Jane', 'age' => 25], $result);
    }

    public function testArraySearchByKeyFound()
    {
        $array = [
            ['name' => 'John', 'age' => 30],
            ['name' => 'Jane', 'age' => 25],
        ];
        $result = Helper::arraySearchByKey($array, 'name', '_Jane');
        $this->assertEquals([], $result); // Verifica se o resultado está correto
    }

    public function testDepara()
    {
        $depara = ['name' => 'full_name', 'age' => 'years'];
        $dados = ['full_name' => 'John', 'years' => 30];
        $result = Helper::depara($depara, $dados);
        $this->assertEquals(['name' => 'John', 'age' => 30], $result);
    }

    public function testArray2csv()
    {
        $array = json_decode($this->json, true);

        // Teste conversao em memoria
        $csv = Helper::array2csv($array);
        $this->assertStringContainsString('John', $csv);

        // Tetse conversao para arquivo
        Helper::array2csv($array, '/tmp/array2csv.csv');
        $this->assertTrue(file_exists('/tmp/array2csv.csv'));
        $csvContent = file_get_contents('/tmp/array2csv.csv');
        $this->assertStringContainsString('jane.smith', $csvContent);

        unlink('/tmp/array2csv.csv');;
    }

    public function testArray2env()
    {
        $config = ['DB_HOST' => 'localhost', 'DB_USER' => 'root'];
        $result = Helper::array2env($config);
        $this->assertContains('DB_HOST="localhost"', $result);
    }

    public function testArrayReduceKeys()
    {
        $origem = ['name' => 'John', 'age' => 30];
        $chaves = ['name'];
        $result = Helper::arrayReduceKeys($origem, $chaves);
        $this->assertEquals(['name' => 'John'], $result);
    }

    public function testRecebeDadosFromView()
    {
        // Teste 1: Valores especiais (undefined, null, empty)
        $dados = [
            'name' => 'John',
            'undefined_value' => 'undefined',
            'null_value' => null,
            'empty_string' => '',
            'zero' => 0
        ];
        Helper::recebeDadosFromView($dados);
        $this->assertArrayHasKey('name', $dados);
        $this->assertArrayHasKey('empty_string', $dados);
        $this->assertArrayHasKey('zero', $dados);
        $this->assertArrayNotHasKey('undefined_value', $dados);
        $this->assertArrayNotHasKey('null_value', $dados);

        // Teste 2: Tipos de dados
        $dados = [
            'boolean_true' => 'true',
            'boolean_false' => 'false',
            'integer' => 42,
            'float' => 3.14,
            'string' => 'Hello',
            'json_object' => '{"key":"value"}',
            'json_array' => '[1,2,3]'
        ];
        Helper::recebeDadosFromView($dados);
        $this->assertTrue($dados['boolean_true']);
        $this->assertFalse($dados['boolean_false']);
        $this->assertIsInt($dados['integer']);
        $this->assertIsFloat($dados['float']);
        $this->assertIsString($dados['string']);
        $this->assertIsArray($dados['json_object']);
        $this->assertIsArray($dados['json_array']);

        // Teste 3: Campos ID e valores numéricos
        $dados = [
            'idUser' => '123',
            'idInvalid' => 'ABC',
            'idZero' => '0',
            'normal_field' => '456'
        ];
        Helper::recebeDadosFromView($dados);
        $this->assertIsInt($dados['idUser']);
        $this->assertEquals(123, $dados['idUser']);
        $this->assertEquals(0, $dados['idInvalid']);
        $this->assertEquals(0, $dados['idZero']);
        $this->assertIsString($dados['normal_field']);

        // Teste 4: Arrays aninhados
        $dados = [
            'user' => [
                'name' => 'John',
                'idProfile' => '789',
                'settings' => [
                    'theme' => 'dark',
                    'notifications' => 'true'
                ]
            ]
        ];
        Helper::recebeDadosFromView($dados);
        $this->assertIsArray($dados['user']);
        $this->assertIsString($dados['user']['name']);
        $this->assertIsInt($dados['user']['idProfile']);
        $this->assertIsArray($dados['user']['settings']);
        $this->assertTrue($dados['user']['settings']['notifications']);

        // Teste 5: Caracteres especiais e substituições
        $dados = [
            'text_with_ns21' => 'NS21test',
            'text_with_html' => '&#34;quote&#34;'
        ];
        Helper::recebeDadosFromView($dados);
        $this->assertEquals('&test', $dados['text_with_ns21']);
        $this->assertEquals('"quote"', $dados['text_with_html']);
    }

    public function testMyFGetsCsv()
    {
        // Teste com um arquivo CSV válido
        $handle = fopen('php://temp', 'r+');
        fputcsv($handle, ['name', 'age']);
        fputcsv($handle, ['John', 30]);
        rewind($handle);
        $result = Helper::myFGetsCsv($handle);
        $this->assertEquals(['name,age'], $result);
        fclose($handle);

        // Teste com um arquivo CSV vazio
        $handle = fopen('php://temp', 'r+');
        $result = Helper::myFGetsCsv($handle);
        $this->assertFalse($result);
        fclose($handle);
    }

    public function testMyFPutsCsv()
    {
        // Teste com um arquivo CSV válido
        $filePath = '/tmp/testMyFPutsCsv.csv';
        Helper::deleteFile($filePath);
        $array = json_decode($this->json, true);
        array_map(fn($item) => Helper::myFPutsCsv($filePath, $item), $array);
        $this->assertFileExists($filePath);

        // Verifica se o conteúdo do arquivo está correto
        $handle = fopen($filePath, 'r');
        // teste da primeira linha
        $line = fgets($handle);
        $this->assertEquals('1,john.doe,John Doe,active,,https://example.com/avatar/1.png,https://example.com/john.doe' . PHP_EOL, $line);
        // teste da segunda linha
        $line = fgets($handle);
        $this->assertEquals('2,jane.smith,Jane Smith,active,,https://example.com/avatar/2.png,https://example.com/jane.smith' . PHP_EOL, $line);
        fclose($handle);

        // Teste com campos que contêm delimitadores
        Helper::deleteFile($filePath);
        Helper::myFPutsCsv($filePath, ['name, with, commas', 'age']);
        $handle = fopen($filePath, 'r');
        $lines = [];
        while (($line = fgets($handle)) !== false) {
            $lines[] = $line;
        }
        fclose($handle);
        $this->assertEquals('"name, with, commas",age' . PHP_EOL, $lines[0]);

        // Limpeza
        Helper::deleteFile($filePath);
    }

    public function testJsonToArrayFromView()
    {
        $json = '{"key": "value"}';
        $result = Helper::jsonToArrayFromView($json);
        $this->assertEquals(['key' => 'value'], $result);
    }

    public function testObjectPHP2Array()
    {
        // Teste com objeto simples
        $object = new stdClass();
        $object->key = 'value';
        $result = Helper::objectPHP2Array($object);
        $this->assertEquals(['key' => 'value'], $result);

        // Teste com objeto aninhado
        $nestedObject = new stdClass();
        $nestedObject->name = 'John';
        $nestedObject->info = new stdClass();
        $nestedObject->info->age = 30;
        $nestedObject->info->city = 'New York';
        $result = Helper::objectPHP2Array($nestedObject);
        $this->assertEquals([
            'name' => 'John',
            'info' => [
                'age' => 30,
                'city' => 'New York'
            ]
        ], $result);

        // Teste com array de objetos
        $obj1 = new stdClass();
        $obj1->id = 1;
        $obj1->name = 'Item 1';

        $obj2 = new stdClass();
        $obj2->id = 2;
        $obj2->name = 'Item 2';

        $arrayOfObjects = (object) [$obj1, $obj2];
        $result = Helper::objectPHP2Array($arrayOfObjects);
        $this->assertEquals([
            ['id' => 1, 'name' => 'Item 1'],
            ['id' => 2, 'name' => 'Item 2']
        ], $result);

        // Teste com tipos diferentes de dados
        $mixedObject = new stdClass();
        $mixedObject->string = 'text';
        $mixedObject->number = 42;
        $mixedObject->boolean = true;
        $mixedObject->null = null;
        $mixedObject->array = (object) [1, 2, 3];
        $result = Helper::objectPHP2Array($mixedObject);
        $this->assertEquals([
            'string' => 'text',
            'number' => 42,
            'boolean' => true,
            'null' => null,
            'array' => [1, 2, 3]
        ], $result);

        // Teste com objeto vazio
        $emptyObject = new stdClass();
        $result = Helper::objectPHP2Array($emptyObject);
        $this->assertEquals([], $result);
    }

    public function testSanitize()
    {
        $result = Helper::sanitize('áàãâéêíóôõúüç-*^&%');
        $this->assertEquals('aaaaeeiooouuc_____', $result);
    }

    public function testConvertAscii()
    {
        $result = Helper::convertAscii('“Hello”');
        $this->assertEquals('"Hello"', $result); // Verifica a saída correta

        // Adicionando mais casos de teste
        $result = Helper::convertAscii('Café, crème brûlée, résumé, naïve, coöperate.');
        $this->assertEquals('Cafe, creme brulee, resume, naive, cooperate.', $result);

        $result = Helper::convertAscii('Fête, façade, déjà vu.');
        $this->assertEquals('Fete, facade, deja vu.', $result);

        $result = Helper::convertAscii('Hello, world! 123.');
        $this->assertEquals('Hello, world! 123.', $result);
    }

    public function testMkdir()
    {
        $path = '/tmp/testDir';
        Helper::mkdir($path);
        $this->assertTrue(is_dir($path));
        rmdir($path); // Clean up
    }

    public function testName2CamelCase()
    {
        $result = Helper::name2CamelCase('my_variable_name');
        $this->assertEquals('myVariableName', $result);
    }

    public function testReverteName2CamelCase()
    {
        $result = Helper::reverteName2CamelCase('myVariableName');
        $this->assertEquals('my_variable_name', $result);
    }

    public function testCreateTreeDir()
    {
        $result = Helper::createTreeDir('/tmp/testDir/testFile.txt');
        $this->assertEquals('/tmp/testDir', $result->path);
        $this->assertEquals('testFile.txt', $result->name);
        $this->assertTrue(is_dir($result->path));
        $this->assertTrue(file_exists($result->path));
        rmdir('/tmp/testDir'); // Clean up
    }

    public function testSaveFile()
    {
        $result = Helper::saveFile('/tmp/testFile.php', '', '<?php echo "Hello";');
        $this->assertTrue($result);
        $this->assertTrue(file_exists('/tmp/testFile.php'));
        unlink('/tmp/testFile.php'); // Clean up
    }

    public function testFileGetEncoding()
    {
        $filePath = '/tmp/testFile.txt';
        file_put_contents($filePath, 'test');
        $result = Helper::fileGetEncoding($filePath);
        $this->assertIsString($result);
        unlink($filePath);
    }

    public function testDeleteFile()
    {
        $filePath = '/tmp/testDeleteFile.txt';
        file_put_contents($filePath, 'test');
        $result = Helper::deleteFile($filePath);
        $this->assertTrue($result);
    }

    public function testHighlightText()
    {
        $text = 'Hello World';
        Helper::highlightText($text, 'World');
        // Verifica se o texto foi destacado corretamente
        $this->assertStringContainsString('<span class="ns-highlight-text">World</span>', $text);
    }

    public function testArrayDiff()
    {
        $arrayNew = ['key1' => 'value1', 'key2' => 'value2'];
        $arrayOld = ['key1' => 'value1', 'key2' => 'value3'];
        $result = Helper::arrayDiff($arrayNew, $arrayOld);
        $this->assertCount(1, $result);
    }

    public function testApplyNamespace()
    {
        $filePath = '/tmp/testFile.php';
        file_put_contents($filePath, '<?php namespace NsUtil;');
        Helper::applyNamespace($filePath);
        $content = file_get_contents($filePath);
        $this->assertStringContainsString('namespace NsUtil;', $content);
        unlink($filePath);
    }

    public function testEficienciaInit()
    {
        $result = Helper::eficiencia_init();
        $this->assertEquals("Method is disabled. Use class Eficiencia()", $result);
    }

    public function testEndEficiencia()
    {
        $result = Helper::endEficiencia();
        $this->assertEquals("Method is disabled. Use class Eficiencia()", $result);
    }

    public function testDeleteDirReturnsFalseForNonExistentDir()
    {
        $result = Helper::deleteDir('/path/to/nonexistent/dir');
        $this->assertTrue($result);
    }

    public function testMyFGetsCsvHandlesEmptyFile()
    {
        $handle = fopen('php://temp', 'r+');
        $result = Helper::myFGetsCsv($handle);
        $this->assertFalse($result);
        fclose($handle);
    }

    public function testFileCSVToArray()
    {
        $filePath = '/tmp/testFile.csv';
        file_put_contents($filePath, "name,age\nJohn,30\nJane,25");
        $result = Helper::fileCSVToArray($filePath);
        $this->assertCount(3, $result);
        unlink($filePath);
    }

    public function testJsonToArrayFromViewHandlesInvalidJson()
    {
        $json = '{"key": "value"'; // Invalid JSON
        $result = Helper::jsonToArrayFromView($json);
        $this->assertEquals(null, $result);
    }

    public function testFileConvertToUtf8()
    {
        $filePath = '/tmp/testFile.txt';
        file_put_contents($filePath, 'test');
        $result = Helper::fileConvertToUtf8($filePath);
        $this->assertNull($result); // Assuming the function returns null on success
        unlink($filePath);
    }

    public function testExtractDataAtributesFromHtmlHandlesNoDataAttributes()
    {
        $html = '<div></div>';
        $result = Helper::extractDataAtributesFromHtml($html);
        $this->assertEmpty($result);
    }
}
