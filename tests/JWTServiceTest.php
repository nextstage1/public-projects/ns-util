<?php

use NsUtil\Services\JWTService;
use PHPUnit\Framework\TestCase;
use NsUtil\Date;

/**
 * Class JWTServiceTest
 * 
 * @package Tests\Services
 */
class JWTServiceTest extends TestCase
{
    private JWTService $jwtService;
    private string $secretKey = 'chave_secreta_teste';

    protected function setUp(): void
    {
        parent::setUp();
        $this->jwtService = new JWTService($this->secretKey);
    }

    /**
     * @test
     * @group jwt
     */
    public function testShouldGenerateValidToken(): void
    {
        $payload = ['user_id' => 1, 'email' => 'test@test.com'];
        $expire = (new Date())->add('1 hour');

        $token = $this->jwtService->generateToken($payload, $expire);

        $this->assertIsString($token);
        $this->assertNotEmpty($token);
    }

    /**
     * @test
     * @group jwt
     */
    public function testShouldDecodeTokenSuccessfully(): void
    {
        $payload = ['user_id' => 1, 'email' => 'test@test.com'];
        $expire = (new Date())->add('1 hour');

        $token = $this->jwtService->generateToken($payload, $expire);
        $decoded = $this->jwtService->decodeToken($token);

        $this->assertIsObject($decoded);
        $this->assertEquals($payload['user_id'], $decoded->user_id);
        $this->assertEquals($payload['email'], $decoded->email);
        $this->assertObjectHasProperty('exp', $decoded);
    }

    /**
     * @test
     * @group jwt
     */
    public function testShouldFailWithExpiredToken(): void
    {
        $this->expectException(\Firebase\JWT\ExpiredException::class);

        $payload = ['user_id' => 1];
        $expire = (new Date())->sub('1 hour');

        $token = $this->jwtService->generateToken($payload, $expire);
        $this->jwtService->decodeToken($token);
    }

    /**
     * @test
     * @group jwt
     */
    public function testShouldFailWithInvalidToken(): void
    {
        $this->expectException(\Firebase\JWT\SignatureInvalidException::class);

        $invalidToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE3MTY0MzkwMDB9.invalid_signature';
        $this->jwtService->decodeToken($invalidToken);
    }
}
