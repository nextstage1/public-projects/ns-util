<?php

declare(strict_types=1);

use NsUtil\Security\Crypto;
use PHPUnit\Framework\TestCase;

class CryptoTest extends TestCase
{
    private Crypto $crypto;
    private string $key = 'thisisaverysecurekey1234';

    protected function setUp(): void
    {
        $this->crypto = new Crypto($this->key);
    }

    public function testConstructorThrowsExceptionForShortKey(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('NsCrypto: Warning: Key with less than 16 characters is considered insecure');
        new Crypto('shortkey');
    }

    public function testEncryptAndDecrypt(): void
    {
        $originalString = 'Hello, World!';
        $encryptedString = $this->crypto->encrypt($originalString);
        $decryptedString = $this->crypto->decrypt($encryptedString);

        $this->assertNotEquals($originalString, $encryptedString);
        $this->assertEquals($originalString, $decryptedString);
    }

    public function testEncryptAndDecryptWithExtraKey(): void
    {
        $originalString = 'Hello, World!';
        $extraKey = 'extraKey123';
        $encryptedString = $this->crypto->encrypt($originalString, $extraKey);
        $decryptedString = $this->crypto->decrypt($encryptedString, $extraKey);

        $this->assertNotEquals($originalString, $encryptedString);
        $this->assertEquals($originalString, $decryptedString);
    }

    public function testGetHash(): void
    {
        $string = 'testString';
        $hash = $this->crypto->getHash($string);

        $this->assertEquals(hash('sha256', $string . $this->key), $hash);
    }

    public function testSimpleEncryptAndDecrypt(): void
    {
        $originalString = 'Hello, World!';
        $encryptedString = $this->crypto->simple('encrypt', $originalString);
        $decryptedString = $this->crypto->simple('decrypt', $encryptedString);

        $this->assertNotEquals($originalString, $encryptedString);
        $this->assertEquals($originalString, $decryptedString);
    }

    public function testSimpleEncryptAndDecryptWithExtraKey(): void
    {
        $originalString = 'Hello, World!';
        $extraKey = 'extraKey123';
        $encryptedString = $this->crypto->simple('encrypt', $originalString, $extraKey);
        $decryptedString = $this->crypto->simple('decrypt', $encryptedString, $extraKey);

        $this->assertNotEquals($originalString, $encryptedString);
        $this->assertEquals($originalString, $decryptedString);
    }

    public function testSimpleThrowsExceptionForInvalidAction(): void
    {
        $action = 'invalid_action';
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Invalid Type '$action' (NSC-84)");
        $this->crypto->simple($action, 'testString');
    }
}
