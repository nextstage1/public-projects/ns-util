<?php

use NsUtil\Helpers\Date;
use PHPUnit\Framework\TestCase;

class DateTest extends TestCase
{
    public function testConstructWithDefaultTimezone()
    {
        $date = new Date();
        $this->assertInstanceOf(Date::class, $date);
    }

    public function testConstructWithSpecificTimezone()
    {
        $date = new Date('NOW', 'Europe/London');
        $this->assertInstanceOf(Date::class, $date);
        $this->assertEquals('Europe/London', $date->format('e'));
    }

    public function testAddDuration()
    {
        $date = new Date('2023-01-01');
        $date->add('1 day');
        $this->assertEquals('2023-01-02', $date->format('Y-m-d', false));
    }

    public function testSubDuration()
    {
        $date = new Date('2023-01-01');
        $date->sub('1 day');
        $this->assertEquals('2022-12-31', $date->format('Y-m-d', false));
    }

    public function testTimestamp()
    {
        $date = new Date('2023-01-01 00:00:00', 'UTC');
        $this->assertEquals(1672531200, $date->timestamp());
    }

    public function testFormatAmerican()
    {
        $date = new Date('2023-01-01 15:30:00');
        $this->assertEquals('2023-01-01 15:30:00', $date->format('american'));
    }

    public function testFormatBrazil()
    {
        $date = new Date('2023-01-01 15:30:00');
        $this->assertEquals('01/01/2023 15:30:00', $date->format('brazil'));
    }

    public function testFormatIso8601()
    {
        $date = new Date('2023-01-01 15:30:00');
        $this->assertEquals('2023-01-01T15:30:00+00:00', $date->format('iso8601'));
    }

    public function testFormatExtenso()
    {
        $date = new Date('2023-01-01');
        $this->assertEquals('01 de January de 2023', $date->format('extenso'));
    }
}
