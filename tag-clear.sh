#!/bin/bash

# Filtrar as tags que são maiores que "1.69.11"
actualVersion=$1

# Expressão regular para verificar o formato "x.y.z"
pattern='^[0-9]+\.[0-9]+\.[0-9]+$'
if [[ "$actualVersion" =~ $pattern ]]; then
    echo "Actual version: ${actualVersion}"
else
    echo "actualVersion is not in the correct format (x.y.z)"
fi

# particionar o conteudo 
old_ifs=$IFS
IFS='.'
read -r actualParte1 actualParte2 actualParte3 <<<"$actualVersion"
IFS=$old_ifs

# Listar todas as tags no repositório
git fetch --all
git fetch --tags

tags_to_remove=$(git tag -l)

# Remover as tags filtradas
for tag in $tags_to_remove; do

    # Salvar o valor atual do IFS para restaurá-lo posteriormente
    old_ifs=$IFS

    # Configurar o IFS para ponto
    IFS='.'

    # Dividir a string em partes
    read -r parte1 parte2 parte3 <<<"$tag"

    # Restaurar o valor original do IFS
    IFS=$old_ifs

    # if [[ $parte2 -gt $actualVersion ]]; then
     # Se parte2 é igual a actualParte2 e parte3 é maior que actualParte3
    if [[ ($parte2 -eq $actualParte2 && $parte3 -gt $actualParte3) ]]; then

        echo "remove $tag"
        git tag -d $tag
        git push origin --delete $tag
    fi
done
